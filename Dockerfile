# API Dockerfile
FROM node:10-alpine
WORKDIR /usr/src/app
COPY package*.json ./

#install python for sap-client
RUN apk upgrade --update && apk add --no-cache python3 python3-dev gcc gfortran freetype-dev musl-dev libpng-dev g++ lapack-dev
#RUN pip3 install --upgrade pip
#RUN pip3 install hdbcli

#
RUN npm install
COPY . .
EXPOSE 1002
CMD ["npm", "start"]
