const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
require("dotenv").config();
const notFound = require("./controllers/not_found/not_found");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use("/employee_files", express.static("employee_files"));

app.use("/login", require("./routes/login/login"));
app.use("/admin", require("./routes/admin/admin"));
app.use("/transactions", require("./routes/transaction/transaction"));
app.use("/micro", require("./routes/micro/index"));

app.use(async (req, res) => {
  data = await notFound();
  res.status(data.status).send(data.body);
});

const port = process.env.PORT || 3335;
app.listen(port, console.log(`Server started at port ${port}.`));

//dev
