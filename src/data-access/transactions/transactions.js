module.exports = function transactionDB({ pool }, { moment }) {
  return Object.freeze({
    // Activity Logs
    addActivityLog,

    // Delivery receipts
    listDeliveryReceipts,
    findDeliveryReceiptByID,
    addDeliveryReceipt,
    updateDeliveryReceipt,

    // Truck infos
    listTrucks,
    findTruckById,
    addTruck,
    updateTruck,

    // Truck type
    listTrucktype,
    findTrucktypeById,
    addTruckType,
    updateTruckType,

    // Transaction type
    listTransactionTypes,
    findTransactionTypeById,
    endTransactionStatusLog,
    addTransactionType,
    updateTransactionType,

    // Status
    listStatus,
    findStatusById,
    findStatusByName,
    addStatus,
    updateStatus,
    listToPOStatus,

    // Driver
    listDriver,
    findDriverById,
    addDriver,
    updateDriver,

    // Rejection receipt
    listRejectionReceipt,
    findRejectionReceiptById,
    findRejectionReceiptByTranId,
    addRejectionReceipt,
    updateRejectionReceipt,

    // Business Entity
    listBusinessEntity,
    findBusinessEntityById,
    findBusinessEntityByName,
    addBusinessEntity,
    updateBusinessEntity,

    // Item
    listItem,
    findItemById,
    findItemByName,
    addItem,
    updateItem,
    getItemByBir,

    // Raw material
    listRawMaterials,
    findRawMaterialsById,
    findRawMaterialsByName,
    addRawMaterials,
    updateRawMaterials,

    // BIR
    listBIR,
    findBIRByBEId,
    findBIRById,
    findBIRByItemId,
    findBIRByRMId,
    addBIR,
    updateBIR,
    findItemsByBusinessEntity,

    // Criteria
    listCriteria,
    listCriteriaOnDC,
    listCriteriaByBE,
    findCriteriaById,
    findCriteriaByName,
    addCriteria,
    updateCriteria,

    // Deduction Criteria
    listDeductionCriteria,
    findDeductionCriteriaById,
    totalDeductionPerCriteria,
    findDeductionCriteriaByDRId,
    addDeductionCriteria,
    updateDeductionCriteria,

    // Price reduction
    listPriceReduction,
    findPriceReductionById,
    addPriceReduction,
    updatePriceReduction,

    // Moisture tally sheet
    listMTS,
    findMTSById,
    findMTSByTransactionId,
    addMTS,
    updateMTS,

    // Reconsideration
    listReconsideration,
    findReconsiderationById,
    addReconsideration,
    approveRecon,
    updateReconsideration,

    // Disposition
    listDisposition,
    findDispositionById,
    findDispositionByName,
    addDisposition,
    updateDisposition,

    // Physical analysis
    listPhysicalAnalysis,
    findPhysicalAnalysisById,
    addPhysicalAnalysis,
    addRemarkPA,
    getRemarkPA,
    getDispoPA,
    addDispositionPA,
    addDispositionCA,
    getDispoCA,
    updatePhysicalAnalysis,
    updatePARID,

    // Chemical Analysis
    listChemicalAnalysis,
    findChemicalAnalysisById,
    addChemicalAnalysis,
    updateChemicalAnalysis,
    updateCARID,

    // Material Rejjection Report
    listMRR,
    findMRRById,
    addMRR,
    updateMRR,

    // RMAF
    countRMAF,
    listRMAF,
    findRMAFById,
    addRMAF,
    updateRMAF,
    insertChemicalAnalysis,

    // Transaction
    listTransaction,
    findTransactionById,
    findTransactionBySupplierCode,
    addTransaction,
    updateTransaction,
    validateTally,
    updateTransactionStatus,
    updateForReconTransaction,
    countTransaction,
    countCue,
    correctTransaction,

    // Deduction Receipt
    listDeductionReceipts,
    findDeductionReceiptById,
    findDeductionReceiptByTransId,
    findDeductionReceiptSumByTransId,
    countDeductionReceiptByTranId,
    addDeductionReceipt,
    updateDeductionReceipt,
    noteDeductionReceipt,
    getDeductionReceiptsWithCriteria,

    // Transaction Status Logs
    listTransactionStatusLogs,
    findTransactionStatusLogsById,
    findTransactionStatusLogsByTransactionId,
    addTransactionStatusLog,
    addTSLInput,
    updateTransactionStatusLog,

    // Remarks
    listRemarks,
    findRemarksById,
    addRemark,
    updateRemark,

    // Employees
    getEmployee,
    getDepartment,

    // Signatures
    addCertification,
    addNote,

    // SAP
    tagPO
  });

  async function getItemByBir(bir_id) {
    return pool.query(
      `SELECT description FROM items WHERE id = 
    (SELECT item_id FROM bir WHERE id = $1)`,
      [bir_id]
    );
  }

  async function tagPO(request_info) {
    const { id, purchase_order_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction SET purchase_order_id = $1, 
    updated_by = $2, updated_at = $3 WHERE id = $4 
    RETURNING *`,
      [purchase_order_id, updated_by, date_today, id]
    );
  }

  async function getDepartment(department_id) {
    return pool.query(`SELECT * FROM departments WHERE id = $1`, [
      department_id
    ]);
  }

  async function getEmployee(employee_id) {
    return pool.query(
      `SELECT
    "public".employees.role_id,
    "public".employees.employee_id,
    "public".employees."password",
    "public".employees.token,
    "public".employees.created_by,
    "public".employees.updated_by,
    "public".employees.created_at,
    "public".employees.updated_at,
    "public".employees.status,
    "public".employees."name",
    "public".employees."id",
    "public".employees.pin_code,
    "public".employees.signature,
    "public".employees.department,
    "public".departments."id" as "department_id",
    "public".departments."name" as "department_name",
    "public".departments.status as "department_status"
    FROM
    "public".employees
    INNER JOIN "public".departments ON "public".employees.department = "public".departments."id"
    WHERE employee_id = $1`,
      [employee_id]
    );
  }

  async function listRemarks() {
    return pool.query(`SELECT * FROM remarks`);
  }

  async function findRemarksById(id) {
    return pool.query(`SELECT * FROM remarks WHERE id = $1`, [id]);
  }

  async function addRemark(request_info) {
    const { name, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO remarks (name, created_by, created_at)
    VALUES ($1, $2, $3) RETURNING *`,
      [name, created_by, date_today]
    );
  }

  async function updateRemark(request_info) {
    const { name, updated_by, id } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE remarks SET name = $1, updated_by = $2,
    updated_at = $3 WHERE id = $4 RETURNING *`,
      [name, updated_by, date_today, id]
    );
  }

  async function listTransactionStatusLogs() {
    return pool.query(`SELECT * FROM transaction_status_logs `);
  }

  async function findTransactionStatusLogsByTransactionId(transaction_id) {
    return pool.query(
      `SELECT * FROM transaction_status_logs WHERE
    transaction_id = $1 ORDER by start_time`,
      [transaction_id]
    );
  }

  async function findTransactionStatusLogsById(id) {
    return pool.query(`SELECT * FROM transaction_status_logs WHERE id = $1`, [
      id
    ]);
  }

  async function addTransactionStatusLog(request_info) {
    const { id, status_id, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO transaction_status_logs 
    (transaction_id, status_id, start_time, created_by, created_at)
    VALUES ($1, $2, $3, $4, $5) RETURNING *`,
      [id, status_id, date_today, created_by, date_today]
    );
  }

  async function addTSLInput(request_info) {
    const { id, input_id, created_by, starttime, endtime } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO transaction_status_logs 
      (transaction_id, status_id, start_time, end_time, created_by, created_at)
      VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
      [id, input_id, starttime, endtime, created_by, date_today]
    );
  }

  async function endTransactionStatusLog(request_info) {
    const { transaction_id, status_id } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction_status_logs SET end_time = $1
      WHERE transaction_id = $2 and status_id = $3 RETURNING *`,
      [date_today, transaction_id, status_id]
    );
  }

  async function updateTransactionStatusLog(request_info) {
    const { id, transaction_id, status_id, end_time } = request_info;

    return pool.query(
      `UPDATE transaction_status_logs SET 
    transaction_id = $1, status_id = $2, end_time = $3, 
    WHERE id = $4 RETURNING *`,
      [transaction_id, status_id, end_time, id]
    );
  }

  async function findItemsByBusinessEntity(id) {
    return pool.query(
      `SELECT
    "public".bir.id,
    "public".bir.business_entity_id,
    "public".bir.item_id,
    "public".bir.raw_material_id
    FROM
    "public".bir
    WHERE "public".bir.business_entity_id = $1
    `,
      [id]
    );
  }

  async function listDeductionReceipts() {
    return pool.query("SELECT * FROM deduction_receipt");
  }

  async function findDeductionReceiptById(id) {
    return pool.query("SELECT * FROM deduction_receipt WHERE id = $1", [id]);
  }

  async function findDeductionReceiptByTransId(transaction_id) {
    return pool.query(
      "SELECT * FROM deduction_receipt WHERE transaction_id = $1",
      [transaction_id]
    );
  }

  async function findDeductionReceiptSumByTransId(transaction_id) {
    return pool.query(
      `SELECT SUM(total_deductions) as sum_total_deductions, count(*)
    FROM deduction_receipt WHERE transaction_id = $1`,
      [transaction_id]
    );
  }

  async function countDeductionReceiptByTranId(transaction_id) {
    return pool.query(
      "SELECT COUNT(*) FROM deduction_receipt WHERE transaction_id = $1",
      [transaction_id]
    );
  }

  async function addDeductionReceipt(request_info) {
    const {
      transaction_id,
      total_deductions,
      prepared_by,
      prepared_signature,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO deduction_receipt (transaction_id,
      date, total_deductions, prepared_by, prepared_signature, 
      created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6,  
      $7) RETURNING *`,
      [
        transaction_id,
        date_today,
        total_deductions,
        prepared_by,
        prepared_signature,
        created_by,
        date_today
      ]
    );
  }

  async function noteDeductionReceipt(request_info) {
    const {
      transaction_id,
      noted_by,
      noted_signature,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE deduction_receipt SET noted_by = $2,
       updated_by = $3, updated_at = $4, noted_signature = $5
        WHERE transaction_id = $1 RETURNING *`,
      [transaction_id, noted_by, updated_by, date_today, noted_signature]
    );
  }

  async function updateDeductionReceipt(request_info) {
    const {
      id,
      transaction_id,
      date,
      total_deductions,
      noted_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE deduction_receipt SET transaction_id = $1,
    date = $2, total_deductions = $3, noted_by = $4, updated_by = $5,
    updated_at = $6 WHERE id = $7 RETURNING *`,
      [
        transaction_id,
        date,
        total_deductions,
        noted_by,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function getDeductionReceiptsWithCriteria(transaction_id) {
    return pool.query(`SELECT
        criteria.description,
        deduction_criteria.criteria_id,
        deduction_criteria.deduction_receipt_id,
        deduction_criteria.quantity,
        deduction_criteria.deduction,
        deduction_criteria.total,
        deduction_criteria."id",
        deduction_receipt."id",
        deduction_receipt.transaction_id,
        deduction_receipt."date",
        deduction_receipt."prepared_by",
        deduction_receipt.total_deductions
      FROM
      criteria
      INNER JOIN deduction_criteria ON deduction_criteria.criteria_id = criteria.id
      INNER JOIN deduction_receipt ON deduction_criteria.deduction_receipt_id = deduction_receipt.id
      WHERE deduction_receipt.transaction_id =  ${transaction_id}`);
  }

  async function listTransaction(request_info) {
    const { date_from, date_to } = request_info;

    return pool.query(
      `SELECT * FROM transaction WHERE created_at BETWEEN SYMMETRIC 
    $1 AND $2 ORDER BY id DESC`,
      [date_from, date_to]
    );
  }

  async function findTransactionById(id) {
    return pool.query("SELECT * FROM transaction WHERE id = $1", [id]);
  }

  async function findTransactionBySupplierCode(supplier_code) {
    return pool.query("SELECT * FROM transaction WHERE supplier_code = $1", [
      supplier_code
    ]);
  }

  async function addTransaction(request_info) {
    const {
      delivery_receipt,
      rmaf_id,
      bir_id,
      status_id,
      plate_number,
      transaction_type_id,
      driver_name,
      supplier_code,
      no_of_bags,
      guard_on_duty,
      qc_control_number,
      queue_number,
      supplier_name,
      supplier_address
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO transaction (
      delivery_receipt_id,
      rmaf_id,
      bir_id,
      status_id,
      plate_number,
      transaction_type_id,
      driver_name,
      supplier_code,
      no_of_bags,
      guard_on_duty,
      qc_control_number,
      created_at,
      updated_at,
      queue_number,
      supplier_name,
      supplier_address
     ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,
       $12, $13, $14, $15, $16) RETURNING *`,
      [
        delivery_receipt,
        rmaf_id,
        bir_id,
        status_id,
        plate_number,
        transaction_type_id,
        driver_name,
        supplier_code,
        no_of_bags,
        guard_on_duty,
        qc_control_number,
        date_today,
        date_today,
        queue_number,
        supplier_name,
        supplier_address
      ]
    );
  }

  async function updateTransaction(request_info) {
    const {
      id,
      delivery_receipt_id,
      rmaf_id,
      bir_id,
      status_id,
      transaction_type_id,
      supplier_code,
      no_of_bags,
      guard_on_duty,
      time_end,
      qc_control_number,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction SET
      delivery_receipt_id = $1,
      rmaf_id = $2,
      bir_id = $3,
      status_id = $4,
      transaction_type_id = $5,
      supplier_code = $6,
      no_of_bags = $7,
      guard_on_duty = $8,
      time_end = $9,
      qc_control_number = $10,
      updated_by = $11,
      updated_at = $12
      WHERE id = $13
      RETURNING *`,
      [
        delivery_receipt_id,
        rmaf_id,
        bir_id,
        status_id,
        transaction_type_id,
        supplier_code,
        no_of_bags,
        guard_on_duty,
        time_end,
        qc_control_number,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function addCertification(request_info, table) {
    const { id, certified_signature, certified_by, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE ${table} SET certified_signature = $1,
    certified_by = $2, updated_by = $3, updated_at = $4 WHERE id = $5
     RETURNING *`,
      [certified_signature, certified_by, updated_by, date_today, id]
    );
  }

  async function addNote(request_info, table) {
    const { id, noted_signature, noted_by, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE ${table} SET noted_signature = $1,
    noted_by = $2, updated_by = $3, updated_at = $4 WHERE id = $5
     RETURNING *`,
      [noted_signature, noted_by, updated_by, date_today, id]
    );
  }

  async function validateTally(request_info) {
    const { id, employee_id, checked_signature } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE deduction_receipt SET checked_by = $1, checked_signature = $2,
     updated_at = $3, updated_by = $4 WHERE transaction_id = $5`,
      [employee_id, checked_signature, date_today, employee_id, id]
    );
  }

  async function correctTransaction(request_info) {
    const { id, driver_id, supplier_address, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction SET
      driver_id = $1,
      supplier_address = $5,
      updated_by = $2,
      updated_at = $3
      WHERE id = $4
      RETURNING *`,
      [driver_id, updated_by, date_today, id, supplier_address]
    );
  }

  async function updateTransactionStatus(request_info) {
    const { id, status_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction SET status_id = $1,
     updated_by = $2, updated_at = $3
     WHERE id = $4 RETURNING *`,
      [status_id, updated_by, date_today, id]
    );
  }

  async function updateForReconTransaction(request_info) {
    const { id, rmaf_id, updated_by, failed_rmaf } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction SET
      rmaf_id = $1,
      failed_rmaf = $2,
      updated_by = $3,
      updated_at = $4
      WHERE id = $5
      RETURNING *`,
      [rmaf_id, failed_rmaf, updated_by, date_today, id]
    );
  }

  async function listRMAF() {
    return pool.query("SELECT * FROM rmaf");
  }

  async function findRMAFById(id) {
    return pool.query("SELECT * FROM rmaf WHERE id = $1", [id]);
  }

  async function addRMAF(request_info) {
    const {
      physical_analysis_id,
      chemical_analysis_id,
      date,
      material_rejection_report_id
    } = request_info;

    return pool.query(
      `INSERT INTO rmaf(physical_analysis_id,
      chemical_analysis_id, date, material_rejection_report_id) 
      VALUES ($1, $2, $3, $4)
      RETURNING *`,
      [
        physical_analysis_id,
        chemical_analysis_id,
        date,
        material_rejection_report_id
      ]
    );
  }

  async function updateRMAF(request_info) {
    const {
      id,
      physical_analysis_id,
      chemical_analysis_id,
      date,
      material_rejection_report_id,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE rmaf SET physical_analysis_id = $1,
    chemical_analysis_id = $2, date = $3 
    material_rejection_report_id = $4, updated_by = $5, updated_at = $6
    WHERE id = $7 RETURNING *`,
      [
        physical_analysis_id,
        chemical_analysis_id,
        date,
        material_rejection_report_id,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function insertChemicalAnalysis(request_info) {
    const { id, chemical_analysis_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE rmaf SET chemical_analysis_id = $1,
    updated_at = $2, updated_by = $3 WHERE id = $4 RETURNING *`,
      [chemical_analysis_id, date_today, updated_by, id]
    );
  }

  async function countTransaction() {
    const month_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM");
    return pool.query(`SELECT count(*) FROM transaction WHERE (SELECT CAST(created_at AS varchar))
      like '${month_today}%'`);
  }
  async function countCue() {
    const day_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");
    return pool.query(`SELECT count(*) FROM transaction WHERE (SELECT CAST(created_at AS varchar))
      like '${day_today}%'`);
  }

  async function countRMAF() {
    const month_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM");
    return pool.query(`SELECT count(*) FROM transaction WHERE (SELECT CAST(created_at AS varchar))
      like '${month_today}%'`);
  }

  async function listMRR() {
    return pool.query("SELECT * FROM material_rejection_report");
  }

  async function findMRRById(id) {
    return pool.query("SELECT * FROM material_rejection_report WHERE id = $1", [
      id
    ]);
  }

  async function addMRR(request_info) {
    const {
      findings,
      specification,
      issued_by,
      noted_by,
      approved_by,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO material_rejection_report 
    (findings, specification, issued_by, noted_by, approved_by,
      created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7)
      RETURNING *`,
      [
        findings,
        specification,
        issued_by,
        noted_by,
        approved_by,
        created_by,
        date_today
      ]
    );
  }

  async function updateMRR(request_info) {
    const {
      id,
      findings,
      specification,
      issued_by,
      noted_by,
      approved_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE material_rejection_report SET findings = $1,
    specification = $2, issued_by = $3, noted_by = $4, approved_by = $5,
    updated_by = $6, updated_at = $7 WHERE id = $8 RETURNING *`,
      [
        findings,
        specification,
        issued_by,
        noted_by,
        approved_by,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function listChemicalAnalysis() {
    return pool.query("SELECT * FROM chemical_analysis");
  }

  async function findChemicalAnalysisById(id) {
    return pool.query("SELECT * FROM chemical_analysis WHERE id = $1", [id]);
  }

  async function addChemicalAnalysis(request_info) {
    const {
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      remarks,
      evaluated_by,
      evaluated_signature,
      certified_by,
      certified_signature,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO chemical_analysis (
        reconsideration_id, crude_protein, calcium, ffa_lauric_acid, brix, carbonate_test,
        remarks, evaluated_by, evaluated_signature, certified_by,
        certified_signature, created_by, created_at) VALUES 
        ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING *`,
      [
        reconsideration_id,
        crude_protein,
        calcium,
        ffa_lauric_acid,
        brix,
        carbonate_test,
        remarks,
        evaluated_by,
        evaluated_signature,
        certified_by,
        certified_signature,
        created_by,
        date_today
      ]
    );
  }

  async function updateChemicalAnalysis(request_info) {
    const {
      id,
      disposition_id,
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      remarks,
      evaluated_by,
      certified_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE chemical_analysis SET disposition_id = $1,
      reconsideration_id = $2, crude_protein = $3, calcium = $4, ffa_lauric_acid = $5,
      brix = $6, carbonate_test = $7, remarks = $8, evaluated_by = $9,
      certified_by = $10, updated_by = $11, updated_at = $12 WHERE id = $13 RETURNING *`,
      [
        disposition_id,
        reconsideration_id,
        crude_protein,
        calcium,
        ffa_lauric_acid,
        brix,
        carbonate_test,
        remarks,
        evaluated_by,
        certified_by,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function updateCARID(request_info) {
    const { id, reconsideration_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE chemical_analysis SET reconsideration_id = $1,
       updated_by = $2, updated_at = $3 WHERE id = $4 RETURNING *`,
      [reconsideration_id, updated_by, date_today, id]
    );
  }

  async function listPhysicalAnalysis() {
    return pool.query("SELECT * FROM physical_analysis");
  }

  async function findPhysicalAnalysisById(id) {
    return pool.query("SELECT * FROM physical_analysis WHERE id = $1", [id]);
  }

  async function addPhysicalAnalysis(request_info) {
    const {
      price_reduction_id,
      reconsideration_id,
      type,
      mc_result,
      impurities,
      foreign_material,
      evaluated_by,
      evaluated_signature,
      certified_by,
      certified_signature,
      created_by,
      moisture_tally_sheet_id
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO physical_analysis (price_reduction_id,
      reconsideration_id, type,
      mc_result, impurities, foreign_material, evaluated_by,
      evaluated_signature, certified_by, certified_signature,
       created_by, created_at, moisture_tally_sheet_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, 
        $10, $11, $12, $13) RETURNING *`,
      [
        price_reduction_id,
        reconsideration_id,
        type,
        mc_result,
        impurities,
        foreign_material,
        evaluated_by,
        evaluated_signature,
        certified_by,
        certified_signature,
        created_by,
        date_today,
        moisture_tally_sheet_id
      ]
    );
  }

  async function addRemarkPA(request_info) {
    const { physical_analysis_id, remarks_id } = request_info;

    return pool.query(
      `INSERT INTO physical_analysis_remarks (physical_analysis_id, remarks_id)
    VALUES ($1, $2) RETURNING *`,
      [physical_analysis_id, remarks_id]
    );
  }

  async function getRemarkPA(physical_analysis_id) {
    return pool.query(
      `SELECT * FROM physical_analysis_remarks WHERE 
    physical_analysis_id = $1`,
      [physical_analysis_id]
    );
  }

  async function addDispositionPA(request_info) {
    const { physical_analysis_id, disposition_id } = request_info;

    return pool.query(
      `INSERT INTO physical_analysis_dispositions (physical_analysis_id, disposition_id)
    VALUES ($1, $2) RETURNING *`,
      [physical_analysis_id, disposition_id]
    );
  }

  async function getDispoPA(physical_analysis_id) {
    return pool.query(
      `SELECT * FROM physical_analysis_dispositions WHERE 
    physical_analysis_id = $1`,
      [physical_analysis_id]
    );
  }

  async function addDispositionCA(request_info) {
    const { chemical_analysis_id, disposition_id } = request_info;

    return pool.query(
      `INSERT INTO chemical_analysis_dispositions (chemical_analysis_id, disposition_id)
    VALUES ($1, $2) RETURNING *`,
      [chemical_analysis_id, disposition_id]
    );
  }

  async function getDispoCA(chemical_analysis_id) {
    return pool.query(
      `SELECT * FROM chemical_analysis_dispositions WHERE 
      chemical_analysis_id = $1`,
      [chemical_analysis_id]
    );
  }

  async function updatePhysicalAnalysis(request_info) {
    const {
      id,
      price_reduction_id,
      reconsideration_id,
      disposition_id,
      type,
      mc_result,
      impurities,
      foreign_material,
      evaluated_by,
      certified_by,
      noted_by,
      updated_by,
      moisture_tally_sheet_id
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE physical_analysis set price_reduction_id = $1,
      reconsideration_id = $2, 
      disposition_id = $3, type = $4, mc_result = $5, impurities = $6,
      foreign_material = $7, evaluated_by = $8, certified_by = $9,
      noted_by = $10, updated_by = $11, updated_at = $12 , moisture_tally_sheet_id = $14
      WHERE id = $13 RETURNING *`,
      [
        price_reduction_id,
        reconsideration_id,
        disposition_id,
        type,
        mc_result,
        impurities,
        foreign_material,
        evaluated_by,
        certified_by,
        noted_by,
        updated_by,
        date_today,
        id,
        moisture_tally_sheet_id
      ]
    );
  }

  async function updatePARID(request_info) {
    const { id, reconsideration_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE physical_analysis set
      reconsideration_id = $1, updated_by = $2, updated_at = $3 
      WHERE id = $4 RETURNING *`,
      [reconsideration_id, updated_by, date_today, id]
    );
  }

  async function listDisposition() {
    return pool.query("SELECT * FROM disposition ORDER BY id");
  }

  async function findDispositionById(id) {
    return pool.query("SELECT * FROM disposition WHERE id = $1", [id]);
  }

  async function findDispositionByName(description) {
    return pool.query("SELECT * FROM disposition WHERE description = $1", [
      description
    ]);
  }

  async function addDisposition(request_info) {
    const { description, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO disposition (description, created_by,
       created_at) VALUES ($1, $2, $3) RETURNING *`,
      [description, created_by, date_today]
    );
  }

  async function updateDisposition(request_info) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE disposition SET description = $1, updated_by = $2,
    updated_at = $3 WHERE id = $4 RETURNING *`,
      [description, updated_by, date_today, id]
    );
  }

  async function listReconsideration() {
    return pool.query("SELECT * FROM reconsideration");
  }

  async function findReconsiderationById(id) {
    return pool.query("SELECT * FROM reconsideration WHERE id = $1", [id]);
  }

  async function addReconsideration(request_info) {
    const {
      for_consideration,
      reason,
      reconsidered_by,
      reconsidered_approved_by,
      noted_by,
      created_by,
      reconsidered_signature
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO reconsideration (for_consideration,
      reason, reconsidered_by, reconsidered_approved_by, noted_by,
       created_by, created_at, reconsidered_signature) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
       RETURNING *`,
      [
        for_consideration,
        reason,
        reconsidered_by,
        reconsidered_approved_by,
        noted_by,
        created_by,
        date_today,
        reconsidered_signature
      ]
    );
  }

  async function approveRecon(request_info) {
    const {
      employee_id,
      reconsidered_approved_signature,
      reconsideration_id
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE reconsideration SET reconsidered_approved_by = $1,
      reconsidered_approved_signature = $2, updated_by = $3, updated_at = $4
      WHERE id = $5`,
      [
        employee_id,
        reconsidered_approved_signature,
        employee_id,
        date_today,
        reconsideration_id
      ]
    );
  }

  async function updateReconsideration(request_info) {
    const {
      id,
      for_consideration,
      reason,
      reconsidered_by,
      reconsidered_approved_by,
      noted_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE reconsideration SET for_consideration = $1,
    reason = $2, reconsidered_by = $3, reconsidered_approved_by = $4,
    noted_by = $5, updated_by = $6, updated_at = $7 WHERE id = $8 
    RETURNING *`,
      [
        for_consideration,
        reason,
        reconsidered_by,
        reconsidered_approved_by,
        noted_by,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function listMTS() {
    return pool.query("SELECT * FROM moisture_tally_sheet ORDER BY id");
  }

  async function findMTSById(id) {
    return pool.query("SELECT * FROM moisture_tally_sheet WHERE id = $1", [id]);
  }

  async function findMTSByTransactionId(transaction_id) {
    return pool.query(
      "SELECT * FROM moisture_tally_sheet WHERE transaction_id = $1",
      [transaction_id]
    );
  }

  async function addMTS(request_info) {
    const {
      moisture_trial_1,
      moisture_trial_2,
      moisture_trial_3,
      transaction_id,
      average,
      analysed_by,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO moisture_tally_sheet (moisture_trial_1, 
      moisture_trial_2, moisture_trial_3, average, analysed_by, created_by, created_at)
       VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
      [
        moisture_trial_1,
        moisture_trial_2,
        moisture_trial_3,
        average,
        analysed_by,
        created_by,
        date_today
      ]
    );
  }

  async function updateMTS(request_info) {
    const {
      id,
      moisture_trial_1,
      moisture_trial_2,
      moisture_trial_3,
      average,
      analysed_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE moisture_tally_sheet SET moisture_trial_1 = $1, 
      moisture_trial_2 = $2, moisture_trial_3 = $3, average = $4,
      analysed_by = $5, updated_by = $6, updated_at = $7 WHERE id = $8 
      RETURNING *`,
      [
        moisture_trial_1,
        moisture_trial_2,
        moisture_trial_3,
        average,
        analysed_by,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function listPriceReduction() {
    return pool.query("SELECT * FROM price_reduction ORDER BY id");
  }

  async function findPriceReductionById(id) {
    return pool.query("SELECT * FROM price_reduction WHERE id = $1", [id]);
  }

  async function addPriceReduction(request_info) {
    const { remarks, certified_by, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO price_reduction (remarks, certified_by, created_by, created_at)
    VALUES ($1, $2, $3, $4) RETURNING *`,
      [remarks, certified_by, created_by, date_today]
    );
  }

  async function updatePriceReduction(request_info) {
    const { id, remarks, certified_by, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE price_reduction SET remarks = $1, certified_by = $2, 
      updated_by = $3, updated_at = $4 WHERE id = $5 RETURNING *`,
      [remarks, certified_by, updated_by, date_today, id]
    );
  }

  async function listDeductionCriteria() {
    return pool.query("SELECT * FROM deduction_criteria");
  }

  async function findDeductionCriteriaById(id) {
    return pool.query("SELECT * FROM deduction_criteria WHERE id = $1", [id]);
  }

  async function findDeductionCriteriaByDRId(id) {
    return pool.query(
      "SELECT * FROM deduction_criteria WHERE deduction_receipt_id = $1",
      [id]
    );
  }

  async function totalDeductionPerCriteria(transaction_id, criteria_id) {
    return pool.query(
      `SELECT SUM(quantity) as quantity, deduction, 
    SUM(total) as total FROM deduction_criteria WHERE deduction_receipt_id 
    in (SELECT id FROM deduction_receipt WHERE transaction_id = $1)
    and criteria_id = $2 GROUP BY deduction`,
      [transaction_id, criteria_id]
    );
  }

  async function addDeductionCriteria(request_info) {
    const {
      criteria_id,
      deduction_receipt_id,
      quantity,
      deduction,
      total
    } = request_info;

    return pool.query(
      `INSERT INTO deduction_criteria (criteria_id, deduction_receipt_id,
      quantity, deduction, total) VALUES
      ($1, $2, $3, $4, $5) RETURNING *`,
      [criteria_id, deduction_receipt_id, quantity, deduction, total]
    );
  }

  async function updateDeductionCriteria(request_info) {
    const {
      id,
      deduction_receipt_id,
      criteria_id,
      quantity,
      deduction,
      total,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE deduction_criteria SET criteria_id = $1, 
    quantity = $2, deduction = $3, total = $4, updated_by = $5, updated_at = $6,
    deduction_receipt_id = $8 WHERE id = $7 RETURNING *`,
      [
        criteria_id,
        quantity,
        deduction,
        total,
        updated_by,
        date_today,
        id,
        deduction_receipt_id
      ]
    );
  }

  async function listCriteria() {
    return pool.query("SELECT * FROM criteria ORDER BY description");
  }

  async function listCriteriaOnDC(transaction_id) {
    return pool.query(
      `SELECT deduction_criteria.criteria_id, criteria.description, criteria.item_code
      FROM  deduction_criteria, criteria 
      WHERE deduction_criteria.deduction_receipt_id 
     in (SELECT id FROM deduction_receipt WHERE transaction_id = $1) AND
     deduction_criteria.criteria_id = criteria.id
     GROUP BY deduction_criteria.criteria_id, criteria.description, criteria.item_code`,
      [transaction_id]
    );
  }

  async function listCriteriaByBE(request_info) {
    const { business_entity_id } = request_info;
    return pool.query(
      `SELECT * FROM criteria
     WHERE business_entity = $1
     ORDER BY description`,
      [business_entity_id]
    );
  }

  async function findCriteriaById(id) {
    return pool.query("SELECT * FROM criteria WHERE id = $1", [id]);
  }

  async function findCriteriaByName(description) {
    return pool.query("SELECT * FROM criteria WHERE description = $1", [
      description
    ]);
  }

  async function addCriteria(request_info) {
    const { description, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO criteria (description, created_by, created_at)
    VALUES ($1, $2, $3) RETURNING *`,
      [description, created_by, date_today]
    );
  }

  async function updateCriteria(request_info) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE criteria SET description = $1,
    updated_by = $2, updated_at = $3 WHERE id = $4 RETURNING *`,
      [description, updated_by, date_today, id]
    );
  }

  async function listBIR() {
    return pool.query("SELECT * FROM bir");
  }

  async function findBIRById(id) {
    return pool.query("SELECT * FROM bir WHERE id = $1", [id]);
  }

  async function findBIRByItemId(item_id) {
    return pool.query("SELECT * FROM bir WHERE item_id = $1", [item_id]);
  }

  async function findBIRByBEId(business_entity_id) {
    return pool.query("SELECT * FROM bir WHERE business_entity_id = $1", [
      business_entity_id
    ]);
  }

  async function findBIRByRMId(raw_material_id) {
    return pool.query("SELECT * FROM bir WHERE raw_material_id = $1", [
      raw_material_id
    ]);
  }

  async function addBIR(request_info) {
    const {
      item_id,
      business_entity_id,
      raw_material_id,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO bir (item_id, business_entity_id, raw_material_id, created_by, created_at)
    VALUES ($1, $2, $3, $4, $5) RETURNING *`,
      [item_id, business_entity_id, raw_material_id, created_by, date_today]
    );
  }

  async function updateBIR(request_info) {
    const {
      id,
      item_id,
      business_entity_id,
      raw_material_id,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE bir set item_id = $1, business_entity_id = $2,
     raw_material_id = $3, updated_by = $4, updated_at = $5 
     WHERE id = $6 RETURNING *`,
      [item_id, business_entity_id, raw_material_id, updated_by, date_today, id]
    );
  }

  async function listRawMaterials() {
    return pool.query("SELECT * FROM raw_materials ORDER BY description");
  }

  async function findRawMaterialsById(id) {
    return pool.query("SELECT * FROM raw_materials WHERE id = $1", [id]);
  }

  async function findRawMaterialsByName(name) {
    return pool.query("SELECT * FROM raw_materials WHERE description = $1", [
      name
    ]);
  }

  async function addRawMaterials(request_info) {
    const { description, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO raw_materials (description, created_by, created_at)
    VALUES ($1, $2, $3) RETURNING *`,
      [description, created_by, date_today]
    );
  }

  async function updateRawMaterials(request_info) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE raw_materials set description = $1, updated_by = $2, updated_at = $3 
    WHERE id = $4 RETURNING *`,
      [description, updated_by, date_today, id]
    );
  }

  async function listItem() {
    return pool.query("SELECT * FROM items ORDER BY description");
  }

  async function findItemById(id) {
    return pool.query("SELECT * FROM items WHERE id = $1", [id]);
  }

  async function findItemByName(name) {
    return pool.query("SELECT * FROM items WHERE description = $1", [name]);
  }

  async function addItem(request_info) {
    const { description, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO items (description, created_by, created_at)
    VALUES ($1, $2, $3) RETURNING *`,
      [description, created_by, date_today]
    );
  }

  async function updateItem(request_info) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE items set description = $1, updated_by = $2, updated_at = $3 
    WHERE id = $4 RETURNING *`,
      [description, updated_by, date_today, id]
    );
  }

  async function listBusinessEntity() {
    return pool.query("SELECT * FROM business_entity ORDER BY description");
  }

  async function findBusinessEntityById(id) {
    return pool.query("SELECT * FROM business_entity WHERE id = $1", [id]);
  }

  async function findBusinessEntityByName(name) {
    return pool.query("SELECT * FROM business_entity WHERE description = $1", [
      name
    ]);
  }

  async function addBusinessEntity(request_info) {
    const { description, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO business_entity (description, created_by, created_at)
    VALUES ($1, $2, $3) RETURNING *`,
      [description, created_by, date_today]
    );
  }

  async function updateBusinessEntity(request_info) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE business_entity set description = $1, updated_by = $2, updated_at = $3 
    WHERE id = $4 RETURNING *`,
      [description, updated_by, date_today, id]
    );
  }

  async function listRejectionReceipt() {
    return pool.query("SELECT * FROM rejection_receipt ORDER BY date");
  }

  async function findRejectionReceiptById(id) {
    return pool.query("SELECT * FROM rejection_receipt WHERE id = $1", [id]);
  }

  async function findRejectionReceiptByTranId(transaction_id) {
    return pool.query(
      "SELECT * FROM rejection_receipt WHERE transaction_id = $1",
      [transaction_id]
    );
  }

  async function addRejectionReceipt(request_info) {
    const {
      transaction_id,
      no_of_bags_rejected,
      reason_of_rejection,
      date
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO rejection_receipt (transaction_id, 
        no_of_bags_rejected, 
        reason_of_rejection,
        date,
        created_at) 
        VALUES ($1, $2, $3, $4, $5) RETURNING *`,
      [
        transaction_id,
        no_of_bags_rejected,
        reason_of_rejection,
        date,
        date_today
      ]
    );
  }

  async function updateRejectionReceipt(request_info) {
    const {
      id,
      transaction_id,
      no_of_bags_rejected,
      reason_of_rejection,
      date,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE rejection_receipt SET transaction_id = $1,
    no_of_bags_rejected = $2, reason_of_rejection = $3, date = $4, updated_by = $5,
    updated_at = $6 WHERE id = $7 RETURNING *`,
      [
        transaction_id,
        no_of_bags_rejected,
        reason_of_rejection,
        date,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function listDriver() {
    return pool.query("SELECT * FROM drivers ORDER BY firstname");
  }

  async function findDriverById(id) {
    return pool.query("SELECT * FROM drivers WHERE id = $1", [id]);
  }

  async function addDriver(request_info) {
    const {
      firstname,
      middlename,
      lastname,
      name_extension,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO drivers (firstname, middlename, lastname, name_extension, created_by, created_at)
      VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
      [firstname, middlename, lastname, name_extension, created_by, date_today]
    );
  }

  async function updateDriver(request_info) {
    const {
      id,
      firstname,
      middlename,
      lastname,
      name_extension,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE drivers SET firstname = $1, middlename = $2, lastname = $3, name_extension = $4, updated_by = $5, updated_at = $6 WHERE id = $7 RETURNING *
    `,
      [
        firstname,
        middlename,
        lastname,
        name_extension,
        updated_by,
        date_today,
        id
      ]
    );
  }

  async function listStatus() {
    return pool.query("SELECT * FROM status ORDER BY name");
  }

  async function listToPOStatus() {
    return pool.query(`SELECT id FROM status WHERE name in
     ('Analysis-Approved', 'Quality Control', 'Quality Control-Passed', 'Completed',
     'For Reconsideration', 'Unloading', 'Unloaded', 'For Outbound')`);
  }

  async function findStatusById(id) {
    return pool.query("SELECT * FROM status WHERE id = $1", [id]);
  }

  async function findStatusByName(name) {
    return pool.query("SELECT * FROM status WHERE name = $1", [name]);
  }

  async function addStatus(request_info) {
    const { name, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO status (name, created_by, created_at)
           VALUES ($1, $2, $3) RETURNING *`,
      [name, created_by, date_today]
    );
  }

  async function updateStatus(request_info) {
    const { id, name, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE status set name = $1, updated_by = $2,
    updated_at = $3 WHERE id = $4  RETURNING *`,
      [name, updated_by, date_today, id]
    );
  }

  async function listTransactionTypes() {
    return pool.query("SELECT * FROM transaction_type ORDER BY name");
  }

  async function findTransactionTypeById(id) {
    return pool.query("SELECT * FROM transaction_type WHERE ID = $1", [id]);
  }

  async function addTransactionType(request_info) {
    const { name, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO transaction_type (name, created_by, created_at) 
                      VALUES ($1, $2, $3) RETURNING *`,
      [name, created_by, date_today]
    );
  }

  async function updateTransactionType(request_info) {
    const { id, name, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE transaction_type set name = $1, updated_by = $2, updated_at = $3 WHERE id = $4 RETURNING *`,
      [name, updated_by, date_today, id]
    );
  }

  async function listTrucktype() {
    return pool.query("SELECT * FROM truck_types ORDER BY truck_type");
  }

  async function findTrucktypeById(id) {
    return pool.query("SELECT * FROM truck_types WHERE id = $1", [id]);
  }

  async function addTruckType(request_info) {
    const { truck_type, truck_model, truck_size, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO truck_types (truck_type, truck_model, truck_size, created_by, created_at)
            VALUES ($1, $2, $3, $4, $5) RETURNING *`,
      [truck_type, truck_model, truck_size, created_by, date_today]
    );
  }

  async function updateTruckType(request_info) {
    const {
      id,
      truck_type,
      truck_model,
      truck_size,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE truck_types set truck_type = $1, truck_model = $2, truck_size = $3,
              updated_by = $4, updated_at = $5 WHERE id = $6 RETURNING *`,
      [truck_type, truck_model, truck_size, updated_by, date_today, id]
    );
  }

  async function listTrucks() {
    return pool.query("SELECT * FROM truck_infos");
  }

  async function findTruckById(id) {
    return pool.query("SELECT * FROM truck_infos WHERE id = $1", [id]);
  }

  async function addTruck(request_info) {
    const { truck_type_id, plate_number, created_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO truck_infos (truck_type_id, plate_number, created_by, created_at)
        VALUES ($1, $2, $3, $4) RETURNING *`,
      [truck_type_id, plate_number, created_by, date_today]
    );
  }

  async function updateTruck(request_info) {
    const { id, truck_type_id, plate_number, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE truck_infos set truck_type_id = $1, plate_number = $2, updated_by = $3,
          updated_at = $4 WHERE id = $5 RETURNING *`,
      [truck_type_id, plate_number, updated_by, date_today, id]
    );
  }

  async function listDeliveryReceipts() {
    return pool.query(
      "SELECT * FROM delivery_receipt ORDER BY description_of_articles"
    );
  }

  async function findDeliveryReceiptByID(id) {
    return pool.query(
      "SELECT * FROM delivery_receipt WHERE id = $1" +
        "ORDER BY description_of_articles",
      [id]
    );
  }

  async function addDeliveryReceipt(request_info) {
    const {
      delivery_receipt,
      no_of_bags,
      unit,
      description_of_articles,
      created_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      "INSERT INTO delivery_receipt (id, quantity, unit, description_of_articles, created_by, created_at, updated_at)" +
        "VALUES ($6, $1, $2, $3, $4, $5, $7) RETURNING *",
      [
        no_of_bags,
        unit,
        description_of_articles,
        created_by,
        date_today,
        delivery_receipt,
        date_today
      ]
    );
  }

  async function updateDeliveryReceipt(request_info) {
    const {
      id,
      quantity,
      unit,
      description_of_articles,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE delivery_receipt set quantity = $1,
                    unit = $2, description_of_articles = $3, updated_by = $4,
                    updated_at = $5 WHERE id = $6 RETURNING *`,
      [quantity, unit, description_of_articles, updated_by, date_today, id]
    );
  }

  // Activity logs
  async function addActivityLog(log_info) {
    const { created_by, action, table, old_values, new_values } = log_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      'INSERT INTO activity_logs (created_by, "action", "table", old_values, new_values, created_at) ' +
        "VALUES ($1, $2, $3, $4, $5, $6)",
      [created_by, action, table, old_values, new_values, date_today]
    );
  }
};
