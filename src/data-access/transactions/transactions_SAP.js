module.exports = function transactionDB({ pool }, { moment }) {
  return Object.freeze({
    // Activity Logs
    addActivityLog,

    // Delivery receipts
    listDeliveryReceipts,
    findDeliveryReceiptByID,
    addDeliveryReceipt,
    updateDeliveryReceipt,

    // Truck infos
    listTrucks,
    findTruckById,
    addTruck,
    updateTruck,

    // Truck type
    listTrucktype,
    findTrucktypeById,
    addTruckType,
    updateTruckType,

    // Transaction type
    listTransactionTypes,
    findTransactionTypeById,
    endTransactionStatusLog,
    addTransactionType,
    updateTransactionType,

    // Status
    listStatus,
    findStatusById,
    findStatusByName,
    addStatus,
    updateStatus,

    // Driver
    listDriver,
    findDriverById,
    addDriver,
    updateDriver,

    // Rejection receipt
    listRejectionReceipt,
    findRejectionReceiptById,
    findRejectionReceiptByTranId,
    addRejectionReceipt,
    updateRejectionReceipt,

    // Business Entity
    listBusinessEntity,
    findBusinessEntityById,
    findBusinessEntityByName,
    addBusinessEntity,
    updateBusinessEntity,

    // Item
    listItem,
    findItemById,
    findItemByName,
    addItem,
    updateItem,

    // Raw material
    listRawMaterials,
    findRawMaterialsById,
    findRawMaterialsByName,
    addRawMaterials,
    updateRawMaterials,

    // BIR
    listBIR,
    findBIRByBEId,
    findBIRById,
    findBIRByItemId,
    findBIRByRMId,
    addBIR,
    updateBIR,
    findItemsByBusinessEntity,

    // Criteria
    listCriteria,
    listCriteriaOnDC,
    listCriteriaByBE,
    findCriteriaById,
    findCriteriaByName,
    addCriteria,
    updateCriteria,

    // Deduction Criteria
    listDeductionCriteria,
    findDeductionCriteriaById,
    totalDeductionPerCriteria,
    findDeductionCriteriaByDRId,
    addDeductionCriteria,
    updateDeductionCriteria,

    // Price reduction
    listPriceReduction,
    findPriceReductionById,
    addPriceReduction,
    updatePriceReduction,

    // Moisture tally sheet
    listMTS,
    findMTSById,
    findMTSByTransactionId,
    addMTS,
    updateMTS,

    // Reconsideration
    listReconsideration,
    findReconsiderationById,
    addReconsideration,
    updateReconsideration,

    // Disposition
    listDisposition,
    findDispositionById,
    findDispositionByName,
    addDisposition,
    updateDisposition,

    // Physical analysis
    listPhysicalAnalysis,
    findPhysicalAnalysisById,
    addPhysicalAnalysis,
    updatePhysicalAnalysis,
    updatePARID,

    // Chemical Analysis
    listChemicalAnalysis,
    findChemicalAnalysisById,
    addChemicalAnalysis,
    updateChemicalAnalysis,
    updateCARID,

    // Material Rejjection Report
    listMRR,
    findMRRById,
    addMRR,
    updateMRR,

    // RMAF
    countRMAF,
    listRMAF,
    findRMAFById,
    addRMAF,
    updateRMAF,

    // Transaction
    listTransaction,
    findTransactionById,
    findTransactionBySupplierCode,
    addTransaction,
    updateTransaction,
    validateTally,
    updateTransactionStatus,
    updateForReconTransaction,
    countTransaction,
    countCue,
    correctTransaction,

    // Deduction Receipt
    listDeductionReceipts,
    findDeductionReceiptById,
    findDeductionReceiptByTransId,
    countDeductionReceiptByTranId,
    addDeductionReceipt,
    updateDeductionReceipt,

    // Transaction Status Logs
    listTransactionStatusLogs,
    findTransactionStatusLogsById,
    findTransactionStatusLogsByTransactionId,
    addTransactionStatusLog,
    updateTransactionStatusLog,

    // Remarks
    listRemarks,
    findRemarksById,
    addRemark,
    updateRemark,

    // Employees
    getEmployee,
    getDepartment
  });

  // async function getDepartment(department_id) {
  //   return pool.query(`SELECT * FROM departments WHERE id = $1`, [
  //     department_id
  //   ]);
  // }

  async function getDepartment(department_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEPART('${department_id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function getEmployee(employee_id) {
  //   return pool.query(
  //     `SELECT
  //   "public".employees.role_id,
  //   "public".employees.employee_id,
  //   "public".employees."password",
  //   "public".employees.token,
  //   "public".employees.created_by,
  //   "public".employees.updated_by,
  //   "public".employees.created_at,
  //   "public".employees.updated_at,
  //   "public".employees.status,
  //   "public".employees."name",
  //   "public".employees."id",
  //   "public".employees.pin_code,
  //   "public".employees.signature,
  //   "public".employees.department,
  //   "public".departments."id" as "department_id",
  //   "public".departments."name" as "department_name",
  //   "public".departments.status as "department_status"
  //   FROM
  //   "public".employees
  //   INNER JOIN "public".departments ON "public".employees.department = "public".departments."id"
  //   WHERE employee_id = $1`,
  //     [employee_id]
  //   );
  // }

  async function getEmployee(employee_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      //$crossjoin(U_BFI_FSQR_USER,U_BFI_FSQR_ROLES)?$expand=U_BFI_FSQR_USER($select=Name,U_FSQR_employeeID,U_FSQR_password,U_FSQR_status,U_FSQR_departID),U_BFI_FSQR_ROLES($select=Code,Name,U_FSQR_status)&$filter=U_BFI_FSQR_USER/U_FSQR_roleID eq U_BFI_FSQR_ROLES/Code and U_BFI_FSQR_USER/U_FSQR_employeeID eq '${employee_id}'
      link: `/b1s/v1/$crossjoin(U_BFI_FSQR_USER,U_BFI_FSQR_DEPART)?$expand=U_BFI_FSQR_USER($select=Code,Name,U_FSQR_roleID,U_FSQR_employeeID,U_FSQR_password,U_FSQR_status,U_FSQR_name,U_FSQR_pincode,U_FSQR_signature,U_FSQR_departID,U_FSQR_created_by,U_FSQR_created_date,U_FSQR_created_time,U_FSQR_updated_by,U_FSQR_updated_date,U_FSQR_updated_time),U_BFI_FSQR_DEPART($select=Code,Name,U_FSQR_name,U_FSQR_status)&$filter=U_BFI_FSQR_USER/U_FSQR_departID eq U_BFI_FSQR_DEPART/Code and U_BFI_FSQR_USER/U_FSQR_employeeID eq '${employee_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listRemarks() {
  //   return pool.query(`SELECT * FROM remarks`);
  // }

  async function listRemarks(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_REMARKS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findRemarksById(id) {
  //   return pool.query(`SELECT * FROM remarks WHERE id = $1`, [id]);
  // }

  async function findRemarksById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_REMARKS('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addRemark(request_info) {
  //   const { name, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO remarks (name, created_by, created_at)
  //   VALUES ($1, $2, $3) RETURNING *`,
  //     [name, created_by, date_today]
  //   );
  // }

  async function addRemark(request_info, SessionId) {
    const { name, created_by } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_REMARKS/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_REMARKS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_name: `${name}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateRemark(request_info) {
  //   const { name, updated_by, id } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE remarks SET name = $1, updated_by = $2,
  //   updated_at = $3 WHERE id = $4 RETURNING *`,
  //     [name, updated_by, date_today, id]
  //   );
  // }

  async function updateRemark(request_info, SessionId) {
    const { name, updated_by, id } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_REMARKS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name: `${name}`,
        U_FSQR_name: `${name}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listTransactionStatusLogs() {
  //   return pool.query(`SELECT * FROM transaction_status_logs `);
  // }

  async function listTransactionStatusLogs(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRANS_LOG`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findTransactionStatusLogsByTransactionId(transaction_id) {
  //   return pool.query(
  //     `SELECT * FROM transaction_status_logs WHERE
  //   transaction_id = $1 ORDER by start_time`,
  //     [transaction_id]
  //   );
  // }

  async function findTransactionStatusLogsByTransactionId(
    transaction_id,
    SessionId
  ) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRANS_LOG('${transaction_id}')?$orderby=FSQR_startTime`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findTransactionStatusLogsById(id) {
  //   return pool.query(`SELECT * FROM transaction_status_logs WHERE id = $1`, [
  //     id
  //   ]);
  // }

  async function findTransactionStatusLogsById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRANS_LOG('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addTransactionStatusLog(request_info) {
  //   const { id, status_id, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO transaction_status_logs
  //   (transaction_id, status_id, start_time, created_by, created_at)
  //   VALUES ($1, $2, $3, $4, $5) RETURNING *`,
  //     [id, status_id, date_today, created_by, date_today]
  //   );
  // }

  async function addTransactionStatusLog(request_info, SessionId) {
    const { id, status_id, created_by } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_TRANS_LOG/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_TRANS_LOG`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_trans_id: `${id}`,
        U_FSQR_stts_id: `${status_id}`,
        U_FSQR_startTime: `${time_today}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function endTransactionStatusLog(request_info) {
  //   const { transaction_id, status_id, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction_status_logs SET end_time = $1,
  //     updated_by = $4, updated_at = $5
  //     WHERE transaction_id = $2 and status_id = $3 RETURNING *`,
  //     [date_today, transaction_id, status_id, updated_by, date_today]
  //   );
  // }

  async function endTransactionStatusLog(SessionId, request_info) {
    const { transaction_id, status_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_TRANS_LOG?$filter=U_FSQR_trans_id eq '${transaction_id}' and U_FSQR_stts_id eq '${status_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_endTime: `${time_today}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateTransactionStatusLog(request_info) {
  //   const {
  //     id,
  //     transaction_id,
  //     status_id,
  //     end_time,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction_status_logs SET
  //   transaction_id = $1, status_id = $2, end_time = $3,
  //   updated_by = $4, updated_at = $5 WHERE id = $6 RETURNING *`,
  //     [transaction_id, status_id, end_time, updated_by, date_today, id]
  //   );
  // }

  async function updateTransactionStatusLog(SessionId, request_info) {
    const {
      id,
      transaction_id,
      status_id,
      end_time,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_TRANS_LOG('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_trans_id: `${transaction_id}`,
        U_FSQR_stts_id: `${status_id}`,
        U_FSQR_endTime: `${end_time}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findItemsByBusinessEntity(id) {
  //   return pool.query(
  //     `SELECT
  //   "public".bir.id,
  //   "public".bir.business_entity_id,
  //   "public".bir.item_id,
  //   "public".bir.raw_material_id
  //   FROM
  //   "public".bir
  //   WHERE "public".bir.business_entity_id = $1
  //   `,
  //     [id]
  //   );
  // }

  async function findItemsByBusinessEntity(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_BIR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listDeductionReceipts() {
  //   return pool.query("SELECT * FROM deduction_receipt");
  // }

  async function listDeductionReceipts(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDeductionReceiptById(id) {
  //   return pool.query("SELECT * FROM deduction_receipt WHERE id = $1", [id]);
  // }

  async function findDeductionReceiptById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDeductionReceiptByTransId(transaction_id) {
  //   return pool.query(`SELECT SUM(total_deductions) as sum_total_deductions, count(*)
  //   FROM deduction_receipt WHERE transaction_id = $1`,
  //    [transaction_id]);
  // }

  async function findDeductionReceiptByTransId(transaction_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT?$apply=aggregate(U_FSQR_total_ddctn with count as TDC, U_FSQR_total_ddctn  with sum as TDS)&$filter=U_FSQR_trans_id eq '${transaction_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function countDeductionReceiptByTranId(transaction_id) {
  //   return pool.query("SELECT COUNT(*) FROM deduction_receipt WHERE transaction_id = $1", [transaction_id]);
  // }

  async function countDeductionReceiptByTranId(transaction_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT?$apply=aggregate(U_FSQR_total_ddctn with count as TDC)&$filter=U_FSQR_trans_id eq '${transaction_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addDeductionReceipt(request_info) {
  //   const {
  //     transaction_id,
  //     total_deductions,
  //     prepared_by,
  //     checked_by,
  //     received_by,
  //     noted_by,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO deduction_receipt (transaction_id,
  //     date, total_deductions, prepared_by, checked_by, received_by,
  //     noted_by, created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6,
  //     $7, $8, $9) RETURNING *`,
  //     [
  //       transaction_id,
  //       date_today,
  //       total_deductions,
  //       prepared_by,
  //       checked_by,
  //       received_by,
  //       noted_by,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addDeductionReceipt(request_info, SessionId) {
    const {
      transaction_id,
      total_deductions,
      prepared_by,
      checked_by,
      received_by,
      noted_by,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_DEDUC_RT/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_trans_id: `${transaction_id}`,
        U_FSQR_date: `${date_today}`,
        U_FSQR_total_ddctn: `${total_deductions}`,
        U_FSQR_prep_by: `${prepared_by}`,
        U_FSQR_checked_by: `${checked_by}`,
        U_FSQR_recvd_by: `${received_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateDeductionReceipt(request_info) {
  //   const {
  //     id,
  //     transaction_id,
  //     date,
  //     total_deductions,
  //     prepared_by,
  //     checked_by,
  //     received_by,
  //     noted_by,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE deduction_receipt SET transaction_id = $1,
  //   date = $2, total_deductions = $3, prepared_by = $4,
  //   checked_by = $5, received_by = $6, noted_by = $7, updated_by = $8,
  //   updated_at = $9 WHERE id = $10 RETURNING *`,
  //     [
  //       transaction_id,
  //       date,
  //       total_deductions,
  //       prepared_by,
  //       checked_by,
  //       received_by,
  //       noted_by,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateDeductionReceipt(request_info, SessionId) {
    const {
      id,
      transaction_id,
      date,
      total_deductions,
      prepared_by,
      checked_by,
      received_by,
      noted_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name: `${id}`,
        U_FSQR_trans_id: `${transaction_id}`,
        U_FSQR_date: `${date_today}`,
        U_FSQR_total_ddctn: `${total_deductions}`,
        U_FSQR_prep_by: `${prepared_by}`,
        U_FSQR_checked_by: `${checked_by}`,
        U_FSQR_recvd_by: `${received_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listTransaction(request_info) {
  //   const { date_from, date_to } = request_info;

  //   return pool.query(
  //     `SELECT * FROM transaction WHERE created_at BETWEEN SYMMETRIC
  //   $1 AND $2 ORDER BY id DESC`,
  //     [date_from, date_to]
  //   );
  // }

  async function listTransaction(request_info, SessionId) {
    const { date_from, date_to } = request_info;

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRNS?$filter=U_FSQR_created_date ge '${date_from}' and U_FSQR_created_date lt '${date_to}'&$orderby=Code desc`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findTransactionById(id) {
  //   return pool.query("SELECT * FROM transaction WHERE id = $1", [id]);
  // }

  async function findTransactionById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRNS('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findTransactionBySupplierCode(supplier_code) {
  //   return pool.query("SELECT * FROM transaction WHERE supplier_code = $1",
  //   [supplier_code])
  // }

  async function findTransactionBySupplierCode(supplier_code, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRNS?$filter=U_FSQR_suppl_code eq '${supplier_code}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addTransaction(request_info) {
  //   const {
  //     delivery_receipt,
  //     rmaf_id,
  //     bir_id,
  //     status_id,
  //     plate_number,
  //     transaction_type_id,
  //     driver_name,
  //     supplier_code,
  //     no_of_bags,
  //     description,
  //     guard_on_duty,
  //     time_end,
  //     qc_control_number,
  //     created_by,
  //     queue_number,
  //     supplier_name,
  //     supplier_address
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO transaction (
  //     delivery_receipt_id,
  //     rmaf_id,
  //     bir_id,
  //     status_id,
  //     plate_number,
  //     transaction_type_id,
  //     driver_name,
  //     supplier_code,
  //     no_of_bags,
  //     description,
  //     guard_on_duty,
  //     time_end,
  //     qc_control_number,
  //     created_by,
  //     created_at,
  //     queue_number,
  //     supplier_name,
  //     supplier_address
  //    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,
  //      $12, $13, $14, $15, $16, $17, $18) RETURNING *`,
  //     [
  //       delivery_receipt,
  //       rmaf_id,
  //       bir_id,
  //       status_id,
  //       plate_number,
  //       transaction_type_id,
  //       driver_name,
  //       supplier_code,
  //       no_of_bags,
  //       description,
  //       guard_on_duty,
  //       time_end,
  //       qc_control_number,
  //       created_by,
  //       date_today,
  //       queue_number,
  //       supplier_name,
  //       supplier_address
  //     ]
  //   );
  // }

  async function addTransaction(request_info, SessionId) {
    const {
      delivery_receipt,
      rmaf_id,
      bir_id,
      status_id,
      plate_number,
      transaction_type_id,
      driver_name,
      driver_id,
      supplier_code,
      no_of_bags,
      description,
      guard_on_duty,
      time_end,
      qc_control_number,
      created_by,
      queue_number,
      supplier_id,
      supplier_name,
      supplier_address
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_TRNS/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_TRNS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_BFI_DR_ID: `${delivery_receipt}`,
        U_FSQR_rmaf_id: `${rmaf_id}`,
        U_FSQR_bir_id: `${bir_id}`,
        U_FSQR_status_id: `${status_id}`,
        U_FSQR_trckpltno: `${plate_number}`,
        U_FSQR_trnstyp_id: `${transaction_type_id}`,
        U_FSQR_driver: `${driver_name}`,
        U_FSQR_driverID: `${driver_id}`,
        U_FSQR_suppl_code: `${supplier_code}`,
        U_FSQR_no_of_bags: `${no_of_bags}`,
        U_FSQR_description: `${description}`,
        U_FSQR_g_on_dty: `${guard_on_duty}`,
        U_FSQR_time_end: `${time_end}`,
        U_FSQR_qc_cntrl_no: `${qc_control_number}`,
        U_FSQR_queue_no: `${queue_number}`,
        U_FSQR_spplr_id: `${supplier_id}`,
        U_FSQR_spplr_name: `${supplier_name}`,
        U_FSQR_spplr_add: `${supplier_address}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateTransaction(request_info) {
  //   const {
  //     id,
  //     delivery_receipt_id,
  //     rmaf_id,
  //     bir_id,
  //     status_id,
  //     transaction_type_id,
  //     supplier_code,
  //     no_of_bags,
  //     description,
  //     guard_on_duty,
  //     time_end,
  //     qc_control_number,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction SET
  //     delivery_receipt_id = $1,
  //     rmaf_id = $2,
  //     bir_id = $3,
  //     status_id = $4,
  //     transaction_type_id = $5,
  //     supplier_code = $6,
  //     no_of_bags = $7,
  //     description = $8,
  //     guard_on_duty = $9,
  //     time_end = $10,
  //     qc_control_number = $11,
  //     updated_by = $12,
  //     updated_at = $13
  //     WHERE id = $14
  //     RETURNING *`,
  //     [
  //       delivery_receipt_id,
  //       rmaf_id,
  //       bir_id,
  //       status_id,
  //       transaction_type_id,
  //       supplier_code,
  //       no_of_bags,
  //       description,
  //       guard_on_duty,
  //       time_end,
  //       qc_control_number,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateTransaction(request_info, SessionId) {
    const {
      id,
      delivery_receipt_id,
      rmaf_id,
      bir_id,
      status_id,
      transaction_type_id,
      supplier_code,
      no_of_bags,
      description,
      guard_on_duty,
      time_end,
      qc_control_number,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_TRNS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_BFI_DR_ID: `${delivery_receipt_id}`,
        U_FSQR_rmaf_id: `${rmaf_id}`,
        U_FSQR_bir_id: `${bir_id}`,
        U_FSQR_status_id: `${status_id}`,
        U_FSQR_trnstyp_id: `${transaction_type_id}`,
        U_FSQR_suppl_code: `${supplier_code}`,
        U_FSQR_no_of_bags: `${no_of_bags}`,
        U_FSQR_description: `${description}`,
        U_FSQR_g_on_dty: `${guard_on_duty}`,
        U_FSQR_time_end: `${time_end}`,
        U_FSQR_qc_cntrl_no: `${qc_control_number}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function validateTally(request_info) {
  //   const { id, status_id, employee_id } = request_info;

  //   const date_today = moment()
  //   .tz("Asia/Manila")
  //   .format("YYYY-MM-DD HH:mm:ss");

  //   await pool.query(`UPDATE deduction_receipt SET checked_by = $1, updated_at = $2,
  //   updated_by = $3 WHERE transaction_id = $4`,
  //    [employee_id, date_today, employee_id, id])
  //   const quesry = await pool.query(`UPDATE transaction SET status_id = $1, updated_by = $2,
  //   updated_at = $3 WHERE id = $4 RETURNING *`,
  //    [status_id, employee_id, date_today, id]);

  // return pool.query(
  //   `UPDATE deduction_receipt SET checked_by = $1, checked_signature = $2,
  //  updated_at = $3, updated_by = $4 WHERE transaction_id = $5`,
  //   [employee_id, checked_signature, date_today, employee_id, id]
  // );

  //    return quesry;
  // }

  async function validateTally(request_info, SessionId) {
    const { id, employee_id } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_checked_by: `${employee_id}`,
        U_FSQR_updated_by: `${employee_id}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function correctTransaction(request_info) {
  //   const { id, truck_info_id, driver_id, supplier_address, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction SET
  //     truck_info_id = $1,
  //     driver_id = $2,
  //     supplier_address = $6,
  //     updated_by = $3,
  //     updated_at = $4
  //     WHERE id = $5
  //     RETURNING *`,
  //     [truck_info_id, driver_id, updated_by, date_today, id, supplier_address]
  //   );
  // }

  async function correctTransaction(request_info, SessionId) {
    const { id, driver_id, supplier_address, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_TRNS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_driverID: `${driver_id}`,
        U_FSQR_spplr_name: `${supplier_address}`,
        U_FSQR_checked_by: `${employee_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateTransactionStatus(request_info) {
  //   const { id, status_id, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction SET status_id = $1,
  //    updated_by = $2, updated_at = $3
  //    WHERE id = $4 RETURNING *`,
  //     [status_id, updated_by, date_today, id]
  //   );
  // }

  async function updateTransactionStatus(request_info, SessionId) {
    const { id, status_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_TRNS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_status_id: `${status_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateForReconTransaction(request_info) {
  //   const { id, rmaf_id, updated_by, failed_rmaf } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction SET
  //     rmaf_id = $1,
  //     failed_rmaf = $2,
  //     updated_by = $3,
  //     updated_at = $4
  //     WHERE id = $5
  //     RETURNING *`,
  //     [rmaf_id, failed_rmaf, updated_by, date_today, id]
  //   );
  // }

  async function updateForReconTransaction(request_info, SessionId) {
    const { id, rmaf_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_TRNS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_rmaf_id: `${rmaf_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listRMAF() {
  //   return pool.query("SELECT * FROM rmaf");
  // }

  async function listRMAF(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RMAF`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findRMAFById(id) {
  //   return pool.query("SELECT * FROM rmaf WHERE id = $1", [id]);
  // }

  async function findRMAFById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RMAF('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addRMAF(request_info) {
  //   const {
  //     physical_analysis_id,
  //     chemical_analysis_id,
  //     date,
  //     material_rejection_report_id,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO rmaf(physical_analysis_id,
  //     chemical_analysis_id, date, material_rejection_report_id,
  //     created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6)
  //     RETURNING *`,
  //     [
  //       physical_analysis_id,
  //       chemical_analysis_id,
  //       date,
  //       material_rejection_report_id,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addRMAF(request_info, SessionId) {
    const {
      physical_analysis_id,
      chemical_analysis_id,
      date,
      material_rejection_report_id,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_RMAF/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_RMAF`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_phy_id: `${physical_analysis_id}`,
        U_FSQR_chem_id: `${chemical_analysis_id}`,
        U_FSQR_date: `${date}`,
        U_FSQR_mrr_id: `${material_rejection_report_id}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateRMAF(request_info) {
  //   const {
  //     id,
  //     physical_analysis_id,
  //     chemical_analysis_id,
  //     date,
  //     material_rejection_report_id,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE rmaf SET physical_analysis_id = $1,
  //   chemical_analysis_id = $2, date = $3
  //   material_rejection_report_id = $4, updated_by = $5, updated_at = $6
  //   WHERE id = $7 RETURNING *`,
  //     [
  //       physical_analysis_id,
  //       chemical_analysis_id,
  //       date,
  //       material_rejection_report_id,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateRMAF(request_info, SessionId) {
    const {
      id,
      physical_analysis_id,
      chemical_analysis_id,
      date,
      material_rejection_report_id,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_RMAF('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_phy_id: `${physical_analysis_id}`,
        U_FSQR_chem_id: `${chemical_analysis_id}`,
        U_FSQR_date: `${date}`,
        U_FSQR_mrr_id: `${material_rejection_report_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function countTransaction() {
  //   const month_today = moment()
  //     .tz("Asia/Manila")
  //   aDGJMKZXQ    1QQ222qA33AZ  .format("YYYY-MM");
  //   return pool.query(`SELECT count(*) FROM transaction WHERE (SELECT CAST(created_at AS varchar))
  //     like '${month_today}%'`);
  // }

  async function countTransaction(SessionId) {
    const startOfMonth = moment()
      .tz("Asia/Manila")
      .startOf("month")
      .format("YYYY-MM-DD");
    const endOfMonth = moment()
      .tz("Asia/Manila")
      .endOf("month")
      .format("YYYY-MM-DD");
    // const DateToday = moment()
    //   .tz("Asia/Manila")
    //   .format("YYYY-MM");

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRNS/$count?$filter=U_FSQR_created_date ge '${startOfMonth}' and U_FSQR_created_date le '${endOfMonth}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function countCue() {
  //   const day_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD");
  //   return pool.query(`SELECT count(*) FROM transaction WHERE (SELECT CAST(created_at AS varchar))
  //     like '${day_today}%'`);
  // }

  async function countCue(SessionId) {
    const day_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_TRNS/$count?$filter=U_FSQR_created_date eq '${day_today}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function countRMAF() {
  //   const month_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM");
  //   return pool.query(`SELECT count(*) FROM transaction WHERE (SELECT CAST(created_at AS varchar))
  //     like '${month_today}%'`);
  // }

  async function countRMAF(SessionId) {
    const startOfMonth = moment()
      .tz("Asia/Manila")
      .startOf("month")
      .format("YYYY-MM-DD");
    const endOfMonth = moment()
      .tz("Asia/Manila")
      .endOf("month")
      .format("YYYY-MM-DD");
    // const DateToday = moment()
    //   .tz("Asia/Manila")
    //   .format("YYYY-MM");

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RMAF/$count?$filter=U_FSQR_created_date ge '${startOfMonth}' and U_FSQR_created_date le '${endOfMonth}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listMRR() {
  //   return pool.query("SELECT * FROM material_rejection_report");
  // }

  async function listMRR(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MRR`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findMRRById(id) {
  //   return pool.query("SELECT * FROM material_rejection_report WHERE id = $1", [
  //     id
  //   ]);
  // }

  async function findMRRById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MRR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addMRR(request_info) {
  //   const {
  //     findings,
  //     specification,
  //     issued_by,
  //     noted_by,
  //     approved_by,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO material_rejection_report
  //   (findings, specification, issued_by, noted_by, approved_by,
  //     created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7)
  //     RETURNING *`,
  //     [
  //       findings,
  //       specification,
  //       issued_by,
  //       noted_by,
  //       approved_by,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addMRR(request_info, SessionId) {
    const {
      findings,
      specification,
      issued_by,
      noted_by,
      approved_by,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_MRR/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_MRR`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_findings: `${findings}`,
        U_FSQR_specs: `${specification}`,
        U_FSQR_issued_by: `${issued_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_appr_by: `${approved_by}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateMRR(request_info) {
  //   const {
  //     id,
  //     findings,
  //     specification,
  //     issued_by,
  //     noted_by,
  //     approved_by,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE material_rejection_report SET findings = $1,
  //   specification = $2, issued_by = $3, noted_by = $4, approved_by = $5,
  //   updated_by = $6, updated_at = $7 WHERE id = $8 RETURNING *`,
  //     [
  //       findings,
  //       specification,
  //       issued_by,
  //       noted_by,
  //       approved_by,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateMRR(request_info, SessionId) {
    const {
      id,
      findings,
      specification,
      issued_by,
      noted_by,
      approved_by,
      updated_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_MRR/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_MRR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_findings: `${findings}`,
        U_FSQR_specs: `${specification}`,
        U_FSQR_issued_by: `${issued_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_appr_by: `${approved_by}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listChemicalAnalysis() {
  //   return pool.query("SELECT * FROM chemical_analysis");
  // }

  async function listChemicalAnalysis(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_CHEM_ANA`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findChemicalAnalysisById(id) {
  //   return pool.query("SELECT * FROM chemical_analysis WHERE id = $1", [id]);
  // }

  async function findChemicalAnalysisById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_CHEM_ANA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addChemicalAnalysis(request_info) {
  //   const {
  //     disposition_id,
  //     reconsideration_id,
  //     crude_protein,
  //     calcium,
  //     ffa_lauric_acid,
  //     brix,
  //     carbonate_test,
  //     remarks,
  //     evaluated_by,
  //     certified_by,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO chemical_analysis (disposition_id,
  //       reconsideration_id, crude_protein, calcium, ffa_lauric_acid, brix, carbonate_test,
  //       remarks, evaluated_by, certified_by, created_by, created_at) VALUES
  //       ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING *`,
  //     [
  //       disposition_id,
  //       reconsideration_id,
  //       crude_protein,
  //       calcium,
  //       ffa_lauric_acid,
  //       brix,
  //       carbonate_test,
  //       remarks,
  //       evaluated_by,
  //       certified_by,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addChemicalAnalysis(request_info, SessionId) {
    const {
      disposition_id,
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      remarks,
      evaluated_by,
      certified_by,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_CHEM_ANA/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_CHEM_ANA`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_dispo_id: `${disposition_id}`,
        U_FSQR_recon_id: `${reconsideration_id}`,
        U_FSQR_crude: `${crude_protein}`,
        U_FSQR_calcium: `${calcium}`,
        U_FSQR_ffa: `${ffa_lauric_acid}`,
        U_FSQR_brix: `${brix}`,
        U_FSQR_carb_test: `${carbonate_test}`,
        U_FSQR_remarks: `${remarks}`,
        U_FSQR_evaluated_by: `${evaluated_by}`,
        U_FSQR_certified_by: `${certified_by}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateChemicalAnalysis(request_info) {
  //   const {
  //     id,
  //     disposition_id,
  //     reconsideration_id,
  //     crude_protein,
  //     calcium,
  //     ffa_lauric_acid,
  //     brix,
  //     carbonate_test,
  //     remarks,
  //     evaluated_by,
  //     certified_by,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE chemical_analysis SET disposition_id = $1,
  //     reconsideration_id = $2, crude_protein = $3, calcium = $4, ffa_lauric_acid = $5,
  //     brix = $6, carbonate_test = $7, remarks = $8, evaluated_by = $9,
  //     certified_by = $10, updated_by = $11, updated_at = $12 WHERE id = $13 RETURNING *`,
  //     [
  //       disposition_id,
  //       reconsideration_id,
  //       crude_protein,
  //       calcium,
  //       ffa_lauric_acid,
  //       brix,
  //       carbonate_test,
  //       remarks,
  //       evaluated_by,
  //       certified_by,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateChemicalAnalysis(request_info, SessionId) {
    const {
      id,
      disposition_id,
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      remarks,
      evaluated_by,
      certified_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_CHEM_ANA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_dispo_id: `${disposition_id}`,
        U_FSQR_recon_id: `${reconsideration_id}`,
        U_FSQR_crude: `${crude_protein}`,
        U_FSQR_calcium: `${calcium}`,
        U_FSQR_ffa: `${ffa_lauric_acid}`,
        U_FSQR_brix: `${brix}`,
        U_FSQR_carb_test: `${carbonate_test}`,
        U_FSQR_remarks: `${remarks}`,
        U_FSQR_evaluated_by: `${evaluated_by}`,
        U_FSQR_certified_by: `${certified_by}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateCARID(request_info) {
  //   const { id, reconsideration_id, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE chemical_analysis SET reconsideration_id = $1,
  //      updated_by = $2, updated_at = $3 WHERE id = $4 RETURNING *`,
  //     [reconsideration_id, updated_by, date_today, id]
  //   );
  // }

  async function updateCARID(request_info, SessionId) {
    const { id, reconsideration_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_CHEM_ANA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_recon_id: `${reconsideration_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listPhysicalAnalysis() {
  //   return pool.query("SELECT * FROM physical_analysis");
  // }

  async function listPhysicalAnalysis(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_PHYANA`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findPhysicalAnalysisById(id) {
  //   return pool.query("SELECT * FROM physical_analysis WHERE id = $1", [id]);
  // }

  async function findPhysicalAnalysisById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_PHYANA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addPhysicalAnalysis(request_info) {
  //   const {
  //     price_reduction_id,
  //     reconsideration_id,
  //     disposition_id,
  //     type,
  //     mc_result,
  //     impurities,
  //     foreign_material,
  //     remarks,
  //     evaluated_by,
  //     certified_by,
  //     noted_by,
  //     created_by,
  //     moisture_tally_sheet_id
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO physical_analysis (price_reduction_id,
  //     reconsideration_id, disposition_id, type,
  //     mc_result, impurities, foreign_material, remarks, evaluated_by, certified_by,
  //     noted_by, created_by, created_at, moisture_tally_sheet_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9,
  //       $10, $11, $12, $13, $14) RETURNING *`,
  //     [
  //       price_reduction_id,
  //       reconsideration_id,
  //       disposition_id,
  //       type,
  //       mc_result,
  //       impurities,
  //       foreign_material,
  //       remarks,
  //       evaluated_by,
  //       certified_by,
  //       noted_by,
  //       created_by,
  //       date_today,
  //       moisture_tally_sheet_id
  //     ]
  //   );
  // }

  async function addPhysicalAnalysis(request_info, SessionId) {
    const {
      price_reduction_id,
      reconsideration_id,
      disposition_id,
      type,
      mc_result,
      impurities,
      foreign_material,
      remarks,
      evaluated_by,
      certified_by,
      noted_by,
      created_by,
      moisture_tally_sheet_id
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_PHYANA/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_PHYANA`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_pr_id: `${price_reduction_id}`,
        U_FSQR_recon_id: `${reconsideration_id}`,
        U_FSQR_dispo_id: `${disposition_id}`,
        U_FSQR_type: `${type}`,
        U_FSQR_mc_result: `${mc_result}`,
        U_FSQR_impurities: `${impurities}`,
        U_FSQR_frgn_mat: `${foreign_material}`,
        U_FSQR_remarks: `${remarks}`,
        U_FSQR_evaluated_by: `${evaluated_by}`,
        U_FSQR_certified_by: `${certified_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_mts_id: `${moisture_tally_sheet_id}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updatePhysicalAnalysis(request_info) {
  //   const {
  //     id,
  //     price_reduction_id,
  //     reconsideration_id,
  //     disposition_id,
  //     type,
  //     mc_result,
  //     impurities,
  //     foreign_material,
  //     evaluated_by,
  //     certified_by,
  //     noted_by,
  //     updated_by,
  //     moisture_tally_sheet_id
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE physical_analysis set price_reduction_id = $1,
  //     reconsideration_id = $2,
  //     disposition_id = $3, type = $4, mc_result = $5, impurities = $6,
  //     foreign_material = $7, evaluated_by = $8, certified_by = $9,
  //     noted_by = $10, updated_by = $11, updated_at = $12 , moisture_tally_sheet_id = $14
  //     WHERE id = $13 RETURNING *`,
  //     [
  //       price_reduction_id,
  //       reconsideration_id,
  //       disposition_id,
  //       type,
  //       mc_result,
  //       impurities,
  //       foreign_material,
  //       evaluated_by,
  //       certified_by,
  //       noted_by,
  //       updated_by,
  //       date_today,
  //       id,
  //       moisture_tally_sheet_id
  //     ]
  //   );
  // }

  async function updatePhysicalAnalysis(request_info, SessionId) {
    const {
      id,
      price_reduction_id,
      reconsideration_id,
      disposition_id,
      type,
      mc_result,
      impurities,
      foreign_material,
      evaluated_by,
      certified_by,
      noted_by,
      updated_by,
      moisture_tally_sheet_id
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_PHYANA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_pr_id: `${price_reduction_id}`,
        U_FSQR_recon_id: `${reconsideration_id}`,
        U_FSQR_dispo_id: `${disposition_id}`,
        U_FSQR_type: `${type}`,
        U_FSQR_mc_result: `${mc_result}`,
        U_FSQR_impurities: `${impurities}`,
        U_FSQR_frgn_mat: `${foreign_material}`,
        U_FSQR_remarks: `${remarks}`,
        U_FSQR_evaluated_by: `${evaluated_by}`,
        U_FSQR_certified_by: `${certified_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_mts_id: `${moisture_tally_sheet_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updatePARID(request_info) {
  //   const { id, reconsideration_id, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE physical_analysis set
  //     reconsideration_id = $1, updated_by = $2, updated_at = $3
  //     WHERE id = $4 RETURNING *`,
  //     [reconsideration_id, updated_by, date_today, id]
  //   );
  // }

  async function updatePARID(request_info, SessionId) {
    const { id, reconsideration_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_PHYANA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_recon_id: `${reconsideration_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listDisposition() {
  //   return pool.query("SELECT * FROM disposition ORDER BY id");
  // }

  async function listDisposition(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DISPO`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDispositionById(id) {
  //   return pool.query("SELECT * FROM disposition WHERE id = $1", [id]);
  // }

  async function findDispositionById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DISPO('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDispositionByName(description) {
  //   return pool.query("SELECT * FROM disposition WHERE description = $1", [
  //     description
  //   ]);
  // }

  async function findDispositionByName(description, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DISPO?$filter=U_FSQR_description eq '${description}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addDisposition(request_info) {
  //   const { description, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO disposition (description, created_by,
  //      created_at) VALUES ($1, $2, $3) RETURNING *`,
  //     [description, created_by, date_today]
  //   );
  // }

  async function addDisposition(request_info, SessionId) {
    const { description, created_by } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_DISPO/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_DISPO`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${description}`,
        U_FSQR_description: `${description}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateDisposition(request_info) {
  //   const { id, description, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE disposition SET description = $1, updated_by = $2,
  //   updated_at = $3 WHERE id = $4 RETURNING *`,
  //     [description, updated_by, date_today, id]
  //   );
  // }

  async function updateDisposition(request_info, SessionId) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_DISPO('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_description: `${description}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listReconsideration() {
  //   return pool.query("SELECT * FROM reconsideration");
  // }

  async function listReconsideration(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RECON`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }
  // async function findReconsiderationById(id) {
  //   return pool.query("SELECT * FROM reconsideration WHERE id = $1", [id]);
  // }

  async function findReconsiderationById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RECON('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addReconsideration(request_info) {
  //   const {
  //     for_consideration,
  //     reason,
  //     reconsidered_by,
  //     reconsidered_approved_by,
  //     noted_by,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO reconsideration (for_consideration,
  //     reason, reconsidered_by, reconsidered_approved_by, noted_by,
  //      created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7)
  //      RETURNING *`,
  //     [
  //       for_consideration,
  //       reason,
  //       reconsidered_by,
  //       reconsidered_approved_by,
  //       noted_by,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addReconsideration(request_info, SessionId) {
    const {
      for_consideration,
      reason,
      reconsidered_by,
      reconsidered_approved_by,
      noted_by,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_RECON/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_RECON`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_for_consi: `${for_consideration}`,
        U_FSQR_reason: `${reason}`,
        U_FSQR_recon_by: `${reconsidered_by}`,
        U_FSQR_recon_appr: `${reconsidered_approved_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateReconsideration(request_info) {
  //   const {
  //     id,
  //     for_consideration,
  //     reason,
  //     reconsidered_by,
  //     reconsidered_approved_by,
  //     noted_by,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE reconsideration SET for_consideration = $1,
  //   reason = $2, reconsidered_by = $3, reconsidered_approved_by = $4,
  //   noted_by = $5, updated_by = $6, updated_at = $7 WHERE id = $8
  //   RETURNING *`,
  //     [
  //       for_consideration,
  //       reason,
  //       reconsidered_by,
  //       reconsidered_approved_by,
  //       noted_by,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateReconsideration(request_info, SessionId) {
    const {
      id,
      for_consideration,
      reason,
      reconsidered_by,
      reconsidered_approved_by,
      noted_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_RECON('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_for_consi: `${for_consideration}`,
        U_FSQR_reason: `${reason}`,
        U_FSQR_recon_by: `${reconsidered_by}`,
        U_FSQR_recon_appr: `${reconsidered_approved_by}`,
        U_FSQR_noted_by: `${noted_by}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listMTS() {
  //   return pool.query("SELECT * FROM moisture_tally_sheet ORDER BY id");
  // }

  async function listMTS(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MTS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findMTSById(id) {
  //   return pool.query("SELECT * FROM moisture_tally_sheet WHERE id = $1", [id]);
  // }

  async function findMTSById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MTS('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addMTS(request_info) {
  //   const {
  //     moisture_trial_1,
  //     moisture_trial_2,
  //     moisture_trial_3,
  //     transaction_id,
  //     average,
  //     analysed_by,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO moisture_tally_sheet (moisture_trial_1,
  //     moisture_trial_2, moisture_trial_3, average, analysed_by, created_by, created_at)
  //      VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
  //     [
  //       moisture_trial_1,
  //       moisture_trial_2,
  //       moisture_trial_3,
  //       average,
  //       analysed_by,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addMTS(request_info, SessionId) {
    const {
      moisture_trial_1,
      moisture_trial_2,
      moisture_trial_3,
      average,
      analysed_by,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_MTS/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_MTS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_trial1: `${moisture_trial_1}`,
        U_FSQR_trial2: `${moisture_trial_2}`,
        U_FSQR_trial3: `${moisture_trial_3}`,
        U_FSQR_average: `${average}`,
        U_FSQR_analysed_by: `${analysed_by}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateMTS(request_info) {
  //   const {
  //     id,
  //     moisture_trial_1,
  //     moisture_trial_2,
  //     moisture_trial_3,
  //     average,
  //     analysed_by,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE moisture_tally_sheet SET moisture_trial_1 = $1,
  //     moisture_trial_2 = $2, moisture_trial_3 = $3, average = $4,
  //     analysed_by = $5, updated_by = $6, updated_at = $7 WHERE id = $8
  //     RETURNING *`,
  //     [
  //       moisture_trial_1,
  //       moisture_trial_2,
  //       moisture_trial_3,
  //       average,
  //       analysed_by,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateMTS(request_info, SessionId) {
    const {
      id,
      moisture_trial_1,
      moisture_trial_2,
      moisture_trial_3,
      average,
      analysed_by,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_MTS('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_trial1: `${moisture_trial_1}`,
        U_FSQR_trial2: `${moisture_trial_2}`,
        U_FSQR_trial3: `${moisture_trial_3}`,
        U_FSQR_average: `${average}`,
        U_FSQR_analysed_by: `${analysed_by}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listPriceReduction() {
  //   return pool.query("SELECT * FROM price_reduction ORDER BY id");
  // }

  async function listPriceReduction(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_PRICE_RED`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findPriceReductionById(id) {
  //   return pool.query("SELECT * FROM price_reduction WHERE id = $1", [id]);
  // }

  async function findPriceReductionById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_PRICE_RED('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addPriceReduction(request_info) {
  //   const { remarks, certified_by, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO price_reduction (remarks, certified_by, created_by, created_at)
  //   VALUES ($1, $2, $3, $4) RETURNING *`,
  //     [remarks, certified_by, created_by, date_today]
  //   );
  // }

  async function addPriceReduction(request_info, SessionId) {
    const { remarks, certified_by, created_by } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_PRICE_RED/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_PRICE_RED`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_remarks: `${remarks}`,
        U_FSQR_cert_by: `${certified_by}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updatePriceReduction(request_info) {
  //   const { id, remarks, certified_by, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE price_reduction SET remarks = $1, certified_by = $2,
  //     updated_by = $3, updated_at = $4 WHERE id = $5 RETURNING *`,
  //     [remarks, certified_by, updated_by, date_today, id]
  //   );
  // }

  async function updatePriceReduction(request_info, SessionId) {
    const { id, remarks, certified_by, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_PRICE_RED('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_remarks: `${remarks}`,
        U_FSQR_cert_by: `${certified_by}`,
        U_FSQR_updatedd_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listDeductionCriteria() {
  //   return pool.query("SELECT * FROM deduction_criteria");
  // }

  async function listDeductionCriteria(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDeductionCriteriaById(id) {
  //   return pool.query("SELECT * FROM deduction_criteria WHERE id = $1", [id]);
  // }

  async function findDeductionCriteriaById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEDUC_RT('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDeductionCriteriaByDRId(id) {
  //   return pool.query(
  //     "SELECT * FROM deduction_criteria WHERE deduction_receipt_id = $1",
  //     [id]
  //   );
  // }

  async function findDeductionCriteriaByDRId(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DED_CRIT('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function totalDeductionPerCriteria(transaction_id, criteria_id){
  //   return pool.query(`SELECT SUM(quantity) as quantity, deduction,
  //   SUM(total) as total FROM deduction_criteria WHERE deduction_receipt_id
  //   in (SELECT id FROM deduction_receipt WHERE transaction_id = $1)
  //   and criteria_id = $2 GROUP BY deduction`,
  //   [transaction_id, criteria_id])
  // }

  async function totalDeductionPerCriteria(
    transaction_id,
    criteria_id,
    SessionId
  ) {
    //   return pool.query(`SELECT SUM(quantity) as quantity, deduction,
    //   SUM(total) as total FROM deduction_criteria WHERE deduction_receipt_id
    //   in (SELECT id FROM deduction_receipt WHERE transaction_id = $1)
    //   and criteria_id = $2 GROUP BY deduction`,
    //   [transaction_id, criteria_id])
  }

  // async function addDeductionCriteria(request_info) {
  //   const {
  //     criteria_id,
  //     deduction_receipt_id,
  //     quantity,
  //     deduction,
  //     total,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO deduction_criteria (criteria_id, deduction_receipt_id,
  //     quantity, deduction, total, created_by, created_at) VALUES
  //     ($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
  //     [
  //       criteria_id,
  //       deduction_receipt_id,
  //       quantity,
  //       deduction,
  //       total,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addDeductionCriteria(request_info, SessionId) {
    const {
      criteria_id,
      deduction_receipt_id,
      quantity,
      deduction,
      total,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_DED_CRIT/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_DED_CRIT`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_crit_ID: `${criteria_id}`,
        U_FSQR_dd_rct_ID: `${deduction_receipt_id}`,
        U_FSQR_quantity: `${quantity}`,
        U_FSQR_deduction: `${deduction}`,
        U_FSQR_total: `${total}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateDeductionCriteria(request_info) {
  //   const {
  //     id,
  //     deduction_receipt_id,
  //     criteria_id,
  //     quantity,
  //     deduction,
  //     total,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE deduction_criteria SET criteria_id = $1,
  //   quantity = $2, deduction = $3, total = $4, updated_by = $5, updated_at = $6,
  //   deduction_receipt_id = $8 WHERE id = $7 RETURNING *`,
  //     [
  //       criteria_id,
  //       quantity,
  //       deduction,
  //       total,
  //       updated_by,
  //       date_today,
  //       id,
  //       deduction_receipt_id
  //     ]
  //   );
  // }

  async function updateDeductionCriteria(request_info, SessionId) {
    const {
      id,
      deduction_receipt_id,
      criteria_id,
      quantity,
      deduction,
      total,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_DED_CRIT('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_crit_ID: `${criteria_id}`,
        U_FSQR_dd_rct_ID: `${deduction_receipt_id}`,
        U_FSQR_quantity: `${quantity}`,
        U_FSQR_deduction: `${deduction}`,
        U_FSQR_total: `${total}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listCriteria() {
  //   return pool.query("SELECT * FROM criteria ORDER BY description");
  // }

  async function listCriteria(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_CRITERIA`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listCriteriaOnDC(transaction_id) {
  //   return pool.query(`SELECT deduction_criteria.criteria_id, criteria.description
  //    FROM  deduction_criteria, criteria
  //    WHERE deduction_criteria.deduction_receipt_id
  //   in (SELECT id FROM deduction_receipt WHERE transaction_id = $1) AND
  //   deduction_criteria.criteria_id = criteria.id
  //   GROUP BY deduction_criteria.criteria_id, criteria.description`,
  //    [transaction_id]);
  // }

  // async function listCriteriaByBE(request_info) {
  //   const { business_entity_id } = request_info;
  //   return pool.query(
  //     `SELECT * FROM criteria
  //    WHERE business_entity = $1
  //    ORDER BY description`,
  //     [business_entity_id]
  //   );
  // }

  async function listCriteriaByBE(request_info, SessionId) {
    const { business_entity_id } = request_info;

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_CRITERIA?$filter=U_FSQR_bus_ent eq '${business_entity_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findCriteriaById(id) {
  //   return pool.query("SELECT * FROM criteria WHERE id = $1", [id]);
  // }

  async function findCriteriaById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_CRITERIA?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findCriteriaByName(description) {
  //   return pool.query("SELECT * FROM criteria WHERE description = $1", [
  //     description
  //   ]);
  // }

  async function findCriteriaByName(description, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_CRITERIA?$filter=U_FSQR_description eq '${description}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addCriteria(request_info) {
  //   const { description, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO criteria (description, created_by, created_at)
  //   VALUES ($1, $2, $3) RETURNING *`,
  //     [description, created_by, date_today]
  //   );
  // }

  async function addCriteria(request_info, SessionId) {
    const { description, created_by } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_CRITERIA/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_CRITERIA`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_description: `${description}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateCriteria(request_info) {
  //   const { id, description, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE criteria SET description = $1,
  //   updated_by = $2, updated_at = $3 WHERE id = $4 RETURNING *`,
  //     [description, updated_by, date_today, id]
  //   );
  // }

  async function updateCriteria(request_info, SessionId) {
    const { id, description, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_CRITERIA('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_description: `${description}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listBIR() {
  //   return pool.query("SELECT * FROM bir");
  // }

  async function listBIR(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_BIR`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findBIRById(id) {
  //   return pool.query("SELECT * FROM bir WHERE id = $1", [id]);
  // }

  async function findBIRById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_BIR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findBIRByItemId(item_id) {
  //   return pool.query("SELECT * FROM bir WHERE item_id = $1", [item_id]);
  // }

  async function findBIRByItemId(item_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_BIR?$filter=U_FSQR_item_id eq '${item_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findBIRByBEId(business_entity_id) {
  //   return pool.query("SELECT * FROM bir WHERE business_entity_id = $1", [
  //     business_entity_id
  //   ]);
  // }

  async function findBIRByBEId(business_entity_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_BIR?$filter=U_FSQR_bus_ent_id eq '${business_entity_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findBIRByRMId(raw_material_id) {
  //   return pool.query("SELECT * FROM bir WHERE raw_material_id = $1", [
  //     raw_material_id
  //   ]);
  // }

  async function findBIRByRMId(raw_material_id) {
    //   return pool.query("SELECT * FROM bir WHERE raw_material_id = $1", [
    //     raw_material_id
    //   ]);
  }

  // async function addBIR(request_info) {
  //   const {
  //     item_id,
  //     business_entity_id,
  //     raw_material_id,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO bir (item_id, business_entity_id, raw_material_id, created_by, created_at)
  //   VALUES ($1, $2, $3, $4, $5) RETURNING *`,
  //     [item_id, business_entity_id, raw_material_id, created_by, date_today]
  //   );
  // }

  async function addBIR(request_info, SessionId) {
    const {
      item_id,
      business_entity_id,
      raw_material_id,
      created_by
    } = request_info;

    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_BIR/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_BIR`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_item_id: `${item_id}`,
        U_FSQR_bus_ent_id: `${business_entity_id}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateBIR(request_info) {
  //   const {
  //     id,
  //     item_id,
  //     business_entity_id,
  //     raw_material_id,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE bir set item_id = $1, business_entity_id = $2,
  //    raw_material_id = $3, updated_by = $4, updated_at = $5
  //    WHERE id = $6 RETURNING *`,
  //     [item_id, business_entity_id, raw_material_id, updated_by, date_today, id]
  //   );
  // }

  async function updateBIR(request_info, SessionId) {
    const {
      id,
      item_id,
      business_entity_id,
      raw_material_id,
      updated_by
    } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_BIR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_item_id: `${item_id}`,
        U_FSQR_bus_ent_id: `${business_entity_id}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function listRawMaterials() {
  //   return pool.query("SELECT * FROM raw_materials ORDER BY description");
  // }

  // async function findRawMaterialsById(id) {
  //   return pool.query("SELECT * FROM raw_materials WHERE id = $1", [id]);
  // }

  // async function findRawMaterialsByName(name) {
  //   return pool.query("SELECT * FROM raw_materials WHERE description = $1", [
  //     name
  //   ]);
  // }

  // async function addRawMaterials(request_info) {
  //   const { description, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO raw_materials (description, created_by, created_at)
  //   VALUES ($1, $2, $3) RETURNING *`,
  //     [description, created_by, date_today]
  //   );
  // }

  // async function updateRawMaterials(request_info) {
  //   const { id, description, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE raw_materials set description = $1, updated_by = $2, updated_at = $3
  //   WHERE id = $4 RETURNING *`,
  //     [description, updated_by, date_today, id]
  //   );
  // }

  // async function listItem() {
  //   return pool.query("SELECT * FROM items ORDER BY description");
  // }

  // async function findItemById(id) {
  //   return pool.query("SELECT * FROM items WHERE id = $1", [id]);
  // }

  // async function findItemByName(name) {
  //   return pool.query("SELECT * FROM items WHERE description = $1", [name]);
  // }

  // async function addItem(request_info) {
  //   const { description, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO items (description, created_by, created_at)
  //   VALUES ($1, $2, $3) RETURNING *`,
  //     [description, created_by, date_today]
  //   );
  // }

  // async function updateItem(request_info) {
  //   const { id, description, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE items set description = $1, updated_by = $2, updated_at = $3
  //   WHERE id = $4 RETURNING *`,
  //     [description, updated_by, date_today, id]
  //   );
  // }

  // async function listBusinessEntity() {
  //   return pool.query("SELECT * FROM business_entity ORDER BY description");
  // }

  // async function findBusinessEntityById(id) {
  //   return pool.query("SELECT * FROM business_entity WHERE id = $1", [id]);
  // }

  // async function findBusinessEntityByName(name) {
  //   return pool.query("SELECT * FROM business_entity WHERE description = $1", [
  //     name
  //   ]);
  // }

  // async function addBusinessEntity(request_info) {
  //   const { description, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO business_entity (description, created_by, created_at)
  //   VALUES ($1, $2, $3) RETURNING *`,
  //     [description, created_by, date_today]
  //   );
  // }

  // async function updateBusinessEntity(request_info) {
  //   const { id, description, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE business_entity set description = $1, updated_by = $2, updated_at = $3
  //   WHERE id = $4 RETURNING *`,
  //     [description, updated_by, date_today, id]
  //   );
  // }

  // async function listRejectionReceipt() {
  //   return pool.query("SELECT * FROM rejection_receipt ORDER BY date");
  // }

  async function listRejectionReceipt(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RR`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findRejectionReceiptById(id) {
  //   return pool.query("SELECT * FROM rejection_receipt WHERE id = $1", [id]);
  // }

  async function findRejectionReceiptById(id) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
    }

  // async function findRejectionReceiptByTranId(transaction_id) {
  //   return pool.query("SELECT * FROM rejection_receipt WHERE transaction_id = $1",
  //    [transaction_id]);
  // }

  async function findRejectionReceiptByTranId(transaction_id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_RR?$filter=FSQR_trans_id eq '${transaction_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
    }

  // async function addRejectionReceipt(request_info) {
  //   const {
  //     transaction_id,
  //     no_of_bags_rejected,
  //     reason_of_rejection,
  //     date,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO rejection_receipt (transaction_id,
  //       no_of_bags_rejected,
  //       reason_of_rejection,
  //       date,
  //       created_by,
  //       created_at)
  //       VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
  //     [
  //       transaction_id,
  //       no_of_bags_rejected,
  //       reason_of_rejection,
  //       date,
  //       created_by,
  //       date_today
  //     ]
  //   );
  // }

  async function addRejectionReceipt(request_info, SessionId) {
      const {
        transaction_id,
        no_of_bags_rejected,
        reason_of_rejection,
        date,
        created_by
      } = request_info;
  
    const codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_RR/$count`
    );
    const codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_RR`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_trans_id: `${transaction_id}`,
        U_FSQR_bags_rej: `${no_of_bags_rejected}`,
        U_FSQR_reason: `${reason_of_rejection}`,
        U_FSQR_date: `${date}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
    }

  // async function updateRejectionReceipt(request_info) {
  //   const {
  //     id,
  //     transaction_id,
  //     no_of_bags_rejected,
  //     reason_of_rejection,
  //     date,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE rejection_receipt SET transaction_id = $1,
  //   no_of_bags_rejected = $2, reason_of_rejection = $3, date = $4, updated_by = $5,
  //   updated_at = $6 WHERE id = $7 RETURNING *`,
  //     [
  //       transaction_id,
  //       no_of_bags_rejected,
  //       reason_of_rejection,
  //       date,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  async function updateRejectionReceipt(request_info, SessionId) {
      const {
        id,
        transaction_id,
        no_of_bags_rejected,
        reason_of_rejection,
        date,
        updated_by
      } = request_info;
  
      const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_RR('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_trans_id: `${transaction_id}`,
        U_FSQR_bags_rej: `${no_of_bags_rejected}`,
        U_FSQR_reason: `${reason_of_rejection}`,
        U_FSQR_date: `${date}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
    }

  // async function listDriver() {
  //   return pool.query("SELECT * FROM drivers ORDER BY firstname");
  // }

  // async function findDriverById(id) {
  //   return pool.query("SELECT * FROM drivers WHERE id = $1", [id]);
  // }

  // async function addDriver(request_info) {
  //   const {
  //     firstname,
  //     middlename,
  //     lastname,
  //     name_extension,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO drivers (firstname, middlename, lastname, name_extension, created_by, created_at)
  //     VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
  //     [firstname, middlename, lastname, name_extension, created_by, date_today]
  //   );
  // }

  // async function updateDriver(request_info) {
  //   const {
  //     id,
  //     firstname,
  //     middlename,
  //     lastname,
  //     name_extension,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE drivers SET firstname = $1, middlename = $2, lastname = $3, name_extension = $4, updated_by = $5, updated_at = $6 WHERE id = $7 RETURNING *
  //   `,
  //     [
  //       firstname,
  //       middlename,
  //       lastname,
  //       name_extension,
  //       updated_by,
  //       date_today,
  //       id
  //     ]
  //   );
  // }

  // async function listStatus() {
  //   return pool.query("SELECT * FROM status ORDER BY name");
  // }

  // async function findStatusById(id) {
  //   return pool.query("SELECT * FROM status WHERE id = $1", [id]);
  // }

  // async function findStatusByName(name) {
  //   return pool.query("SELECT * FROM status WHERE name = $1", [name]);
  // }

  // async function addStatus(request_info) {
  //   const { name, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO status (name, created_by, created_at)
  //          VALUES ($1, $2, $3) RETURNING *`,
  //     [name, created_by, date_today]
  //   );
  // }

  // async function updateStatus(request_info) {
  //   const { id, name, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE status set name = $1, updated_by = $2,
  //   updated_at = $3 WHERE id = $4  RETURNING *`,
  //     [name, updated_by, date_today, id]
  //   );
  // }

  // async function listTransactionTypes() {
  //   return pool.query("SELECT * FROM transaction_type ORDER BY name");
  // }

  // async function findTransactionTypeById(id) {
  //   return pool.query("SELECT * FROM transaction_type WHERE ID = $1", [id]);
  // }

  // async function addTransactionType(request_info) {
  //   const { name, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO transaction_type (name, created_by, created_at)
  //                     VALUES ($1, $2, $3) RETURNING *`,
  //     [name, created_by, date_today]
  //   );
  // }

  // async function updateTransactionType(request_info) {
  //   const { id, name, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE transaction_type set name = $1, updated_by = $2, updated_at = $3 WHERE id = $4 RETURNING *`,
  //     [name, updated_by, date_today, id]
  //   );
  // }

  // async function listTrucktype() {
  //   return pool.query("SELECT * FROM truck_types ORDER BY truck_type");
  // }

  // async function findTrucktypeById(id) {
  //   return pool.query("SELECT * FROM truck_types WHERE id = $1", [id]);
  // }

  // async function addTruckType(request_info) {
  //   const { truck_type, truck_model, truck_size, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO truck_types (truck_type, truck_model, truck_size, created_by, created_at)
  //           VALUES ($1, $2, $3, $4, $5) RETURNING *`,
  //     [truck_type, truck_model, truck_size, created_by, date_today]
  //   );
  // }

  // async function updateTruckType(request_info) {
  //   const {
  //     id,
  //     truck_type,
  //     truck_model,
  //     truck_size,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE truck_types set truck_type = $1, truck_model = $2, truck_size = $3,
  //             updated_by = $4, updated_at = $5 WHERE id = $6 RETURNING *`,
  //     [truck_type, truck_model, truck_size, updated_by, date_today, id]
  //   );
  // }

  // async function listTrucks() {
  //   return pool.query("SELECT * FROM truck_infos");
  // }

  // async function findTruckById(id) {
  //   return pool.query("SELECT * FROM truck_infos WHERE id = $1", [id]);
  // }

  // async function addTruck(request_info) {
  //   const { truck_type_id, plate_number, created_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO truck_infos (truck_type_id, plate_number, created_by, created_at)
  //       VALUES ($1, $2, $3, $4) RETURNING *`,
  //     [truck_type_id, plate_number, created_by, date_today]
  //   );
  // }

  // async function updateTruck(request_info) {
  //   const { id, truck_type_id, plate_number, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE truck_infos set truck_type_id = $1, plate_number = $2, updated_by = $3,
  //         updated_at = $4 WHERE id = $5 RETURNING *`,
  //     [truck_type_id, plate_number, updated_by, date_today, id]
  //   );
  // }

  // async function listDeliveryReceipts() {
  //   return pool.query(
  //     "SELECT * FROM delivery_receipt ORDER BY description_of_articles"
  //   );
  // }

  // async function findDeliveryReceiptByID(id) {
  //   return pool.query(
  //     "SELECT * FROM delivery_receipt WHERE id = $1" +
  //       "ORDER BY description_of_articles",
  //     [id]
  //   );
  // }

  // async function addDeliveryReceipt(request_info) {
  //   const {
  //     delivery_receipt,
  //     no_of_bags,
  //     unit,
  //     description_of_articles,
  //     created_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     "INSERT INTO delivery_receipt (id, quantity, unit, description_of_articles, created_by, created_at)" +
  //       "VALUES ($6, $1, $2, $3, $4, $5) RETURNING *",
  //     [
  //       no_of_bags,
  //       unit,
  //       description_of_articles,
  //       created_by,
  //       date_today,
  //       delivery_receipt
  //     ]
  //   );
  // }

  // async function updateDeliveryReceipt(request_info) {
  //   const {
  //     id,
  //     quantity,
  //     unit,
  //     description_of_articles,
  //     updated_by
  //   } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE delivery_receipt set quantity = $1,
  //                   unit = $2, description_of_articles = $3, updated_by = $4,
  //                   updated_at = $5 WHERE id = $6 RETURNING *`,\\\\\\\\\\\\\\
  //     [quantity, unit, description_of_articles, updated_by, date_today, id]
  //   );
  // }

  // // Activity logs
  // async function addActivityLog(log_info) {
  //   const { created_by, action, table, old_values, new_values } = log_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     'INSERT INTO activity_logs (created_by, "action", "table", old_values, new_values, created_at) ' +
  //       "VALUES ($1, $2, $3, $4, $5, $6)",
  //     [created_by, action, table, old_values, new_values, date_today]
  //   );
  // }
};
