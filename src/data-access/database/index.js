const Pool = require("pg").Pool;
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/../../config/config.json")[env];

const pool = new Pool(config);

module.exports = pool;