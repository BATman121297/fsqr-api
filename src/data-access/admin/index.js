const { axiosRequest } = require("../../axios/axios");

const pool = require("../database/index");
//Moment
const moment = require("moment-timezone");
const admin = require("./admin");
// const admin = require("./admin_SAP");

require("tls").DEFAULT_MIN_VERSION = "TLSv1";
var https = require("https");

const admin_db = admin({ pool }, { moment }, { axiosRequest }, { https });


module.exports = admin_db;
