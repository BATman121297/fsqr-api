module.exports = function adminDB({ pool }, { moment }) {
  return Object.freeze({
    //Admin
    //Modules
    listAllModules,
    addModule,
    updateModule,
    findModuleByDescription,
    findModuleById,

    //Actions
    findActionsByRole,
    listAllActions,
    addAction,
    updateAction,
    findActionByDescription,
    findActionById,

    //Roles
    listAllRoles,
    findRoleById,
    findRoleByName,
    addRole,
    updateRole,

    //Access Rights
    listAllAccessRights,
    findAccessRightsByActionInRole,
    findAccessRightById,
    addAccessRights,
    updateAccessRights,
    removeAccessRight,

    // Employees
    listAllEmployees,
    getEmployeeById,
    getEmployeeByEmpId,
    addEmployee,
    updateEmployee,
    changePincode,
    resetPassword,
    updatePassword,

    // Activity logs
    addActivityLog,
    listActivityLogs,

    // Departments
    listAllDepartments,
    findDepartmentById,
    findDepartmentByName,
    addDepartment,
    updateDepartment
  });


  // Departments
  async function listAllDepartments() {
    return pool.query("SELECT * FROM departments");
  }

  async function findDepartmentById(id) {
    return pool.query("SELECT * FROM departments WHERE id = $1", [id])
  }

  async function findDepartmentByName(name) {
    return pool.query("SELECT * FROM departments WHERE name = $1", [name])
  }

  async function addDepartment(request_info) {
    const { name, status, created_by } = request_info;

    const date_today = moment()
    .tz("Asia/Manila")
    .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(`INSERT INTO departments (name, status, created_by, created_at)VALUES ($1, $2, $3, $4) RETURNING *`, 
    [name, status, created_by, date_today]);
  }

  async function updateDepartment(request_info) {
    const { id, name, status, updated_by} = request_info;

    const date_today = moment()
    .tz("Asia/Manila")
    .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(`UPDATE departments SET name = $1, status = $5, updated_by = $2,
    updated_at = $3 WHERE id = $4 RETURNING *`, [name, updated_by, date_today, id, status])
   }

  // Change pincode
  async function changePincode(request_info) {
    const { pin_code, employee_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return await pool.query(
      `UPDATE employees SET pin_code = $1,
    updated_by = $2, updated_at = $3 
    WHERE employee_id = $4 RETURNING *`,
      [pin_code, updated_by, date_today, employee_id]
    );
  }

  //Modules
  async function listAllModules() {
    return pool.query(`SELECT
        "public".modules."id",
        "public".modules.description,
        "public".modules.status,
        "public".modules.created_by,
        "public".modules.updated_by,
        "public".modules.created_at,
        "public".modules.updated_at
        FROM
        "public".modules
        `);
  }

 
  async function addModule(module_info) {
    const { description, status, employee_id } = module_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return await pool.query(
      `INSERT INTO modules (description,status,created_by,updated_by,created_at,updated_at)
     VALUES ($1,$2,$3,$4,$5,$6) RETURNING *`,
      [description, status, employee_id, employee_id, date_today, date_today]
    );
  }

  async function updateModule({ id, ...update_info }) {
    const { description, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE modules SET description = $1, status = $2, updated_by = $3, updated_at = 
      $4 where id = $5 RETURNING *`,
      [description, status, employee_id, date_today, id]
    );
  }

  async function findModuleByDescription(description) {
    return pool.query(
      `SELECT
    "public".modules.description
    FROM
    "public".modules
    WHERE
    "public".modules.description = $1`,
      [description]
    );
  }

  async function findModuleById(id) {
    return pool.query(`SELECT * FROM modules WHERE id = $1`, [id]);
  }

  //Actions
  async function listAllActions() {
    return pool.query(`SELECT
    "actions"."id",
    "actions"."module_id",
    "actions"."description",
    "actions"."created_by",
    "actions"."updated_by",
    "actions"."status",
    "actions"."created_at" AS "createdAt",
    "actions"."updated_at" AS "updatedAt",
    "actions"."module_id" AS "moduleId",
    "module"."id" AS "module.id",
    "module"."description" AS "module.description",
    "module"."created_by" AS "module.created_by",
    "module"."updated_by" AS "module.updated_by",
    "module"."status" AS "module.status",
    "module"."created_at" AS "module.createdAt",
    "module"."updated_at" AS "module.updatedAt" 
    FROM
      "actions" AS "actions"
      LEFT OUTER JOIN "modules" AS "module" ON "actions"."module_id" = "module"."id";`);
  }

  async function addAction(action_info) {
    const { module_id, description, status, employee_id } = action_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO actions (module_id,description,status,created_by,updated_by,created_at,updated_at)
    VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING *`,
      [
        module_id,
        description,
        status,
        employee_id,
        employee_id,
        date_today,
        date_today
      ]
    );
  }

  async function updateAction({ id, ...update_info }) {
    const { description, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE actions SET description = $1, status = $2, updated_by = $3, updated_at = $4 where id = $5 RETURNING *`,
      [description, status, employee_id, date_today, id]
    );
  }

  async function findActionByDescription(module_id, description) {
    return pool.query(
      `SELECT
    "public".actions.description
    FROM
    "public".actions
    WHERE
    "public".actions.module_id = $1 AND
    "public".actions.description = $2`,
      [module_id, description]
    );
  }

  async function findActionById(id) {
    return await pool.query(`SELECT * FROM actions WHERE id = $1`, [id]);
  }

  //Roles
  async function listAllRoles() {
    return pool.query(`SELECT * FROM roles order by name`);
  }

  async function findRoleById(id) {
    return await pool.query(`SELECT * FROM roles WHERE id = $1`, [id]);
  }

  async function findRoleByName(name) {
    return await pool.query(`SELECT name FROM roles WHERE name = $1`, [name]);
  }

  async function addRole(role_info) {
    const { name, status, employee_id } = role_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return await pool.query(
      `INSERT INTO roles (name,status,created_by,updated_by,created_at,updated_at)
      VALUES ($1,$2,$3,$4,$5,$6) RETURNING *`,
      [name, status, employee_id, employee_id, date_today, date_today]
    );
  }

  async function updateRole({ id, ...update_info } = {}) {
    const { name, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return await pool.query(
      `UPDATE roles SET name  = $1, status = $2, updated_by = $3, updated_at = $4 where id = $5 RETURNING *`,
      [name, status, employee_id, date_today, id]
    );
  }

  //Access rights
  async function listAllAccessRights() {
    return pool.query(`SELECT
    "public".access_rights."id",
    "public".access_rights.action_id,
    "public".access_rights.role_id,
    "public".access_rights.status,
    "public".access_rights.created_by,
    "public".access_rights.updated_by,
    "public".access_rights.created_at,
    "public".access_rights.updated_at,
    "public".roles."id",
    "public".roles."name" as role_name,
    "public".actions."id" as actions_id,
    "public".actions.description as actions_name
    FROM
    "public".access_rights
    INNER JOIN "public".actions ON "public".access_rights.action_id = "public".actions."id"
    INNER JOIN "public".roles ON "public".access_rights.role_id = "public".roles."id"
    `);
  }

  async function findActionsByRole(role_id) {
    return pool.query(
      `SELECT DISTINCT access_rights.action_id, access_rights.role_id, actions.description
                        FROM access_rights
                        INNER JOIN actions ON access_rights.action_id = actions.id
                        INNER JOIN roles ON access_rights.role_id = roles.id
                        WHERE access_rights.role_id = $1`,
      [role_id]
    );
  }

  async function removeAccessRight(role_id, action_id) {
    return pool.query(
      `DELETE FROM access_rights
            WHERE role_id = $1 and action_id = $2`,
      [role_id, action_id]
    );
  }

  async function findAccessRightsByActionInRole(role_id, action_id) {
    return pool.query(
      `SELECT * FROM access_rights WHERE role_id = $1 AND action_id = $2`,
      [role_id, action_id]
    );
  }

  async function findAccessRightById(id) {
    return pool.query(`SELECT * FROM access_rights WHERE id = $1`, [id]);
  }

  async function addAccessRights(access_rights_info) {
    const { action_id, role_id, status, employee_id } = access_rights_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `INSERT INTO access_rights (action_id,role_id,status,created_by,updated_by,created_at,updated_at)
    VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING *`,
      [
        action_id,
        role_id,
        status,
        employee_id,
        employee_id,
        date_today,
        date_today
      ]
    );
  }

  async function updateAccessRights({ id, ...update_info }) {
    const { action_id, role_id, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    // `UPDATE roles SET name  = $1, status = $2, updated_by = $3,
    //  updated_at = '${date_today}' where id = ${id}`

    return pool.query(
      `UPDATE access_rights SET action_id = $1, role_id = $2,
       status = $3, updated_by = $4,
       updated_at = $5 where id = $6 RETURNING *`,
      [action_id, role_id, status, employee_id, date_today, id]
    );
  }

  // Employees
  async function listAllEmployees() {
    return pool.query(
      `SELECT * FROM employees`
    );
  }

  async function getEmployeeById(id) {
    return pool.query(`SELECT * FROM employees WHERE id = $1`, [id]);
  }

  async function getEmployeeByEmpId(employee_id) {
    return pool.query(`SELECT * FROM employees WHERE employee_id = $1`, [
      employee_id
    ]);
  }

  async function addEmployee(employee_info) {
    const {
      role_id,
      name,
      employee_id,
      password,
      created_by,
      status,
      pin_code,
      department
    } = employee_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      "INSERT INTO employees (role_id, name, employee_id, password, created_by, status, created_at, pin_code, department)" +
        "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *",
      [role_id, name, employee_id, password, created_by, status, date_today, pin_code, department]
    );
  }

  async function updateEmployee({ id, ...employee_info }) {
    const { role_id, name, updated_by, status, department } = employee_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      "UPDATE employees set role_id = $1, name = $2, updated_by = $3, status = $4, updated_at = $5, department = $7 WHERE id = $6 RETURNING *",
      [role_id, name, updated_by, status, date_today, id, department]
    );
  }

  async function updatePassword(request_info) {
    const { password, id, updated_by } = request_info;

    const date_today = moment()
    .tz("Asia/Manila")
    .format("YYYY-MM-DD HH:mm:ss");
  
    return pool.query(`UPDATE employees set password = $1,
    updated_by = $2, updated_at = $3 WHERE employee_id = $4
    RETURNING *`,
    [ password, updated_by, date_today, id])
  }

  async function resetPassword(request_info) {
    const { password, employee_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    
      return pool.query(`UPDATE employees set password = $1, 
      updated_by = $2, updated_at = $3 WHERE employee_id = $4
      RETURNING *`,
      [ password, updated_by, date_today, employee_id])

  }

  // Activity logs
  async function addActivityLog(log_info) {
    const { created_by, action, table, old_values, new_values } = log_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      'INSERT INTO activity_logs (created_by, "action", "table", old_values, new_values, created_at) ' +
        "VALUES ($1, $2, $3, $4, $5, $6)",
      [created_by, action, table, old_values, new_values, date_today]
    );
  }

  async function listActivityLogs(date_from, date_to) {
    date_to = moment(date_to)
      .add(1, "day")
      .format("YYYY-MM-DD");

    return pool.query(
      `SELECT activity_logs.id,
                activity_logs.created_by,
                activity_logs.action,
                activity_logs.table,
                activity_logs.old_values,
                activity_logs.new_values,
                activity_logs.created_at,
                employees.name
            FROM activity_logs
            INNER JOIN employees ON activity_logs.created_by = employees.employee_id
            WHERE activity_logs.created_at BETWEEN SYMMETRIC 
            $1 AND $2 ORDER BY activity_logs.created_at DESC`,
      [date_from, date_to]
    );
  }
};
