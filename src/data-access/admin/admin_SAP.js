const axios = require("axios");

module.exports = function adminDB({ pool }, { moment }, { axiosRequest }, { https }) {
  return Object.freeze({
    //Admin
    //Modules
    listAllModules,
    addModule,
    updateModule,
    findModuleByDescription,
    findModuleById,

    //Actions
    findActionsByRole,
    listAllActions,
    addAction,
    updateAction,
    findActionByDescription,
    findActionById,

    //Roles
    listAllRoles,
    findRoleById,
    findRoleByName,
    addRole,
    updateRole,

    //Access Rights
    listAllAccessRights,
    findAccessRightsByActionInRole,
    findAccessRightById,
    addAccessRights,
    updateAccessRights,
    removeAccessRight,

    // Employees
    listAllEmployees,
    getEmployeeById,
    getEmployeeByEmpId,
    addEmployee,
    updateEmployee,
    changePincode,
    resetPassword,
    updatePassword,

    // Activity logs
    // addActivityLog,
    // listActivityLogs,

    // Departments
    listAllDepartments,
    findDepartmentById,
    findDepartmentByName,
    addDepartment,
    updateDepartment
  });

  // Departments
  // async function listAllDepartments() {
  //   return pool.query("SELECT * FROM departments");
  // }

  async function listAllDepartments(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEPART`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDepartmentById(id) {
  //   return pool.query("SELECT * FROM departments WHERE id = $1", [id])
  // }

  async function findDepartmentById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEPART?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findDepartmentByName(name) {
  //   return pool.query("SELECT * FROM departments WHERE name = $1", [name])
  // }

  async function findDepartmentByName(name, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_DEPART?$filter=Name eq '${name}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addDepartment(request_info) {
  //   const { name, status, created_by } = request_info;

  //   const date_today = moment()
  //   .tz("Asia/Manila")
  //   .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(`INSERT INTO departments (name, status, created_by, created_at)VALUES ($1, $2, $3, $4) RETURNING *`,
  //   [name, status, created_by, date_today]);
  // }

  async function addDepartment(SessionId, request_info) {
    const { name, status, created_by } = request_info;

    let codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_DEPART/$count`
    );
    codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_DEPART`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_name: `${name}`,
        U_FSQR_status: `${status}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateDepartment(request_info) {
  //   const { id, name, status, updated_by} = request_info;

  //   const date_today = moment()
  //   .tz("Asia/Manila")
  //   .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(`UPDATE departments SET name = $1, status = $5, updated_by = $2,
  //   updated_at = $3 WHERE id = $4 RETURNING *`, [name, updated_by, date_today, id, status])
  //  }

  async function updateDepartment(SessionId, request_info) {
    const { id, name, status, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_DEPART?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name: `${name}`,
        U_FSQR_name: `${name}`,
        U_FSQR_status: `${status}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // Change pincode
  // async function changePincode(request_info) {
  //   const { pincode, employee_id, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return await pool.query(
  //     `UPDATE employees SET pin_code = $1,
  //   updated_by = $2, updated_at = $3
  //   WHERE employee_id = $4 RETURNING *`,
  //     [pincode, updated_by, date_today, employee_id]
  //   );
  // }

  async function changePincode(SessionId, request_info) {
    const { pincode, employee_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_USER?$filter=U_FSQR_employeeID eq '${employee_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_pincode: `${pincode}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  //Modules
  // async function listAllModules() {
  //   return pool.query(`SELECT
  //       "public".modules."id",
  //       "public".modules.description,
  //       "public".modules.status,
  //       "public".modules.created_by,
  //       "public".modules.updated_by,
  //       "public".modules.created_at,
  //       "public".modules.updated_at
  //       FROM
  //       "public".modules
  //       `);
  // }

  async function listAllModules(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MODULES`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  async function addModule(SessionId, module_info) {
    const { description, status, employee_id } = module_info;

    let codeMax;
    const res = await axios({
      method: "GET",
      url: `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_MODULES/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      httpsAgent: new https.Agent({ rejectUnauthorized: false })
    });
    codeMax = res.data;  

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_MODULES`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${description}`,
        U_FSQR_desc: `${description}`,
        U_FSQR_status: `${status}`,
        U_FSQR_created_by: `${employee_id}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data;
  }

  // async function updateModule({ id, ...update_info }) {
  //   const { description, status, employee_id } = update_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE modules SET description = $1, status = $2, updated_by = $3, updated_at =
  //     $4 where id = $5 RETURNING *`,
  //     [description, status, employee_id, date_today, id]
  //   );
  // }

  async function updateModule( SessionId, update_info ) {
    const { id, description, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_MODULES('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_desc: `${description}`,
        U_FSQR_status: `${status}`,
        U_FSQR_updated_by: `${employee_id}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findModuleByDescription(description) {
  //   return pool.query(
  //     `SELECT
  //   "public".modules.description
  //   FROM
  //   "public".modules
  //   WHERE
  //   "public".modules.description = $1`,
  //     [description]
  //   );
  // }

  async function findModuleByDescription(SessionId, description) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MODULES?$select=U_FSQR_desc&$filter=U_FSQR_desc eq '${description}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findModuleById(id) {
  //   return pool.query(`SELECT * FROM modules WHERE id = $1`, [id]);
  // }

  async function findModuleById(SessionId, id) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_MODULES?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  //Actions
  async function listAllActions(SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/$crossjoin(U_BFI_FSQR_ACTIONS,U_BFI_FSQR_MODULES)?$expand=U_BFI_FSQR_ACTIONS($select=Code,Name,U_FSQR_modID,U_FSQR_desc,U_FSQR_status,U_FSQR_created_by,U_FSQR_created_date,U_FSQR_created_time,U_FSQR_updated_by,U_FSQR_updated_date,U_FSQR_updated_time),U_BFI_FSQR_MODULES($select=Code,Name,U_FSQR_desc,U_FSQR_status,U_FSQR_created_by,U_FSQR_created_date,U_FSQR_created_time,U_FSQR_updated_by,U_FSQR_updated_date,U_FSQR_updated_time)&$filter=U_BFI_FSQR_ACTIONS/U_FSQR_modID eq U_BFI_FSQR_MODULES/Code`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addAction(action_info) {
  //   const { module_id, description, status, employee_id } = action_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO actions (module_id,description,status,created_by,updated_by,created_at,updated_at)
  //   VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING *`,
  //     [
  //       module_id,
  //       description,
  //       status,
  //       employee_id,
  //       employee_id,
  //       date_today,
  //       date_today
  //     ]
  //   );
  // }

  async function addAction(SessionId, action_info) {
    const { module_id, description, status, employee_id } = action_info;

    let codeMax;
    const res = await axios({
      method: "GET",
      url: `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_ACTIONS/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      httpsAgent: new https.Agent({ rejectUnauthorized: false })
    });
    codeMax = res.data;  

    let IntcodeMax = parseInt(codeMax, 10) + 1;


    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_ACTIONS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${description}`,
        U_FSQR_modID: `${module_id}`,
        U_FSQR_desc: `${description}`,
        U_FSQR_status: `${status}`,
        U_FSQR_created_by: `${employee_id}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data;
  }

  // async function updateAction({ id, ...update_info }) {
  //   const { description, status, employee_id } = update_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE actions SET description = $1, status = $2, updated_by = $3, updated_at = $4 where id = $5 RETURNING *`,
  //     [description, status, employee_id, date_today, id]
  //   );
  // }

  async function updateAction(SessionId, update_info ) {
    const { id, description, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_ACTIONS('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name: `${description}`,
        U_FSQR_desc: `${description}`,
        U_FSQR_status: `${status}`,
        U_FSQR_updated_by: `${employee_id}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findActionByDescription(module_id, description) {
  //   return pool.query(
  //     `SELECT
  //   "public".actions.description
  //   FROM
  //   "public".actions
  //   WHERE
  //   "public".actions.module_id = $1 AND
  //   "public".actions.description = $2`,
  //     [module_id, description]
  //   );
  // }

  async function findActionByDescription(SessionId, module_id, description) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ACTIONS?$select=U_FSQR_desc&$filter=U_FSQR_modID eq '${module_id}' and U_FSQR_desc eq '${description}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findActionById(id) {
  //   return await pool.query(`SELECT * FROM actions WHERE id = $1`, [id]);
  // }

  async function findActionById(SessionId, id) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ACTIONS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  //Roles
  // async function listAllRoles() {
  //   return pool.query(`SELECT * FROM roles order by name`);
  // }

  async function listAllRoles(SessionId) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ROLES?$orderby=Code asc`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findRoleById(id) {
  //   return await pool.query(`SELECT * FROM roles WHERE id = $1`, [id]);
  // }

  async function findRoleById(SessionId, id) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ROLES?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value
  }

  // async function findRoleByName(name) {
  //   return await pool.query(`SELECT name FROM roles WHERE name = $1`, [name]);
  // }

  async function findRoleByName(SessionId, name) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ROLES?$filter=Name eq '${name}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addRole(role_info) {
  //   const { name, status, employee_id } = role_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return await pool.query(
  //     `INSERT INTO roles (name,status,created_by,updated_by,created_at,updated_at)
  //     VALUES ($1,$2,$3,$4,$5,$6) RETURNING *`,
  //     [name, status, employee_id, employee_id, date_today, date_today]
  //   );
  // }

  async function addRole(SessionId, role_info) {
    const { name, status, employee_id } = role_info;

    let codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_ROLES/$count`
    );
    codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_ROLES`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_name: `${name}`,
        U_FSQR_status: `${status}`,
        U_FSQR_created_by: `${employee_id}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateRole({ id, ...update_info } = {}) {
  //   const { name, status, employee_id } = update_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");
  //   return await pool.query(
  //     `UPDATE roles SET name  = $1, status = $2, updated_by = $3, updated_at = $4 where id = $5 RETURNING *`,
  //     [name, status, employee_id, date_today, id]
  //   );
  // }

  async function updateRole({ SessionId, id, ...update_info } = {}) {
    const { name, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_ROLES?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name: `${name}`,
        U_FSQR_name: `${name}`,
        U_FSQR_status: `${status}`,
        U_FSQR_updated_by: `${employee_id}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  //Access rights
  // async function listAllAccessRights() {
  //   return pool.query(`SELECT
  //   "public".access_rights."id",
  //   "public".access_rights.action_id,
  //   "public".access_rights.role_id,
  //   "public".access_rights.status,
  //   "public".access_rights.created_by,
  //   "public".access_rights.updated_by,
  //   "public".access_rights.created_at,
  //   "public".access_rights.updated_at,
  //   "public".roles."id",
  //   "public".roles."name" as role_name,
  //   "public".actions."id" as actions_id,
  //   "public".actions.description as actions_name
  //   FROM
  //   "public".access_rights
  //   INNER JOIN "public".actions ON "public".access_rights.action_id = "public".actions."id"
  //   INNER JOIN "public".roles ON "public".access_rights.role_id = "public".roles."id"
  //   `);
  // }

  async function listAllAccessRights(SessionId) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/$crossjoin(U_BFI_FSQR_ACC_RIGHTS,U_BFI_FSQR_ROLES,U_BFI_FSQR_ACTIONS)?$expand=U_BFI_FSQR_ACC_RIGHTS($select=Code,Name,U_FSQR_actionID,U_FSQR_roleID,U_FSQR_status,U_FSQR_created_by,U_FSQR_created_date,U_FSQR_created_time,U_FSQR_updated_by,U_FSQR_updated_date,U_FSQR_updated_time),U_BFI_FSQR_ROLES($select=Code,Name),U_BFI_FSQR_ACTIONS($select=Code,Name,U_FSQR_desc)&$filter=U_BFI_FSQR_ACC_RIGHTS/U_FSQR_actionID eq U_BFI_FSQR_ACTIONS/Code and U_BFI_FSQR_ACC_RIGHTS/U_FSQR_roleID eq U_BFI_FSQR_ROLES/Code`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findActionsByRole(role_id) {
  //   return pool.query(
  //     `SELECT DISTINCT access_rights.action_id, access_rights.role_id, actions.description
  //                       FROM access_rights
  //                       INNER JOIN actions ON access_rights.action_id = actions.id
  //                       INNER JOIN roles ON access_rights.role_id = roles.id
  //                       WHERE access_rights.role_id = $1`,
  //     [role_id]
  //   );
  // }

  async function findActionsByRole(SessionId, role_id) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/$crossjoin(U_BFI_FSQR_ACC_RIGHTS,U_BFI_FSQR_ROLES,U_BFI_FSQR_ACTIONS)?$expand=U_BFI_FSQR_ACC_RIGHTS($select=U_FSQR_actionID,U_FSQR_roleID),U_BFI_FSQR_ACTIONS($select=U_FSQR_desc)&$filter=U_BFI_FSQR_ACC_RIGHTS/U_FSQR_actionID eq U_BFI_FSQR_ACTIONS/Code and U_BFI_FSQR_ACC_RIGHTS/U_FSQR_roleID eq U_BFI_FSQR_ROLES/Code and U_BFI_FSQR_ACC_RIGHTS/U_FSQR_roleID eq '${role_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function removeAccessRight(role_id, action_id) {
  //   return pool.query(
  //     `DELETE FROM access_rights
  //           WHERE role_id = $1 and action_id = $2`,
  //     [role_id, action_id]
  //   );
  // }

  async function removeAccessRight(SessionId, role_id, action_id) {

    const query = await axiosRequest({
      method: "DELETE",
      link: `/b1s/v1/U_BFI_FSQR_ACC_RIGHTS?$filter=U_FSQR_roleID eq '${role_id}' and U_FSQR_actionID eq '${action_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findAccessRightsByActionInRole(role_id, action_id) {
  //   return pool.query(
  //     `SELECT * FROM access_rights WHERE role_id = $1 AND action_id = $2`,
  //     [role_id, action_id]
  //   );
  // }

  async function findAccessRightsByActionInRole(SessionId, role_id, action_id) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ACC_RIGHTS?$filter=U_FSQR_roleID eq '${role_id}' and U_FSQR_actionID eq '${action_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function findAccessRightById(id) {
  //   return pool.query(`SELECT * FROM access_rights WHERE id = $1`, [id]);
  // }

  async function findAccessRightById(SessionId, id) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_ACC_RIGHTS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addAccessRights(access_rights_info) {
  //   const { action_id, role_id, status, employee_id } = access_rights_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `INSERT INTO access_rights (action_id,role_id,status,created_by,updated_by,created_at,updated_at)
  //   VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING *`,
  //     [
  //       action_id,
  //       role_id,
  //       status,
  //       employee_id,
  //       employee_id,
  //       date_today,
  //       date_today
  //     ]
  //   );
  // }

  async function addAccessRights(SessionId, access_rights_info) {
    const { action_id, role_id, status, employee_id } = access_rights_info;

    let codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_ACC_RIGHTS/$count`
    );
    codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_ACC_RIGHTS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${IntcodeMax}`,
        U_FSQR_actionID: `${action_id}`,
        U_FSQR_roleID: `${role_id}`,
        U_FSQR_status: `${status}`,
        U_FSQR_created_by: `${employee_id}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateAccessRights({ id, ...update_info }) {
  //   const { action_id, role_id, status, employee_id } = update_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     `UPDATE access_rights SET action_id = $1, role_id = $2,
  //      status = $3, updated_by = $4,
  //      updated_at = $5 where id = $6 RETURNING *`,
  //     [action_id, role_id, status, employee_id, date_today, id]
  //   );
  // }

  async function updateAccessRights({ SessionId, id, ...update_info }) {
    const { action_id, role_id, status, employee_id } = update_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_ACC_RIGHTS?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_actionID: `${action_id}`,
        U_FSQR_roleID: `${role_id}`,
        U_FSQR_status: `${status}`,
        U_FSQR_updated_by: `${employee_id}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // Employees
  // async function listAllEmployees() {
  //   return pool.query(
  //     `SELECT * FROM employees`
  //   );
  // }

  async function listAllEmployees(SessionId) {

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_USER`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function getEmployeeById(id) {
  //   return pool.query(`SELECT * FROM employees WHERE id = $1`, [id]);
  // }

  async function getEmployeeById(id, SessionId) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_USER('${id}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function getEmployeeByEmpId(employee_id) {
  //   return pool.query(`SELECT * FROM employees WHERE employee_id = $1`, [
  //     employee_id
  //   ]);
  // }

  async function getEmployeeByEmpId(SessionId, employee_id) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_BFI_FSQR_USER?$filter=U_FSQR_employeeID eq '${employee_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function addEmployee(employee_info) {
  //   const {
  //     role_id,
  //     name,
  //     employee_id,
  //     password,
  //     created_by,
  //     status,
  //     pin_code,
  //     department
  //   } = employee_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     "INSERT INTO employees (role_id, name, employee_id, password, created_by, status, created_at, pin_code, department)" +
  //       "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *",
  //     [role_id, name, employee_id, password, created_by, status, date_today, pin_code, department]
  //   );
  // }

  async function addEmployee(SessionId, employee_info) {
    const {
      role_id,
      name,
      employee_id,
      password,
      created_by,
      status,
      pin_code,
      department
    } = employee_info;

    let codeMax = await axios.get(
      `https://18.136.35.41:50000/b1s/v1/U_BFI_FSQR_USER/$count`
    );
    codeMax = codeMax.data;

    let IntcodeMax = parseInt(codeMax, 10) + 1;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_BFI_FSQR_USER`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: `${IntcodeMax}`,
        Name: `${name}`,
        U_FSQR_roleID: `${role_id}`,
        U_FSQR_name: `${name}`,
        U_FSQR_employeeID: `${employee_id}`,
        U_FSQR_password: `${password}`,
        U_FSQR_status: `${status}`,
        U_FSQR_pincode: `${pin_code}`,
        U_FSQR_departID: `${department}`,
        U_FSQR_created_by: `${created_by}`,
        U_FSQR_created_date: `${date_today}`,
        U_FSQR_created_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updateEmployee({ id, ...employee_info }) {
  //   const { role_id, name, updated_by, status, department } = employee_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     "UPDATE employees set role_id = $1, name = $2, updated_by = $3, status = $4, updated_at = $5, department = $7 WHERE id = $6 RETURNING *",
  //     [role_id, name, updated_by, status, date_today, id, department]
  //   );
  // }

  async function updateEmployee({ SessionId, id, ...employee_info }) {
    const { role_id, name, updated_by, status, department } = employee_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_USER?$filter=Code eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name: `${name}`,
        U_FSQR_roleID: `${role_id}`,
        U_FSQR_name: `${name}`,
        U_FSQR_status: `${status}`,
        U_FSQR_departID: `${department}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function updatePassword(request_info) {
  //   const { password, id, updated_by } = request_info;

  //   const date_today = moment()
  //   .tz("Asia/Manila")
  //   .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(`UPDATE employees set password = $1,
  //   updated_by = $2, updated_at = $3 WHERE employee_id = $4
  //   RETURNING *`,
  //   [ password, updated_by, date_today, id])
  // }

  async function updatePassword(SessionId, request_info) {
    const { password, id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_USER?$filter=U_FSQR_employeeID eq '${id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_password: `${password}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // async function resetPassword(request_info) {
  //   const { password, employee_id, updated_by } = request_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //     return pool.query(`UPDATE employees set password = $1,
  //     updated_by = $2, updated_at = $3 WHERE employee_id = $4
  //     RETURNING *`,
  //     [ password, updated_by, date_today, employee_id])

  // }

  async function resetPassword(SessionId, request_info) {
    const { password, employee_id, updated_by } = request_info;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HH:mm:ss");

    const query = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_BFI_FSQR_USER?$filter=U_FSQR_employeeID eq '${employee_id}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_FSQR_password: `${password}`,
        U_FSQR_updated_by: `${updated_by}`,
        U_FSQR_updated_date: `${date_today}`,
        U_FSQR_updated_time: `${time_today}`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  // Activity logs
  // async function addActivityLog(log_info) {
  //   const { created_by, action, table, old_values, new_values } = log_info;

  //   const date_today = moment()
  //     .tz("Asia/Manila")
  //     .format("YYYY-MM-DD HH:mm:ss");

  //   return pool.query(
  //     'INSERT INTO activity_logs (created_by, "action", "table", old_values, new_values, created_at) ' +
  //       "VALUES ($1, $2, $3, $4, $5, $6)",
  //     [created_by, action, table, old_values, new_values, date_today]
  //   );
  // }

  // async function listActivityLogs(date_from, date_to) {
  //   date_to = moment(date_to)
  //     .add(1, "day")
  //     .format("YYYY-MM-DD");

  //   return pool.query(
  //     `SELECT activity_logs.id,
  //               activity_logs.created_by,
  //               activity_logs.action,
  //               activity_logs.table,
  //               activity_logs.old_values,
  //               activity_logs.new_values,
  //               activity_logs.created_at,
  //               employees.name
  //           FROM activity_logs
  //           INNER JOIN employees ON activity_logs.created_by = employees.employee_id
  //           WHERE activity_logs.created_at BETWEEN SYMMETRIC
  //           $1 AND $2 ORDER BY activity_logs.created_at DESC`,
  //     [date_from, date_to]
  //   );
  // }
};
