const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
const unirest = require("unirest");
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const { client } = require("../sap-client/index");

// ############
const micro = require("./query");

const micros = micro({ client, axios, dotenv, https, unirest });

module.exports = { micros };
