const micro = ({ client, axios, dotenv, https, unirest }) => {
  // #########
  dotenv.config();
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  // base url for sap
  const url = process.env.SAP_URL;

  return Object.freeze({
    microMaxCode,
    microBatchReq,
    microSelectHeader,
    retriveStatusId,
    microSelectDetails,
    transactionChangeStatus,
    fmpMicroItem,
    fmpMacroItem,
    pdfItems,
    cdfItems,
    items,
    businessEntities,
    tagPO,
    postAttachment,
    grpoUpdateAttachment,
    getPODetails,
    checkIfItemIsMicro,
    postsAttachment,
  });

  async function microMaxCode({}) {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
        AS "maxCode" FROM "${db}"."@FSQR_MICRO";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function microBatchReq({ data }) {
    try {
      const res = await axios.post(`${url}/$batch`, data.str, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${data.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const datas = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return datas;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  }
  async function microSelectHeader({ data }) {
    try {
      const db = process.env.DB;
      let sql = "";
      if (data.id) {
        //   select one
        sql = `SELECT DISTINCT a."Code",a."U_suppl_code",b."U_packaged_no",b."U_expiry_date"
        FROM "${db}"."@FSQR_TRNS" a LEFT JOIN "${db}"."@FSQR_MICRO" b
        ON b."U_transaction_id" = a."Code" WHERE b."U_transaction_id" IS NOT NULL AND
        a."Code" = '${data.id}' AND a."U_status_id" = '${data.statusId}';`;
      } else {
        //   select all
        sql = `SELECT DISTINCT a."Code",a."U_suppl_code",b."U_packaged_no",b."U_expiry_date"
        FROM "${db}"."@FSQR_TRNS" a LEFT JOIN "${db}"."@FSQR_MICRO" b
        ON b."U_transaction_id" = a."Code" WHERE b."U_transaction_id" IS NOT NULL AND
        a."U_status_id" = '${data.statusId}';`;
      }

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function microSelectDetails(id) {
    try {
      const db = process.env.DB;
      const sql = `SELECT "U_quantity","U_remarks" FROM "${db}"."@FSQR_MICRO"
      WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function retriveStatusId(name) {
    try {
      const db = process.env.DB;
      const sql = `SELECT "Code" FROM "${db}"."@FSQR_STATUS" 
      WHERE LOWER("U_name") = LOWER('${name}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function transactionChangeStatus({ data }) {
    try {
      const cookie = data.cookie; // store cookie
      delete data.cookie; // remove cookie
      const code = data.Code;
      delete data.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRNS('${code}')`,
        data: {
          ...data,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  }
  async function fmpMicroItem({}) {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",a."ItmsGrpCod", b."WhsCode", c."WhsName"
      FROM "${bfi}"."OITM" a
      INNER JOIN "${bfi}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${bfi}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      WHERE c."WhsName" = 'FEEDMILL PLANT 1 WAREHOUSE 1' and a."ItmsGrpCod" = '314'
      AND a."U_APP_BU_Feedmill" = 'Y';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function fmpMacroItem({}) {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",a."ItmsGrpCod", b."WhsCode", c."WhsName"
      FROM "${bfi}"."OITM" a
      INNER JOIN "${bfi}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${bfi}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      WHERE c."WhsName" = 'FEEDMILL PLANT 1 WAREHOUSE 1' and a."ItmsGrpCod" = '313'
      AND a."U_APP_BU_Feedmill" = 'Y';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }

  async function pdfItems({}) {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",c."WhsName",d."ItmsGrpNam"
      FROM "${bfi}"."OITM" a
      INNER JOIN "${bfi}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${bfi}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      INNER JOIN "${bfi}"."OITB" d ON a."ItmsGrpCod" = d."ItmsGrpCod"
      WHERE c."WhsName" = 'RICE PLANT PDF RM WAREHOUSE' and a."ItmsGrpCod" = '310'
      AND a."U_APP_BU_Paddy" = 'Y';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }

  async function items({ item_code }) {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT "U_APP_BU_Feedmill", "U_APP_BU_Paddy", "U_APP_BU_CDF"
      FROM "${bfi}"."OITM" 
      WHERE "ItemCode" = '${item_code}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }

  async function businessEntities({ business_entity_of_item }) {
    try {
      const bfi = process.env.DB;

      const sql = `SELECT "Code","U_description" FROM "${bfi}"."@FSQR_BUS_ENTITY"
      WHERE "Name" = '${business_entity_of_item}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }

  async function cdfItems({}) {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",a."ItmsGrpCod", b."WhsCode", c."WhsName"
      FROM "${bfi}"."OITM" a
      INNER JOIN "${bfi}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${bfi}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      WHERE c."WhsName" = 'CORN DRYING FACILITY RM WAREHOUSE' and a."ItmsGrpCod" = '313'
      AND a."U_APP_BU_CDF" = 'Y';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function tagPO({ data }) {
    try {
      const cookie = data.cookie; // store cookie
      delete data.cookie; // remove cookie
      const code = data.Code;
      delete data.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRNS('${code}')`,
        data: {
          ...data,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  }
  async function postAttachment({ imagePath }) {
    try {
      const result = await new Promise((resolve) => {
        let Request = unirest.post(`${url}/Attachments2`);
        Request.strictSSL(true).headers({
          "Content-Type": "multipart/form-data",
          Cookie: `B1SESSION=${imagePath.bfiSession}`,
        });
        if (imagePath.coaPath) {
          Request.attach("file", imagePath.coaPath);
        }
        if (imagePath.bolPath) {
          Request.attach("file", imagePath.bolPath);
        }
        if (imagePath.noaPath) {
          Request.attach("file", imagePath.noaPath);
        }
        Request.end(function (res) {
          if (res.error) throw new Error(res.error);
          resolve(res.raw_body);
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function grpoUpdateAttachment({ data }) {
    try {
      const cookie = data.cookie; // store cookie
      delete data.cookie; // remove cookie
      const code = data.Code;
      delete data.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/PurchaseDeliveryNotes(${code})`,
        data: {
          ...data,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  }
  async function getPODetails({ data }) {
    try {
      const { session, id } = data;
      const res = await axios({
        method: "GET",
        url: `${url}/PurchaseOrders?$select=CardCode,CardName,Address,DocumentLines&$filter=DocEntry eq ${id}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `B1SESSION=${session};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  }
  async function checkIfItemIsMicro({ code }) {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT COUNT(*) AS bools
      FROM "${bfi}"."OITM" a
      INNER JOIN "${bfi}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${bfi}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      WHERE c."WhsName" = 'FEEDMILL PLANT 1 WAREHOUSE 1' and a."ItmsGrpCod" = '314'
      AND a."U_APP_BU_Feedmill" = 'Y' AND a."ItemCode" = '${code}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function postsAttachment({ imagePath }) {
    try {
      const result = await new Promise((resolve) => {
        let Request = unirest.post(`${url}/Attachments2`);
        Request.strictSSL(true).headers({
          "Content-Type": "multipart/form-data",
          Cookie: `B1SESSION=${imagePath.bfiSession}`,
        });
        if (imagePath.wsPath) {
          Request.attach("file", imagePath.wsPath);
        }
        if (imagePath.dsPath) {
          Request.attach("file", imagePath.dsPath);
        }
        if (imagePath.rmafPath) {
          Request.attach("file", imagePath.rmafPath);
        }
        if (imagePath.rrPath) {
          Request.attach("file", imagePath.rrPath);
        }
        Request.end(function (res) {
          if (res.error) throw new Error(res.error);
          resolve(res.raw_body);
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
};

module.exports = micro;
