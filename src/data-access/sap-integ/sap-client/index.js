const sap = require("@sap/hana-client");
const dotenv = require("dotenv");
dotenv.config();

const client = sap.createClient({
  host: process.env.SAP_HOST,
  port: process.env.SAP_PORT,
  user: process.env.SAP_USERNAME,
  password: process.env.SAP_PASSWORD
});

module.exports = { client };
