const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const { client } = require("../sap-client/index");

// ############

// base url for sap
const url = process.env.SAP_URL;

const emp = {
  findById: async (info) => {
    try {
      const id = info.getEmployeeId();
      const db = process.env.DB;
      const bfi = process.env.REVIVE_DB;

      const sql = `SELECT CONCAT(c."lastName",CONCAT(', ',c."firstName")) AS name,a."U_employee_id",a."U_password",
      a."U_pin_code",a."U_status", b."Code",b."U_name",b."U_status" AS "roleStatus",a."U_department"
      FROM "${db}"."@FSQR_USER" a LEFT JOIN "${db}"."@FSQR_ROLES" b
      ON a."U_role_id" = b."Code" LEFT JOIN "${bfi}"."OHEM" c ON c."empID" = a."U_APP_EMP_ID"
      WHERE a."U_employee_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findByIdDefaults: async (id) => {
    try {
      const db = process.env.DB;
      const bfi = process.env.REVIVE_DB;

      const sql = `SELECT CONCAT(c."lastName",CONCAT(', ',c."firstName")) AS name,a."U_employee_id",a."U_password",
      a."U_pin_code",a."U_status", b."Code",b."U_name",b."U_status" AS "roleStatus",a."U_department"
      FROM "${db}"."@FSQR_USER" a LEFT JOIN "${db}"."@FSQR_ROLES" b
      ON a."U_role_id" = b."Code" LEFT JOIN "${bfi}"."OHEM" c ON c."empID" = a."U_APP_EMP_ID"
      WHERE a."U_employee_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listAllModules: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code","U_description","U_status" 
      FROM "${db}"."@FSQR_MODULES";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findAccessRightByRoleId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code","U_action_id","U_status"
      FROM "${db}"."@FSQR_ACC_RIGHTS" WHERE "U_role_id" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findActionById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code","U_module_id","U_description"
      FROM "${db}"."@FSQR_ACTIONS" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getDepartment: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code","U_name","U_status"
      FROM "${db}"."@FSQR_DEPART" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  // employees use cases
  listAllEmployees: async () => {
    try {
      const db = process.env.DB;
      const rev = process.env.REVIVE_DB;

      const sql = `SELECT b."lastName",b."firstName",a.* FROM 
      "${db}"."@FSQR_USER" a LEFT JOIN
      "${rev}"."OHEM" b ON b."empID" = a."U_APP_EMP_ID";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findRoleById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ROLES" WHERE "Code" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  empIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_USER" WHERE "U_employee_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  empCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_USER" WHERE "U_APP_EMP_ID" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  empMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_USER";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  empAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_USER`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  empCheckExist: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_USER" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  returnCode: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@FSQR_USER" WHERE "U_employee_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  empIdUpdateCheck: async (id, code) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_USER" WHERE "U_employee_id" = '${id}' AND "Code" <> '${code}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  empUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_USER('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  // list SAP employees
  sapEmployees: async () => {
    try {
      const bfi = process.env.REVIVE_DB;

      const sql = `select a."empID", a."lastName", a."middleName", a."firstName", a."userId" 
      from "${bfi}"."OHEM" a;`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
};

module.exports = { emp };
