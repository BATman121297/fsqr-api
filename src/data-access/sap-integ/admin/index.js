const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const { client } = require("../sap-client/index");

// ############

// base url for sap
const url = process.env.SAP_URL;

const admin = {
  listAllModules: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_MODULES";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  modulesMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_MODULES";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  modulesDescCheck: async desc => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_MODULES" 
      WHERE LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  modulesAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_MODULES`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  modulesUpdateCheck: async (desc, id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_MODULES" 
      WHERE LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  modulesUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_MODULES('${code}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  // done modules
  listAllActions: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT a.*,
      b."Code" AS "moduleId",
      b."U_description" AS "moduleDesc",
      b."U_created_by" AS "moduleCreateBy",
      b."U_updated_by" AS "moduleUpdateBy",
      b."U_status" AS "moduleStatus",
      b."U_created_date" AS "moduleCreatedDate",
      b."U_updated_date" AS "moduleUpdateDate"
      FROM "${db}"."@FSQR_ACTIONS" a
      LEFT JOIN "${db}"."@FSQR_MODULES" b
      ON b."Code" = a."U_module_id";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findActionsByRole: async id => {
    try {
      const db = process.env.DB;

      const sql = `SELECT DISTINCT a."U_role_id",a."U_action_id",b."U_description"
      FROM 
      "${db}"."@FSQR_ACC_RIGHTS" a LEFT JOIN "${db}"."@FSQR_ACTIONS" b
      ON b."Code" = a."U_action_id" LEFT JOIN "${db}"."@FSQR_ROLES" c
      ON c."Code" = a."U_role_id" WHERE a."U_role_id" = '${id}';`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  actionsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_ACTIONS";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  actionsDescCheck: async desc => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ACTIONS" 
      WHERE LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  actionsAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_ACTIONS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  actionsUpdateCheck: async (desc, id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ACTIONS" 
      WHERE LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  actionsUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_ACTIONS('${code}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  // done actions
  listAllAccessRights: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT a."Code",a."U_action_id",a."U_role_id",a."U_status",
      a."U_created_by",a."U_updated_by",a."U_created_date",a."U_created_time",
      a."U_updated_date",a."U_updated_time",c."Code" AS "roleId",c."U_name" AS "roleName",
      b."Code" AS "actionId",b."U_description" AS "actionDesc"
      FROM "${db}"."@FSQR_ACC_RIGHTS" a LEFT JOIN "${db}"."@FSQR_ACTIONS" b
      ON b."Code" = a."U_action_id" LEFT JOIN "${db}"."@FSQR_ROLES" c 
      ON c."Code" = a."U_role_id";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  accessRightsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_ACC_RIGHTS";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  accessRightsAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_ACC_RIGHTS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  accessRightCheckExist: async ({ info }) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ACC_RIGHTS"
      WHERE "U_role_id" = ${info.role_id} AND "U_action_id" = ${info.action_id};`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  accessRightUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_ACC_RIGHTS('${code}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  // done access rights
  listAllRoles: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ROLES" ORDER BY "U_name";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rolesMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_ROLES";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  roleNameCheck: async names => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ROLES" 
      WHERE LOWER("U_name") = LOWER('${names}');`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  roleAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_ROLES`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  roleIdCheck: async id => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ROLES" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  roleNameUpdateCheck: async (names, id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ROLES" 
      WHERE LOWER("U_name") = LOWER('${names}') AND "Code" <> '${id}';`;
      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rolesActions: async id => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code" 
      FROM "${db}"."@FSQR_ACC_RIGHTS"
      WHERE "U_role_id" = '${id}';`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rolesUpdate: async ({ info }) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.str, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error
      };
      return err;
    }
  },
  // done roles
  listAllDepartments: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEPART";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  deptNameCheck: async names => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEPART"
      WHERE LOWER("U_name") = LOWER('${names}');`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  deptMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_DEPART";`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  deptAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_DEPART`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  deptUpdateCheck: async (names, id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEPART" 
      WHERE LOWER("U_name") = LOWER('${names}') AND "Code" <> '${id}';`;
      const result = await new Promise(resolve => {
        client.connect(function(err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function(err, rows) {
            resolve(rows);
            // client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  deptUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_DEPART('${code}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error
      };
      return err;
    }
  }
};

module.exports = { admin };
