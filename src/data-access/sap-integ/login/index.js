const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

const logins = {
  // login SL credentials;
  userLogin: async () => {
    try {
      const data = {
        CompanyDB: process.env.DB,
        Password: process.env.FSQR_PW,
        UserName: process.env.FSQR_USER
      };
      const res = await axios.post(`${url}/Login`, data, {
        headers: {
          "Content-Type": "application/json"
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const d = {
        status: res.status,
        msg: res.statusText,
        data: res.data.SessionId
      };
      return d;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  }
};

module.exports = { logins };
