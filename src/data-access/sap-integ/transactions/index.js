const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
const moment = require("moment-timezone");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";
const { axiosRequest } = require("../../../axios/axios");

const { client } = require("../sap-client/index");

// ############

// base url for sap
const url = process.env.SAP_URL;

const trns = {
  findQR: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_TRNS"
      WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });

      console.log(result);
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateEntry: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRNS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  updateExit: async ({ info }) => {
    try {
      console.log("UpdateEXIT");
      console.log({ info });
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRNS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      console.log(res.status);

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listExit: async () => {
    try {
      const db = process.env.DB;

      const date_today = moment().tz("Asia/Manila").format("YYYY-MM-DD");

      console.log(date_today);

      const sql = `SELECT a."Code",a."U_status_id",a."U_trckpltno",a."U_driver", b."U_name"
      FROM
      "${db}"."@FSQR_TRNS" a
      INNER JOIN "${db}"."@FSQR_STATUS" b ON b."Code" =  a."U_status_id" where
       b."U_name" = 'For Outbound' and a."U_created_date" = '${date_today}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listEntry: async () => {
    try {
      const db = process.env.DB;

      const date_today = moment().tz("Asia/Manila").format("YYYY-MM-DD");

      console.log(date_today);

      const sql = `SELECT a."Code",a."U_status_id",a."U_trckpltno",a."U_driver", b."U_name"
      FROM
      "${db}"."@FSQR_TRNS" a
      INNER JOIN "${db}"."@FSQR_STATUS" b ON b."Code" =  a."U_status_id" where
       b."U_name" = 'Analysis-Approved' and a."U_created_date" = '${date_today}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      console.log(result);
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  // warehouse
  listWarehouse: async () => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT "WhsCode","WhsName" FROM "${db}"."OWHS"`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  deleteWarehouse: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "DELETE",
        url: `${url}/U_FSQR_BUS_ENTITY('${code}')`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // bir
  listBIR: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_BIR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findBusinessEntityById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_BUS_ENTITY" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findItemById: async (id) => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT * FROM "${db}"."OITM" WHERE "ItemCode" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findBIRById: async (id) => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT "ItemCode","ItemName","U_APP_BU_Feedmill","ItmsGrpCod",
      "U_APP_BU_Paddy","U_APP_BU_CDF" FROM "${db}"."OITM" WHERE "ItemCode" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findRawMaterialsById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RAW_MAT" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  returnBUId: async (bu) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@FSQR_BUS_ENTITY"
      WHERE LOWER("U_description") = LOWER('${bu}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  returnRawMatsId: async (rawMat) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@FSQR_RAW_MAT" 
      WHERE LOWER("U_description") = LOWER('${rawMat}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  birMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_BIR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addBIR: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_BIR`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  findItemsByBusinessEntity: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code","U_item_id","U_business_entity_id","U_raw_material_id"
      FROM "${db}"."@FSQR_BIR" WHERE "U_business_entity_id" = ${id};`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findItemsByBusinessEntityFEEDMILL: async (BUname) => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",a."ItmsGrpCod", b."WhsCode", c."WhsName",d."ItmsGrpNam"
      FROM "${db}"."OITM" a
      INNER JOIN "${db}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${db}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      INNER JOIN "${db}"."OITB" d ON a."ItmsGrpCod" = d."ItmsGrpCod"
      WHERE c."WhsName" = '${BUname}' and (a."ItmsGrpCod" = '314' or a."ItmsGrpCod" = '313')`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findItemsByBusinessEntityRICE: async (BUname) => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",a."ItmsGrpCod", b."WhsCode", c."WhsName",d."ItmsGrpNam"
      FROM "${db}"."OITM" a
      INNER JOIN "${db}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${db}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      INNER JOIN "${db}"."OITB" d ON a."ItmsGrpCod" = d."ItmsGrpCod"
      WHERE c."WhsName" = '${BUname}' and a."ItmsGrpCod" = '310'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findItemsByBusinessEntityCORN: async (BUname) => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",a."ItemName",a."ItmsGrpCod", b."WhsCode", c."WhsName",d."ItmsGrpNam"
      FROM "${db}"."OITM" a
      INNER JOIN "${db}"."OITW" b ON a."ItemCode" = b."ItemCode"
      INNER JOIN "${db}"."OWHS" c ON b."WhsCode" = c."WhsCode"
      INNER JOIN "${db}"."OITB" d ON a."ItmsGrpCod" = d."ItmsGrpCod"
      WHERE c."WhsName" = '${BUname}' and a."ItmsGrpCod" = '313'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateBIR: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_BIR('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // business entity
  listBusinessEntity: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_BUS_ENTITY" ORDER BY "U_description";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      console.log(result);
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  busEntityDescCheck: async (desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_BUS_ENTITY" 
      WHERE LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  busEntityMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_BUS_ENTITY";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addBusEntity: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_BUS_ENTITY`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  busEntityIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_BUS_ENTITY" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  busEntityDescCheckUpdate: async (id, desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_BUS_ENTITY" 
      WHERE LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updatebusEntity: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_BUS_ENTITY('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // chemical analysis
  listChemicalAnalysis: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT c.*,
      (
      SELECT string_agg(b."U_disposition_id",', ') FROM 
      "${db}"."@FSQR_CHEM_ANA" a LEFT JOIN "${db}"."@FSQR_CAD" b
      ON b."U_chemical_analysis_id" = a."Code" WHERE a."Code" = c."Code"
      ) AS disposition
      FROM "${db}"."@FSQR_CHEM_ANA" c;`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findReconsiderationById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RECON" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findDispositionById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DISPO" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  chemAnalysisMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_CHEM_ANA";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  chemAnalysisDispoMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_CAD";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  chemAnalysisAndDispoInsert: async ({ info }) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.str, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  chemAnalysisUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_CHEM_ANA('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  reconsiderationUpdate: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_RECON('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // criteria
  criteriaDescCheck: async (desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CRITERIA" WHERE 
      LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  criteriaMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_CRITERIA";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addCriteria: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_CRITERIA`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listCriteria: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CRITERIA" ORDER BY "U_description";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listCriteriaByBE: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CRITERIA" 
      WHERE "U_business_entity" = '${id}' ORDER BY "U_description";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  criteriaIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CRITERIA" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  criteriaDescCheckUpdate: async (id, desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CRITERIA" WHERE 
      LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateCriteria: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_CRITERIA('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // deduction criteria
  addDeductionCriteria: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_DED_CRIT`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  deductionCriteriaMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_DED_CRIT";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listDeductionCriteria: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DED_CRIT";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findCriteriaById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CRITERIA" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findDeductionReceiptById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEDUC_RT" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateDeductionCriteria: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_DED_CRIT('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // deduction receipt
  deductionReceiptMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_DEDUC_RT";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addDeductionReceipt: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_DEDUC_RT`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  findDeductionReceiptSumByTransId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COUNT(*) AS counts, SUM("U_total_deductions") AS total_deductions
      FROM "${db}"."@FSQR_DEDUC_RT" WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findDeductionReceiptByTransId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEDUC_RT" WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listDeductionReceipts: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEDUC_RT";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listCriteriaOnDC: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT a."U_criteria_id",b."U_description",b."U_item_code"
      FROM "${db}"."@FSQR_DED_CRIT" a, "${db}"."@FSQR_CRITERIA" b
      WHERE a."U_deduction_receipt_id" IN (SELECT "Code" FROM "${db}"."@FSQR_DEDUC_RT"
      WHERE "U_transaction_id" = '${id}') AND a."U_criteria_id" = b."Code"
      GROUP BY a."U_criteria_id",b."U_description",b."U_item_code";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  totalDeductionPerCriteria: async (transactionId, criteriaId) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT SUM("U_quantity") as qty, "U_deduction", SUM("U_total") as total
      FROM "${db}"."@FSQR_DED_CRIT" WHERE "U_deduction_receipt_id"
      IN (SELECT "Code" FROM "${db}"."@FSQR_DEDUC_RT" WHERE "U_transaction_id" = '${transactionId}')
      AND "U_criteria_id" = '${criteriaId}' GROUP BY "U_deduction";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findRejectionReceiptByTranId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RR" WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findDeductionCriteriaByDRId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DED_CRIT" 
      WHERE "U_deduction_receipt_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateDeductionReceipt: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_DEDUC_RT('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // delivery receipt
  delReceiptMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_DR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addDelReceipt: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_DR`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listDeliveryReceipts: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DR" ORDER BY "U_description_of_articles";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateDr: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_DR('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // disposition
  dispositionDescCheck: async (desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DISPO" WHERE 
      LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  dispositionMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_DISPO";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addDisposition: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_DISPO`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listDisposition: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DISPO";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  dispositionIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DISPO" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  dispositionDescCheckUpdate: async (id, desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DISPO" WHERE 
      LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateDisposition: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_DISPO('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // employee
  checkPinCode: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "U_pin_code" FROM "${db}"."@FSQR_USER" 
      WHERE "U_employee_id"='${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  // items
  itemsDescCheck: async (desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ITEMS" WHERE 
      LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  itemsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_ITEMS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addItem: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_ITEMS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listItem: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ITEMS" ORDER BY "U_description";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  itemsIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ITEMS" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  itemsDescCheckUpdate: async (id, desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_ITEMS" WHERE 
      LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateItem: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_ITEMS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // mrr
  mrrMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_MRR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addMrr: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_MRR`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listMRR: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_MRR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateMrr: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_MRR('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // MTS
  mtsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_MTS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addMTS: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_MTS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  selectMTS: async (id) => {
    try {
      const db = process.env.DB;
      let sql;
      if (id) {
        // select one
        sql = `SELECT * FROM "${db}"."@FSQR_MTS" WHERE "Code" = '${id}';`;
      } else {
        // select all
        sql = `SELECT * FROM "${db}"."@FSQR_MTS";`;
      }

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateMTS: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_MTS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // Physical Analysis
  paMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_PHYANA";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  physicalAnalysisBatch: async ({ info }) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.str, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  paRemarksMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_PAR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  paDispositionMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_PAD";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  listPhysicalAnalysis: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PHYANA";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findPriceReductionById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PRICE_RED"
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findMTSById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_MTS"
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getDisposition: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PAD"
      WHERE "U_physical_analysis_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updatePA: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_PHYANA('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  getRemarks: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PAR"
      WHERE "U_physical_analysis_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  // price reduction
  priceReductionMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_PRICE_RED";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addPriceReduction: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_PRICE_RED`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listPriceReduction: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PRICE_RED" ORDER BY "U_remarks";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  priceReductionCheckExist: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PRICE_RED"
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updatePR: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_PRICE_RED('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // raw materials
  rawMatsDescCheck: async (desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RAW_MAT" WHERE 
      LOWER("U_description") = LOWER('${desc}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rawMatsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_RAW_MAT";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addRawMats: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_RAW_MAT`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listRawMats: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RAW_MAT" ORDER BY "U_description";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rawMatsIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RAW_MAT" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rawMatsDescCheckUpdate: async (id, desc) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RAW_MAT" WHERE 
      LOWER("U_description") = LOWER('${desc}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateRawMats: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_RAW_MAT('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // reconsideration
  reconMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_RECON";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addRecon: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_RECON`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listRecon: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RECON";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  reconIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RECON" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  // rejection receipt
  rejReceiptMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_RR";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addRejectReceipt: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_RR`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listRejectionReceipt: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RR" ORDER BY "U_created_date" DESC;`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findTransactionById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_TRNS" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rejReceiptIdCheck: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RR" 
      WHERE "Code" = '${id}'`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateRejReceipt: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_RR('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // remarks
  remarksMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_REMARKS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  remarksNameCheck: async (names) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_REMARKS" WHERE 
      LOWER("U_name") = LOWER('${names}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addRemarks: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_REMARKS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listRemarks: async (id) => {
    try {
      const db = process.env.DB;

      let sql;
      if (id) {
        // select one
        sql = `SELECT * FROM "${db}"."@FSQR_REMARKS" WHERE "Code" = '${id}';`;
      } else {
        // select all
        sql = `SELECT * FROM "${db}"."@FSQR_REMARKS";`;
      }

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  remarksCheckUpdate: async (id, names) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_REMARKS" WHERE 
      LOWER("U_name") = LOWER('${names}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateRemarks: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_REMARKS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // RMAF
  rmafMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_RMAF";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addRMAF: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_RMAF`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listRMAF: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RMAF";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findPhysicalAnalysisById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PHYANA" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findChemicalAnalysisById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CHEM_ANA" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findMRRById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_MRR" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateRMAF: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_RMAF('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // Status
  statusMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_STATUS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  statusNameCheck: async (names) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_STATUS" WHERE 
      LOWER("U_name") = LOWER('${names}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addStatus: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_STATUS`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listStatus: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_STATUS" ORDER BY "U_name";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  checkStatusId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_STATUS" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  statusCheckUpdate: async (id, names) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_STATUS" WHERE 
      LOWER("U_name") = LOWER('${names}') AND "Code" <> '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateStatus: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_STATUS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // transactions
  returnItemDesc: async (id) => {
    try {
      const db = process.env.DBPOST;

      const sql = `SELECT "ItemName" FROM "${db}"."OITM" WHERE "ItemCode" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  countTransaction: async (dates) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT CAST(COUNT(*) AS INTEGER) AS "counts" FROM "${db}"."@FSQR_TRNS" WHERE
      "U_created_date" LIKE '${dates}%';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  transactionsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_TRNS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  transactionsMicroDetailsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_MICRO_DETAILS";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getAllItemsInMicro: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "U_itemCode" FROM "${db}"."@FSQR_MICRO_DETAILS" 
      WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  transactionLogsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_TRANS_LOG";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  transactionInsert: async ({ info }) => {
    try {
      const res = await axios.post(`${url}/$batch`, info.str, {
        headers: {
          "Content-Type": "multipart/mixed;boundary=a",
          Cookie: `${info.cookie}`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });
      const data = {
        status: res.status,
        msg: res.statusText,
        response: res.data,
      };
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  listTransaction: async ({ info }) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_TRNS"
      WHERE "U_created_date" BETWEEN '${info.date_from}' AND '${info.date_to}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        response: e.response.data.error,
      };
      return err;
    }
  },
  truckScaleSAPLogin: async () => {
    try {
      const data = {
        CompanyDB: process.env.TSIS_DB,
        Password: process.env.PW,
        UserName: process.env.USERS,
      };
      const res = await axios.post(`${url}/Login`, data, {
        headers: {
          "Content-Type": "application/json",
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const d = {
        status: res.status,
        msg: res.statusText,
        data: res.data.SessionId,
      };
      return d;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  bfiSAPLogin: async () => {
    try {
      const data = {
        CompanyDB: process.env.DBPOST,
        Password: process.env.PW,
        UserName: process.env.USERS,
      };
      const res = await axios.post(`${url}/Login`, data, {
        headers: {
          "Content-Type": "application/json",
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const d = {
        status: res.status,
        msg: res.statusText,
        data: res.data.SessionId,
      };
      return d;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listToPOStatus: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "Code" FROM "${db}"."@FSQR_STATUS" WHERE LOWER("U_name") IN
      ('analysis-approved', 'quality control', 'quality control-passed', 'completed',
      'for reconsideration', 'unloading', 'unloaded', 'for outbound');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  truckScaleGetNetWeight: async ({ info }) => {
    try {
      const tsisUrl = process.env.TSIS_URL;
      const code = info.code;

      const res = await axios({
        method: "GET",
        url: `${tsisUrl}/transactions/grpo/${code}`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.view,
      };
      return req;
    } catch (e) {
      const err = {
        status: 400,
        msg: "Error get net weight tsis route.",
      };
      return err;
    }
  },
  getPOdetailsBFI: async (SessionId, DocEntry) => {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/PurchaseOrders?$filter=DocumentStatus eq 'bost_Open' and DocEntry eq ${DocEntry}`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`,
      },
    }).catch((err) => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  },
  getDeductionReceiptsWithCriteria: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT a."U_description",b."U_criteria_id",b."U_deduction_receipt_id",b."U_quantity",
      b."U_deduction",b."U_total",b."Code" AS dedCriteriaID,c."Code" AS dedReceiptID,
      c."U_transaction_id",c."U_created_date",c."U_prepared_by",c."U_total_deductions"
      FROM "${db}"."@FSQR_CRITERIA" a
      LEFT JOIN "${db}"."@FSQR_DED_CRIT" b ON b."U_criteria_id" = a."Code"
      LEFT JOIN "${db}"."@FSQR_DEDUC_RT" c ON b."U_deduction_receipt_id" = c."Code"
      WHERE c."U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findDeliveryReceiptByID: async (id) => {
    try {
      const db = process.env.DB;
      // crash?
      const sql = `SELECT * FROM "${db}"."@FSQR_DR" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findRMAFById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_RMAF" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getRemarkPA: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PAR" WHERE "U_physical_analysis_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getDispoPA: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_PAD" WHERE "U_physical_analysis_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getDispoCA: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_CAD" WHERE "U_chemical_analysis_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getDepartment: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_DEPART" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findStatusById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_STATUS" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findTransactionTypeById: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_TRANS_TYPE" WHERE "Code" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  countDeductionReceiptByTranId: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COUNT(*) AS counts FROM "${db}"."@FSQR_DEDUC_RT"
      WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  findTransactionBySupplierCode: async (code) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_TRNS" 
      WHERE LOWER("U_suppl_code") = LOWER('${code}');`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateTransaction: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRNS('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  countCue: async (dates) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT CAST(COUNT(*) AS INTEGER) AS "counts" FROM "${db}"."@FSQR_TRNS" WHERE
      "U_created_date" LIKE '${dates}%';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getMicroPackages: async (id) => {
    try {
      const db = process.env.DB;

      const sql = `SELECT "U_packaged_no","U_expiry_date","U_quantity","U_remarks"
      FROM "${db}"."@FSQR_MICRO" WHERE "U_transaction_id" = '${id}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  getPrimaryAlternativeUnit: async (code) => {
    try {
      const bfi = process.env.DBPOST;

      const sql = `SELECT a."ItemCode",c."UomName" AS unit,b."BaseQty",a."PUoMEntry" 
      as primary_unit,b."UomEntry" FROM "${bfi}"."OITM" a 
      LEFT JOIN "${bfi}"."UGP1" b ON b."UgpEntry" = a."UgpEntry"
      LEFT JOIN "${bfi}"."OUOM" c ON c."UomEntry" = b."UomEntry"
      WHERE a."ItemCode" = '${code}';`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  // transaction logs
  trnsLogsMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_TRANS_LOG";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addtrnsLogs: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_TRANS_LOG`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listTransactionStatusLogs: async (id) => {
    try {
      const db = process.env.DB;
      let sql;
      if (id) {
        // select one by transaction id
        sql = `SELECT * FROM "${db}"."@FSQR_TRANS_LOG" WHERE "U_transaction_id" = '${id}';`;
      } else {
        // select all
        sql = `SELECT * FROM "${db}"."@FSQR_TRANS_LOG" ORDER BY "U_created_date";`;
      }

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateTrnsLogs: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRANS_LOG('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  // transaction type
  trnsTypeMaxCode: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@FSQR_TRANS_TYPE";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  addTrnsType: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_FSQR_TRANS_TYPE`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
  listTransactionTypes: async () => {
    try {
      const db = process.env.DB;

      const sql = `SELECT * FROM "${db}"."@FSQR_TRANS_TYPE" ORDER BY "U_name";`;

      const result = await new Promise((resolve) => {
        client.connect(function (err) {
          /*if (err) {
            console.log("Connect error", err);
          }*/
          client.exec(sql, function (err, rows) {
            resolve(rows);
            //client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  updateTrnsType: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const code = info.Code;
      delete info.Code;

      const res = await axios({
        method: "PATCH",
        url: `${url}/U_FSQR_TRANS_TYPE('${code}')`,
        data: {
          ...info,
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`,
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      });

      const req = {
        status: res.status,
        msg: res.statusText,
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data,
      };
      return err;
    }
  },
};

module.exports = { trns };
