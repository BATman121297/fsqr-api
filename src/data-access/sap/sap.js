const axios = require("axios");

module.exports = function adminDB({ axiosRequest }, { moment }, { https }) {
  return Object.freeze({
    login,

    getSuppliers,
    getPOSupplier,
    getPOCardCode,
    getPO,

    getItemPriceList,
    getItemManage,

    getNetWeight,

    endorseGRPO,
    addAPCredit
  });

  async function login(login_info) {
    const { CompanyDB, Password, UserName } = login_info;
    const res = await axios({
      method: "POST",
      url: `https://18.136.35.41:50000/b1s/v1/Login`,
      data: {
        CompanyDB,
        Password,
        UserName
      },
      headers: {
        "Content-Type": "application/json"
      },
      httpsAgent: new https.Agent({ rejectUnauthorized: false })
    });
    const data = res.data;
    return data;
  }

  async function getSuppliers(SessionId) {
    let suppliers = [];

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/BusinessPartners?$select=CardCode,CardName`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    const query2 = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/BusinessPartners?$select=CardCode,CardName&$skip=300`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    const query3 = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/BusinessPartners?$select=CardCode,CardName&$skip=600`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    const query4 = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/BusinessPartners?$select=CardCode,CardName&$skip=900`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    let supA = query.data.value,
      supB = query2.data.value,
      supC = query3.data.value,
      supD = query4.data.value;

    const supAB = supA.concat(supB);
    const supCD = supC.concat(supD);

    suppliers = supAB.concat(supCD);

    return suppliers;
  }

  async function getItemPriceList(SessionId, ItemCode){
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/Items?$select=ItemPrices&$filter=ItemCode eq '${ItemCode}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  async function getItemManage(SessionId, ItemCode){
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/Items?$select=ItemCode,ManageBatchNumbers&$filter=ManageBatchNumbers eq 'tYES' and ItemCode eq '${ItemCode}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    console.log(query.data.value)

    return query.data.value;
  }

  async function getPOSupplier(SessionId, CardName) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/PurchaseOrders?$select=DocEntry,DocNum,CardName,Address,U_APP_Farmer,DocumentLines&$filter=DocumentStatus eq 'bost_Open' and CardName eq '${CardName}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  async function getPOCardCode(SessionId, DocEntry) {
    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/PurchaseOrders?$filter=DocumentStatus eq 'bost_Open' and DocEntry eq ${DocEntry}`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  async function getPO(SessionId, request_info) {
    const {
      date_from,
      date_to
    } = request_info

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/PurchaseOrders?$filter=DocumentStatus eq 'bost_Open'`,
      //  and DocDate lt '${date_to}' and DocDate  gt '${date_from}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  async function getNetWeight(SessionId, supplier_code) {
    // const res = await axios({
    //   method: "GET",
    //   url: `/sml.svc/V_TRANS?$filter=U_TS_TRCK_CODE eq '${supplier_code}'`,
    //   data: {
    //     user: "fsqr",
    //     pw: "superpw64"
    //   },
    //   headers: {
    //     "Content-Type": "application/json",
    //     Authorization: `B1SESSION=${SessionId}`
    //   }
    // }).catch(err => {
    //   if (err.response) {
    //     throw new Error(err.response.data.error);
    //   } else {
    //     return [];
    //   }
    // });
    // const data = res.data;
    // return data;

    const query = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/sml.svc/V_TRANS?$filter=U_TS_TRCK_CODE eq '${supplier_code}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else throw new Error(err.response.data);
    });

    return query.data.value;
  }

  async function endorseGRPO(SessionId, request_info) {
    const {
      CardCode,
      U_APP_PlateNo,
      U_APP_TruckModel,
      U_APP_Driver,
      DocumentLines
    } = request_info;

    //console.log(DocumentLines);

    // let codeMax = await axios({
    //   method: "GET",
    //   url: `https://18.136.35.41:50000/b1s/v1/PurchaseDeliveryNotes/$count`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   httpsAgent: new https.Agent({ rejectUnauthorized: false })
    // });

    // codeMax = codeMax.data;

    // let DocNum = parseInt(codeMax, 10) + 1;
    const res = await axios({
      method: "POST",
      url: `https://18.136.35.41:50000/b1s/v1/PurchaseDeliveryNotes`,
      data: {
        CardCode,
        U_APP_PlateNo,
        U_APP_TruckModel,
        U_APP_Driver,
        DocumentLines
      },
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      httpsAgent: new https.Agent({ rejectUnauthorized: false })
    }).catch(err => {
      console.log(err.response.data.error.message);
      throw new Error(err.response.data.error.message.value);
    });

    const data = res.data;
    return data;
  }

  async function addAPCredit(SessionId, request_info) {
    const {
      CardCode,
      CardName,
      DocObjectCode,
      U_APP_PlateNo,
      U_APP_TruckModel,
      U_APP_Driver,
      U_APP_FSQR_GRPORef,
      U_APP_Farmer,
      U_APP_WithDeductions,
      DocumentLines
    } = request_info;

    // let codeMax = await axios({
    //   method: "GET",
    //   url: `https://18.136.35.41:50000/b1s/v1/Drafts/$count`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   httpsAgent: new https.Agent({ rejectUnauthorized: false })
    // });

    // codeMax = codeMax.data;

    // let DocNum = parseInt(codeMax, 10) + 1;

    console.log(CardCode,
      CardName,
      DocObjectCode,
      U_APP_PlateNo,
      U_APP_TruckModel,
      U_APP_Driver,
      U_APP_FSQR_GRPORef,
      U_APP_Farmer,
      U_APP_WithDeductions,
      DocumentLines);

    const res = await axios({
      method: "POST",
      url: `https://18.136.35.41:50000/b1s/v1/Drafts`,
      data: {
        CardCode,
        CardName,
        DocObjectCode,
        U_APP_PlateNo,
        U_APP_TruckModel,
        U_APP_Driver,
        U_APP_FSQR_GRPORef,
        U_APP_Farmer,
        U_APP_WithDeductions,
        DocumentLines
      },
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      httpsAgent: new https.Agent({ rejectUnauthorized: false })
    }).catch(err => {
      throw new Error(err.response.data.error.message.value);
    });

    const data = res.data;

    return data;
  }
};
