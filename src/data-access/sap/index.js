const { axiosRequest } = require("../../axios/axios");
//Moment
const moment = require("moment-timezone");
const sap = require("./sap");

require("tls").DEFAULT_MIN_VERSION = "TLSv1";
var https = require("https");

const sap_db = sap({ axiosRequest }, { moment }, { https });

module.exports = sap_db;
