module.exports = function employeeDb({ pool }) {
  return Object.freeze({
    findById,
    updateToken,
    checkToken,
    checkPinCode,
    // Modules
    listAllModules,
    findModuleById,
    // Access Rights
    findAccessRightByRoleId,
    // Actions
    findActionById,
    // Roles
    findRoleById,
    getDepartment
  });

  async function getDepartment(department_id) {
    return pool.query(`SELECT * FROM departments WHERE id = $1`, [
      department_id
    ]);
  }

  async function findById(employee_id) {
    return pool.query(
      `SELECT
      "public".employees.name,
      "public".employees.employee_id,
      "public".employees."password",
      "public".employees."pin_code",
      "public".employees.status,
      "public".roles."id" as role_id,
      "public".roles."name" as role_name,
      "public".roles.status as role_status,
      "public".employees.department as department
      FROM
      "public".employees
      INNER JOIN "public".roles ON "public".employees.role_id = "public".roles."id"
      WHERE
      "public".employees.employee_id = $1`,
      [employee_id]
    );
  }

  async function updateToken(token, employee_id) {
    return pool.query(
      `UPDATE
      employees SET token = $1
       WHERE employee_id = $2`,
      [token, employee_id]
    );
  }

  async function checkToken(employee_id, token) {
    return pool.query(
      `SELECT
      "public".employees.employee_id,
      "public".employees.token
      FROM
      "public".employees
      WHERE
      "public".employees.employee_id = $1 AND
      "public".employees.token = $2
      `,
      [employee_id, token]
    );
  }

  async function checkPinCode(employee_id) {
    return pool.query(
      `SELECT
      "public".employees.pin_code
      FROM
      "public".employees
      WHERE "public".employees.employee_id = $1`,
      [employee_id]
    );
  }

  async function findRoleById(id) {
    return await pool.query(
      `SELECT id, name, status FROM roles WHERE id = $1`,
      [id]
    );
  }

  async function findModuleById(id) {
    return await modules.findOne(`SELECT id FROM modules WHERE id = $1`, [id]);
  }

  async function findActionById(id) {
    return await pool.query(
      `SELECT id,
      module_id,
      description 
      FROM actions 
      WHERE id = $1`,
      [id]
    );
  }

  async function findAccessRightByRoleId(role_id) {
    return pool.query(
      `SELECT id,
                      action_id,
                      status
                      FROM access_rights 
                      WHERE role_id = $1`,
      [role_id]
    );
  }

  async function listAllModules() {
    return pool.query(`SELECT
        "public".modules."id",
        "public".modules.description,
        "public".modules.status
        FROM
        "public".modules
        `);
  }
};
