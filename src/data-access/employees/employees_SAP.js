module.exports = function employeeDb({ ...axios, moment }) {
    return Object.freeze({
        findById,
        updateToken,
        checkToken,
        checkPinCode,
        // Modules
        listAllModules,
        findModuleById,
        // Access Rights
        findAccessRightByRoleId,
        // Actions
        findActionById,
        // Roles
        findRoleById,
        getDepartment
    });

    // async function getDepartment(department_id) {
    //     return pool.query(`SELECT * FROM departments WHERE id = $1`, [
    //         department_id
    //     ]);
    // }

    async function getDepartment(department_id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_DEPART?$filter=Code eq '${department_id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;

    }

    // async function findById(employee_id) {
    //     return pool.query(
    //     `SELECT
    // "public".employees.name,
    // "public".employees.employee_id,
    // "public".employees."password",
    // "public".employees.status,
    // "public".roles."id" as role_id,
    // "public".roles."name" as role_name,
    // "public".roles.status as role_status,
    // "public".employees.department as department
    // FROM
    // "public".employees
    // INNER JOIN "public".roles ON "public".employees.role_id = "public".roles."id"
    // WHERE
    // "public".employees.employee_id = $1`,
    //         [employee_id]
    //     );
    // }

    async function findById(employee_id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/$crossjoin(U_BFI_FSQR_USER,U_BFI_FSQR_ROLES)?$expand=U_BFI_FSQR_USER($select=Name,U_FSQR_employeeID,U_FSQR_password,U_FSQR_status,U_FSQR_departID),U_BFI_FSQR_ROLES($select=Code,Name,U_FSQR_status)&$filter=U_BFI_FSQR_USER/U_FSQR_roleID eq U_BFI_FSQR_ROLES/Code and U_BFI_FSQR_USER/U_FSQR_employeeID eq '${employee_id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;
    }

    // async function updateToken(token, employee_id) {
    //     return pool.query(
    //         `UPDATE
    //     employees SET token = $1
    //      WHERE employee_id = $2`,
    //         [token, employee_id]
    //     );
    // }

    // async function checkToken(employee_id, token) {
    //     return pool.query(
    //         `SELECT
    //     "public".employees.employee_id,
    //     "public".employees.token
    //     FROM
    //     "public".employees
    //     WHERE
    //     "public".employees.employee_id = $1 AND
    //     "public".employees.token = $2
    //     `,
    //         [employee_id, token]
    //     );
    // }

    // async function checkPinCode(employee_id) {
    //     return pool.query(
    //         `SELECT
    //     "public".employees.pin_code
    //     FROM
    //     "public".employees
    //     WHERE "public".employees.employee_id = $1`,
    //         [employee_id]
    //     );
    // }

    async function checkPinCode(employee_id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_USER?$select=U_FSQR_pincode&$filter=U_FSQR_employeeID eq '${employee_id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;
    }

    // async function findRoleById(id) {
    //     return await pool.query(
    //         `SELECT id, name, status FROM roles WHERE id = $1`,
    //         [id]
    //     );
    // }

    async function findRoleById(id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_ROLES?$select=Code,Name&$filter=Code eq '${id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;

    }

    // async function findModuleById(id) {
    //     return await modules.findOne(`SELECT id FROM modules WHERE id = $1`, [id]);
    // }

    async function findModuleById(id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_MODULES?$select=Code&$filter=Code eq '${id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;

    }

    // async function findActionById(id) {
    //     return await pool.query(
    //         `SELECT id,
    //     module_id,
    //     description 
    //     FROM actions 
    //     WHERE id = $1`,
    //         [id]
    //     );
    // }

    async function findActionById(id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_ACTIONS?$select=Code,U_FSQR_modID,U_FSQR_desc&$filter=Code eq '${id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;
    }

    // async function findAccessRightByRoleId(role_id) {
    //     return pool.query(
    //       `SELECT id,
    //                       action_id,
    //                       status
    //                       FROM access_rights 
    //                       WHERE role_id = $1`,
    //       [role_id]
    //     );
    //   }

    async function findAccessRightByRoleId(role_id, SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_ACC_RIGHTS?$select=U_FSQR_actionID,U_FSQR_status&$filter=U_FSQR_roleID eq '${role_id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;

    }

    // async function listAllModules() {
    //     return pool.query(`SELECT
    //       "public".modules."id",
    //       "public".modules.description,
    //       "public".modules.status
    //       FROM
    //       "public".modules
    //       `);
    // }

    async function listAllModules(SessionId) {

        const query = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_BFI_FSQR_MODULES?$select=Code,U_FSQR_desc,U_FSQR_status`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).catch(err => {
            if (err.response.data.error)
                throw new Error(err.response.data.error.message.value);
            else throw new Error(err.response.data);
        });

        return query.data.value;
        
    }
};