--a
Content-Type: multipart/mixed;boundary=b

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserTablesMD

{
    "TableName": "BFI_FSQR_RR",
    "TableDescription": "BFIFSQRBIR",
    "TableType": "bott_NoObject"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_bags_rej",
    "Type": "db_Numeric",
    "Description": "FSQRBAGSREJECTED",
    "TableName": "@BFI_FSQR_RR"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_reason",
    "Type": "db_Alpha",
    "Description": "FSQRREASON",
    "TableName": "@BFI_FSQR_RR"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_date",
    "Type": "db_Date",
    "Description": "FSQRDATE", 
    "TableName": "@BFI_FSQR_RR"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_created_by",
    "Type": "db_Numeric",
    "Description": "FSQRCREATEDBY",
    "TableName": "@BFI_FSQR_RR"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_updated_by",
    "Type": "db_Numeric",
    "Description": "FSQRUPDATEDBY",
    "TableName": "@BFI_FSQR_RR"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_created_at",
    "Type": "db_Date",
    "Description": "FSQRCREATEDAT", 
    "TableName": "@BFI_FSQR_RR"
}

--b
Content-Type:application/http
Content-Transfer-Encoding:binary

POST /b1s/v1/UserFieldsMD

{
    "Name": "FSQR_updated_at",
    "Type": "db_Date",
    "Description": "FSQRUPDATEDAT",
    "TableName": "@BFI_FSQR_RR"
}

--b--

--a--