const express = require("express");
const router = express.Router();

//Express-callback
const makeCallback = require("../../express-callback");

//Authorization
const getAuthorization = require("../../token/index").getAuthorization;

//Uploading
const { upload } = require("../../upload/");``

// Controllers
const {
  // SAP
  _getSuppliers,
  _getPOSuppliers,
  _getPOs,
  _getNetWeight,
  _postGRPO,

  // Delivery receipts
  getDReceipts,
  postDeliveryReceipt,
  putDeliveryReceipt,

  // Trucks
  getTruckInfos,
  getTruckTypes,
  postTruckInfos,
  postTruckTypes,
  putTruck,
  putTrucktype,

  // Transaction types
  getTransactionType,
  postTransactionType,
  putTransactionType,

  // Status
  getListStatus,
  postStatus,
  putStatus,

  // Driver
  getListDriver,
  postDriver,
  putDriver,

  // Business Entity
  getListBusinessEntity,
  postBusinessEntity,
  putBusinessEntity,

  getListWarehouse,
  deleteWarehouse,

  // Items
  getListItem,
  postItem,
  putItem,

  // Raw Materials
  getListRawMaterial,
  postRawMaterial,
  putRawMaterial,

  // BIR
  postBIR,
  putBIR,
  getListBIR,
  getListItemsBIR,

  // Criteria
  postCriteria,
  putCriteria,
  getListCriteria,

  // Deduction Criteria
  postDeductionCriteria,
  putDeductionCriteria,
  getListDeductionCriteria,

  // Price Reduction
  postPriceReduction,
  putPriceReduction,
  getListPriceReduction,

  // MTS
  postMTS,
  putMTS,
  getListMTS,

  // Reconsideration
  postReconsideration,
  putReconsideration,
  getListReconsideration,

  // Disposition
  postDisposition,
  putDisposition,
  getListDisposition,

  // Physical analysis
  postPhysicalAnalysis,
  putPhysicalAnalysis,
  getListPhysicalAnalysis,

  // Chemical Analysis
  postChemicalAnalysis,
  putChemicalAnalysis,
  getListChemicalAnalysis,

  // MRR
  postMRR,
  putMRR,
  getListMRR,

  // RMAF
  postQCCN,
  getListRMAF,
  postRMAF,
  putRMAF,

  // Transaction
  postTransaction,
  getListTransaction,
  putTransaction,
  getTransactionCueNumber,
  getFindTransaction,
  uploadSignature,

  // Rejection receipt
  postRejectionReceipt,
  getListRejectionReceipt,
  putRejectionReceipt,

  // Deduction receipt
  postDeductionReceipt,
  getListDeductionReceipt,
  putDeductionReceipt,

  // Transaction status logs
  postTransactionStatusLogs,
  getListTransactionStatusLogs,
  putTransactionStatusLogs,
  getFindTransactionStatusLogs,

  // Remarks
  postRemarks,
  getListRemarks,
  putRemarks,
  getFindRemarks,

  checkPincode,
  getTransactionEntry,
  getTransactionExit,
  putEntry,
  putExit,
  getFindQR,
} = require("../../controllers/transaction");

// ------------------------------- Routes ------------------------------

// Net weight
router.post("/sap/net_weight", makeCallback(_getNetWeight));

// SAP
router.post("/sap/suppliers", makeCallback(_getSuppliers));
router.post("/sap/po_suppliers", makeCallback(_getPOSuppliers));
router.post("/sap/po_micro", makeCallback(_getPOs));
router.post("/sap/endorse_grpo", makeCallback(_postGRPO));

// Employee
router.post("/employee/check_pincode", makeCallback(checkPincode));

// Delivery receipts
router.post("/deliveryreceipts", makeCallback(getDReceipts));
router.post(
  "/deliveryreceipts/add_deliveryreceipt",
  makeCallback(postDeliveryReceipt)
);
router.put(
  "/deliveryreceipts/update_deliveryreceipt",
  makeCallback(putDeliveryReceipt)
);

// Truck
router.post("/trucks", getAuthorization, makeCallback(getTruckInfos));
router.post(
  "/trucks/add_truck",
  getAuthorization,
  makeCallback(postTruckInfos)
);
router.put("/trucks/update_truck", getAuthorization, makeCallback(putTruck));

router.post("/trucktype", getAuthorization, makeCallback(getTruckTypes));
router.post(
  "/trucktype/add_trucktype",
  getAuthorization,
  makeCallback(postTruckTypes)
);
router.put(
  "/trucktype/update_trucktype",
  getAuthorization,
  makeCallback(putTrucktype)
);

// Transaction type
router.post("/transactiontypes", makeCallback(getTransactionType));
router.post(
  "/transactiontypes/add_transactiontype",
  makeCallback(postTransactionType)
);
router.put(
  "/transactiontypes/update_transactiontype",
  makeCallback(putTransactionType)
);

// Status
router.post("/status", makeCallback(getListStatus));
router.post("/status/add_status", makeCallback(postStatus));
router.put("/status/update_status", makeCallback(putStatus));

// Business Entity
router.post("/business_entity", makeCallback(getListBusinessEntity));
router.post(
  "/business_entity/add_business_entity",
  makeCallback(postBusinessEntity)
);
router.put(
  "/business_entity/update_business_entity",
  makeCallback(putBusinessEntity)
);

router.post("/business_entity/get_warehouse", makeCallback(getListWarehouse));
router.delete(
  "/business_entity/delete_warehouse",
  makeCallback(deleteWarehouse)
);
// router.post(
//   "/business_entity",
//   getAuthorization,
//   makeCallback(getListBusinessEntity)
// );
// router.post(
//   "/business_entity/add_business_entity",
//   getAuthorization,
//   makeCallback(postBusinessEntity)
// );
// router.put(
//   "/business_entity/update_business_entity",
//   getAuthorization,
//   makeCallback(putBusinessEntity)
// );

// Items
router.post("/items", makeCallback(getListItem));
router.post("/items/add_item", makeCallback(postItem));
router.put("/items/update_item", makeCallback(putItem));

// RawMaterials
router.post("/raw_materials", makeCallback(getListRawMaterial));
router.post("/raw_materials/add_raw_material", makeCallback(postRawMaterial));
router.put("/raw_materials/update_raw_material", makeCallback(putRawMaterial));

// BIR
router.post("/bir", makeCallback(getListBIR));
router.post("/bir/add_bir", makeCallback(postBIR));
router.post("/bir/list_items_bir", makeCallback(getListItemsBIR));
router.put("/bir/update_bir", makeCallback(putBIR));

// Criteria
router.post("/criteria", makeCallback(getListCriteria));
router.post("/criteria/add_criteria", makeCallback(postCriteria));
router.put("/criteria/update_criteria", makeCallback(putCriteria));

// Deduction Criteria

router.post("/deduction_criteria", makeCallback(getListDeductionCriteria));
router.post(
  "/deduction_criteria/add_deduction_criteria",
  makeCallback(postDeductionCriteria)
);
router.put(
  "/deduction_criteria/update_deduction_criteria",
  makeCallback(putDeductionCriteria)
);

// Price reduction
router.post("/price_reduction", makeCallback(getListPriceReduction));
router.post(
  "/price_reduction/add_price_reduction",
  makeCallback(postPriceReduction)
);
router.put(
  "/price_reduction/update_price_reduction",
  makeCallback(putPriceReduction)
);

// MTS
router.post("/mts", makeCallback(getListMTS));
router.post("/mts/add_mts", makeCallback(postMTS));
router.put("/mts/update_mts", makeCallback(putMTS));

// Reconsideration
router.post("/reconsideration", makeCallback(getListReconsideration));
router.post(
  "/reconsideration/add_reconsideration",
  makeCallback(postReconsideration)
);
router.put(
  "/reconsideration/update_reconsideration",
  makeCallback(putReconsideration)
);

// Disposition
router.post("/disposition/add_disposition", makeCallback(postDisposition));
router.post("/disposition", makeCallback(getListDisposition));
router.put("/disposition/update_disposition", makeCallback(putDisposition));

// Physical Analysis
router.post("/physical_analysis", makeCallback(getListPhysicalAnalysis));
router.post(
  "/physical_analysis/add_physical_analysis",
  makeCallback(postPhysicalAnalysis)
);
router.put(
  "/physical_analysis/update_physical_analysis",
  makeCallback(putPhysicalAnalysis)
);

// Chemical Analysis
router.post("/chemical_analysis", makeCallback(getListChemicalAnalysis));
router.post(
  "/chemical_analysis/add_chemical_analysis",
  makeCallback(postChemicalAnalysis)
);
router.put(
  "/chemical_analysis/update_chemical_analysis",
  makeCallback(putChemicalAnalysis)
);

// MRR
router.post("/mrr", makeCallback(getListMRR));
router.post("/mrr/add_mrr", makeCallback(postMRR));
router.put("/mrr/update_mrr", makeCallback(putMRR));

// RMAF
router.post("/rmaf", makeCallback(getListRMAF));
router.post("/rmaf/get_qccn", makeCallback(postQCCN));
router.post("/rmaf/add_rmaf", makeCallback(postRMAF));
router.put("/rmaf/update_rmaf", makeCallback(putRMAF));

// Transaction
router.post("/transactions", makeCallback(getListTransaction));
router.post("/transactions/add_transaction", makeCallback(postTransaction));
router.post(
  "/transactions/get_queue_number",
  makeCallback(getTransactionCueNumber)
);
router.post("/transactions/find_transaction", makeCallback(getFindTransaction));
router.post("/transactions/get_entry", makeCallback(getTransactionEntry));
router.post("/transactions/get_exit", makeCallback(getTransactionExit));
router.put("/transactions/update_transaction", makeCallback(putTransaction));
router.post(
  "/transactions/upload_signature",
  upload.single("image"),
  makeCallback(uploadSignature)
);
router.put("/transactions/update_entry", makeCallback(putEntry));
router.put("/transactions/update_exit", makeCallback(putExit));
router.post("/transactions/find_qr", makeCallback(getFindQR));

// for attachments
router.post(
  "/transactions/upload/attachments",
  upload.any(),
  makeCallback(uploadSignature)
);

// RejectionReceipt
router.post("/rejection_receipt", makeCallback(getListRejectionReceipt));
router.post(
  "/rejection_receipt/add_rejection_receipt",
  makeCallback(postRejectionReceipt)
);
router.put(
  "/rejection_receipt/update_rejection_receipt",
  makeCallback(putRejectionReceipt)
);

// Deduction Receipt
router.post(
  "/deduction_receipt/add_deduction_receipt",
  makeCallback(postDeductionReceipt)
);
router.post("/deduction_receipt", makeCallback(getListDeductionReceipt));
router.put(
  "/deduction_receipt/update_deduction_receipt",
  makeCallback(putDeductionReceipt)
);

// Transaction status logs
router.post(
  "/transaction_status_logs",
  makeCallback(getListTransactionStatusLogs)
);
router.post(
  "/transaction_status_logs/add_transaction_status_logs",
  makeCallback(postTransactionStatusLogs)
);
router.post(
  "/transaction_status_logs/find_transaction_status_logs",
  makeCallback(getFindTransactionStatusLogs)
);
router.put(
  "/transaction_status_logs/update_transaction_status_logs",
  makeCallback(putTransactionStatusLogs)
);

// Remarks
router.post("/remarks", makeCallback(getListRemarks));
router.post("/remarks/add_remarks", makeCallback(postRemarks));
router.post("/remarks/find_remarks", makeCallback(getFindRemarks));
router.put("/remarks/update_remarks", makeCallback(putRemarks));

module.exports = router;
