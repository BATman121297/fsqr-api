const express = require("express");
const router = express.Router();

//Express-callback
const makeCallback = require("../../express-callback");

//Authorization
const getAuthorization = require("../../token/index").getAuthorization;

//Controllers
const {
  //Modules
  getModules,
  postModule,
  putModule,

  //Actions
  getActions,
  postAction,
  putAction,
  getActionRole,

  //Roles
  getRoles,
  postRole,
  putRole,

  //Access Rights
  getAccessRights,
  postAccessRight,
  putAccessRight,

  // Employee
  getEmployees,
  postEmployees,
  putEmployees,
  putPincode,
  putPassword,
  fetchSAPEmployeess,

  getActivityLogs,

  getDeparment,
  postDeparment,
  putDepartment,
} = require("../../controllers/admin");

//Routes

//Modules
router.post("/modules", makeCallback(getModules));
router.post("/modules/add_module", makeCallback(postModule));
router.put("/modules/update_module", makeCallback(putModule));

// router.post("/modules", getAuthorization, makeCallback(getModules));
// router.post("/modules/add_module", getAuthorization, makeCallback(postModule));
// router.put("/modules/update_module", getAuthorization, makeCallback(putModule));

//Actions
router.post("/actions", makeCallback(getActions));
router.post("/actions/action_role", makeCallback(getActionRole));
router.post("/actions/add_action", makeCallback(postAction));
router.put("/actions/update_action", makeCallback(putAction));

// router.post("/actions", getAuthorization, makeCallback(getActions));
// router.post("/actions/add_action", getAuthorization, makeCallback(postAction));
// router.put("/actions/update_action", getAuthorization, makeCallback(putAction));
// router.post(
//   "/actions/action_role",
//   getAuthorization,
//   makeCallback(getActionRole)
// );

//Roles
router.post("/roles", makeCallback(getRoles));
router.post("/roles/add_role", makeCallback(postRole));
router.put("/roles/update_role", makeCallback(putRole));

// router.post("/roles", getAuthorization, makeCallback(getRoles));
// router.post("/roles/add_role", getAuthorization, makeCallback(postRole));
// router.put("/roles/update_role", getAuthorization, makeCallback(putRole));

//AccessRights
router.post("/access_rights", makeCallback(getAccessRights));
router.post("/access_rights/add_access_right", makeCallback(postAccessRight));
router.put("/access_rights/update_access_right", makeCallback(putAccessRight));

// router.post("/access_rights", getAuthorization, makeCallback(getAccessRights));
// router.post(
//   "/access_rights/add_access_right",
//   getAuthorization,
//   makeCallback(postAccessRight)
// );
// router.put(
//   "/access_rights/update_access_right",
//   getAuthorization,
//   makeCallback(putAccessRight)
// );

// Employees
router.post("/employees", makeCallback(getEmployees));
router.post("/sap-employees", makeCallback(fetchSAPEmployeess));
router.post("/employees/add_employee", makeCallback(postEmployees));
router.put("/employees/update_employee", makeCallback(putEmployees));
router.put("/employees/update_pincode", makeCallback(putPincode));
router.put("/employees/reset_password", makeCallback(putPassword));

// router.post("/employees", getAuthorization, makeCallback(getEmployees));
// router.post(
//   "/employees/add_employee",
//   getAuthorization,
//   makeCallback(postEmployees)
// );
// router.put(
//   "/employees/update_employee",
//   getAuthorization,
//   makeCallback(putEmployees)
// );
// router.put(
//   "/employees/update_pincode",
//   getAuthorization,
//   makeCallback(putPincode)
// );
// router.put(
//   "/employees/reset_password",
//   getAuthorization,
//   makeCallback(putPassword)
// );

// Activity Logs
router.post("/activitylogs", getAuthorization, makeCallback(getActivityLogs));

// Department
router.post("/departments", makeCallback(getDeparment));
router.post("/departments/add_department", makeCallback(postDeparment));
router.put("/departments/update_department", makeCallback(putDepartment));

// router.post("/departments", getAuthorization, makeCallback(getDeparment));
// router.post(
//   "/departments/add_department",
//   getAuthorization,
//   makeCallback(postDeparment)
// );
// router.put(
//   "/departments/update_department",
//   getAuthorization,
//   makeCallback(putDepartment)
// );

module.exports = router;
