const express = require("express");
const router = express.Router();

const {
  transactionMicroAdds,
  transactionMicroSelects,
  transactionMicroUpdates,
  fsqrHaulings,
  tagPOTransactions,
  attachmentsPosts,
  poDetailss,
} = require("../../controllers/micro/index");

const makeCallback = require("../../express-callback");

// add micro;
router.post("/add", makeCallback(transactionMicroAdds));

// select all and one micro;
router.post("/select", makeCallback(transactionMicroSelects));

// update micro; NOT GONNA USE!!!
// router.put("/update", makeCallback(transactionMicroUpdates));

// hauling integrate to fsqr
router.post("/hauling/list-items", makeCallback(fsqrHaulings));

// tag PO to transaction
router.put("/tag", makeCallback(tagPOTransactions));

// hauling integrate to fsqr
router.post("/post-attachments", makeCallback(attachmentsPosts));

// get details from a PO
router.post("/po-details", makeCallback(poDetailss));

module.exports = router;
