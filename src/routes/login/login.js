const express = require('express');
const router = express.Router();

const login = require('../../controllers/login');

const makeCallback = require('../../express-callback');

router.post('/',makeCallback(login));

module.exports = router;