const transactionTagPO = ({ micros }) => {
  return async function posts(info) {
    const { transaction_id, po_id } = info;
    if (!transaction_id) throw new Error(`Please enter transaction ID.`);
    if (!po_id) throw new Error(`Please enter P.O. ID.`);

    const data = {
      Code: transaction_id,
      U_PO: po_id,
      cookie: info.cookie,
    };

    const res = await micros.tagPO({ data });
    return res;
  };
};

module.exports = transactionTagPO;
