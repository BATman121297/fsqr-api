const postAttachments = ({ micros, path, dotenv, trns }) => {
  return async function select(info) {
    dotenv.config();

    // micro attachments
    if (info.isMicro) {
      const { grpoDocEntry, coa, bol, noa } = info;

      if (!grpoDocEntry) throw new Error(`Please enter GRPO DocEntry.`);

      // login to biotech schema;
      const bfi = await trns.bfiSAPLogin();
      if (bfi.status !== 200)
        throw new Error(`Cannot connect to BioTech, please try again.`);

      const bfiSession = bfi.data; // session

      // certificate of analysis
      let coaPath = null;
      if (coa) coaPath = path.resolve(`employee_files/${coa}`);

      // bill of landing
      let bolPath = null;
      if (bol) bolPath = path.resolve(`employee_files/${bol}`);

      // notice of arrival
      let noaPath = null;
      if (noa) noaPath = path.resolve(`employee_files/${noa}`);

      const imagePath = {
        noaPath,
        bolPath,
        coaPath,
        bfiSession,
      };

      const post = await micros.postAttachment({ imagePath });
      let postJSON = null;

      try {
        postJSON = JSON.parse(post);
      } catch (e) {
        throw new Error(`Files not uploaded to SAP.`);
      }

      let attachmentEntry = postJSON.AbsoluteEntry; //hold entry to be update in GRPO

      // update attachment to grpo
      const data = {
        Code: grpoDocEntry,
        cookie: `B1SESSION=${bfiSession}`,
        AttachmentEntry: attachmentEntry,
      };

      const res = await micros.grpoUpdateAttachment({ data });
      if (res.status !== 204)
        throw new Error(`Error on attaching file to GRPO.`);

      const resp = {
        msg: "Attached files successfully.",
      };

      return resp;
    }

    // macro attachments
    if (info.isMacro) {
      const {
        grpoDocEntry,
        weight_slip,
        deduction_slip,
        rmaf,
        rejection_receipt,
      } = info;

      if (!grpoDocEntry) throw new Error(`Please enter GRPO DocEntry.`);

      // login to biotech schema;
      const bfi = await trns.bfiSAPLogin();
      if (bfi.status !== 200)
        throw new Error(`Cannot connect to BioTech, please try again.`);

      const bfiSession = bfi.data; // session

      // weight slip
      let wsPath = null;
      if (weight_slip) wsPath = path.resolve(`employee_files/${weight_slip}`);

      // deduction slip
      let dsPath = null;
      if (deduction_slip)
        dsPath = path.resolve(`employee_files/${deduction_slip}`);

      // rmaf
      let rmafPath = null;
      if (rmaf) rmafPath = path.resolve(`employee_files/${rmaf}`);

      // rejection receipt
      let rrPath = null;
      if (rejection_receipt)
        rrPath = path.resolve(`employee_files/${rejection_receipt}`);

      const imagePath = {
        bfiSession,
        wsPath,
        dsPath,
        rmafPath,
        rrPath,
      };

      const post = await micros.postsAttachment({ imagePath });
      let postJSON = null;

      try {
        postJSON = JSON.parse(post);
      } catch (e) {
        throw new Error(`Files not uploaded to SAP.`);
      }

      let attachmentEntry = postJSON.AbsoluteEntry; //hold entry to be update in GRPO

      // update attachment to grpo
      const data = {
        Code: grpoDocEntry,
        cookie: `B1SESSION=${bfiSession}`,
        AttachmentEntry: attachmentEntry,
      };

      const res = await micros.grpoUpdateAttachment({ data });
      if (res.status !== 204)
        throw new Error(`Error on attaching file to GRPO.`);

      const resp = {
        msg: "Attached files successfully.",
      };

      return resp;
    }
  };
};

module.exports = postAttachments;
