const updateMicroTransaction = ({ micros }) => {
  return async function posts(info) {
    // NO USE!!!!
    const { id, status } = info;
    if (!id) throw new Error(`Please enter ID to be updated.`);
    if (!status) throw new Error(`Please enter status.`);
    const statuses = await micros.retriveStatusId(status);

    if (statuses.length == 0) throw new Error(`Status doesn't exist.`);
    const statusId = statuses[0].Code;

    const data = {
      Code: id,
      U_status_id: statusId,
      cookie: info.cookie
    };

    const res = await micros.transactionChangeStatus({ data });
    return res;
  };
};

module.exports = updateMicroTransaction;
