require("dotenv").config();

const detailsPO = ({ micros, trns }) => {
  return async function posts(info) {
    const { id } = info;

    if (!id) throw new Error(`Please enter DocEntry of the PO.`);

    // login to biotech schema
    const bfi = await trns.bfiSAPLogin();
    if (bfi.status !== 200)
      throw new Error(`Cannot connect to BioTech, please try again.`);

    const bfiSession = bfi.data; // session
    // ###############################

    // get PO details;
    let data = {
      session: bfiSession,
      id,
    };
    const po = await micros.getPODetails({ data });
    // clear object
    data = null;

    if (po.status !== 200)
      throw new Error(`Error retrieveing PO, please try again.`);

    if (po.data.length == 0) throw new Error(`PO doesn't exist.`);

    for (let i = 0; i < po.data.length; i++) {
      const e = po.data[i];

      data = {
        supplier_id: e.CardCode,
        supplier_name: e.CardName,
        address: e.Address,
      };

      let items = [];

      for (let q = 0; q < e.DocumentLines.length; q++) {
        const w = e.DocumentLines[q];

        // get one item; then determine if item is for micro;
        // return error if item is not for micro;
        const code = w.ItemCode;
        let bool = await micros.checkIfItemIsMicro({ code });
        bool = bool[0].BOOLS;

        if (bool == 0) throw new Error(`Item is not for micro transaction.`);

        // push to array
        items.push({
          id: code,
          description: w.ItemDescription,
          qty: w.Quantity,
          unit: w.MeasureUnit,
        });

        data.items = items;
      }
    }

    return data;
  };
};

module.exports = detailsPO;
