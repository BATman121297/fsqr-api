const haulingFsqr = ({ micros }) => {
  return async function posts(info) {
    const { bu, cookie, raw_mats, item_code } = info;

    if (!bu) {
      // Check if item code is provided
      if (!item_code) {
        throw new Error(`Please item code.`);
      }
      // Get item
      const item = await micros.items({ item_code });
      let business_entity_of_item;
      // Check if item is on any business entity
      if (item[0].U_APP_BU_Feedmill == "Y") {
        business_entity_of_item = "FEEDMILL PLANT 1 WAREHOUSE 1"
      } else if (item[0].U_APP_BU_Paddy == "Y") {
        business_entity_of_item = "RICE PLANT PDF RM WAREHOUSE"
      } else if (item[0].U_APP_BU_CDF == "Y") {
        business_entity_of_item = "CORN DRYING FACILITY RM WAREHOUSE"
      }
      // Get ID of business entity
      const res = await micros.businessEntities({ business_entity_of_item });
      if (res[0]) {
        // Return ID of business entity
        return res
      } else {
        return res
      }
    }

    // list all items base on BU;
    if (bu.toLowerCase() == "FEEDMILL".toLowerCase()) {
      // require micro or macro
      if (!raw_mats) throw new Error(`Please select raw material type.`);

      if (raw_mats.toLowerCase() == "micro") {
        // get all micro items of FMP
        const res = await micros.fmpMicroItem({});
        return res;
      } else if (raw_mats.toLowerCase() == "macro") {
        // get all macro items of FMP
        const res = await micros.fmpMacroItem({});
        return res;
      } else {
        // throw error
        throw new Error(`Invalid raw material type. Micro or Macro only.`);
      }
    }

    if (bu.toLowerCase() == "RICE".toLowerCase()) {
      const res = await micros.pdfItems({});
      return res;
    }

    if (bu.toLowerCase() == "CORN".toLowerCase()) {
      const res = await micros.cdfItems({});
      return res;
    }
  };
};

module.exports = haulingFsqr;
