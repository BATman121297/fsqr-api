const selectMicroTransaction = ({ micros }) => {
  return async function select(info) {
    const { transaction_id, status } = info;

    if (!status) throw new Error(`Please enter status.`);

    const statuses = await micros.retriveStatusId(status);

    if (statuses.length == 0) throw new Error(`No data available`);
    const statusId = statuses[0].Code;

    const id = transaction_id;
    const data = {
      id,
      statusId
    };

    // select only analysis approved status transactions; select all and select one;
    const res = await micros.microSelectHeader({ data });
    if (res.length == 0) return res;
    for (let i = 0; i < res.length; i++) {
      const e = res[i];
      // transaction id
      const id = e.Code;

      // get details
      const det = await micros.microSelectDetails(id);
      e.micro = det; // append
    }
    return res;
  };
};

module.exports = selectMicroTransaction;
