const { micros } = require("../../data-access/sap-integ/micro/index"); // data access
const { trns } = require("../../data-access/sap-integ/transactions/index");

const path = require("path");
const dotenv = require("dotenv");
// ###################
const addMicroTransaction = require("./micro-add");
const selectMicroTransaction = require("./micro-select");
const updateMicroTransaction = require("./micro-trns-update-status");
const haulingFsqr = require("./list-items-on-bu");
const transactionTagPO = require("./auto-tag-po");
const postAttachments = require("./post-attachments");
const detailsPO = require("./get-po-data");
// ###################
const addMicroTransactions = addMicroTransaction({ micros });
const selectMicroTransactions = selectMicroTransaction({ micros });
const updateMicroTransactions = updateMicroTransaction({ micros });
const haulingFsqrs = haulingFsqr({ micros });
const transactionTagPOs = transactionTagPO({ micros });
const postAttachmentss = postAttachments({
  micros,
  path,
  dotenv,
  trns,
});
const detailsPOs = detailsPO({ micros, trns });
// ###################
const services = Object.freeze({
  addMicroTransactions,
  selectMicroTransactions,
  updateMicroTransactions,
  haulingFsqrs,
  transactionTagPOs,
  postAttachmentss,
  detailsPOs,
});

module.exports = services;
module.exports = {
  addMicroTransactions,
  selectMicroTransactions,
  updateMicroTransactions,
  haulingFsqrs,
  transactionTagPOs,
  postAttachmentss,
  detailsPOs,
};
