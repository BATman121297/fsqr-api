const { addMicroTransactions } = require("../../entities/micro/index");
const addMicroTransaction = ({ micros }) => {
  return async function posts(info) {
    await addMicroTransactions(info);

    // get max code;
    const code = await micros.microMaxCode({});
    let max = code[0].maxCode;

    let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

    const arr = info.data;
    for (let i = 0; i < arr.length; i++) {
      const e = arr[i];
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_MICRO

{
  "Code": "${max}",
  "Name": "${max}",
  "U_transaction_id": "${info.transaction_id}",
  "U_packaged_no": "${e.package_no}",
  "U_expiry_date": "${e.expiry_date}",
  "U_quantity": ${e.quantity},
  "U_remarks": "${e.remarks}",
  "U_prepared_by": null,
  "U_checked_by": null,
  "U_received_by": null,
  "U_noted_by": null,
  "U_prepared_signature": null,
  "U_checked_signature": null,
  "U_noted_signature": null,
  "U_created_by": "${info.created_by}",
  "U_created_date": "${info.created_date}",
  "U_created_time": "${info.created_time}",
  "U_updated_by": "${info.created_by}",
  "U_updated_date": "${info.created_date}",
  "U_updated_time": "${info.created_time}"
}
`;
      max++;
    }

    // update status to unloading of transaction;
    const status = await micros.retriveStatusId("Unloading");

    if (status.length == 0) throw new Error(`Status doesn't exist.`);
    const statusId = status[0].Code;

    batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/U_FSQR_TRNS('${info.transaction_id}')

{
  "U_status_id": "${statusId}"
}
`;

    // close batch str
    batchStr += `
--b--
--a--
`;
    const data = {
      str: batchStr,
      cookie: info.cookie,
    };
    const res = await micros.microBatchReq({ data });
    return res;
  };
};

module.exports = addMicroTransaction;
