const makeLogin = require("../../entities/login");

module.exports = function makeLoginEmployee({ emp, logins }) {
  return async function login(loginInfo) {
    let default_pin_code = false;

    const res = await logins.userLogin();

    if (loginInfo.checkDefaults) {
      let defaults = {};
      let existing = await emp.findByIdDefaults(loginInfo.id);
      existing = existing[0];
      if ((existing.U_password = existing.U_employee_id)) {
        defaults.password = true;
      }
      if (existing.U_pin_code == "1234") {
        defaults.pin_code = true;
      }

      return defaults;
    }

    // SL account session id;
    const sessionId = res.data;

    const employee = makeLogin(loginInfo);

    const existing = await emp.findById(employee);
    if (existing.length > 0) {
      // employee exist
      const data = existing[0];
      const pin = data.U_pin_code;

      if (pin == "1234") default_pin_code = true;

      const pw = data.U_password;

      if (pw !== employee.getPassword()) throw new Error("Incorrect password");

      const status = data.roleStatus;

      if (status.toLowerCase() !== "active") {
        throw new Error(
          "Your account is currently inactive. Contact system administrator"
        );
      }

      // split role name
      const employeeRole = data.U_name.split("-");
      const role = employeeRole[0];

      // console.log(role)

      // if(role == "rm"){
      //   console.log("rm here!")
      //   if(loginInfo.department){
      //     let id = data.Code;
      //     // let employee
      //     const changeDepartmentID = await admin_db.updateEmployee(id,department)
      //   }
      //   if(!loginInfo.department_id){
      //     throw new Error(
      //       "Group ID is required!"
      //     );
      //   }
      // }

      // get all modules
      const modules = await emp.listAllModules();

      // get all access rights
      const empAccessRights = await emp.findAccessRightByRoleId(data.Code);

      const moduleArr = []; // remodeled modules
      for (let i = 0; i < modules.length; i++) {
        const e = modules[i];
        const info = {
          id: e.Code,
          description: e.U_description,
          status: e.U_status,
        };
        moduleArr.push(info);
      }

      for (let i = 0; i < empAccessRights.length; i++) {
        const e = empAccessRights[i];

        // action id
        const actionId = e.U_action_id;
        const action = await emp.findActionById(actionId);
        for (let k = 0; k < moduleArr.length; k++) {
          const s = moduleArr[k];

          if (s.id == action[0].U_module_id) {
            // declare empty array for actions
            if (!s.actions) {
              s.actions = [];
            }
            // push corressponding action
            const info = {
              id: action[0].Code,
              module_id: action[0].U_module_id,
              description: action[0].U_description,
            };
            s.actions.push(info);
          }
        }
      }

      const removeNoActions = async () => {
        for (let i = 0; i < moduleArr.length; i++) {
          const e = moduleArr[i];
          if (!e.actions || e.status.toLowerCase() !== "active") {
            moduleArr.splice(i, 1);
            await removeNoActions();
          }
        }
      };

      await removeNoActions();

      let department = await emp.getDepartment(data.U_department);
      department = department[0];
      return {
        id: data.U_employee_id,
        name: data.NAME,
        roleParam: role,
        role: {
          id: data.Code,
          name: data.U_name,
          status: data.roleStatus,
        },
        department: {
          id: department.Code,
          name: department.U_name,
          status: department.U_status,
        },
        default_pin_code,
        modules: moduleArr,
        token: sessionId,
      };
    } else {
      // employee doesn't exist
      throw new Error("User credentials are not valid");
    }
  };
};
