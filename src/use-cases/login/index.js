const makePostLogin = require("./login");
const employeeDb = require("../../data-access/employees");
const authenticate = require("../../token");
const encryption = require("../../encryption/encrypting");

// SL login;
const { logins } = require("../../data-access/sap-integ/login/index");
const { emp } = require("../../data-access/sap-integ/employees/index");

// const login = makePostLogin({ employeeDb }, { authenticate }, { encryption });

const login = makePostLogin({ emp, logins });

const loginService = Object.freeze(login);

module.exports = loginService;
module.exports = login;
