module.exports = function listSuppliers({ sap_db }) {
  return async function makeListSuppliers() {
    const login_info = {
      CompanyDB: "BIOTECH_FARMS_TEST",
      Password: "1234",
      UserName: "BFI07"
    };

    let login = await sap_db.login(login_info);
    let sessionId = login.SessionId;

    let suppliers = await sap_db.getSuppliers(sessionId);
    let listSuppliers = [];

    if (suppliers.length == 0) {
      suppliers = [];
    }

    for (let i = 0; i < suppliers.length; i++) {
    //   const data = {
    //     value: suppliers[i].CardCode,
    //     text: suppliers[i].CardName
    //   };
      listSuppliers.push(suppliers[i].CardName);
    }

    return { listSuppliers, suppliers };
  };
};
