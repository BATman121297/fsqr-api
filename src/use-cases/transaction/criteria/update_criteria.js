const { makeUpdateCriteria } = require("../../../entities/criteria");

module.exports = function putCriteria({ trns }) {
  return async function makeEditCriteria(request_info) {
    await makeUpdateCriteria(request_info);

    // check if id exist
    const { id, description } = request_info;

    const exist = await trns.criteriaIdCheck(id);
    if (exist.length == 0) throw new Error("Criteria doesn't exist.");

    // check if description exist
    const check = await trns.criteriaDescCheckUpdate(id, description);
    if (check.length > 0)
      throw new Error("Criteria description already exist.");

    const info = {
      Code: id,
      U_description: description,
      U_deduction_per_criteria: request_info.deduction_per_criteria,
      U_business_entity: request_info.business_entity,
      U_status: request_info.status,
      U_item_code: request_info.item_code,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateCriteria({ info });
    return res;
  };
};
