const { makeCriteria } = require("../../../entities/criteria");

module.exports = function postCriteria({ trns }) {
  return async function makePostCriteria(request_info) {
    await makeCriteria(request_info);

    const { description } = request_info;

    // check if exist
    const check = await trns.criteriaDescCheck(description);
    if (check.length > 0) throw new Error("Description already exist");

    // get max code
    const code = await trns.criteriaMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_description: request_info.description,
      U_deduction_per_criteria: request_info.deduction_per_criteria,
      U_business_entity: request_info.business_entity,
      U_status: "active",
      U_item_code: request_info.item_code,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addCriteria({ info });
    return res;
  };
};
