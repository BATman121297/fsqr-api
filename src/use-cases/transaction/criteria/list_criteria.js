module.exports = function listCriteria({ trns }) {
  return async function makeListCriteria(request_info) {
    let criteria = [];
    let crit;
    if (request_info.business_entity_id) {
      crit = await trns.listCriteriaByBE(request_info.business_entity_id);
    } else {
      crit = await trns.listCriteria();
    }

    for (let i = 0; i < crit.length; i++) {
      const e = crit[i];

      const info = {
        id: e.Code,
        description: e.U_description,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        deduction_per_criteria: e.U_deduction_per_criteria,
        business_entity: e.U_business_entity,
        status: e.U_status,
        item_code: e.U_item_code,
      };

      criteria.push(info);
    }

    return { criteria };
  };
};
