module.exports = function listMRR({ trns }) {
  return async function makeListMRR() {
    const res = await trns.listMRR();
    let mrr = [];
    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        findings: e.U_findings,
        specification: e.U_specification,
        issued_by: e.U_issued_by,
        noted_by: e.U_noted_by,
        approved_by: e.U_approved_by,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };

      mrr.push(info);
    }
    return { mrr };
  };
};
