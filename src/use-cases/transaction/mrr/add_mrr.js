const { makeMRR } = require("../../../entities/mrr");

module.exports = function postMRR({ trns }) {
  return async function makePostMRR(request_info) {
    await makeMRR(request_info);

    // get max code
    const code = await trns.mrrMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_findings: request_info.findings,
      U_specification: request_info.specification,
      U_issued_by: request_info.issued_by,
      U_noted_by: request_info.noted_by,
      U_approved_by: request_info.approved_by,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    // insert
    const res = await trns.addMrr({ info });
    return res;
  };
};
