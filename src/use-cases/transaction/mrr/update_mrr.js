const { makeUpdateMRR } = require("../../../entities/mrr");

module.exports = function putMRR({ trns }) {
  return async function makeEditMRR(request_info) {
    await makeUpdateMRR(request_info);

    const info = {
      Code: request_info.id,
      U_findings: request_info.findings,
      U_specification: request_info.specification,
      U_issued_by: request_info.issued_by,
      U_noted_by: request_info.noted_by,
      U_approved_by: request_info.approved_by,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    // update
    const res = await trns.updateMrr({ info });
    return res;
  };
};
