module.exports = function listRMAF({ trns }) {
  return async function makeListRMAF() {
    const res = await trns.listRMAF();
    let rmaf = [];
    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        physical_analysis_id: e.U_physical_analysis_id,
        chemical_analysis_id: e.U_chemical_analysis_id,
        date: e.U_date,
        material_rejection_report_id: e.U_material_rejection_report_id,
        updated_by: e.U_updated_by,
        updated_at: e.U_updated_date,
      };

      rmaf.push(info);
    }

    for (let i = 0; i < rmaf.length; i++) {
      const e = rmaf[i];

      // physical analysis
      const pa_id = e.physical_analysis_id;
      let phy = await trns.findPhysicalAnalysisById(pa_id);
      phy = phy[0];
      const phy_info = [
        {
          id: phy.Code,
          price_reduction_id: phy.U_price_reduction_id,
          reconsideration_id: phy.U_reconsideration_id,
          type: phy.U_type,
          mc_result: phy.U_mc_result,
          impurities: phy.U_impurities,
          foreign_material: phy.U_foreign_material,
          evaluated_by: phy.U_evaluated_by,
          certified_by: phy.U_certified_by,
          noted_by: phy.U_noted_by,
          created_by: phy.U_created_by,
          updated_by: phy.U_updated_by,
          created_at: phy.U_created_date,
          updated_at: phy.U_updated_date,
          moisture_tally_sheet_id: phy.U_mts_id,
          evaluated_signature: phy.U_evaluated_signature,
          certified_signature: phy.U_certified_signature,
          noted_signature: phy.U_noted_signature,
        },
      ];
      e.physical_analysis = phy_info;

      // chemical analysis
      const ca_id = e.chemical_analysis_id;
      let ca = await trns.findChemicalAnalysisById(ca_id);
      ca = ca[0];
      const ca_info = [
        {
          id: ca.Code,
          reconsideration_id: ca.U_reconsideration_id,
          crude_protein: ca.U_crude_protein,
          calcium: ca.U_calcium,
          ffa_lauric_acid: ca.U_ffa_lauric_acid,
          brix: ca.U_brix,
          carbonate_test: ca.U_carbonate_test,
          evaluated_by: ca.U_evaluated_by,
          certified_by: ca.U_certified_by,
          created_by: ca.U_created_by,
          updated_by: ca.U_updated_by,
          created_at: ca.U_created_date,
          updated_at: ca.U_updated_date,
          evaluated_signature: ca.U_evaluated_signature,
          certified_signature: ca.U_certified_signature,
          remarks: ca.U_remarks,
        },
      ];
      e.chemical_analysis = ca_info;

      // mrr
      const m_id = e.material_rejection_report_id;
      let mrr = await trns.findMRRById(m_id);
      mrr = mrr[0];
      const mrr_info = [
        {
          id: mrr.Code,
          findings: mrr.U_findings,
          specification: mrr.U_specification,
          issued_by: mrr.U_issued_by,
          noted_by: mrr.U_noted_by,
          approved_by: mrr.U_approved_by,
          created_by: mrr.U_created_by,
          updated_by: mrr.U_updated_by,
          created_at: mrr.U_created_date,
          updated_at: mrr.U_updated_date,
        },
      ];
      e.material_rejection_report = mrr_info;
    }

    return { rmaf };
  };
};
