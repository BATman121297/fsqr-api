const { makeRMAF } = require("../../../entities/rmaf");

module.exports = function postRMAF({ trns }) {
  return async function makePostRMAF(request_info) {
    await makeRMAF(request_info);

    // get max code
    const code = await trns.rmafMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_physical_analysis_id: request_info.physical_analysis_id,
      U_chemical_analysis_id: request_info.chemical_analysis_id,
      U_date: request_info.date,
      U_material_rejection_report_id: request_info.material_rejection_report_id,
      U_updated_by: request_info.employee_id,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie,
    };

    const res = await trns.addRMAF({ info });
    res.id = max;
    return res;
  };
};
