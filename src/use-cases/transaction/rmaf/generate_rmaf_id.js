module.exports = function postRMAFID({ trns }, { moment }) {
  return async function makePostRMAFID() {
    const date = new Date().toDateString();
    const dates = moment(date).format("YYYY-MM");

    const res = await trns.countTransaction(dates);
    let count = res[0].counts;
    count++; // + 1

    const seriesNumber = formatCount(count);
    const year = moment().format("YY");
    const month = moment().format("MM");
    const QCCN = "RMB" + year + month + seriesNumber;
    return { QCCN };
  };
};

const formatCount = number => {
  let toString = number.toString();

  while (toString.length < 4) {
    toString = "0" + toString;
  }

  return toString;
};
