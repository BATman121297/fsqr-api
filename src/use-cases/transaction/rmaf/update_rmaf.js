const { makeUpdateRMAF } = require("../../../entities/rmaf");

module.exports = function putRMAF({ trns }) {
  return async function makeEditRMAF(request_info) {
    if (!request_info.addChemAna) {
      await makeUpdateRMAF(request_info);
    }

    const info = {
      Code: request_info.id,
      U_physical_analysis_id: request_info.physical_analysis_id,
      U_chemical_analysis_id: request_info.chemical_analysis_id,
      U_material_rejection_report_id: request_info.material_rejection_report_id,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateRMAF({ info });
    return res;
  };
};
