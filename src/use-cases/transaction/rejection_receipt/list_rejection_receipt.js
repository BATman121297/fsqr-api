module.exports = function listRejectionReceipt({ trns }) {
  return async function makeListRejectionReceipt() {
    const res = await trns.listRejectionReceipt();

    let rejection_receipts = [];
    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        transaction_id: e.U_transaction_id,
        no_of_bags_rejected: e.U_no_of_bags_rejected,
        reason_of_rejection: e.U_reason_of_rejection,
        date: e.U_date,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };
      rejection_receipts.push(info);
    }

    for (let i = 0; i < rejection_receipts.length; i++) {
      const e = rejection_receipts[i];
      // transaction id
      const t_id = e.transaction_id;
      let trn = await trns.findTransactionById(t_id);
      if (trn.length > 0) {
        trn = trn[0];

        const trn_info = [
          {
            id: trn.Code,
            status_id: trn.U_status_id,
            bir_id: trn.U_bir_id,
            rmaf_id: trn.U_rmaf_id,
            delivery_receipt_id: trn.U_DR_id,
            transaction_type_id: trn.U_trnstyp_id,
            purchase_order_id: trn.U_PO,
            supplier_id: trn.U_spplr_id,
            driver_id: trn.U_driverID,
            supplier_code: trn.U_suppl_code,
            qc_control_number: trn.U_qc_cntrl_no,
            queue_number: trn.U_queue_no,
            no_of_bags: trn.U_no_of_bags,
            supplier_name: trn.U_spplr_name,
            supplier_address: trn.U_spplr_add,
            driver_name: trn.U_driver,
            plate_number: trn.U_trckpltno,
            time_end: trn.U_time_end,
            guard_on_duty: trn.U_g_on_dty,
            updated_by: trn.U_updated_by,
            created_at: trn.U_created_date,
            updated_at: trn.U_updated_date,
            time_start: trn.U_time_start,
          },
        ];
        e.transaction = trn_info;
      }
    }
    return { rejection_receipts };
  };
};
