const { makeRejectionReceipt } = require("../../../entities/rejection_receipt");

module.exports = function postRejectionReceipt({ trns }) {
  return async function makePostRejectionReceipt(request_info) {
    await makeRejectionReceipt(request_info);

    // get max code
    const code = await trns.rejReceiptMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_transaction_id: request_info.transaction_id,
      U_no_of_bags_rejected: request_info.no_of_bags_rejected,
      U_reason_of_rejection: request_info.reason_of_rejection,
      U_date: null,
      U_created_by: request_info.employee_id,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.employee_id,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    // insert
    const res = await trns.addRejectReceipt({ info });
    return res;
  };
};
