const {
  makeUpdateRejectionReceipt
} = require("../../../entities/rejection_receipt");

module.exports = function putRejectionReceipt({ trns }) {
  return async function makeEditRejectionReceipt(request_info) {
    await makeUpdateRejectionReceipt(request_info);

    // check if id exist
    const { id } = request_info;

    const exist = await trns.rejReceiptIdCheck(id);
    if (exist.length == 0) throw new Error("Rejection Receipt doesn't exist.");

    const info = {
      Code: request_info.id,
      U_transaction_id: request_info.transaction_id,
      U_no_of_bags_rejected: request_info.no_of_bags_rejected,
      U_reason_of_rejection: request_info.reason_of_rejection,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateRejReceipt({ info });
    return res;
  };
};
