const { makeBIR } = require("../../../entities/bir");

module.exports = function postBIR({ trns }) {
  return async function makePostBIR(request_info) {
    await makeBIR(request_info);

    // get max code
    const code = await trns.birMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_item_id: request_info.item_id,
      U_business_entity_id: request_info.business_entity_id,
      U_raw_material_id: request_info.raw_material_id,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addBIR({ info });
    return res;
  };
};
