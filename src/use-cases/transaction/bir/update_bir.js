const { makeUpdateBIR } = require("../../../entities/bir");

module.exports = function putBIR({ trns }) {
  return async function makeEditBIR(request_info) {
    await makeUpdateBIR(request_info);

    const info = {
      Code: request_info.id,
      U_item_id: request_info.item_id,
      U_business_entity_id: request_info.business_entity_id,
      U_raw_material_id: request_info.raw_material_id,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateBIR({ info });
    return res;
    // // Get old value
    // const oldResult = await transaction_db.findBIRById(request_info.id);
    // const old_values = JSON.stringify(oldResult.rows[0]);

    // const result = await transaction_db.updateBIR(request_info);

    // if (result.rowCount > 0) {
    //   delete request_info.role;
    //   delete request_info.modules;
    // } else {
    //   throw new Error("Update failed.");
    // }

    // // Store new values
    // const new_values = JSON.stringify(result.rows[0]);

    // // Define log_info to insert to activity logs
    // const log_info = {
    //   created_by: request_info.updated_by,
    //   action: "update",
    //   table: "bir",
    //   old_values,
    //   new_values
    // };

    // await transaction_db.addActivityLog(log_info);

    // return result.rows[0];
  };
};
