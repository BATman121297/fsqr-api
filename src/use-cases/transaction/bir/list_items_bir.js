module.exports = function listBIR({ trns }) {
  return async function makeListBIR(request_info) {

    const warehouse = request_info.business_entity_name;

    let warehouseName = warehouse.split(" ");
    let wN = warehouseName[0];

    let listBIR;

    if (wN == "RICE") {
      listBIR = await trns.findItemsByBusinessEntityRICE(
        request_info.business_entity_name
      );
    } else if (wN == "FEEDMILL") {
      listBIR = await trns.findItemsByBusinessEntityFEEDMILL(
        request_info.business_entity_name
      );
    } else if (wN == "CORN") {
      listBIR = await trns.findItemsByBusinessEntityCORN(
        request_info.business_entity_name
      );
    } else {
      throw new Error("Warehouse is not defined!");
    }

    let bir = [];
    for (let i = 0; i < listBIR.length; i++) {
      const e = listBIR[i];

      // item id
      const iid = e.ItemCode;
      let i_id = await trns.findItemById(iid);
      i_id = i_id[0];

      // raw material id
      const rid = e.ItmsGrpCod;

      let rm_id;

      if (rid == "310") {
        rm_id = 1;
      } else if (rid == "313") {
        rm_id = 1;
      } else if (rid == "314") {
        rm_id = 2;
      } else {
        throw new Error(`Item Code does not belong to any Raw Material Type!`);
      }

      let r_id = await trns.findRawMaterialsById(rm_id);
      r_id = r_id[0];

      const info = {
        id: e.ItemCode,
        business_entity_id: e.WhsCode,
        item_id: e.ItemCode,
        raw_material_id: e.ItmsGrpCod,
        items: [
          {
            id: i_id.ItemCode,
            description: i_id.ItemName,
          },
        ],
        raw_materials: [
          {
            id: r_id.Code,
            description: r_id.U_description,
          },
        ],
      };

      bir.push(info);
    }


    return { bir };
  };
};
