module.exports = function listBIR({ trns }) {
  return async function makeListBIR() {
    const birs = await trns.listBIR();

    let bir = [];

    for (let i = 0; i < birs.length; i++) {
      const e = birs[i];

      // business entity id
      const bid = e.U_business_entity_id;
      let b_id = await trns.findBusinessEntityById(bid);
      b_id = b_id[0];

      // item id
      const iid = e.U_item_id;
      let i_id = await trns.findItemById(iid);
      i_id = i_id[0];

      // raw material id
      const rid = e.U_raw_material_id;
      let r_id = await trns.findRawMaterialsById(rid);
      r_id = r_id[0];

      const info = {
        id: e.Code,
        item_id: e.U_item_id,
        business_entity_id: e.U_business_entity_id,
        raw_material_id: e.U_raw_material_id,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        business_entity: [
          {
            id: b_id.Code,
            description: b_id.U_description,
            created_by: b_id.U_created_by,
            updated_by: b_id.U_updated_by,
            created_at: b_id.U_created_date,
            updated_at: b_id.U_updated_date,
            department_id: b_id.U_department_id,
          },
        ],
        items: [
          {
            id: i_id.Code,
            description: i_id.U_description,
            created_by: i_id.U_created_by,
            updated_by: i_id.U_updated_by,
            created_at: i_id.U_created_date,
            updated_at: i_id.U_updated_date,
          },
        ],
        raw_materials: [
          {
            id: r_id.Code,
            description: r_id.U_description,
            created_by: r_id.U_created_by,
            updated_by: r_id.U_updated_by,
            created_at: r_id.U_created_date,
            updated_at: r_id.U_updated_date,
          },
        ],
      };

      bir.push(info);
    }

    return { bir };
  };
};
