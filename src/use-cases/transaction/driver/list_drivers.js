module.exports = function listDriver({ transaction_db }) {
    return async function makeListDriver() {
      let listDriver = await transaction_db.listDriver();
  
      let driver = listDriver.rows;
      if (!driver.length) {
        driver = [];
      }
  
      return { driver };
    };
  };
  