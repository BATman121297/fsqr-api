const { makeUpdateDriver } = require("../../../entities/drivers");

module.exports = function putDriver({ transaction_db }) {
  return async function makeEditDriver(request_info) {
    await makeUpdateDriver(request_info, { transaction_db });

    // Get old value
    const oldResult = await transaction_db.findDriverById(
      request_info.id
    );
    const old_values = JSON.stringify(oldResult.rows[0]);

    const result = await transaction_db.updateDriver(request_info);

    if (result.rowCount > 0) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error("Update failed.");
    }

    // Store new values
    const new_values = JSON.stringify(result.rows[0]);

    // Define log_info to insert to activity logs
    const log_info = {
      created_by: request_info.updated_by,
      action: "update",
      table: "driver",
      old_values,
      new_values
    };

    await transaction_db.addActivityLog(log_info);

    return result.rows[0];
  };
};
