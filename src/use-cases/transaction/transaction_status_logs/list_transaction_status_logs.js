module.exports = function listTransactionStatusLogs({ trns }) {
  return async function makeListTransactionStatusLogs() {
    const res = await trns.listTransactionStatusLogs();
    let transactionStatusLogs = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        transaction_id: e.U_transaction_id,
        status_id: e.U_status_id,
        start_time: e.U_start_time,
        end_time: e.U_end_time,
        created_at: e.U_created_date,
        created_by: e.U_created_by,
      };

      transactionStatusLogs.push(info);
    }
    return { transactionStatusLogs };
  };
};
