const {
  updateTransactionStatusLogs
} = require("../../../entities/transaction_status_logs");

module.exports = function putTransactionStatusLogs({ trns }) {
  return async function makeEditTransactionStatusLogs(request_info) {
    await updateTransactionStatusLogs(request_info);

    const info = {
      Code: request_info.id,
      U_transaction_id: request_info.transaction_id,
      U_status_id: request_info.status_id,
      U_end_time: request_info.end_time,
      cookie: request_info.cookie
    };

    // update
    const res = await trns.updateTrnsLogs({ info });
    return res;
  };
};
