const {
  makeTransactionStatusLogs
} = require("../../../entities/transaction_status_logs");

module.exports = function postTransactionStatusLogs({ trns }) {
  return async function makePostTransactionStatusLogs(request_info) {
    await makeTransactionStatusLogs(request_info);

    // get status id;
    const status = await trns.statusNameCheck(request_info.status);
    request_info.status_id = status[0].Code;

    // max code
    const code = await trns.trnsLogsMaxCode();
    const max = code[0].maxCode;
    const info = {
      Code: max,
      Name: max,
      U_transaction_id: request_info.transaction_id,
      U_status_id: request_info.status_id,
      U_start_time: request_info.created_time,
      U_end_time: request_info.created_time,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addtrnsLogs({ info }); // add transaction logs
    return res;
  };
};
