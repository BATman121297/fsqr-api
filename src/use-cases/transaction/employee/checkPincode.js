const { makeCheckPin } = require("../../../entities/employees");

module.exports = function checkPincode({ trns }) {
  return async function makeListEmployees(request_info) {
    await makeCheckPin(request_info);

    const { employee_id, pincode } = request_info;

    // get pin code
    const pin = await trns.checkPinCode(employee_id);
    const pins = pin[0].U_pin_code;

    let bool = false;

    if (pins == pincode) {
      bool = true;
    }

    return bool;
  };
};
