require("dotenv").config();

module.exports = function listNetWeight({ trns }) {
  return async function makeListNetWeight(request_info) {
    const info = {
      user: process.env.TSIS_USER,
      pw: process.env.TSIS_PW,
      code: request_info.supplier_code
    };

    const net_weight = await trns.truckScaleGetNetWeight({ info });
    return net_weight;
  };
};
