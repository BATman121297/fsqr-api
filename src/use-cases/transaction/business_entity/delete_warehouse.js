const {
  makeDeleteWarehouse
} = require("../../../entities/business_entity");

module.exports = function deleteWarehouse({ trns }) {
  return async function makeRemoveWarehouse(request_info) {
    await makeDeleteWarehouse(request_info);

    const { id, description } = request_info;

   

    // check if exist
    const exist = await trns.busEntityIdCheck(id);
    if (exist.length == 0) throw new Error(`Business Entity doesn't exist.`);

    // check if description exist
    const check = await trns.busEntityDescCheckUpdate(id, description);
    if (check.length > 0)
      throw new Error(`Business Entity already exist exist.`);

    const info = {
      Code: request_info.id,
      Name: request_info.description,
      U_description: request_info.description,
      cookie: request_info.cookie
    };

    const res = await trns.deleteWarehouse({ info });

   
    return res;
  };
};



