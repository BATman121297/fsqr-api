const {
  makeUpdateBusinessEntity
} = require("../../../entities/business_entity");

module.exports = function putBusinessEntity({ trns }) {
  return async function makeEditBusinessEntity(request_info) {
    await makeUpdateBusinessEntity(request_info);

    const { id, description } = request_info;

    // check if exist
    const exist = await trns.busEntityIdCheck(id);
    if (exist.length == 0) throw new Error(`Business Entity doesn't exist.`);

    // check if description exist
    const check = await trns.busEntityDescCheckUpdate(id, description);
    if (check.length > 0)
      throw new Error(`Business Entity already exist exist.`);

    const info = {
      Code: request_info.id,
      Name: request_info.description,
      U_description: request_info.description,
      U_department_id: request_info.department_id,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updatebusEntity({ info });
    return res;
  };
};
