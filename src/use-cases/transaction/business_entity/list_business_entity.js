module.exports = function listBusinessEntity({ trns }) {
  return async function makeListBusinessEntity() {
    const res = await trns.listBusinessEntity();

    let business_entities = [];
    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        description: e.U_description,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        department_id: e.U_department_id,
      };
      business_entities.push(info);
    }

    return { business_entities };
  };
};
