const { makeBusinessEntity } = require("../../../entities/business_entity");

module.exports = function postBusinessEntity({ trns }) {
  return async function makePostBusinessEntity(request_info) {
    await makeBusinessEntity(request_info);

    // check if exist
    const check = await trns.busEntityDescCheck(request_info.description);
    if (check.length > 0) throw new Error("Business entity already exists");

    // get max code
    const code = await trns.busEntityMaxCode();
    const max = code[0].maxCode;

    // insert
    const info = {
      Code: max,
      Name: request_info.description,
      U_description: request_info.description,
      U_department_id: request_info.department_id,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addBusEntity({ info });
    return res;

    // const result = await transaction_db.addBusinessEntity(request_info);

    // if (!result) {
    //   throw new Error("Insert failed.");
    // }

    // const old_values = "{}";
    // const new_values = JSON.stringify(result.rows[0]);

    // const log_info = {
    //   created_by: request_info.created_by,
    //   action: "create",
    //   table: "business_entity",
    //   old_values,
    //   new_values
    // };

    // await transaction_db.addActivityLog(log_info);

    // return result.rows;
  };
};
