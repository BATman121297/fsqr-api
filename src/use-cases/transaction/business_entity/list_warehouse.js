module.exports = function listWarehouse({ trns }) {
  return async function makeListWarehouse() {
    const res = await trns.listWarehouse();
    return { res };
  };
};
