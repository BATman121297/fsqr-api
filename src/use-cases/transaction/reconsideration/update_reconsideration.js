const {
  makeUpdateReconsideration
} = require("../../../entities/reconsideration");

module.exports = function putReconsideration({ trns }) {
  return async function makeEditReconsideration(request_info) {
    await makeUpdateReconsideration(request_info);

    // check if id exist
    const { id } = request_info;

    const exist = await trns.reconIdCheck(id);
    if (exist.length == 0) throw new Error("Reconsideration doesn't exist.");

    const info = {
      Code: id,
      U_for_consideration: request_info.for_consideration,
      U_reason: request_info.reason,
      U_reconsidered_by: request_info.reconsidered_by,
      U_reconsidered_approved_by: request_info.reconsidered_approved_by,
      U_noted_by: request_info.noted_by,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.reconsiderationUpdate({ info });
    return res;
  };
};
