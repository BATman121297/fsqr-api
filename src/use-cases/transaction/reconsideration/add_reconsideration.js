const { makeReconsideration } = require("../../../entities/reconsideration");

module.exports = function postReconsideration({ trns }) {
  return async function makePostReconsideration(request_info) {
    await makeReconsideration(request_info);

    // get max code
    const code = await trns.reconMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_for_consideration: request_info.for_consideration,
      U_reason: request_info.reason,
      U_reconsidered_by: request_info.reconsidered_by,
      U_reconsidered_approved_by: request_info.reconsidered_approved_by,
      U_reconsidered_signature: null,
      U_reconsidered_approved_signature: null,
      U_noted_by: request_info.noted_by,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_by,
      cookie: request_info.cookie
    };

    const res = await trns.addRecon({ info });
    return res;
  };
};
