module.exports = function listReconsideration({ trns }) {
  return async function makeListReconsideration() {
    const res = await trns.listRecon();
    let reconsiderations = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        for_consideration: e.U_for_consideration,
        reason: e.U_reason,
        reconsidered_by: e.U_reconsidered_by,
        reconsidered_approved_by: e.U_reconsidered_approved_by,
        noted_by: e.U_noted_by,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        reconsidered_signature: e.U_reconsidered_signature,
        reconsidered_approved_signature: e.U_reconsidered_approved_signature,
      };

      reconsiderations.push(info);
    }

    return { reconsiderations };
  };
};
