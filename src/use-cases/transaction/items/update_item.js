const { makeUpdateItem } = require("../../../entities/items");

module.exports = function putItem({ trns }) {
  return async function makeEditItem(request_info) {
    await makeUpdateItem(request_info);

    // check if id exist
    const { id, description } = request_info;

    const exist = await trns.itemsIdCheck(id);
    if (exist.length == 0) throw new Error("Item doesn't exist.");

    // check if description exist
    const check = await trns.itemsDescCheckUpdate(id, description);
    if (check.length > 0) throw new Error("Item description already exist.");

    // update
    const info = {
      Code: id,
      Name: description,
      U_description: description,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateItem({ info });
    return res;
  };
};
