module.exports = function listItem({ trns }) {
  return async function makeListItem() {
    const items = await trns.listItem();

    return { items };
  };
};
