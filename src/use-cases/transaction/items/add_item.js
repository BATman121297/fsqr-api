const { makeItem } = require("../../../entities/items");

module.exports = function postItem({ trns }) {
  return async function makePostItem(request_info) {
    await makeItem(request_info);

    const { description } = request_info;

    // check if description exist
    const check = await trns.itemsDescCheck(description);
    if (check.length > 0) throw new Error("Description already exist");

    // get max code
    const code = await trns.itemsMaxCode();
    const max = code[0].maxCode;


    const info = {
      Code: max,
      Name: description,
      U_description: description,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    // insert
    const res = await trns.addItem({ info });
    return res;
  };
};
