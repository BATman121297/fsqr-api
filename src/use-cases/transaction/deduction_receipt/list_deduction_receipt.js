module.exports = function listDeductionReceipt({ trns }) {
  return async function makeListDeductionReceipt(request_info) {
    let listDeductionReceipts, listDR;

    if (request_info.id) {
      // select single DR
      const id = request_info.id;
      listDeductionReceipts = await trns.findDeductionReceiptById(id);
    } else if (request_info.transaction_id) {
      // select DR by transaction id
      const id = request_info.transaction_id;
      listDeductionReceipts = await trns.findDeductionReceiptSumByTransId(id);
      listDR = await trns.findDeductionReceiptByTransId(id);

      // remodel
      // dummy arrays
      let listDedRec = [];
      let listDRs = [];
      for (let i = 0; i < listDeductionReceipts.length; i++) {
        const e = listDeductionReceipts[i];
        const info = {
          sum_total_deductions: e.TOTAL_DEDUCTIONS,
          count: e.COUNTS,
        };
        listDedRec.push(info);
      }
      listDeductionReceipts = [];
      listDeductionReceipts = listDedRec;

      for (let i = 0; i < listDR.length; i++) {
        const e = listDR[i];
        const info = {
          id: e.Code,
          transaction_id: e.U_transaction_id,
          date: e.U_date,
          total_deductions: e.U_total_deductions,
          prepared_by: e.U_prepared_by,
          checked_by: e.U_checked_by,
          received_by: e.U_received_by,
          noted_by: e.U_noted_by,
          created_by: e.U_created_by,
          updated_by: e.U_updated_by,
          created_at: e.U_created_date,
          updated_at: e.U_updated_date,
          prepared_signature: e.U_prepared_signature,
          checked_signature: e.U_checked_signature,
          noted_signature: e.U_noted_signature,
        };
        listDRs.push(info);
      }
      listDR = [];
      listDR = listDRs;
      // ##
    } else {
      // select all DR
      listDeductionReceipts = await trns.listDeductionReceipts();
    }

    let deductionReceipts = listDeductionReceipts;
    let dr = []; // hold for remodel; empty main array afterwards

    let listDeductionCriteria = [],
      criteria,
      deduction;

    if (request_info.transaction_id) {
      // transaction id
      const id = request_info.transaction_id;
      criteria = await trns.listCriteriaOnDC(id);
      for (let i = 0; i < criteria.length; i++) {
        const e = criteria[i];

        // criteria id
        const c_id = e.U_criteria_id;
        deduction = await trns.totalDeductionPerCriteria(id, c_id);
        if (deduction.length > 0) {
          deduction = deduction[0];
          deduction = [
            {
              quantity: deduction.QTY,
              deduction: deduction.U_deduction,
              total: deduction.TOTAL,
            },
          ];
          // append then push to array
          deduction[0].criteria_id = e.U_criteria_id;
          deduction[0].criteria_desc = e.U_description;
          deduction[0].ItemCode = e.U_item_code;
          listDeductionCriteria.push(deduction[0]);
        }
      }

      // append
      deductionReceipts[0].listDeductionCriteria = listDeductionCriteria;
      deductionReceipts[0].tallies = listDR;
      deductionReceipts = deductionReceipts[0];

      // get rejection of transction; then append
      let rejection = await trns.findRejectionReceiptByTranId(id);
      if (rejection.length > 0) {
        rejection = rejection[0];
        const rej_info = [
          {
            id: rejection.Code,
            transaction_id: rejection.U_transaction_id,
            no_of_bags_rejected: rejection.U_no_of_bags_rejected,
            reason_of_rejection: rejection.U_reason_of_rejection,
            date: rejection.U_date,
            created_by: rejection.U_created_by,
            updated_by: rejection.U_updated_by,
            created_at: rejection.U_created_date,
            updated_at: rejection.U_updated_date,
          },
        ];
        deductionReceipts.rejection = rej_info;
      }
    } else {
      // if there is no transaction id in the body;

      for (let i = 0; i < deductionReceipts.length; i++) {
        const e = deductionReceipts[i];

        const info = {
          id: e.Code,
          transaction_id: e.U_transaction_id,
          date: e.U_date,
          total_deductions: e.U_total_deductions,
          prepared_by: e.U_prepared_by,
          checked_by: e.U_checked_by,
          received_by: e.U_received_by,
          noted_by: e.U_noted_by,
          created_by: e.U_created_by,
          updated_by: e.U_updated_by,
          created_at: e.U_created_date,
          updated_at: e.U_updated_date,
          prepared_signature: e.U_prepared_signature,
          checked_signature: e.U_checked_signature,
          noted_signature: e.U_noted_signature,
        };

        dr.push(info);
      }

      // clear array;
      deductionReceipts = [];

      for (let i = 0; i < dr.length; i++) {
        const e = dr[i];

        // primary key
        const id = e.id;

        listDeductionCriteria = await trns.findDeductionCriteriaByDRId(id);
        let dedCriteria = [];

        if (listDeductionCriteria.length > 0) {
          listDeductionCriteria = listDeductionCriteria[0];
          dedCriteria = [
            {
              id: listDeductionCriteria.Code,
              criteria_id: listDeductionCriteria.U_criteria_id,
              deduction_receipt_id:
                listDeductionCriteria.U_deduction_receipt_id,
              quantity: listDeductionCriteria.U_quantity,
              deduction: listDeductionCriteria.U_deduction,
              total: listDeductionCriteria.U_total,
            },
          ];
          for (let x = 0; x < dedCriteria.length; x++) {
            const d = dedCriteria[x];
            // criteria id
            const c_id = d.criteria_id;
            criteria = await trns.findCriteriaById(c_id);
            criteria = criteria[0];
            const criteria_info = [
              {
                id: criteria.Code,
                description: criteria.U_description,
                created_by: criteria.U_created_by,
                updated_by: criteria.U_updated_by,
                created_at: criteria.U_created_date,
                updated_at: criteria.U_updated_date,
                deduction_per_criteria: criteria.U_deduction_per_criteria,
                business_entity: criteria.U_business_entity,
                status: criteria.U_status,
                item_code: criteria.U_item_code,
              },
            ];

            // append
            dedCriteria[x].criteria = criteria_info;
          }
        }

        e.deduction_criteria = dedCriteria;
      }

      deductionReceipts = dr;
    }

    return { deductionReceipts };
  };
};
