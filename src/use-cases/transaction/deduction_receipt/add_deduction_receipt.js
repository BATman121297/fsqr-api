const { makeDeductionReceipt } = require("../../../entities/deduction_receipt");

module.exports = function postDeductionReceipt({ trns }) {
  return async function makePostDeductionReceipt(request_info) {
    await makeDeductionReceipt(request_info);

    // max code
    const code = await trns.deductionReceiptMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_transaction_id: request_info.transaction_id,
      U_date: null,
      U_total_deductions: request_info.total_deductions,
      U_prepared_by: request_info.prepared_by,
      U_checked_by: null,
      U_received_by: null,
      U_noted_by: null,
      U_prepared_signature: request_info.prepared_signature,
      U_checked_signature: null,
      U_noted_signature: null,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie,
    };

    const res = await trns.addDeductionReceipt({ info });
    // return id
    res.id = max;
    return res;
  };
};
