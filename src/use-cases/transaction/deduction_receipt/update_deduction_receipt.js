const {
  makeUpdateDeductionReceipt,
} = require("../../../entities/deduction_receipt");

module.exports = function putDeductionReceipt({ trns }) {
  return async function makeEditDeductionReceipt(request_info) {
    if (!request_info.noted_by) {
      await makeUpdateDeductionReceipt(request_info);
    }

    const info = {
      Code: request_info.id,
      U_transaction_id: request_info.transaction_id,
      U_total_deductions: request_info.total_deductions,
      U_prepared_by: request_info.prepared_by,
      U_checked_by: request_info.checked_by,
      U_received_by: request_info.received_by,
      U_noted_by: request_info.noted_by,
      U_noted_signature: request_info.noted_signature,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie,
    };
    console.log(info);
    const res = await trns.updateDeductionReceipt({ info });
    return res;
  };
};
