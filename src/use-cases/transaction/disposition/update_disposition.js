const { makeUpdateDisposition } = require("../../../entities/disposition");

module.exports = function putDisposition({ trns }) {
  return async function makeEditDisposition(request_info) {
    await makeUpdateDisposition(request_info);

    const { id, description } = request_info;

    // check if id exist
    const exist = await trns.dispositionIdCheck(id);
    if (exist.length == 0) throw new Error("Disposition doesn't exist.");

    // check if description exist
    const check = await trns.dispositionDescCheckUpdate(id, description);
    if (check.length > 0)
      throw new Error("Disposition description already exist.");

    // update

    const info = {
      Code: id,
      U_description: description,
      U_tag: request_info.tag,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateDisposition({ info });
    return res;
  };
};
