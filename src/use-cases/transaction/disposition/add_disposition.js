const { makeDisposition } = require("../../../entities/disposition");

module.exports = function postDisposition({ trns }) {
  return async function makePostDisposition(request_info) {
    await makeDisposition(request_info);

    const { description } = request_info;

    // check if exist
    const check = await trns.dispositionDescCheck(description);
    if (check.length > 0) throw new Error("Description already exist");

    // get max code
    const code = await trns.dispositionMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_description: description,
      U_tag: request_info.tag,
      U_status: "active",
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    // insert
    const res = await trns.addDisposition({ info });
    return res;
  };
};
