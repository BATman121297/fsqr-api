module.exports = function listDisposition({ trns }) {
  return async function makeListDisposition() {
    const disp = await trns.listDisposition();
    let dispositions = [];

    for (let i = 0; i < disp.length; i++) {
      const e = disp[i];

      const info = {
        id: e.Code,
        description: e.U_description,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        tag: e.U_tag,
        status: e.U_status,
      };

      dispositions.push(info);
    }
    return { dispositions };
  };
};
