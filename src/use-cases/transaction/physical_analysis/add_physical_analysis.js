const { makePhysicalAnalysis } = require("../../../entities/physical_analysis");

module.exports = function postPhysicalAnalysis({ trns }) {
  return async function makePostPhysicalAnalysis(request_info) {
    await makePhysicalAnalysis(request_info);
    // insert physical analysis

    // get max code
    const code = await trns.paMaxCode();
    const paMax = code[0].maxCode;

    const codes = await trns.paRemarksMaxCode();
    let paRMax = codes[0].maxCode;

    const codess = await trns.paDispositionMaxCode();
    let paDMax = codess[0].maxCode;

    // make batch request; insert in two tables;
    let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

    // insert into physical analysis
    batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_PHYANA

{
  "Code": "${paMax}",
  "Name": "${paMax}",
  "U_price_reduction_id": "${request_info.price_reduction_id}",
  "U_reconsideration_id": "${request_info.reconsideration_id}",
  "U_mts_id": "${request_info.moisture_tally_sheet_id}",
  "U_type": "${request_info.type}",
  "U_mc_result": "${request_info.mc_result}",
  "U_impurities": "${request_info.impurities}",
  "U_foreign_material": "${request_info.foreign_material}",
  "U_evaluated_by": "${request_info.evaluated_by}",
  "U_certified_by": "${request_info.certified_by}",
  "U_noted_by": "${request_info.noted_by}",
  "U_evaluated_signature": "${request_info.evaluated_signature}",
  "U_certified_signature": "${request_info.certified_signature}",
  "U_noted_signature": "${request_info.noted_signature}",
  "U_created_by": "${request_info.created_by}",
  "U_created_date": "${request_info.created_date}",
  "U_created_time": "${request_info.created_time}",
  "U_updated_by": "${request_info.created_by}",
  "U_updated_date": "${request_info.created_date}",
  "U_updated_time": "${request_info.created_time}"
}
`;

    // insert into physical analysis remarks
    for (let i = 0; i < request_info.remarks.length; i++) {
      const e = request_info.remarks[i];

      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_PAR

{
  "Code": "${paRMax}",
  "Name": "${paRMax}",
  "U_physical_analysis_id": "${paMax}",
  "U_remarks_id": "${e}"
}
`;

      paRMax++;
    }

    for (let i = 0; i < request_info.disposition_id.length; i++) {
      const e = request_info.disposition_id[i];

      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_PAD

{
  "Code": "${paDMax}",
  "Name": "${paDMax}",
  "U_physical_analysis_id": "${paMax}",
  "U_disposition_id": "${e}"
}
`;
      paDMax++;
    }

    batchStr += `
--b--
--a--
`;

    const info = {
      str: batchStr,
      cookie: request_info.cookie,
    };

    // insert
    const res = await trns.physicalAnalysisBatch({ info });
    res.id = paMax;
    return res;
  };
};
