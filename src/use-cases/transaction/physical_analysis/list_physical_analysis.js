module.exports = function listPhysicalAnalysis({ trns }) {
  return async function makeListPhysicalAnalysis() {
    const pa = await trns.listPhysicalAnalysis();

    let physical_analysis = [];
    for (let i = 0; i < pa.length; i++) {
      const e = pa[i];

      const info = {
        id: e.Code,
        price_reduction_id: e.U_price_reduction_id,
        reconsideration_id: e.U_reconsideration_id,
        type: e.U_type,
        mc_result: e.U_mc_result,
        impurities: e.U_impurities,
        foreign_material: e.U_foreign_material,
        evaluated_by: e.U_evaluated_by,
        certified_by: e.U_certified_by,
        noted_by: e.U_noted_by,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        moisture_tally_sheet_id: e.U_mts_id,
        evaluated_signature: e.U_evaluated_signature,
        certified_signature: e.U_certified_signature,
        noted_signature: e.U_noted_signature,
      };

      physical_analysis.push(info);
    }

    for (let i = 0; i < physical_analysis.length; i++) {
      const e = physical_analysis[i];

      // price reduction
      const pr_id = e.price_reduction_id;
      let pr = await trns.findPriceReductionById(pr_id);
      pr = pr[0];
      const pr_info = [
        {
          id: pr.Code,
          remarks: pr.U_remarks,
          certified_by: pr.U_certified_by,
          created_by: pr.U_created_by,
          updated_by: pr.U_updated_by,
          created_at: pr.U_created_date,
          updated_at: pr.U_updated_date,
        },
      ];
      e.priceReduction = pr_info;

      // moisture tally sheet id
      const m_id = e.moisture_tally_sheet_id;
      let mst = await trns.findMTSById(m_id);
      mst = mst[0];
      const mst_info = [
        {
          id: mst.Code,
          moisture_trial_1: mst.U_moisture_trial_1,
          moisture_trial_2: mst.U_moisture_trial_2,
          moisture_trial_3: mst.U_moisture_trial_3,
          average: mst.U_average,
          analysed_by: mst.U_analysed_by,
          created_by: mst.U_created_by,
          updated_by: mst.U_updated_by,
          created_at: mst.U_created_date,
          updated_at: mst.U_updated_date,
        },
      ];
      e.moisture = mst_info;

      // reconsideration id
      const r_id = e.reconsideration_id;
      let re = await trns.findReconsiderationById(r_id);
      re = re[0];
      const re_info = [
        {
          id: re.Code,
          for_consideration: re.U_for_consideration,
          reason: re.U_reason,
          reconsidered_by: re.U_reconsidered_by,
          reconsidered_approved_by: re.U_reconsidered_approved_by,
          noted_by: re.U_noted_by,
          created_by: re.U_created_by,
          updated_by: re.U_updated_by,
          created_at: re.U_created_date,
          updated_at: re.U_updated_date,
          reconsidered_signature: re.U_reconsidered_signature,
          reconsidered_approved_signature: re.U_reconsidered_approved_signature,
        },
      ];
      e.reconsideration = re_info;

      const id = e.id; // physical analysis id
      const dispo = await trns.getDisposition(id);

      let disposition = [];

      // get all disposition
      for (let x = 0; x < dispo.length; x++) {
        const s = dispo[x];
        // disposition id
        const d_id = s.U_disposition_id;
        let res = await trns.findDispositionById(d_id);
        res = res[0];
        const dispo_info = {
          id: res.Code,
          description: res.U_description,
          tag: res.U_tag,
          status: res.U_status,
          created_by: res.U_created_by,
          created_at: res.U_created_date,
          updated_by: res.U_updated_by,
          updated_at: res.U_updated_date,
        };
        disposition.push(dispo_info);
      }

      e.disposition = disposition;
    }
    
    return { physical_analysis };
  };
};
