const {
  makeUpdatePhysicalAnalysis
} = require("../../../entities/physical_analysis");

module.exports = function putPhysicalAnalysis({ trns }) {
  return async function makeEditPhysicalAnalysis(request_info) {
    if (
      !request_info.onlyRecon &&
      !request_info.addCertification &&
      !request_info.addNote
    ) {
      await makeUpdatePhysicalAnalysis(request_info);
    }

    let result;

    if (request_info.onlyRecon) {
      // update reconsideration id
      const info = {
        Code: request_info.id,
        U_reconsideration_id: request_info.reconsideration_id,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };

      result = await trns.updatePA({ info });
    } else if (request_info.addCertification) {
      const info = {
        Code: request_info.id,
        U_certified_by: request_info.certified_by,
        U_certified_signature: request_info.certified_signature,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };

      result = await trns.updatePA({ info });
    } else if (request_info.addNote) {
      const info = {
        Code: request_info.id,
        U_noted_by: request_info.noted_by,
        U_noted_signature: request_info.noted_signature,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };

      result = await trns.updatePA({ info });
    } else {
      // batch update
      // make batch request;
      let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

      // get id of disposition; to delete
      const id = request_info.id;

      const res = await trns.getDisposition(id);

      for (let i = 0; i < res.length; i++) {
        const e = res[i].Code;
        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
DELETE /b1s/v1/U_FSQR_PAD('${e}')\n`;
      }

      // get id of remarks; to delete;
      const rem = await trns.getRemarks(id);
      for (let i = 0; i < rem.length; i++) {
        const e = rem[i].Code;
        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
DELETE /b1s/v1/U_FSQR_PAR('${e}')\n`;
      }

      // patch main; physical analysis
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/U_FSQR_PHYANA('${request_info.id}')

{
  "U_price_reduction_id": "${request_info.price_reduction_id}",
  "U_reconsideration_id": "${request_info.reconsideration_id}",
  "U_mts_id": "${request_info.moisture_tally_sheet_id}",
  "U_type": "${request_info.type}",
  "U_mc_result": "${request_info.mc_result}",
  "U_impurities": "${request_info.impurities}",
  "U_foreign_material": "${request_info.foreign_material}",
  "U_evaluated_by": "${request_info.evaluated_by}",
  "U_certified_by": "${request_info.certified_by}",
  "U_noted_by": "${request_info.noted_by}",
  "U_evaluated_signature": "${request_info.evaluated_signature}",
  "U_certified_signature": "${request_info.certified_signature}",
  "U_noted_signature": "${request_info.noted_signature}",
  "U_updated_by": "${request_info.updated_by}",
  "U_updated_date": "${request_info.update_date}",
  "U_updated_time": "${request_info.update_time}"
}
`;

      const codes = await trns.paRemarksMaxCode();
      let paRMax = codes[0].maxCode;

      const codess = await trns.paDispositionMaxCode();
      let paDMax = codess[0].maxCode;

      // insert into physical analysis remarks
      for (let i = 0; i < request_info.remarks.length; i++) {
        const e = request_info.remarks[i];

        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_PAR

{
  "Code": "${paRMax}",
  "Name": "${paRMax}",
  "U_physical_analysis_id": "${request_info.id}",
  "U_remarks_id": "${e}"
}
`;

        paRMax++;
      }

      for (let i = 0; i < request_info.disposition_id.length; i++) {
        const e = request_info.disposition_id[i];

        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_PAD
  
{
  "Code": "${paDMax}",
  "Name": "${paDMax}",
  "U_physical_analysis_id": "${request_info.id}",
  "U_disposition_id": "${e}"
}
`;
        paDMax++;
      }

      batchStr += `
--b--
--a--
`;

      const info = {
        str: batchStr,
        cookie: request_info.cookie
      };

      // update
      const req = await trns.physicalAnalysisBatch({ info });
      return req;
    }

    return result;
    // if (
    //   !request_info.onlyRecon &&
    //   !request_info.addCertification &&
    //   !request_info.addNote
    // ) {
    //   await makeUpdatePhysicalAnalysis(request_info, { transaction_db });
    // }
    // // Get old value
    // const oldResult = await transaction_db.findPhysicalAnalysisById(
    //   request_info.id
    // );
    // const old_values = JSON.stringify(oldResult.rows[0]);
    // let result;
    // if (request_info.onlyRecon) {
    //   result = await transaction_db.updatePARID(request_info);
    // } else if (request_info.addCertification) {
    //   result = await transaction_db.addCertification(
    //     request_info,
    //     "physical_analysis"
    //   );
    // } else if (request_info.addNote) {
    //   result = await transaction_db.addNote(request_info, "physical_analysis");
    // } else {
    //   result = await transaction_db.updatePhysicalAnalysis(request_info);
    // }
    // if (result.rowCount > 0) {
    //   delete request_info.role;
    //   delete request_info.modules;
    // } else {
    //   throw new Error("Update failed.");
    // }
    // // Store new values
    // const new_values = JSON.stringify(result.rows[0]);
    // // Define log_info to insert to activity logs
    // const log_info = {
    //   created_by: request_info.updated_by,
    //   action: "update",
    //   table: "physical_analysis",
    //   old_values,
    //   new_values
    // };
    // await transaction_db.addActivityLog(log_info);
    // return result.rows[0];
  };
};
