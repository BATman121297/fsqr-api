const { updateTruck } = require("../../../entities/trucks");

module.exports = function putTruck({ transaction_db }) {
  return async function makeEditTruck(request_info) {
    await updateTruck(request_info, { transaction_db });

    // Get old value
    const oldResult = await transaction_db.findTruckById(
      request_info.id
    );
    const old_values = JSON.stringify(oldResult.rows[0]);

    const result = await transaction_db.updateTruck(request_info);

    if (result.rowCount > 0) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error("Update failed.");
    }

    // Store new values
    const new_values = JSON.stringify(result.rows[0]);

    // Define log_info to insert to activity logs
    const log_info = {
      created_by: request_info.updated_by,
      action: "update",
      table: "truck_infos",
      old_values,
      new_values
    };

    await transaction_db.addActivityLog(log_info);

    return result.rows[0];
  };
};
