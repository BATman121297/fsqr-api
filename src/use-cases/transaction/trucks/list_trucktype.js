module.exports = function listTruckType({ transaction_db }) {
    return async function makeListTruckType() {
      let listTruckTypes = await transaction_db.listTrucktype();
  
      let truckType = listTruckTypes.rows;
      if (!truckType.length) {
        truckType = [];
      }
  
      return { truckType };
    };
  };
  