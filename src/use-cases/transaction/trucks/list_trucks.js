module.exports = function listTruck({ transaction_db }) {
    return async function makeListTruck() {
      let listTrucks = await transaction_db.listTrucks();
  
      let truck = listTrucks.rows;
      if (!truck.length) {
        truck = [];
      }
  
      return { truck };
    };
  };
  