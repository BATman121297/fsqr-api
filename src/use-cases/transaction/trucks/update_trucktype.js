const { updateTrucktype } = require("../../../entities/trucks");

module.exports = function putTrucktype({ transaction_db }) {
  return async function makeEditTrucktype(request_info) {
    await updateTrucktype(request_info, { transaction_db });

    // Get old value
    const oldResult = await transaction_db.findTrucktypeById(
      request_info.id
    );
    const old_values = JSON.stringify(oldResult.rows[0]);

    const result = await transaction_db.updateTruckType(request_info);

    if (result.rowCount > 0) {
      delete request_info.role;
      delete request_info.modules;
    } else {
      throw new Error("Update failed.");
    }

    // Store new values
    const new_values = JSON.stringify(result.rows[0]);

    // Define log_info to insert to activity logs
    const log_info = {
      created_by: request_info.updated_by,
      action: "update",
      table: "truck_types",
      old_values,
      new_values
    };

    await transaction_db.addActivityLog(log_info);

    return result.rows[0];
  };
};
