const { makeTruck } = require("../../../entities/trucks") ;

module.exports = function postTruck(
  { transaction_db }
) {
  return async function makePostTruck(request_info) {
    await makeTruck(request_info);

    const result = await transaction_db.addTruck(request_info);

    if (!result) {
      throw new Error("Insert failed.");
    }

    const old_values = "{}";
    const new_values = JSON.stringify(result.rows[0]);

    const log_info = {
        created_by: request_info.created_by,
        action: "create",
        table: "truck_infos",
        old_values,
        new_values 
    };

    await transaction_db.addActivityLog(log_info);

    return result.rows;
  };
};
