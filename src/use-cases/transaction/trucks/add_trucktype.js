const { makeTrucktype } = require("../../../entities/trucks") ;

module.exports = function postTrucktype(
  { transaction_db }
) {
  return async function makePostTrucktype(request_info) {
    await makeTrucktype(request_info);

    const result = await transaction_db.addTruckType(request_info);

    if (!result) {
      throw new Error("Insert failed.");
    }

    const old_values = "{}";
    const new_values = JSON.stringify(result.rows[0]);

    const log_info = {
        created_by: request_info.created_by,
        action: "create",
        table: "truck_types",
        old_values,
        new_values 
    };

    await transaction_db.addActivityLog(log_info);

    return result.rows;
  };
};
