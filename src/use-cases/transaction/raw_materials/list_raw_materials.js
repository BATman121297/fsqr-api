module.exports = function listRawMaterials({ trns }) {
  return async function makeListRawMaterials() {
    const res = await trns.listRawMats();
    let raw_materials = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        description: e.U_description,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };

      raw_materials.push(info);
    }
    return { raw_materials };
  };
};
