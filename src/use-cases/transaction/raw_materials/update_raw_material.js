const { makeUpdateRawMaterials } = require("../../../entities/raw_materials");

module.exports = function putRawMaterials({ trns }) {
  return async function makeEditRawMaterials(request_info) {
    await makeUpdateRawMaterials(request_info);

    // check if id exist
    const { id, description } = request_info;

    const exist = await trns.rawMatsIdCheck(id);
    if (exist.length == 0) throw new Error("Raw Material doesn't exist.");

    // check if description exist
    const check = await trns.rawMatsDescCheckUpdate(id, description);
    if (check.length > 0)
      throw new Error("Raw Material description already exist.");

    const info = {
      Code: id,
      Name: description,
      U_description: description,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateRawMats({ info });
    return res;
  };
};
