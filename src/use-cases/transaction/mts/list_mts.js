module.exports = function listMTS({ trns }) {
  return async function makeListMTS(request_info) {
    const res = await trns.selectMTS(request_info.id);
    let mts = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        moisture_trial_1: e.U_moisture_trial_1,
        moisture_trial_2: e.U_moisture_trial_2,
        moisture_trial_3: e.U_moisture_trial_3,
        average: e.U_average,
        analysed_by: e.U_analysed_by,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };

      mts.push(info);
    }

    return { mts };
  };
};
