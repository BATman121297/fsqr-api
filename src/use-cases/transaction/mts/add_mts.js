const { makeMTS } = require("../../../entities/mts");

module.exports = function postMTS({ trns }) {
  return async function makePostMTS(request_info) {
    await makeMTS(request_info);

    // get max code
    const code = await trns.mtsMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_moisture_trial_1: request_info.moisture_trial_1,
      U_moisture_trial_2: request_info.moisture_trial_2,
      U_moisture_trial_3: request_info.moisture_trial_3,
      U_average: request_info.average,
      U_analysed_by: request_info.analysed_by,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie,
    };

    const res = await trns.addMTS({ info });
    res.id = max;
    return res;
  };
};
