const { makeUpdateMTS } = require("../../../entities/mts");

module.exports = function putMTS({ trns }) {
  return async function makeEditMTS(request_info) {
    await makeUpdateMTS(request_info);

    // check if id exist
    const { id } = request_info;

    const exist = await trns.selectMTS(id);
    if (exist.length == 0) throw new Error("MTS doesn't exist.");

    const info = {
      Code: id,
      U_moisture_trial_1: request_info.moisture_trial_1,
      U_moisture_trial_2: request_info.moisture_trial_2,
      U_moisture_trial_3: request_info.moisture_trial_3,
      U_average: request_info.average,
      U_analysed_by: request_info.analysed_by,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateMTS({ info });
    return res;
  };
};
