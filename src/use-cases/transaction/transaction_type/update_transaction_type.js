const { updateTransactionType } = require("../../../entities/transaction_type");

module.exports = function putTransactionType({ trns }) {
  return async function makeEditTransactionType(request_info) {
    await updateTransactionType(request_info);

    const info = {
      Code: request_info.id,
      U_name: request_info.name,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    // update
    const res = await trns.updateTrnsType({ info });
    return res;
  };
};
