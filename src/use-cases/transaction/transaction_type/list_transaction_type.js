module.exports = function listTransactionType({ trns }) {
  return async function makeListTransactionType() {
    const res = await trns.listTransactionTypes();

    let transactionTypes = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];
      const info = {
        id: e.Code,
        name: e.U_name,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };

      transactionTypes.push(info);
    }
    return { transactionTypes };
  };
};
