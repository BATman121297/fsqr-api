const { makeTransactionType } = require("../../../entities/transaction_type");

module.exports = function postTransactionType({ trns }) {
  return async function makePostTransactionType(request_info) {
    await makeTransactionType(request_info);

    // get max code
    const code = await trns.trnsTypeMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_name: request_info.name,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addTrnsType({ info });
    return res;
  };
};
