const { makeRemarks } = require("../../../entities/remarks");

module.exports = function postRemarks({ trns }, { moment }) {
  return async function makePostRemarks(request_info) {
    await makeRemarks(request_info);

    const { name } = request_info;

    // get max code
    const code = await trns.remarksMaxCode();
    const max = code[0].maxCode;

    // check if exist
    const check = await trns.remarksNameCheck(name);
    if (check.length > 0) throw new Error("Name already exist");

    const info = {
      Code: max,
      Name: max,
      U_name: name,
      U_tag: request_info.tag,
      U_status: "active",
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addRemarks({ info });
    return res;
  };
};
