module.exports = function listRemarks({ trns }) {
  return async function makeListRemarks() {
    const res = await trns.listRemarks();
    let remarks = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];
      const info = {
        id: e.Code,
        name: e.U_name,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        tag: e.U_tag,
        status: e.U_status,
      };
      remarks.push(info);
    }
    return { remarks };
  };
};
