const { makeUpdateRemarks } = require("../../../entities/remarks");

module.exports = function putRemarks({ trns }) {
  return async function makeEditRemarks(request_info) {
    await makeUpdateRemarks(request_info);

    // check if id exist
    const { id, name } = request_info;

    const exist = await trns.listRemarks(id);
    if (exist.length == 0) throw new Error("Remark doesn't exist.");

    // check if description exist
    const check = await trns.remarksCheckUpdate(id, name);
    if (check.length > 0) throw new Error("Remark already exist.");

    const info = {
      Code: id,
      U_name: name,
      U_tag: request_info.tag,
      U_status: request_info.status,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateRemarks({ info });
    return res;
  };
};
