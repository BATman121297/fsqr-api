const {
  makeDeductionCriteria,
} = require("../../../entities/deduction_criteria");

module.exports = function postDeductionCriteria({ trns }) {
  return async function makePostDeductionCriteria(request_info) {
    await makeDeductionCriteria(request_info);

    //
    // get max code
    const code = await trns.deductionCriteriaMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_criteria_id: request_info.criteria_id,
      U_deduction_receipt_id: request_info.deduction_receipt_id,
      U_quantity: request_info.quantity,
      U_deduction: request_info.deduction,
      U_total: request_info.total,
      U_created_by: request_info.employee_id,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.employee_id,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie,
    };

    const res = await trns.addDeductionCriteria({ info });
    res.id = max;
    return res;
  };
};
