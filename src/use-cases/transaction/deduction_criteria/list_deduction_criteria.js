module.exports = function listDeductionCriteria({ trns }) {
  return async function makeListDeductionCriteria() {
    const res = await trns.listDeductionCriteria();
    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      // criteria id
      const cid = e.U_criteria_id;

      const criteria = await trns.findCriteriaById(cid);
      e.criteria = criteria;

      // deduction receipt id
      const did = e.U_deduction_receipt_id;
      const deduction_receipt = await trns.findDeductionReceiptById(did);
      e.deduction_receipt = deduction_receipt;
    }

    return { res };
  };
};
