const {
  makeUpdateDeductionCriteria
} = require("../../../entities/deduction_criteria");

module.exports = function putDeductionCriteria({ trns }) {
  return async function makeEditDeductionCriteria(request_info) {
    await makeUpdateDeductionCriteria(request_info);


    const info = {
      Code: request_info.id,
      U_criteria_id: request_info.criteria_id,
      U_deduction_receipt_id: request_info.deduction_receipt_id,
      U_quantity: request_info.quantity,
      U_deduction: request_info.deduction,
      U_total: request_info.total,
      U_updated_by: request_info.employee_id,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateDeductionCriteria({ info });
    return res;
    // // Get old value
    // const oldResult = await transaction_db.findDeductionCriteriaById(
    //   request_info.id
    // );
    // const old_values = JSON.stringify(oldResult.rows[0]);

    // const result = await transaction_db.updateDeductionCriteria(request_info);

    // if (result.rowCount > 0) {
    //   delete request_info.role;
    //   delete request_info.modules;
    // } else {
    //   throw new Error("Update failed.");
    // }

    // // Store new values
    // const new_values = JSON.stringify(result.rows[0]);

    // // Define log_info to insert to activity logs
    // const log_info = {
    //   created_by: request_info.updated_by,
    //   action: "update",
    //   table: "deduction_criteria",
    //   old_values,
    //   new_values
    // };

    // await transaction_db.addActivityLog(log_info);

    // return result.rows[0];
  };
};
