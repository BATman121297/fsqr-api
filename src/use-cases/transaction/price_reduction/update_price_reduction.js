const {
  makeUpdatePriceReduction
} = require("../../../entities/price_reduction");

module.exports = function putPriceReduction({ trns }) {
  return async function makeEditPriceReduction(request_info) {
    await makeUpdatePriceReduction(request_info);

    // check if id exist
    const { id } = request_info;

    const exist = await trns.priceReductionCheckExist(id);
    if (exist.length == 0) throw new Error("Price reduction doesn't exist.");

    // update
    const info = {
      Code: id,
      U_remarks: request_info.remarks,
      U_certified_by: request_info.certified_by,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updatePR({ info });
    return res;
  };
};
