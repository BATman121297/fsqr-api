module.exports = function listPriceReduction({ trns }) {
  return async function makeListPriceReduction() {
    const res = await trns.listPriceReduction();
    let price_reduction = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        remarks: e.U_remarks,
        certified_by: e.U_certified_by,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };

      price_reduction.push(info);
      
    }
    return { price_reduction };
  };
};
