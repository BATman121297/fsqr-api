const { makePriceReduction } = require("../../../entities/price_reduction");

module.exports = function postPriceReduction({ trns }) {
  return async function makePostPriceReduction(request_info) {
    await makePriceReduction(request_info);


    // get max code
    const code = await trns.priceReductionMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_remarks: request_info.remarks,
      U_certified_by: request_info.certified_by,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    // insert
    const res = await trns.addPriceReduction({ info });
    return res;
  };
};
