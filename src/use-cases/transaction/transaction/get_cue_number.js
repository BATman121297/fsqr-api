module.exports = function postCueNumber({ trns, moment }) {
  return async function makePostCueNumber(request_info) {
    const business_entity = request_info.business_entity;
    if (!business_entity) {
      throw new Error("Business Entity required");
    }

    const d = new Date().toDateString();
    const dates = moment(d).format("YYYY-MM-DD");
    const counts = await trns.countCue(dates);
    let count = counts[0].counts;
    count++;
    const seriesNumber = formatCount(count);



    let beCode;
    if (business_entity.includes("-")) {
     beCode = business_entity.split("-");
    } else if (business_entity.includes(" ")) {

       be = business_entity.split(" ");

      if (be[0] == "CORN") {
        beCode = "CDF";
      } else if (be[0] == "RICE") {
        beCode = "PDF";
      } else if (be[0] == "FEEDMILL") {
        beCode = "FMP";
      } else {
        throw new Error("Prefix for warehouse is missing");
      }

    } else {
      throw new Error("Business Entity is required");
    }



    let cueCode;

    if (request_info.supplier_name.toLowerCase() == "revive") {
      cueCode = beCode + "-" + seriesNumber + "-R";
    } else {
      cueCode = beCode + "-" + seriesNumber;
    }



    return cueCode;
  };
};

const formatCount = (number) => {
  let toString = number.toString();

  while (toString.length < 4) {
    toString = "0" + toString;
  }

  return toString;
};
