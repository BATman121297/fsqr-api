const { makeUpdateEntry } = require("../../../entities/transaction");

module.exports = function putEntry({ trns }) {
  return async function makeEditEntry(request_info) {
    await makeUpdateEntry(request_info);


    const { cookie } = request_info; // SL cookie
    delete request_info.cookie;

    //const { id, time_start } = request_info;

    // // check if exist
    // const check = await trns.updateEntry(time_start, id);


    // console.log("check")
    // console.log(check)

    // if (check.length > 0) {
    //   // exist
    //   throw new Error("Module already exists");
    // } else {
      // update here
      const info = {
        Code: request_info.id,
        Name: request_info.name,
        U_time_start: request_info.time_start,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie
      };

      const res = await trns.updateEntry({ info });

      return res;
    // }
  };
};
