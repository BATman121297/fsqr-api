const { makeTransaction } = require("../../../entities/transaction");

module.exports = function postTransaction({ trns }, { moment }) {
  return async function makePostTransaction(request_info) {
    const business_entity = request_info.business_entity;

    if (!business_entity) {
      throw new Error("Business Entity is required");
    }

    const { created_date } = request_info;
    const dates = moment(created_date).format("YYYY-MM");
    const counts = await trns.countTransaction(dates);
    const count = counts[0].counts;

    const seriesNumber = formatCount(count);

    const year = moment().format("YY");
    const month = moment().format("MM");

    let supplierCode;
    if (business_entity.includes("-")) {
      const beCode = business_entity.split("-");

      supplierCode = beCode[0] + "-" + year + month + seriesNumber;

      request_info.supplier_code = supplierCode;
      request_info.queue_no = beCode[0] + "-" + seriesNumber;
      if (beCode[0] != "UNC") {
        await makeTransaction(request_info);

        // if no status in body req; auto sampling status
        const status = await trns.statusNameCheck("sampling");
        request_info.status_id = status[0].Code;
      }
    } else if (business_entity.includes(" ")) {
      let Prefix;

      const beCode = business_entity.split(" ");

      if (beCode[0] == "CORN") {
        Prefix = "CDF";
      } else if (beCode[0] == "RICE") {
        Prefix = "PDF";
      } else if (beCode[0] == "FEEDMILL") {
        Prefix = "FMP";
      } else {
        throw new Error("Prefix for warehouse is missing");
      }

      supplierCode = Prefix + "-" + year + month + seriesNumber;

      request_info.supplier_code = supplierCode;
      request_info.queue_no = Prefix + "-" + seriesNumber;

      // if no status in body req; auto sampling status
      const status = await trns.statusNameCheck("sampling");
      request_info.status_id = status[0].Code;
    } else {
      throw new Error("Business Entity is required");
    }

    // get BIR data
    const bir_id = request_info.bir_id;
    const bir = await trns.findBIRById(bir_id);

    // do checking; BU UDF fields and ItmsGrpCod

    // check BU UDF;
    for (let i = 0; i < bir.length; i++) {
      const e = bir[i];

      const { U_APP_BU_Feedmill, U_APP_BU_Paddy, U_APP_BU_CDF, ItmsGrpCod } = e;

      // check if has this UDF
      if (!U_APP_BU_Feedmill || !U_APP_BU_Paddy || !U_APP_BU_CDF)
        throw new Error(`Item doesn't have Business Entity.`);

      // if item doesnt belong to any BU
      if (
        U_APP_BU_Feedmill == "N" &&
        U_APP_BU_Paddy == "N" &&
        U_APP_BU_CDF == "N"
      )
        throw new Error(`Item doesn't belong to any Business Entity.`);

      // 310 && 313 are macro; 314 is micro;
      if (ItmsGrpCod !== 310 && ItmsGrpCod !== 313 && ItmsGrpCod !== 314)
        throw new Error(`Item Code does not belong to any Raw Material Type!`);
    }

    // get item data
    if (bir.length > 0) {
      const item_id = bir[0].ItemCode;
      const item = await trns.findItemById(item_id);

      request_info.description_of_articles = item[0].ItemName;
      request_info.unit = "sack";
    }

    if (!request_info.delivery_receipt) {
      // if no dr; return error; for user to input dr; to be auto generate soon; phase 2?
      const err = {
        code: 1,
        msg: "No delivery receipt.",
      };
      throw new Error(JSON.stringify(err));
    }

    // create batch request; insert into delivery receipt,
    // transactions, and transaction logs;

    let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

    // get max code; delivery receipt
    const codes = await trns.delReceiptMaxCode();
    const drMax = codes[0].maxCode;

    batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_DR

{
  "Code": "${drMax}",
  "Name": "${drMax}",
  "U_dr_id": "${request_info.delivery_receipt}",
  "U_quantity": "${request_info.no_of_bags ? request_info.no_of_bags : 0}",
  "U_unit": "${request_info.unit}",
  "U_description_of_articles": "${request_info.description_of_articles}",
  "U_created_by": "${request_info.created_by}",
  "U_created_date": "${request_info.created_date}",
  "U_created_time": "${request_info.created_time}",
  "U_updated_by": "${request_info.created_by}",
  "U_updated_date": "${request_info.created_date}",
  "U_updated_time": "${request_info.created_by}"
}
`;

    // get max code; transaction
    const code = await trns.transactionsMaxCode();
    const trnsMax = code[0].maxCode;

    // macro; single item only
    if (request_info.isMacro) {
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_TRNS

{
  "Code": "${trnsMax}",
  "Name": "${trnsMax}",
  "U_DR_id": "${drMax}",
  "U_rmaf_id": null,
  "U_bir_id": "${request_info.bir_id}",
  "U_status_id": "${request_info.status_id}",
  "U_trckpltno": "${request_info.plate_number}",
  "U_trnstyp_id": "${request_info.transaction_type_id}",
  "U_driverID": null,
  "U_driver": "${request_info.driver_name}",
  "U_suppl_code": "${request_info.supplier_code}",
  "U_no_of_bags": "${request_info.no_of_bags}",
  "U_description": "${request_info.description_of_articles}",
  "U_g_on_dty": "${request_info.guard_on_duty}",
  "U_time_end": null,
  "U_qc_cntrl_no": "${request_info.supplier_code}",
  "U_created_by": "${request_info.created_by}",
  "U_created_date": "${request_info.created_date}",
  "U_created_time": "${request_info.created_time}",
  "U_updated_by": "${request_info.created_by}",
  "U_updated_date": "${request_info.created_date}",
  "U_updated_time": "${request_info.created_time}",
  "U_queue_no": "${request_info.queue_no}",
  "U_spplr_id": null,
  "U_spplr_name": "${request_info.supplier_name}",
  "U_spplr_add": "${request_info.address}",
  "U_PO": null
}
`;
    }

    // micro; multiple items
    if (request_info.isMicro) {
      // transaction
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_TRNS

{
  "Code": "${trnsMax}",
  "Name": "${trnsMax}",
  "U_DR_id": "${drMax}",
  "U_rmaf_id": null,
  "U_bir_id": null,
  "U_status_id": "${request_info.status_id}",
  "U_trckpltno": "${request_info.plate_number}",
  "U_trnstyp_id": "${request_info.transaction_type_id}",
  "U_driverID": null,
  "U_driver": "${request_info.driver_name}",
  "U_suppl_code": "${request_info.supplier_code}",
  "U_no_of_bags": null,
  "U_description": "${
        request_info.description_of_articles
          ? request_info.description_of_articles
          : null
        }",
  "U_g_on_dty": "${request_info.guard_on_duty}",
  "U_time_end": null,
  "U_qc_cntrl_no": "${request_info.supplier_code}",
  "U_created_by": "${request_info.created_by}",
  "U_created_date": "${request_info.created_date}",
  "U_created_time": "${request_info.created_time}",
  "U_updated_by": "${request_info.created_by}",
  "U_updated_date": "${request_info.created_date}",
  "U_updated_time": "${request_info.created_time}",
  "U_queue_no": "${request_info.queue_no}",
  "U_spplr_id": null,
  "U_spplr_name": "${request_info.supplier_name}",
  "U_spplr_add": "${request_info.address}",
  "U_PO": "${request_info.PO}"
}
`;

      // get max code for micro details table;
      const code = await trns.transactionsMicroDetailsMaxCode();
      let microMax = code[0].maxCode;

      for (let i = 0; i < request_info.microItems.length; i++) {
        const e = request_info.microItems[i];

        // get item description;
        const name = await trns.returnItemDesc(e.id);
        let itemName = "";
        if (name.length > 0) itemName = name[0].ItemName;

        batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_MICRO_DETAILS

{
  "Code": "${microMax}",
  "Name": "${microMax}",
  "U_transaction_id": "${trnsMax}",
  "U_itemCode": "${e.id}",
  "U_itemDesc": "${itemName}",
  "U_qty": "${e.qty}",
  "U_unitMeasure": "${e.unit}"
}
`;
        microMax++;
      }
    }

    // get "input" status;
    const status = await trns.statusNameCheck("input");
    if (status.length == 0) throw new Error("Status 'Input' does not exist.");

    const statusId = status[0].Code;

    // get max code; transaction logs
    const codelogs = await trns.transactionLogsMaxCode();
    const trnsLogsMax = codelogs[0].maxCode;

    batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_TRANS_LOG

{
  "Code": "${trnsLogsMax}",
  "Name": "${trnsLogsMax}",
  "U_transaction_id": ${trnsMax},
  "U_status_id": ${statusId},
  "U_start_time": "${request_info.start_time}",
  "U_end_time": "${request_info.end_time}",
  "U_created_by": "${request_info.created_by}",
  "U_created_date": "${request_info.created_date}",
  "U_created_time": "${request_info.created_time}"
}
`;

    batchStr += `
--b--
--a--
`;

    const info = {
      str: batchStr,
      cookie: request_info.cookie,
    };

    const res = await trns.transactionInsert({ info });
    res.transactionId = trnsMax; // return transaction id for auto tag of PO;
    return res;
  };
};

const formatCount = (number) => {
  let toString = number.toString();

  while (toString.length < 3) {
    toString = "0" + toString;
  }

  return toString;
};
