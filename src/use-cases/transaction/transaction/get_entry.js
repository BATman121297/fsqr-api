module.exports = function listEntry({ trns }) {
    return async function makeListEntry() {
      const result = await trns.listEntry();
      return result;
    };
  };
  