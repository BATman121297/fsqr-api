const { makeFindQR } = require("../../../entities/transaction");
module.exports = function findQR({ trns }, { moment }) {
  return async function makeFindQR(request_info) {
    let id = request_info.id;
    let find = await trns.findQR(id);
    // console.log("use-cases");
    // console.log(find);

    const { cookie } = request_info; // SL cookie
    delete request_info.cookie;

    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    const time_today = moment()
      .tz("Asia/Manila")
      .format("HHmm");

    if (find) {
      let findTransaction = await trns.findTransactionById(id);
      // console.log("FindTransaction");
      // console.log(findTransaction);

      if (findTransaction) {
        // console.log("Welcome");
        if (!find[0].U_time_start && !find[0].U_time_end) {
          // console.log("Time start must be start here!");

          const info = {
            Code: request_info.id,
            U_updated_by: request_info.updated_by,
            U_time_start: time_today,
            U_updated_date: date_today,
            U_updated_time: time_today,
            cookie
          };
          // console.log({ info });

          const res = await trns.updateEntry({ info });

          return res;
        } else if (find[0].U_time_start && !find[0].U_time_end) {
          // console.log("Time end must be here!");

          const info = {
            Code: request_info.id,
            U_updated_by: request_info.updated_by,
            U_time_end: time_today,
            U_updated_date: date_today,
            U_updated_time: time_today,
            cookie
          };
          // console.log("exit");
          // console.log({ info });

          const res = await trns.updateExit({ info });

          return res;
        } else if (find[0].U_time_start && find[0].U_time_end) {
          throw new Error("Time start and Time end is already exist");
        } else {
          throw new Error("Time start and Time end is already exist");
        }
      } else {
        throw new Error("Transaction ID not found");
      }
    } else {
      throw new Error("Error, There is must be wrong! Please check!");
    }
  };
};
