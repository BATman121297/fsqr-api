const { makeUpdateExit } = require("../../../entities/transaction");

module.exports = function putExit({ trns }) {
  return async function makeEditExit(request_info) {
    await makeUpdateExit(request_info);


    const { cookie } = request_info; // SL cookie
    delete request_info.cookie;

    //const { id, time_start } = request_info;

    // // check if exist
    // const check = await trns.updateEntry(time_start, id);


    // console.log("check")
    // console.log(check)

    // if (check.length > 0) {
    //   // exist
    //   throw new Error("Module already exists");
    // } else {
      // update here
      const info = {
        Code: request_info.id,
        Name: request_info.name,
        U_time_end: request_info.time_end,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie
      };

      const res = await trns.updateExit({ info });

      return res;
    // }
  };
};
