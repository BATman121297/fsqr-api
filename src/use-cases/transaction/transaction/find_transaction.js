require("dotenv").config();

module.exports = function findTransaction({ trns, getDeductionReceipt }) {
  return async function makefindTransaction(request_info) {
    let findTransaction, remarks, disposition_id, disposition_id2;

    // ############################### SL Login (not in use)
    /*
    // login to truckscale schema;
    const tsis = await trns.truckScaleSAPLogin();
    if (tsis.status !== 200)
      throw new Error(`Cannot connect to TruckScale, please try again.`);

    const tsisSession = tsis.data; // session
    */

    // login to biotech schema;
    const bfi = await trns.bfiSAPLogin();
    if (bfi.status !== 200)
      throw new Error(`Cannot connect to BioTech, please try again.`);

    const bfiSession = bfi.data; // session
    // ###############################

    // Get transactions
    if (request_info.employee_id.toLowerCase() == "truck_scale") {
      const code = request_info.supplier_code;
      findTransaction = await trns.findTransactionBySupplierCode(code);
      let ts = findTransaction[0];
      findTransaction = null;
      findTransaction = [
        {
          id: ts.Code,
          status_id: ts.U_status_id,
          bir_id: ts.U_bir_id,
          rmaf_id: ts.U_rmaf_id,
          delivery_receipt_id: ts.U_DR_id,
          transaction_type_id: ts.U_trnstyp_id,
          purchase_order_id: ts.U_PO,
          supplier_id: ts.U_spplr_id,
          driver_id: ts.U_driverID,
          supplier_code: ts.U_suppl_code,
          qc_control_number: ts.U_qc_cntrl_no,
          queue_number: ts.U_queue_no,
          no_of_bags: ts.U_no_of_bags,
          supplier_name: ts.U_spplr_name,
          supplier_address: ts.U_spplr_add,
          driver_name: ts.U_driver,
          plate_number: ts.U_trckpltno,
          time_end: ts.U_time_end,
          guard_on_duty: ts.U_g_on_dty,
          updated_by: ts.U_updated_by,
          created_at: ts.U_created_date,
          updated_at: ts.U_updated_date,
          time_start: ts.U_time_start,
        },
      ];
    } else {
      const id = request_info.transaction_id;
      findTransaction = await trns.findTransactionById(id);
      let ts = findTransaction[0];
      findTransaction = null;
      findTransaction = [
        {
          id: ts.Code,
          status_id: ts.U_status_id,
          bir_id: ts.U_bir_id,
          rmaf_id: ts.U_rmaf_id,
          delivery_receipt_id: ts.U_DR_id,
          transaction_type_id: ts.U_trnstyp_id,
          purchase_order_id: ts.U_PO,
          supplier_id: ts.U_spplr_id,
          driver_id: ts.U_driverID,
          supplier_code: ts.U_suppl_code,
          qc_control_number: ts.U_qc_cntrl_no,
          queue_number: ts.U_queue_no,
          no_of_bags: ts.U_no_of_bags,
          supplier_name: ts.U_spplr_name,
          supplier_address: ts.U_spplr_add,
          driver_name: ts.U_driver,
          plate_number: ts.U_trckpltno,
          time_end: ts.U_time_end,
          guard_on_duty: ts.U_g_on_dty,
          updated_by: ts.U_updated_by,
          created_at: ts.U_created_date,
          updated_at: ts.U_updated_date,
          time_start: ts.U_time_start,
        },
      ];
    }
    let transactions = findTransaction;

    for (let i = 0; i < transactions.length; i++) {
      const e = transactions[i];

      remarks = [];
      disposition_id = [];
      disposition_id2 = [];

      if (request_info.getWeight) {
        // get net weight; tsis route;
        info = {};
        info = {
          user: process.env.TSIS_USER,
          pw: process.env.TSIS_PW,
          code: e.supplier_code,
        };
        const net_weight = await trns.truckScaleGetNetWeight({ info });
        if (net_weight.status !== 200)
          throw new Error(`Problem retrieving net weight, please try again.`);

        // check if there is data
        if (net_weight.data.length > 0) {
          const nw = net_weight.data;
          // append to transaction;
          e.weight = nw;
        }
        // ####### done get data from truckscale
      }
      // if there is PO get PO details;
      if (e.purchase_order_id) {
        const PO = e.purchase_order_id;

        // this retrive on BFI db;
        const poDetails = await trns.getPOdetailsBFI(bfiSession, PO);
        e.po_details = poDetails;
      }
      // get deduction receipt with criteria
      // ###
      // get deduction receipt with criteria
      const id = e.id; // transaction id
      const receipt_info = {
        employee_id: request_info.employee_id,
        transaction_id: id,
      };
      let deductions = await getDeductionReceipt(receipt_info);
      e.deductions = deductions;

      // get total deductions
      let listDeductionCriteria = [];
      if (deductions.length > 0) {
        // if there is deductions
        const criteria = await trns.listCriteriaOnDC(id);

        for (let x = 0; x < criteria.length; x++) {
          const d = criteria[x];
          const c_id = d.U_criteria_id; // criteria id
          const deduction = await trns.totalDeductionPerCriteria(id, c_id);

          if (deduction.length > 0) {
            deduction[0].criteria_id = d.U_criteria_id;
            deduction[0].criteria_desc = d.U_description;
            deduction[0].item_code = d.U_item_code;
            listDeductionCriteria.push(deduction[0]);
          }
        }
        // #####
        let deductionReceipts = {};
        deductionReceipts.listDeductionCriteria = listDeductionCriteria[0];

        const rejection = await trns.findRejectionReceiptByTranId(id);
        deductionReceipts.rejection = rejection[0];
        e.deduction_receipt = deductionReceipts;
      }

      // micro packages
      let package = await trns.getMicroPackages(id);
      e.micro_package = package;
      // ###########

      // delivery receipt id
      const d_id = e.delivery_receipt_id;
      let delivery_receipt = await trns.findDeliveryReceiptByID(d_id);
      if (delivery_receipt.length > 0) {
        delivery_receipt = [
          {
            id: delivery_receipt[0].Code,
            quantity: delivery_receipt[0].U_quantity,
            unit: delivery_receipt[0].U_unit,
            descriptions_of_articles:
              delivery_receipt[0].U_description_of_articles,
          },
        ];
      }
      // rmaf id
      const r_id = e.rmaf_id;
      let rmaf = await trns.findRMAFById(r_id);
      let rmafs = rmaf; // hold for rmaf to be remodel
      rmaf = null;
      for (let x = 0; x < rmafs.length; x++) {
        const d = rmafs[x];

        // remodel
        rmaf = [
          {
            id: d.Code,
            physical_analysis_id: d.U_physical_analysis_id,
            chemical_analysis_id: d.U_chemical_analysis_id,
            date: d.U_date,
            material_rejection_report_id: d.U_material_rejection_report_id,
            updated_by: d.U_updated_by,
            updated_date: d.U_updated_date,
            updated_time: d.U_updated_time,
          },
        ];

        // physical analysis id
        const p_id = d.U_physical_analysis_id;
        let physical = await trns.findPhysicalAnalysisById(p_id);
        let physicals = physical; // hold dummy
        physical = null;
        for (let q = 0; q < physicals.length; q++) {
          const w = physicals[q];

          // remodel
          physical = [
            {
              id: w.Code,
              price_reduction_id: w.U_price_reduction_id,
              reconsideration_id: w.U_reconsideration_id,
              mts_id: w.U_mts_id,
              dispo_id: w.U_dispo_id,
              type: w.U_type,
              mc_result: w.U_mc_result,
              impurities: w.U_impurities,
              foreign_material: w.U_foreign_material,
              evaluated_by: w.U_evaluated_by,
              certified_by: w.U_certified_by,
              noted_by: w.U_noted_by,
              evaluated_signature: w.U_evaluated_signature,
              certified_signature: w.U_certified_signature,
              noted_signature: w.U_noted_signature,
              created_by: w.U_created_by,
              created_date: w.U_created_date,
              created_time: w.U_created_time,
              updated_by: w.U_updated_by,
              updated_date: w.U_updated_date,
              updated_time: w.U_updated_time,
            },
          ];
          // ###
          let getRemarks = await trns.getRemarkPA(p_id);

          for (let a = 0; a < getRemarks.length; a++) {
            const s = getRemarks[a];
            remarks.push(s.U_remarks_id);
          }

          // ###
          let getDisposition = await trns.getDispoPA(p_id);
          for (let a = 0; a < getDisposition.length; a++) {
            const s = getDisposition[a];
            disposition_id.push(s.U_disposition_id);
          }

          // ###
          // price reduction id
          const pr_id = w.U_price_reduction_id;
          let price_reduction = await trns.findPriceReductionById(pr_id);
          if (price_reduction.length > 0) {
            let pr = price_reduction;
            price_reduction = null;
            pr = pr[0];
            // remodel
            price_reduction = [
              {
                id: pr.Code,
                remarks: pr.U_remarks,
                certified_by: pr.U_certified_by,
                created_by: pr.U_created_by,
                created_date: pr.U_created_date,
                created_time: pr.U_created_time,
                updated_by: pr.U_updated_by,
                updated_date: pr.U_updated_date,
                updated_time: pr.U_updated_time,
              },
            ];
          }

          // reconsideration id
          const r_id = w.U_reconsideration_id;
          let reconsideration = await trns.findReconsiderationById(r_id);
          if (reconsideration.length > 0) {
            let re = reconsideration;
            reconsideration = null;
            re = re[0];

            // remodel
            reconsideration = [
              {
                id: re.Code,
                for_consideration: re.U_for_consideration,
                reason: re.U_reason,
                reconsidered_by: re.U_reconsidered_by,
                reconsidered_approved_by: re.U_reconsidered_approved_by,
                reconsidered_signature: re.U_reconsidered_signature,
                reconsidered_approved_signature:
                  re.U_reconsidered_approved_signature,
                noted_by: re.U_noted_by,
                created_by: re.U_created_by,
                created_date: re.U_created_date,
                created_time: re.U_created_time,
                updated_by: re.U_updated_by,
                updated_date: re.U_updated_date,
                updated_time: re.U_updated_time,
              },
            ];
          }

          // moisture tally id
          const m_id = w.U_mts_id;
          let moisture_tally_sheet = await trns.findMTSById(m_id);
          if (moisture_tally_sheet.length > 0) {
            let mts = moisture_tally_sheet;
            moisture_tally_sheet = null;
            mts = mts[0];

            // remodel
            moisture_tally_sheet = [
              {
                id: mts.Code,
                moisture_trial_1: mts.U_moisture_trial_1,
                moisture_trial_2: mts.U_moisture_trial_2,
                moisture_trial_3: mts.U_moisture_trial_3,
                average: mts.U_average,
                analysed_by: mts.U_analysed_by,
                created_by: mts.U_created_by,
                created_date: mts.U_created_date,
                created_time: mts.U_created_time,
                updated_by: mts.U_updated_by,
                updated_date: mts.U_updated_date,
                updated_time: mts.U_updated_time,
              },
            ];
          }
          // physical.push({
          //   price_reduction: price_reduction[0],
          //   reconsideration: reconsideration[0],
          //   moisture_tally_sheet: moisture_tally_sheet[0],
          //   remarks: remarks,
          //   disposition_id: disposition_id,
          // });
          physical[0].price_reduction = price_reduction;
          physical[0].reconsideration = reconsideration;
          physical[0].moisture_tally_sheet = moisture_tally_sheet;
          physical[0].remarks = remarks;
          physical[0].disposition_id = disposition_id;
        }

        // chemical analysis id
        const c_id = d.U_chemical_analysis_id;
        let chemical = await trns.findChemicalAnalysisById(c_id);

        let chemicals = chemical;
        chemical = null;
        for (let a = 0; a < chemicals.length; a++) {
          const s = chemicals[a];

          // remodel
          chemical = [
            {
              id: s.Code,
              reconsideration_id: s.U_reconsideration_id,
              crude_protein: s.U_crude_protein,
              calcium: s.U_calcium,
              ffa_lauric_acid: s.U_ffa_lauric_acid,
              brix: s.U_brix,
              carbonate_test: s.U_carbonate_test,
              remarks: s.U_remarks,
              evaluated_by: s.U_evaluated_by,
              evaluated_signature: s.U_evaluated_signature,
              certified_by: s.U_certified_by,
              certified_signature: s.U_certified_signature,
              created_by: s.U_created_by,
              created_date: s.U_created_date,
              created_time: s.U_created_time,
              updated_by: s.U_updated_by,
              updated_date: s.U_updated_date,
              updated_time: s.U_updated_time,
            },
          ];
          // reconsideration id
          const r_id = s.U_reconsideration_id;
          let reconsideration = await trns.findReconsiderationById(r_id);
          if (reconsideration.length > 0) {
            let re = reconsideration;
            reconsideration = null;
            re = re[0];

            // remodel
            reconsideration = [
              {
                id: re.Code,
                for_consideration: re.U_for_consideration,
                reason: re.U_reason,
                reconsidered_by: re.U_reconsidered_by,
                reconsidered_approved_by: re.U_reconsidered_approved_by,
                reconsidered_signature: re.U_reconsidered_signature,
                reconsidered_approved_signature:
                  re.U_reconsidered_approved_signature,
                noted_by: re.U_noted_by,
                created_by: re.U_created_by,
                created_date: re.U_created_date,
                created_time: re.U_created_time,
                updated_by: re.U_updated_by,
                updated_date: re.U_updated_date,
                updated_time: re.U_updated_time,
              },
            ];
          }

          let getDisposition = await trns.getDispoCA(c_id);
          for (let q = 0; q < getDisposition.length; q++) {
            const w = getDisposition[q];
            disposition_id2.push(w.U_disposition_id);
          }

          chemical.push({
            reconsideration: reconsideration[0],
            disposition_id: disposition_id2,
          });
        }

        // ##
        // mrr id
        const mrr_id = d.U_material_rejection_report_id;
        let mrr = await trns.findMRRById(mrr_id);
        if (mrr.length > 0) {
          let mrrs = mrr;
          mrr = null;
          mrrs = mrrs[0];
          // remodel
          mrr = [
            {
              id: mrrs.Code,
              findings: mrrs.U_findings,
              specification: mrrs.U_specification,
              issued_by: mrrs.U_issued_by,
              noted_by: mrrs.U_noted_by,
              approved_by: mrrs.U_approved_by,
              created_by: mrrs.U_created_by,
              created_date: mrrs.U_created_date,
              created_time: mrrs.U_created_time,
              updated_by: mrrs.U_updated_by,
              updated_date: mrrs.U_updated_date,
              updated_time: mrrs.U_updated_time,
            },
          ];
        }

        rmaf[0].physical_analysis = physical;
        rmaf[0].chemical_analysis = chemical;
        rmaf[0].material_rejection_report = mrr[0];
      }

      // ##

      // BIR id
      const bir_id = e.bir_id;
      let bir = [];
      if (bir_id) {
        // macro
        bir = await trns.findBIRById(bir_id);
        for (let q = 0; q < bir.length; q++) {
          const w = bir[q];

          let b_id;

          if (w.U_APP_BU_Feedmill == "Y") {
            let bu = await trns.returnBUId(`FEEDMILL PLANT 1 WAREHOUSE 1`);
            if (bu.length > 0) {
              b_id = bu[0].Code;
            }
          } else if (w.U_APP_BU_Paddy == "Y") {
            let bu = await trns.returnBUId(`RICE PLANT PDF RM WAREHOUSE`);
            if (bu.length > 0) {
              b_id = bu[0].Code;
            }
          } else if (w.U_APP_BU_CDF == "Y") {
            let bu = await trns.returnBUId(`CORN DRYING FACILITY RM WAREHOUSE`);
            if (bu.length > 0) {
              b_id = bu[0].Code;
            }
          } else {
            throw new Error(
              `Item does not belong to any Business Unit! Please make sure this item is tagged in Business Unit!`
            );
          }

          // business entity id
          const business_entity = await trns.findBusinessEntityById(b_id);
          if (business_entity.length > 0) {
            w.business_entity = [
              {
                id: business_entity[0].Code,
                description: business_entity[0].U_description,
                department_id: business_entity[0].U_department_id,
              },
            ];
          }

          // item id;
          const item_id = w.ItemCode;
          const items = await trns.findItemById(item_id);
          if (items.length > 0) {
            // append
            w.items = [
              {
                id: items[0].ItemCode,
                description: items[0].ItemName,
              },
            ];
          }

          // raw material id;
          let rm_id;
          if (w.ItmsGrpCod == "310") {
            let rm = await trns.returnRawMatsId(`macro`);
            if (rm.length > 0) {
              rm_id = rm[0].Code;
            }
          } else if (w.ItmsGrpCod == "313") {
            let rm = await trns.returnRawMatsId(`macro`);
            if (rm.length > 0) {
              rm_id = rm[0].Code;
            }
          } else if (w.ItmsGrpCod == "314") {
            let rm = await trns.returnRawMatsId(`micro`);
            if (rm.length > 0) {
              rm_id = rm[0].Code;
            }
          } else {
            throw new Error(
              `Item Code does not belong to any Raw Material Type!`
            );
          }

          const raw_materials = await trns.findRawMaterialsById(rm_id);
          if (raw_materials.length > 0) {
            // append
            // w.raw_materials = raw_materials[0];
            w.raw_materials = [
              {
                id: raw_materials[0].Code,
                description: raw_materials[0].U_description,
              },
            ];
          }

          // business entity department id;
          if (business_entity.length > 0) {
            const dept_id = business_entity[0].U_department_id;
            const department = await trns.getDepartment(dept_id);

            // append
            // w.department = department[0];
            w.department = [
              {
                id: department[0].Code,
                name: department[0].U_name,
              },
            ];
          }
        }
      } else {
        // micro
        const id = e.id; // transaction id

        // get all items in details; micro;
        const items = await trns.getAllItemsInMicro(id);
        for (let i = 0; i < items.length; i++) {
          const e = items[i].U_itemCode;

          let bir_dummy = await trns.findBIRById(e);

          for (let a = 0; a < bir_dummy.length; a++) {
            const w = bir_dummy[a];

            let b_id;

            if (w.U_APP_BU_Feedmill == "Y") {
              let bu = await trns.returnBUId(`FEEDMILL PLANT 1 WAREHOUSE 1`);
              if (bu.length > 0) {
                b_id = bu[0].Code;
              }
            } else if (w.U_APP_BU_Paddy == "Y") {
              let bu = await trns.returnBUId(`RICE PLANT PDF RM WAREHOUSE`);
              if (bu.length > 0) {
                b_id = bu[0].Code;
              }
            } else if (w.U_APP_BU_CDF == "Y") {
              let bu = await trns.returnBUId(
                `CORN DRYING FACILITY RM WAREHOUSE`
              );
              if (bu.length > 0) {
                b_id = bu[0].Code;
              }
            } else {
              throw new Error(
                `Item does not belong to any Business Unit! Please make sure this item is tagged in Business Unit!`
              );
            }

            // business entity id
            const business_entity = await trns.findBusinessEntityById(b_id);
            if (business_entity.length > 0) {
              w.business_entity = [
                {
                  id: business_entity[0].Code,
                  description: business_entity[0].U_description,
                  department_id: business_entity[0].U_department_id,
                },
              ];
            }

            // item id;
            const item_id = w.ItemCode;
            const items = await trns.findItemById(item_id);
            if (items.length > 0) {
              // append
              w.items = [
                {
                  id: items[0].ItemCode,
                  description: items[0].ItemName,
                },
              ];
            }

            // raw material id;
            let rm_id;
            if (w.ItmsGrpCod == "310") {
              let rm = await trns.returnRawMatsId(`macro`);
              if (rm.length > 0) {
                rm_id = rm[0].Code;
              }
            } else if (w.ItmsGrpCod == "313") {
              let rm = await trns.returnRawMatsId(`macro`);
              if (rm.length > 0) {
                rm_id = rm[0].Code;
              }
            } else if (w.ItmsGrpCod == "314") {
              let rm = await trns.returnRawMatsId(`micro`);
              if (rm.length > 0) {
                rm_id = rm[0].Code;
              }
            } else {
              throw new Error(
                `Item Code does not belong to any Raw Material Type!`
              );
            }

            const raw_materials = await trns.findRawMaterialsById(rm_id);
            if (raw_materials.length > 0) {
              // append
              // w.raw_materials = raw_materials[0];
              w.raw_materials = [
                {
                  id: raw_materials[0].Code,
                  description: raw_materials[0].U_description,
                },
              ];
            }

            // department
            // business entity department id;
            if (business_entity.length > 0) {
              const dept_id = business_entity[0].U_department_id;
              const department = await trns.getDepartment(dept_id);

              // append
              // w.department = department[0];
              w.department = [
                {
                  id: department[0].Code,
                  name: department[0].U_name,
                },
              ];
            }

            // get primary unit and alternative unit;
            const unit = await trns.getPrimaryAlternativeUnit(item_id);
            let primary = {}; // object primary
            let alt = []; // alternative units array
            for (let a = 0; a < unit.length; a++) {
              const b = unit[a];

              // make object for primary unit
              if (b.PRIMARY_UNIT == b.UomEntry) {
                primary = {
                  itemCode: b.ItemCode,
                  unit: b.UNIT,
                  baseQty: b.BaseQty,
                };
              } else {
                // alternative unit
                alt.push({
                  unit: b.UNIT,
                  baseQty: b.BaseQty,
                });
              }
            }

            if (unit.length > 0) {
              w.units = [
                {
                  primary,
                  alt,
                },
              ];
            } else {
              w.units = [];
            }

            bir.push(bir_dummy[0]);
          }
        }
      }

      if (bir.length > 0) {
        let dum = []; // hold bir
        for (let i = 0; i < bir.length; i++) {
          const e = bir[i];
          dum.push({
            id: e.ItemCode,
            item_id: e.ItemCode,
            business_entity: e.business_entity,
            items: e.items,
            raw_materials: e.raw_materials,
            department: e.department,
            units: e.units,
          });
        }

        e.bir = dum;
      }

      // ##
      // status id
      const s_id = e.status_id;
      let status = await trns.findStatusById(s_id);

      let s = status;
      status = null;
      s = s[0];

      // remodel
      status = [
        {
          id: s.Code,
          name: s.U_name,
          status: s.U_status,
          created_by: s.U_created_by,
          created_date: s.U_created_date,
          created_time: s.U_created_time,
          updated_by: s.U_updated_by,
          updated_date: s.U_updated_date,
          updated_time: s.U_updated_time,
        },
      ];

      // transaction type id
      const ttype_id = e.transaction_type_id;
      let transaction_type = await trns.findTransactionTypeById(ttype_id);
      let tt = transaction_type;
      transaction_type = null;
      tt = tt[0];

      // remodel
      transaction_type = [
        {
          id: tt.Code,
          name: tt.U_name,
          created_by: tt.U_created_by,
          created_date: tt.U_created_date,
          created_time: tt.U_created_time,
          updated_by: tt.U_updated_by,
          updated_date: tt.U_updated_date,
          updated_time: tt.U_updated_time,
        },
      ];
      // ##
      e.delivery_receipt = delivery_receipt;
      e.rmaf = rmaf;
      e.status = status;
      e.transaction_type = transaction_type;
    }

    return { transactions };
  };
};
