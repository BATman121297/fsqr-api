const { makeUpdateTransaction } = require("../../../entities/transaction");
const { makeCorrectTransaction } = require("../../../entities/transaction");

module.exports = function putTransaction({ trns }) {
  return async function makeEditTransaction(request_info) {
    // if (
    //  // !request_info.tagPO ||
    //  // !request_info.forRecon ||
    //  // !request_info.updateStatus ||
    //  // !request_info.validateTally ||
    //   (!request_info.updateMts && !correctTransaction)
    // ) {
    //   await makeUpdateTransaction(request_info);
    // }
    if (request_info.correctTransaction) {
      await makeCorrectTransaction(request_info);
    }

    // retrieve previous status before update; for status logs
    let oldStatus;
    const t_id = request_info.id; // transaction id
    const res = await trns.findTransactionById(t_id);
    oldStatus = res[0].U_status_id;

    // if no status in body req; auto sampling status
    if (!request_info.status_id) {
      const status = await trns.statusNameCheck("sampling");
      request_info.status_id = status[0].Code;
    }

    let result;
    if (request_info.forRecon) {
      // update only RMAF
      const info = {
        Code: request_info.id,
        U_rmaf_id: request_info.rmaf_id,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      result = await trns.updateTransaction({ info });
    } else if (request_info.updateStatus) {
      // if there is status specified; get the status' ID
      if (request_info.status) {
        const statuss = request_info.status;

        const status = await trns.statusNameCheck(statuss);
        request_info.status_id = status[0].Code;
      }

      // update status of transaction only;
      const info = {
        Code: request_info.id,
        U_status_id: request_info.status_id,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      result = await trns.updateTransaction({ info });
    } else if (request_info.validateTally) {
      // update deduction receipt of transaction;

      // retrieve deduction receipt;
      const id = request_info.id; // transaction id
      const res = await trns.findDeductionReceiptByTransId(id);
      for (let i = 0; i < res.length; i++) {
        const e = res[i];

        // deduction receipt id;
        const d_id = e.Code;

        // update deduction receipt
        const info = {
          Code: d_id,
          U_checked_by: request_info.updated_by,
          U_updated_by: request_info.updated_by,
          U_updated_date: request_info.update_date,
          U_updated_time: request_info.update_time,
          U_checked_signature: request_info.checked_signature,
          cookie: request_info.cookie,
        };

        await trns.updateDeductionReceipt({ info });
      }

      // update status of transaction only;
      const info = {
        Code: request_info.id,
        U_status_id: request_info.status_id,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      result = await trns.updateTransaction({ info });
    } else if (request_info.tagPO) {
      // update PO only;
      const info = {
        Code: request_info.id,
        U_PO: request_info.purchase_order_id,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      result = await trns.updateTransaction({ info });
    } else {
      await makeUpdateTransaction(request_info);
      // update transaction;
      const info = {
        Code: request_info.id,
        U_DR_id: request_info.delivery_receipt_id,
        U_rmaf_id: request_info.rmaf_id,
        U_bir_id: request_info.bir_id,
        U_status_id: request_info.status_id,
        U_trckpltno: request_info.plate_number,
        U_trnstyp_id: request_info.transaction_type_id,
        U_driver: request_info.driver_name,
        U_suppl_code: request_info.supplier_code,
        U_no_of_bags: request_info.no_of_bags,
        U_description: request_info.description,
        U_g_on_dty: request_info.guard_on_duty,
        U_qc_cntrl_no: request_info.qc_control_number,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      result = await trns.updateTransaction({ info });
    }

    if (request_info.hasPhysicalRecon) {
      // update reconsideration
      const info = {
        Code: request_info.reconsideration_id,
        U_reconsidered_approved_by: request_info.employee_id,
        U_reconsidered_approved_signature:
          request_info.reconsidered_approved_signature,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      await trns.reconsiderationUpdate({ info });
    }

    // if there is change in status; insert to logs;
    if (oldStatus != request_info.status_id) {
      // get max code
      const code = await trns.trnsLogsMaxCode();
      const max = code[0].maxCode;
      const info = {
        Code: max,
        Name: max,
        U_transaction_id: request_info.id,
        U_status_id: request_info.status_id,
        U_start_time: request_info.update_time,
        U_end_time: request_info.update_time,
        U_created_by: request_info.updated_by,
        U_created_date: request_info.update_date,
        U_created_time: request_info.update_time,
        cookie: request_info.cookie,
      };

      await trns.addtrnsLogs({ info }); // add transaction logs
    }

    return result;
  };
};
