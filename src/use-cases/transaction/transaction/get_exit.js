module.exports = function listExit({ trns }) {
  return async function makeListExit() {
    const result = await trns.listExit();
    return result;
  };
};
