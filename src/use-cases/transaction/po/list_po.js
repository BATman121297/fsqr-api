require("dotenv").config();
const moment = require("moment")
module.exports = function listPOs({ sap_db }, { micros }) {
   return async function makeListPO(request_info) {
      // Set login info
      const login_info = {
         CompanyDB: process.env.DBPOST,
         Password: process.env.PW,
         UserName: process.env.USERS,
      };
      let login = await sap_db.login(login_info);
      let sessionId = login.SessionId;

      // If date range is not provided, set date range
      if (!request_info.date_from && !request_info.date_to) {
         request_info.date_from = moment().subtract(1, 'days').format("YYYY-MM-DD");
         request_info.date_to = moment().add(1, 'days').format("YYYY-MM-DD");
      } else {
         request_info.date_from = moment(request_info.date_from).subtract(1, 'days').format("YYYY-MM-DD");
         request_info.date_to = moment(request_info.date_to).add(1, 'days').format("YYYY-MM-DD");
      }

      // Search PO
      let po = await sap_db.getPO(sessionId, request_info);

      // remove non-micro items
      for (let i = po.length - 1; i >= 0; i--) {
         if (po[i].DocumentLines[0]) {
            let bool = await micros.checkIfItemIsMicro({ code: po[i].DocumentLines[0].ItemCode });
            bool = bool[0].BOOLS;
   
            if (bool == 0) po.splice(i, 1);
         }
        

      }



      if (po.length == 0) {
         po = [];
      }

      return po;
   };
};
