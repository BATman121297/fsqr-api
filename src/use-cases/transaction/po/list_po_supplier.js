require("dotenv").config();
module.exports = function listPOSupplier({ sap_db }) {
  return async function makeListPOSupplier(request_info) {
    const login_info = {
      CompanyDB: process.env.DBPOST,
      Password: process.env.PW,
      UserName: process.env.USERS,
    };
    let login = await sap_db.login(login_info);
    let sessionId = login.SessionId;
    let po = await sap_db.getPOSupplier(sessionId, request_info.CardName);
    if (po.length == 0) {
      po = [];
    }

    return po;
  };
};
