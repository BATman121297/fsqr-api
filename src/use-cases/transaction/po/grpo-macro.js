const { makeEndorseGRPO } = require("../../../entities/po");
const moment = require("moment-timezone");
require("dotenv").config();

module.exports = function endorseGRPO({ sap_db }) {
  return async function makesEndorseGRPO(request_info) {
    // entity
    await makeEndorseGRPO(request_info);

    // creds;
    const login_info = {
      CompanyDB: process.env.DBPOST,
      Password: process.env.PW,
      UserName: process.env.USERS,
    };

    let login = await sap_db.login(login_info);
    let sessionId = login.SessionId;

    let grpo;

    // if there is no price
    if (request_info.po_details[0].DocumentLines[0].Price == 0) {
      let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;
      let getPrice = await sap_db.getItemPriceList(sessionId, ItemCode);

      let price = getPrice[0].ItemPrices[0].Price;

      request_info.U_APP_WithDeductions = "Y";
      request_info.U_APP_DRNo = request_info.delivery_receipt[0].id;
      request_info.DocumentLines[0].Quantity =
        request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].TaxCode = "IVAT-E";
      request_info.U_APP_RMAFNo = request_info.rmaf_id[0].id;
      request_info.DocumentLines[0].U_APP_WeightSlipNo =
        request_info.weight[0].DocEntry;
      request_info.DocumentLines[0].UnitPrice = price;

      // if item is yellow corn on cubs
      if (
        request_info.po_details[0].DocumentLines[0].ItemCode === "RM16-00013"
      ) {
        // if it has rmaf moisture tally sheet average
        if (
          request_info.rmaf_id[0].physical_analysis[0].moisture_tally_sheet[0]
            .average
        ) {
          let _mcR =
            request_info.rmaf_id[0].physical_analysis[0].moisture_tally_sheet[0]
              .average;

          if (_mcR >= 32.01 && _mcR <= 35) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC01",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else if (_mcR >= 35.01 && _mcR <= 38) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC02",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else if (_mcR >= 38.01) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC03",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else if (_mcR <= 32) {
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC05",
              Quantity: _mcR,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else {
            throw new Error("MC Result Error");
          }
        } else {
          // no moisture tally sheet average
          console.log("Moisture Content is not presented");
        }
      } else {
        // not yellow corn on cubs
        console.log("Not Yellow Corn on Cobs");
      }
    } else {
      // there is price
      request_info.U_APP_WithDeductions = "Y";
      request_info.U_APP_DRNo = request_info.delivery_receipt[0].id;
      request_info.DocumentLines[0].Quantity =
        request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].TaxCode = "IVAT-E";
      request_info.U_APP_RMAFNo = request_info.rmaf_id[0].id;
      request_info.DocumentLines[0].U_APP_WeightSlipNo =
        request_info.weight[0].DocEntry;

      // if item is yellow corn on cubs
      if (
        request_info.po_details[0].DocumentLines[0].ItemCode === "RM16-00013"
      ) {
        // if it has rmaf moisture tally sheet average
        if (
          request_info.rmaf_id[0].physical_analysis[0].moisture_tally_sheet[0]
            .average
        ) {
          let _mcR =
            request_info.rmaf_id[0].physical_analysis[0].moisture_tally_sheet[0]
              .average;

          if (_mcR >= 32.01 && _mcR <= 35) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC01",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else if (_mcR >= 35.01 && _mcR <= 38) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC02",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else if (_mcR >= 38.01) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC03",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else if (_mcR <= 32) {
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC05",
              Quantity: _mcR,
              TaxCode: "IVAT-E",
              UoMCode: "Manual",
              U_APP_RMAFNo: request_info.rmaf_id[0].id,
            });
          } else {
            throw new Error("MC Result Error");
          }
        } else {
          // no moisture tally sheet average
          console.log("Moisture Content is not presented");
        }
      } else {
        // not yellow corn on cubs
        console.log("Not Yellow Corn on Cobs");
      }

      // ############

      // check if item is manage by serial
      let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;
      let ItemManageBy = await sap_db.getItemManage(sessionId, ItemCode);

      if (ItemManageBy[0]) {
        if (ItemManageBy[0] === undefined) {
          console.log("Not batch/serial");
        }
        if (ItemManageBy[0].ManageBatchNumbers === "tYES") {
          let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;

          const date_today = moment().tz("Asia/Manila").format("HHmmss");

          let IDT = ItemCode + date_today;
          let QTY =
            request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;

          request_info.DocumentLines[0].BatchNumbers = [
            {
              BatchNumber: IDT,
              Quantity: QTY,
            },
          ];
        }
      } else {
        console.log("ItemManageBy has no value");
      }

      // ############
      // if it has deduction receipt; for MACRO only;
      if (request_info.deduction_receipt) {
        for (let i = 0; i < request_info.deduction_receipt.length; i++) {
          const e = request_info.deduction_receipt[i];

          // if no item code; and rejected
          if (!e.ItemCode && e.criteria_desc.toLowerCase() == "rejected")
            console.log("Rejected");
          else if (e.quantity == 0) console.log("Quantity is zero");
          else {
            if (request_info.po_details[0].DocumentLines[0].Price == 0) {
              let ItemCode =
                request_info.po_details[0].DocumentLines[0].ItemCode;
              let getPrice = await sap_db.getItemPriceList(sessionId, ItemCode);

              let price = getPrice[0].ItemPrices[0].Price;

              let LineStart = 0;
              (LineStart = i + 1),
                request_info.DocumentLines.push({
                  LineNum: LineStart + 1,
                  ItemCode: e.ItemCode,
                  ItemDescription: e.criteria_desc,
                  Quantity: -Math.abs(e.total),
                  Price: price,
                  TaxCode: "IVAT-E",
                  UoMCode: "Manual",
                  U_APP_DeductionCriteria: e.criteria_desc,
                  U_APP_RMAFNo: request_info.rmaf_id[0].id,
                });
            } else {
              let LineStart = 0;
              (LineStart = i + 1),
                request_info.DocumentLines.push({
                  LineNum: LineStart + 1,
                  ItemCode: e.ItemCode,
                  ItemDescription: e.criteria_desc,
                  Quantity: -Math.abs(e.total),
                  Price: request_info.po_details[0].DocumentLines[0].Price,
                  TaxCode: "IVAT-E",
                  UoMCode: "Manual",
                  U_APP_DeductionCriteria: e.criteria_desc,
                  U_APP_RMAFNo: request_info.rmaf_id[0].id,
                });
            }
          }
        }
        grpo = await sap_db.endorseGRPO(sessionId, request_info);
      } else {
        grpo = await sap_db.endorseGRPO(sessionId, request_info);
      }

      if (grpo.length == 0) {
        grpo = [];
      }

      return grpo;
    }
  };
};
