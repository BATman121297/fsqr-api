const { makeEndorseGRPO } = require("../../../entities/po");
const moment = require("moment-timezone");

module.exports = function endorseGRPO({ sap_db }) {
  return async function makesEndorseGRPO(request_info) {
    await makeEndorseGRPO(request_info);
    
    const login_info = {
      CompanyDB: "BIOTECH_FARMS_TEST",
      Password: "1234",
      UserName: "BFI07"
    };

    let login = await sap_db.login(login_info);
    let sessionId = login.SessionId;

    let grpo;

    //PRICE
    if (request_info.po_details[0].DocumentLines[0].Price == 0) {
      let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;
      let getPrice = await sap_db.getItemPriceList(sessionId, ItemCode);

      let price = getPrice[0].ItemPrices[0].Price;

      request_info.U_APP_WithDeductions = "Y";
      request_info.U_APP_DRNo = request_info.delivery_receipt[0].id;
      request_info.DocumentLines[0].Quantity =
        request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].TaxCode = "IVAT-E";
      request_info.U_APP_RMAFNo = request_info.rmaf_id;
      request_info.DocumentLines[0].U_BFI_InboundWt =
        request_info.weight[0].IB_WEIGHT;
      request_info.DocumentLines[0].U_BFI_OutboundWt =
        request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].U_BFI_NetWt =
        request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].U_APP_WeightSlipNo =
        request_info.weight[0].DocEntry;
      request_info.DocumentLines[0].UnitPrice = price;

      //MC
      if (
        request_info.po_details[0].DocumentLines[0].ItemCode === "RM16-00013"
      ) {
        if (
          request_info.rmaf[0].physical_analysis[0].moisture_tally_sheet[0]
            .average
        ) {
          let _mcR =
            request_info.rmaf[0].physical_analysis[0].moisture_tally_sheet[0]
              .average;

          if (_mcR >= 32.01 && _mcR <= 35) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC01",
              Quantity: -Math.abs(res),
              Price: request_info.DocumentLines[0].UnitPrice,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else if (_mcR >= 35.01 && _mcR <= 38) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC02",
              Quantity: -Math.abs(res),
              Price: request_info.DocumentLines[0].UnitPrice,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else if (_mcR >= 38.01) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC03",
              Quantity: -Math.abs(res),
              Price: request_info.DocumentLines[0].UnitPrice,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else if (_mcR <= 32) {
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC05",
              Quantity: _mcR,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else {
            throw new Error("MC Result Error");
          }
        } else {
          console.log("Moisture Content is not presented");
        }
      } else {
        console.log("Not Yellow Corn on Cobs");
      } //MC
    } else {
      request_info.U_APP_WithDeductions = "Y";
      request_info.U_APP_DRNo = request_info.delivery_receipt[0].id;
      request_info.DocumentLines[0].Quantity =
        request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].TaxCode = "IVAT-E";
      request_info.U_APP_RMAFNo = request_info.rmaf_id;
      request_info.DocumentLines[0].U_BFI_InboundWt =
        request_info.weight[0].IB_WEIGHT;
      request_info.DocumentLines[0].U_BFI_OutboundWt =
        request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].U_BFI_NetWt =
        request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;
      request_info.DocumentLines[0].U_APP_WeightSlipNo =
        request_info.weight[0].DocEntry;

      //MC
      if (
        request_info.po_details[0].DocumentLines[0].ItemCode === "RM16-00013"
      ) {
        if (
          request_info.rmaf[0].physical_analysis[0].moisture_tally_sheet[0]
            .average
        ) {
          let _mcR =
            request_info.rmaf[0].physical_analysis[0].moisture_tally_sheet[0]
              .average;

          if (_mcR >= 32.01 && _mcR <= 35) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC01",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else if (_mcR >= 35.01 && _mcR <= 38) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC02",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else if (_mcR >= 38.01) {
            let nob = request_info.weight[0].U_TS_NUM_BAGS;
            let res = nob * 3;
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC03",
              Quantity: -Math.abs(res),
              Price: request_info.po_details[0].DocumentLines[0].Price,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else if (_mcR <= 32) {
            request_info.DocumentLines.push({
              LineNum: 1,
              ItemCode: "MC05",
              Quantity: _mcR,
              TaxCode: "IVAT-E",
              UoMCode: "Manual"
            });
          } else {
            throw new Error("MC Result Error");
          }
        } else {
          console.log("Moisture Content is not presented");
        }
      } else {
        console.log("Not Yellow Corn on Cobs");
      } //MC
    } //else

    //BATCH/SERIAL
    let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;
    let ItemManageBy = await sap_db.getItemManage(sessionId, ItemCode);

    if (ItemManageBy[0]) {
      console.log(ItemManageBy);
      if (ItemManageBy[0] === undefined) {
        console.log("Not batch/serial");
      }
      if (ItemManageBy[0].ManageBatchNumbers === "tYES") {
        let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;

        const date_today = moment()
          .tz("Asia/Manila")
          .format("HHmmss");

        let IDT = ItemCode + date_today;
        let QTY =
          request_info.weight[0].IB_WEIGHT - request_info.weight[0].OB_WEIGHT;

        request_info.DocumentLines[0].BatchNumbers = [
          {
            BatchNumber: IDT,
            Quantity: QTY
          }
        ];
      }
    } else {
      console.log("ItemManageBy has no value");
    }
    //--BATCH/SERIAL

    if (request_info.deduction_receipt) {
      for (
        let i = 0;
        i < request_info.deduction_receipt.listDeductionCriteria.length;
        i++
      ) {
        if (
          !request_info.deduction_receipt.listDeductionCriteria[i].item_code &&
          request_info.deduction_receipt.listDeductionCriteria[i]
            .criteria_desc == "Rejected"
        ) {
          console.log("Rejected");
        } else if (
          request_info.deduction_receipt.listDeductionCriteria[i].quantity == 0
        ) {
          console.log("Quantity is zero");
        } else {
          if (request_info.po_details[0].DocumentLines[0].Price == 0) {
            let ItemCode = request_info.po_details[0].DocumentLines[0].ItemCode;
            let getPrice = await sap_db.getItemPriceList(sessionId, ItemCode);

            let price = getPrice[0].ItemPrices[0].Price;

            let LineStart = 0;
            (LineStart = i + 1),
              request_info.DocumentLines.push({
                LineNum: LineStart + 1,
                ItemCode:
                  request_info.deduction_receipt.listDeductionCriteria[i]
                    .item_code,
                ItemDescription:
                  request_info.deduction_receipt.listDeductionCriteria[i]
                    .criteria_desc,
                Quantity: -Math.abs(
                  request_info.deduction_receipt.listDeductionCriteria[i].total
                ),
                Price: price,
                TaxCode: "IVAT-E",
                UoMCode: "Manual",
                U_APP_DeductionCriteria:
                  request_info.deduction_receipt.listDeductionCriteria[i]
                    .criteria_desc
              });
          } else {
            let LineStart = 0;
            (LineStart = i + 1),
              request_info.DocumentLines.push({
                LineNum: LineStart + 1,
                ItemCode:
                  request_info.deduction_receipt.listDeductionCriteria[i]
                    .item_code,
                ItemDescription:
                  request_info.deduction_receipt.listDeductionCriteria[i]
                    .criteria_desc,
                Quantity: -Math.abs(
                  request_info.deduction_receipt.listDeductionCriteria[i].total
                ),
                Price: request_info.po_details[0].DocumentLines[0].Price,
                TaxCode: "IVAT-E",
                UoMCode: "Manual",
                U_APP_DeductionCriteria:
                  request_info.deduction_receipt.listDeductionCriteria[i]
                    .criteria_desc
              });
          }
        }
      }

      grpo = await sap_db.endorseGRPO(sessionId, request_info);

      console.log(grpo.DocNum);
      //console.log(grpo.U_APP_Farmer);

      let Kilogram = request_info.DocumentLines[0].Quantity;
      let toTon = Kilogram * 0.00110231;

      request_info.DocumentLines = [];

      request_info.U_APP_Farmer = request_info.po_details[0].U_APP_Farmer;
      request_info.U_APP_FSQR_GRPORef = grpo.DocNum;

      console.log(request_info.U_APP_FSQR_GRPORef);
      console.log(request_info.U_APP_Farmer);

      request_info.DocObjectCode = "19";

      if (toTon) {
        if (toTon <= 20) {
          //request_info.DocCurrency: "PHP";
          request_info.DocumentLines.push({
            LineNum: 0,
            ItemCode: "TS01",
            Quantity: 1,
            TaxCode: "IVAT-E",
            UoMCode: "Manual",
            U_APP_DeductionCriteria:
              "Php 80.00 per Truck regardless the net weight delivered"
          });
          request_info.DocumentLines.push({
            LineNum: 1,
            ItemCode: "LF01",
            Quantity: 1,
            TaxCode: "IVAT-E",
            UoMCode: "Manual",
            U_APP_DeductionCriteria: "Lab Fee for Less than 20 tons"
          });
        } else if (toTon > 20) {
          request_info.DocumentLines.push({
            LineNum: 0,
            ItemCode: "TS02",
            Quantity: 1,
            TaxCode: "IVAT-E",
            UoMCode: "Manual",
            U_APP_DeductionCriteria:
              "Php 150.00 per Truck regardless the net weight delivered"
          });
          request_info.DocumentLines.push({
            LineNum: 1,
            ItemCode: "LF02",
            Quantity: 1,
            TaxCode: "IVAT-E",
            UoMCode: "Manual",
            U_APP_DeductionCriteria: "Lab Fee for More than 20 tons"
          });
        } else {
          throw new Error("Ton Error");
        }
      }

      await sap_db.addAPCredit(sessionId, request_info);
    } else {
      grpo = await sap_db.endorseGRPO(sessionId, request_info);
    }

    if (grpo.length == 0) {
      grpo = [];
    }

    return grpo;
  };
};
