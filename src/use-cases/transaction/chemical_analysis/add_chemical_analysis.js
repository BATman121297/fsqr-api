const { makeChemicalAnalysis } = require("../../../entities/chemical_analysis");

module.exports = function postChemicalAnalysis({ trns }) {
  return async function makePostChemicalAnalysis(request_info) {
    await makeChemicalAnalysis(request_info);

    // make batch request; insert in two tables;
    let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

    // get max code
    const code = await trns.chemAnalysisMaxCode();
    const max = code[0].maxCode;

    // insert to chem analysis
    batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_CHEM_ANA

{
  "Code": "${max}",
  "Name": "${max}",
  "U_reconsideration_id": "${request_info.reconsideration_id}",
  "U_crude_protein": "${request_info.crude_protein}",
  "U_calcium": "${request_info.calcium}",
  "U_ffa_lauric_acid": "${request_info.ffa_lauric_acid}",
  "U_brix": "${request_info.brix}",
  "U_carbonate_test": "${request_info.carbonate_test}",
  "U_remarks": "${request_info.remarks}",
  "U_evaluated_by": "${request_info.evaluated_by}",
  "U_evaluated_signature": "${request_info.evaluated_signature}",
  "U_certified_by": "${request_info.certified_by}",
  "U_certified_signature": null,
  "U_created_by": "${request_info.created_by}",
  "U_created_date": "${request_info.created_date}",
  "U_created_time": "${request_info.created_time}",
  "U_updated_by": "${request_info.created_by}",
  "U_updated_date": "${request_info.created_date}",
  "U_updated_time": "${request_info.created_time}"
}
`;

    // get max code for disposition
    const codes = await trns.chemAnalysisDispoMaxCode();
    let maxs = codes[0].maxCode;
    for (let i = 0; i < request_info.disposition_id.length; i++) {
      const e = request_info.disposition_id[i];

      // insert to chem analysis disposition
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_CAD

{
  "Code": "${maxs}",
  "Name": "${maxs}",
  "U_chemical_analysis_id": "${max}",
  "U_disposition_id": "${e}"
}
`;
      maxs++;
    }

    batchStr += `
--b--
--a--
`;

    const info = {
      str: batchStr,
      cookie: request_info.cookie,
    };
    // insert query
    const res = await trns.chemAnalysisAndDispoInsert({ info });
    res.id = max;
    return res;
  };
};
