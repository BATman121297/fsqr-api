module.exports = function listChemicalAnalysis({ trns }) {
  return async function makeListChemicalAnalysis() {
    const list = await trns.listChemicalAnalysis();

    let chemical_analysis = [];
    for (let i = 0; i < list.length; i++) {
      const e = list[i];

      if (e.DISPOSITION) {
        const disp = e.DISPOSITION;
        const dispArray = disp.split(", ");
        delete e.DISPOSITION; // remove disposition

        const disposition = []; // array to hold all disposition data

        for (let i = 0; i < dispArray.length; i++) {
          const id = dispArray[i];

          const dispositions = await trns.findDispositionById(id);
          const info = {
            id: dispositions[0].Code,
            description: dispositions[0].U_description,
            tag: dispositions[0].U_tag,
            status: dispositions[0].U_status,
          };
          disposition.push(info);
        }

        // append disposition array;
        e.disposition = disposition;
      }

      // reconsideration id
      const rid = e.U_reconsideration_id;
      let recon = await trns.findReconsiderationById(rid);
      recon = recon[0];
      const reconsideration = [
        {
          id: recon.Code,
          for_reconsideration: recon.U_for_consideration,
          reason: recon.U_reason,
          reconsidered_by: recon.U_reconsidered_by,
          reconsidered_approved_by: recon.U_reconsidered_approved_by,
          reconsidered_signature: recon.U_reconsidered_signature,
          reconsidered_approved_signature:
            recon.U_reconsidered_approved_signature,
          noted_by: recon.U_noted_by,
          created_by: recon.U_created_by,
          created_at: recon.U_created_date,
          updated_at: recon.U_created_time,
        },
      ];
      e.reconsideration = reconsideration;

      const info = {
        id: e.Code,
        reconsideration_id: e.U_reconsideration_id,
        crude_protein: e.U_crude_protein,
        calcium: e.U_calcium,
        ffa_lauric_acid: e.U_ffa_lauric_acid,
        brix: e.U_brix,
        carbonate_test: e.U_carbonate_test,
        evaluated_by: e.U_evaluated_by,
        certified_by: e.U_certified_by,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        evaluated_signature: e.U_evaluated_signature,
        certified_signature: e.U_certified_signature,
        remarks: e.U_remarks,
        reconsideration: e.reconsideration,
        disposition: e.disposition,
      };

      chemical_analysis.push(info);
    }

    return { chemical_analysis };
  };
};
