const {
  makeUpdateChemicalAnalysis
} = require("../../../entities/chemical_analysis");

module.exports = function putChemicalAnalysis({ trns }) {
  return async function makeEditChemicalAnalysis(request_info) {
    if (
      !request_info.onlyRecon &&
      !request_info.addCertification &&
      !request_info.addNote
    ) {
      await makeUpdateChemicalAnalysis(request_info);
    }

    let res;
    if (request_info.addCertification) {
      const info = {
        Code: request_info.id,
        U_certified_by: request_info.certified_by,
        U_certified_signature: request_info.certified_signature,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };
      res = await trns.chemAnalysisUpdate({ info });
    } else if (request_info.addNote) {
      const info = {
        Code: request_info.reconsideration_id,
        U_noted_by: request_info.noted_by,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };
      res = await trns.reconsiderationUpdate({ info });
    } else if (request_info.onlyRecon) {
      const info = {
        Code: request_info.id,
        U_reconsideration_id: request_info.reconsideration_id,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };
      res = await trns.chemAnalysisUpdate({ info });
    } else {
      // update; check sa if gamiton ba ni or dili
      /*
      id,
      disposition_id,
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      remarks,
      evaluated_by,
      certified_by,
      updated_by
      */
      // const info = {
      //   Code: request_info.id,
      //   U_reconsideration_id: "1",
      //   U_crude_protein: "150",
      //   U_calcium: "150",
      //   U_ffa_lauric_acid: "100",
      //   U_brix: "150",
      //   U_carbonate_test: "150",
      //   U_remarks: "pass",
      //   U_evaluated_by: "154151386",
      //   U_evaluated_signature: "dummy",
      //   U_certified_by: "154151387",
      //   U_certified_signature: "dummy",
      //   U_created_by: "154151386",
      //   U_created_date: "2019-03-24",
      //   U_created_time: "12:50:00",
      //   U_updated_by: "154151386",
      //   U_updated_date: "2019-03-24",
      //   U_updated_time: "12:51:00"
      // };
      // previous function
      // result = await transaction_db.updateChemicalAnalysis(request_info);
    }
    return res;
  };
};
