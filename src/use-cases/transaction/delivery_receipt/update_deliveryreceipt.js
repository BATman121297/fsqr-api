const { updateDeliveryReceipt } = require("../../../entities/delivery_receipt");

module.exports = function putDeliveryReceipt({ trns }) {
  return async function makeEditDeliveryReceipt(request_info) {
    await updateDeliveryReceipt(request_info);

    const info = {
      Code: request_info.id,
      U_dr_id: request_info.dr_id,
      U_quantity: request_info.quantity,
      U_unit: request_info.unit,
      U_description_of_articles: request_info.description_of_articles,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateDr({ info });
    return res;
  };
};
