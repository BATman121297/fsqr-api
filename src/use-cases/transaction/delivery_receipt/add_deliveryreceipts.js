const { makeDeliveryReceipt } = require("../../../entities/delivery_receipt");

module.exports = function postDeliveryReceipt({ trns }) {
  return async function makePostDeliveryReceipt(request_info) {
    await makeDeliveryReceipt(request_info);

    //
    // get max code
    const code = await trns.delReceiptMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_dr_id: request_info.dr_id,
      U_quantity: request_info.quantity,
      U_unit: request_info.unit,
      U_description_of_articles: request_info.description_of_articles,
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addDelReceipt({ info });
    return res;
  };
};
