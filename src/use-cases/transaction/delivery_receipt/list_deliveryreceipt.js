module.exports = function listDeliveryReceipt({ trns }) {
  return async function makeListDeliveryReceipt() {
    const res = await trns.listDeliveryReceipts();
    let deliveryReceipts = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];

      const info = {
        id: e.Code,
        quantity: e.U_quantity,
        unit: e.U_unit,
        description_of_articles: e.U_description_of_articles,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };

      deliveryReceipts.push(info);
    }
    return { deliveryReceipts };
  };
};
