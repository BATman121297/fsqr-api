const { makeUpdateStatus } = require("../../../entities/status");

module.exports = function putStatus({ trns }) {
  return async function makeEditStatus(request_info) {
    await makeUpdateStatus(request_info);

    // check if id exist
    const { id, name } = request_info;

    const exist = await trns.checkStatusId(id);
    if (exist.length == 0) throw new Error("Status doesn't exist.");

    // check if description exist
    const check = await trns.statusCheckUpdate(id, name);
    if (check.length > 0) throw new Error("Status already exist.");

    const info = {
      Code: id,
      U_name: name,
      U_status: request_info.status,
      U_updated_by: request_info.updated_by,
      U_updated_date: request_info.update_date,
      U_updated_time: request_info.update_time,
      cookie: request_info.cookie
    };

    const res = await trns.updateStatus({ info });
    return res;
  };
};
