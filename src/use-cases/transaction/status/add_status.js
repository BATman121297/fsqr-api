const { makeStatus } = require("../../../entities/status");

module.exports = function postStatus({ trns }) {
  return async function makePostStatus(request_info) {
    await makeStatus(request_info);

    const { name } = request_info;

    // get max code
    const code = await trns.statusMaxCode();
    const max = code[0].maxCode;

    // check if exist
    const check = await trns.statusNameCheck(name);
    if (check.length > 0) throw new Error("Status already exist");

    const info = {
      Code: max,
      Name: max,
      U_name: name,
      U_status: "active",
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    const res = await trns.addStatus({ info });
    return res;
  };
};
