module.exports = function listStatus({ trns }) {
  return async function makeListStatus() {
    const res = await trns.listStatus();
    let status = [];

    for (let i = 0; i < res.length; i++) {
      const e = res[i];
      const info = {
        id: e.Code,
        name: e.U_name,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        status: e.U_status,
      };

      status.push(info);
    }
    return { status };
  };
};
