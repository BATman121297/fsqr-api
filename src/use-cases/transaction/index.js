const moment = require("moment-timezone");
const encryption = require("../../encryption/encrypting");
// const escpos = require("escpos");

// sl db
const { trns } = require("../../data-access/sap-integ/transactions/index");

// Data access
const transaction_db = require("../../data-access/transactions/");
const employee_db = require("../../data-access/employees");
const { micros } = require("../../data-access/sap-integ/micro/index"); 
const sap_db = require("../../data-access/sap");

// SAP
const listSuppliers = require("./suppliers/list_suppliers");
const listPOSuppliers = require("./po/list_po_supplier");
const endorseGRPO = require("./po/grpo-macro");
const listPO = require("./po/list_po");

const getSuppliers = listSuppliers({ sap_db });
const getPOSuppliers = listPOSuppliers({ sap_db });
const postGRPO = endorseGRPO({ sap_db });
const getPOs = listPO({ sap_db }, { micros });

// Truckscale
const getNetWeight = require("./truckscale/net_weight");

const getnetweight = getNetWeight({ trns });

// Delivery receipt
const makeDeliveryReceipt = require("./delivery_receipt/add_deliveryreceipts");
const listDeliveryReceipt = require("./delivery_receipt/list_deliveryreceipt");
const makeUpdateDeliveryReceipt = require("./delivery_receipt/update_deliveryreceipt");

const addDeliveryReceipt = makeDeliveryReceipt({ trns });
const getDeliverReceipts = listDeliveryReceipt({ trns });
const updateDeliveryReceipt = makeUpdateDeliveryReceipt({ trns });

// Trucks
const makeTruck = require("./trucks/add_truck");
const makeTrucktype = require("./trucks/add_trucktype");
const listTrucks = require("./trucks/list_trucks");
const listTrucktypes = require("./trucks/list_trucktype");
const makeUpdateTruck = require("./trucks/update_truck");
const makeUpdateTrucktype = require("./trucks/update_trucktype");

const addTruck = makeTruck({ transaction_db });
const addTrucktype = makeTrucktype({ transaction_db });
const getTrucks = listTrucks({ transaction_db });
const getTrucktypes = listTrucktypes({ transaction_db });
const updateTruck = makeUpdateTruck({ transaction_db });
const updateTrucktype = makeUpdateTrucktype({ transaction_db });

// Transaction type
const makeTransactionType = require("./transaction_type/add_transaction_type");
const makeUpdateTransactionType = require("./transaction_type/update_transaction_type");
const listTransactionTypes = require("./transaction_type/list_transaction_type");

const addTransactionType = makeTransactionType({ trns });
const updateTransactionType = makeUpdateTransactionType({ trns });
const getTransactionTypes = listTransactionTypes({ trns });

// Status
const makeStatus = require("./status/add_status");
const makeUpdateStatus = require("./status/update_status");
const listStatus = require("./status/list_status");

const addStatus = makeStatus({ trns });
const updateStatus = makeUpdateStatus({ trns });
const getStatus = listStatus({ trns });

// Driver
const makeDriver = require("./driver/add_driver");
const makeUpdateDriver = require("./driver/update_driver");
const listDriver = require("./driver/list_drivers");

const addDriver = makeDriver({ trns });
const updateDriver = makeUpdateDriver({ trns });
const getDriver = listDriver({ trns });

// BusinessEntity
const makeBusinessEntity = require("./business_entity/add_business_entity");
const makeUpdateBusinessEntity = require("./business_entity/update_business_entity");
const listBusinessEntity = require("./business_entity/list_business_entity");

const listWarehouse = require("./business_entity/list_warehouse");
const makeDeleteWarehouse = require("./business_entity/delete_warehouse");

const addBusinessEntity = makeBusinessEntity({ trns });
const updateBusinessEntity = makeUpdateBusinessEntity({ trns });
const getBusinessEntity = listBusinessEntity({ trns });

const getWarehouse = listWarehouse({ trns });
const removeWarehouse = makeDeleteWarehouse({ trns });

// Item
const makeItem = require("./items/add_item");
const makeUpdateItem = require("./items/update_item");
const listItem = require("./items/list_items");

const addItem = makeItem({ trns });
const updateItem = makeUpdateItem({ trns });
const getItem = listItem({ trns });

// Raw Material
const makeRawMaterial = require("./raw_materials/add_raw_material");
const makeUpdateRawMaterial = require("./raw_materials/update_raw_material");
const listRawMaterial = require("./raw_materials/list_raw_materials");

const addRawMaterial = makeRawMaterial({ trns });
const updateRawMaterial = makeUpdateRawMaterial({ trns });
const getRawMaterial = listRawMaterial({ trns });

// BIR
const makeBIR = require("./bir/add_bir");
const makeUpdateBIR = require("./bir/update_bir");
const listBIR = require("./bir/list_bir");
const listItemsBIR = require("./bir/list_items_bir");

const addBIR = makeBIR({ trns });
const updateBIR = makeUpdateBIR({ trns });
const getBIR = listBIR({ trns });
const getItemsBIR = listItemsBIR({ trns });

// Criteria
const makeCriteria = require("./criteria/add_criteria");
const makeUpdateCriteria = require("./criteria/update_criteria");
const listCriteria = require("./criteria/list_criteria");

const addCriteria = makeCriteria({ trns });
const updateCriteria = makeUpdateCriteria({ trns });
const getCriteria = listCriteria({ trns });

// Deduction Criteria
const makeDeductionCriteria = require("./deduction_criteria/add_deduction_criteria");
const makeUpdateDeductionCriteria = require("./deduction_criteria/update_deduction_criteria");
const listDeductionCriteria = require("./deduction_criteria/list_deduction_criteria");

const addDeductionCriteria = makeDeductionCriteria({ trns });
const updateDeductionCriteria = makeUpdateDeductionCriteria({ trns });
const getDeductionCriteria = listDeductionCriteria({ trns });

// Price Reduction
const makePriceReduction = require("./price_reduction/add_price_reduction");
const makeUpdatePriceReduction = require("./price_reduction/update_price_reduction");
const listPriceReduction = require("./price_reduction/list_price_reduction");

const addPriceReduction = makePriceReduction({ trns });
const updatePriceReduction = makeUpdatePriceReduction({ trns });
const getPriceReduction = listPriceReduction({ trns });

// MTS
const makeMTS = require("./mts/add_mts");
const makeUpdateMTS = require("./mts/update_mts");
const listMTS = require("./mts/list_mts");

const addMTS = makeMTS({ trns });
const updateMTS = makeUpdateMTS({ trns });
const getMTS = listMTS({ trns });

// Reconsideration
const makeReconsideration = require("./reconsideration/add_reconsideration");
const makeUpdateReconsideration = require("./reconsideration/update_reconsideration");
const listReconsideration = require("./reconsideration/list_reconsideration");

const addReconsideration = makeReconsideration({ trns });
const updateReconsideration = makeUpdateReconsideration({ trns });
const getReconsideration = listReconsideration({ trns });

// Disposition
const makeDisposition = require("./disposition/add_disposition");
const makeUpdateDisposition = require("./disposition/update_disposition");
const listDisposition = require("./disposition/list_disposition");

const addDisposition = makeDisposition({ trns });
const updateDisposition = makeUpdateDisposition({ trns });
const getDisposition = listDisposition({ trns });

// Physical Analysis
const makePhysicalAnalysis = require("./physical_analysis/add_physical_analysis");
const makeUpdatePhysicalAnalysis = require("./physical_analysis/update_physical_analysis");
const listPhysicalAnalysis = require("./physical_analysis/list_physical_analysis");

const addPhysicalAnalysis = makePhysicalAnalysis({ trns });
const updatePhysicalAnalysis = makeUpdatePhysicalAnalysis({ trns });
const getPhysicalAnalysis = listPhysicalAnalysis({ trns });

// Chemical Analysis
const makeChemicalAnalysis = require("./chemical_analysis/add_chemical_analysis");
const makeUpdateChemicalAnalysis = require("./chemical_analysis/update_chemical_analysis");
const listChemicalAnalysis = require("./chemical_analysis/list_chemical_analysis");

const addChemicalAnalysis = makeChemicalAnalysis({ trns });
const updateChemicalAnalysis = makeUpdateChemicalAnalysis({ trns });
const getChemicalAnalysis = listChemicalAnalysis({ trns });

// MRR
const makeMRR = require("./mrr/add_mrr");
const makeUpdateMRR = require("./mrr/update_mrr");
const listMRR = require("./mrr/list_mrr");

const addMRR = makeMRR({ trns });
const updateMRR = makeUpdateMRR({ trns });
const getMRR = listMRR({ trns });

// RMAF
const makeQCCN = require("./rmaf/generate_rmaf_id");
const makeRMAF = require("./rmaf/add_rmaf");
const makeUpdateRMAF = require("./rmaf/update_rmaf");
const listRMAF = require("./rmaf/list_rmaf");

const getQCCN = makeQCCN({ trns }, { moment });
const addRMAF = makeRMAF({ trns });
const updateRMAF = makeUpdateRMAF({ trns });
const getRMAF = listRMAF({ trns });

// Rejection Receipt
const makeRejectionReceipt = require("./rejection_receipt/add_rejection_receipt");
const makeUpdateRejectionReceipt = require("./rejection_receipt/update_rejection_receipt");
const listRejectionReceipt = require("./rejection_receipt/list_rejection_receipt");

const addRejectionReceipt = makeRejectionReceipt({ trns });
const updateRejectionReceipt = makeUpdateRejectionReceipt({ trns });
const getRejectionReceipt = listRejectionReceipt({ trns });

// Deduction Receipt
const makeDeductionReceipt = require("./deduction_receipt/add_deduction_receipt");
const makeUpdateDeductionReceipt = require("./deduction_receipt/update_deduction_receipt");
const listDeductionReceipt = require("./deduction_receipt/list_deduction_receipt");

const addDeductionReceipt = makeDeductionReceipt({ trns });
const updateDeductionReceipt = makeUpdateDeductionReceipt({ trns });
const getDeductionReceipt = listDeductionReceipt({ trns });

// Transaction status logs
const makeTransactionStatusLogs = require("./transaction_status_logs/add_transaction_status_logs");
const makeUpdateTransactionStatusLogs = require("./transaction_status_logs/edit_transaction_status_logs");
const listTransactionStatusLogs = require("./transaction_status_logs/list_transaction_status_logs");
const makefindTransactionStatusLogs = require("./transaction_status_logs/find_transaction_status_logs");

const addTransactionStatusLogs = makeTransactionStatusLogs({ trns });
const updateTransactionStatusLogs = makeUpdateTransactionStatusLogs({ trns });
const getTransactionStatusLogs = listTransactionStatusLogs({ trns });
const findTransactionStatusLogs = makefindTransactionStatusLogs({ trns });

// Transaction
const makeTransaction = require("./transaction/add_transaction");
const makeUpdateTransaction = require("./transaction/update_transaction");
const listTransaction = require("./transaction/list_transaction");
const makeCueNumber = require("./transaction/get_cue_number");
const makeFindTransaction = require("./transaction/find_transaction");
const listEntry = require("./transaction/get_entry");
const listExit = require("./transaction/get_exit");
const makeUpdateEntry = require("./transaction/update_entry");
const makeUpdateExit = require("./transaction/update_exit");
const makeFindQR = require("./transaction/find_qr");

const addTransaction = makeTransaction({ trns }, { moment });
const updateTransaction = makeUpdateTransaction({ trns });
const getTransaction = listTransaction({ trns, getDeductionReceipt });
const getCueNumber = makeCueNumber({ trns, moment });
const findTransaction = makeFindTransaction({ trns, getDeductionReceipt });
const getEntry = listEntry({ trns });
const getExit = listExit({ trns });
const updateEntry = makeUpdateEntry({ trns });
const updateExit = makeUpdateExit({ trns });
const findQR = makeFindQR({ trns }, { moment });

// Remarks
const makeRemarks = require("./remarks/add_remarks");
const makeUpdateRemarks = require("./remarks/update_remarks");
const listRemarks = require("./remarks/list_remarks");
const makeFindRemarks = require("./remarks/find_remarks");

const addRemarks = makeRemarks({ trns }, { moment });
const updateRemarks = makeUpdateRemarks({ trns });
const getRemarks = listRemarks({ trns });
const findRemarks = makeFindRemarks({ trns });

// Employee
const makeCheckPin = require("./employee/checkPincode");

const getCheckPin = makeCheckPin({ trns });

const transactionService = Object.freeze({
  // Truck scale
  getnetweight,

  // SAP -suppliers
  getSuppliers,
  getPOs,


  // SAP - supplier po
  getPOSuppliers,
  postGRPO,

  // Employee
  getCheckPin,

  // Remarks
  addRemarks,
  updateRemarks,
  getRemarks,
  findRemarks,

  // Transaction status logs
  addTransactionStatusLogs,
  updateTransactionStatusLogs,
  getTransactionStatusLogs,
  findTransactionStatusLogs,

  // Delivery receipts
  addDeliveryReceipt,
  getDeliverReceipts,
  updateDeliveryReceipt,

  // Trucks
  addTruck,
  addTrucktype,
  getTrucks,
  getTrucktypes,
  updateTruck,
  updateTrucktype,

  // Transaction type
  addTransactionType,
  updateTransactionType,
  getTransactionTypes,

  // Status
  addStatus,
  updateStatus,
  getStatus,

  // Driver
  addDriver,
  updateDriver,
  getDriver,

  // Business Entity
  addBusinessEntity,
  updateBusinessEntity,
  getBusinessEntity,

  getWarehouse,
  removeWarehouse,

  // Items
  addItem,
  updateItem,
  getItem,

  // Raw Materials
  addRawMaterial,
  updateRawMaterial,
  getRawMaterial,

  // BIR
  addBIR,
  updateBIR,
  getBIR,
  getItemsBIR,

  // Criteria
  addCriteria,
  updateCriteria,
  getCriteria,

  // Deduction Criteria
  addDeductionCriteria,
  updateDeductionCriteria,
  getDeductionCriteria,

  // Price Reduction
  addPriceReduction,
  updatePriceReduction,
  getPriceReduction,

  // MTS
  addMTS,
  updateMTS,
  getMTS,

  // Reconsideration
  addReconsideration,
  updateReconsideration,
  getReconsideration,

  // Disposition
  addDisposition,
  updateDisposition,
  getDisposition,

  // Physical Analysis
  addPhysicalAnalysis,
  updatePhysicalAnalysis,
  getPhysicalAnalysis,

  // Chemical Analysis
  addChemicalAnalysis,
  updateChemicalAnalysis,
  getChemicalAnalysis,

  // MRR
  addMRR,
  updateMRR,
  getMRR,

  // RMAF
  getQCCN,
  addRMAF,
  updateRMAF,
  getRMAF,

  // Transaction
  addTransaction,
  updateTransaction,
  getTransaction,
  getCueNumber,
  findTransaction,
  getEntry,
  getExit,
  updateEntry,
  updateExit,
  findQR,

  // Rejection Receipt
  addRejectionReceipt,
  updateRejectionReceipt,
  getRejectionReceipt,

  // Deduction Receipt
  addDeductionReceipt,
  updateDeductionReceipt,
  getDeductionReceipt,
});

module.exports = transactionService;

module.exports = {
  // Truck scale
  getnetweight,

  // SAP -suppliers
  getSuppliers,

  // SAP - supplier po
  getPOSuppliers,
  postGRPO,
  getPOs,

  // Checkpin
  getCheckPin,

  // Remarks
  addRemarks,
  updateRemarks,
  getRemarks,
  findRemarks,

  // Transaction status logs
  addTransactionStatusLogs,
  updateTransactionStatusLogs,
  getTransactionStatusLogs,
  findTransactionStatusLogs,

  // Delivery receipts
  addDeliveryReceipt,
  getDeliverReceipts,
  updateDeliveryReceipt,

  // Trucks
  addTruck,
  addTrucktype,
  getTrucks,
  getTrucktypes,
  updateTruck,
  updateTrucktype,

  // Transaction type
  addTransactionType,
  updateTransactionType,
  getTransactionTypes,

  // Status
  addStatus,
  updateStatus,
  getStatus,

  // Driver
  addDriver,
  updateDriver,
  getDriver,

  // Business Entity
  addBusinessEntity,
  updateBusinessEntity,
  getBusinessEntity,

  getWarehouse,
  removeWarehouse,

  // Items
  addItem,
  updateItem,
  getItem,

  // Raw Materials
  addRawMaterial,
  updateRawMaterial,
  getRawMaterial,

  // BIR
  addBIR,
  updateBIR,
  getBIR,
  getItemsBIR,

  // Criteria
  addCriteria,
  updateCriteria,
  getCriteria,

  // Deduction Criteria
  addDeductionCriteria,
  updateDeductionCriteria,
  getDeductionCriteria,

  // Price Reduction
  addPriceReduction,
  updatePriceReduction,
  getPriceReduction,

  // MTS
  addMTS,
  updateMTS,
  getMTS,

  // Reconsideration
  addReconsideration,
  updateReconsideration,
  getReconsideration,

  // Disposition
  addDisposition,
  updateDisposition,
  getDisposition,

  // Physical Analysis
  addPhysicalAnalysis,
  updatePhysicalAnalysis,
  getPhysicalAnalysis,

  // Chemical Analysis
  addChemicalAnalysis,
  updateChemicalAnalysis,
  getChemicalAnalysis,

  // MRR
  addMRR,
  updateMRR,
  getMRR,

  // RMAF
  getQCCN,
  addRMAF,
  updateRMAF,
  getRMAF,

  // Transaction
  addTransaction,
  updateTransaction,
  getTransaction,
  getCueNumber,
  findTransaction,
  getEntry,
  getExit,
  updateEntry,
  updateExit,
  findQR,

  // Rejection Receipt
  addRejectionReceipt,
  updateRejectionReceipt,
  getRejectionReceipt,

  // Deduction Receipt
  addDeductionReceipt,
  updateDeductionReceipt,
  getDeductionReceipt,
};
