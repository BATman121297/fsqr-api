const { makeAccessRight } = require("../../../entities/access_rights");

module.exports = function postAccessRight({ admin }) {
  return async function makePostAccessRight(request_info) {
    await makeAccessRight(request_info);

    // get max code
    const code = await admin.accessRightsMaxCode();
    const max = code[0].maxCode;

    const info = {
      Code: max,
      Name: max,
      U_action_id: request_info.action_id,
      U_role_id: request_info.role_id,
      U_status: "active",
      U_created_by: request_info.created_by,
      U_created_date: request_info.created_date,
      U_created_time: request_info.created_time,
      U_updated_by: request_info.created_by,
      U_updated_date: request_info.created_date,
      U_updated_time: request_info.created_time,
      cookie: request_info.cookie
    };

    // insert query
    const res = await admin.accessRightsAdd({ info });
    return res;
    // ##############
  };
};
