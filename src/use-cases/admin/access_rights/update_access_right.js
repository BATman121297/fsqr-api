const { makeUpdateAccessRight } = require("../../../entities/access_rights");

module.exports = function putAccessRight({ admin }) {
  return async function makeEditAccessRight(request_info) {
    await makeUpdateAccessRight(request_info);
    let info = {};

    // Check access right in role if exists

    info = {
      action_id: request_info.action_id,
      role_id: request_info.role_id
    };

    const check = await admin.accessRightCheckExist({ info });

    if (check.length > 0) {
      // exist
      if (check[0].U_status.toLowerCase() == "active") {
        throw new Error("Access right already exists in selected role");
      }
    } else {
      // update
      info = {}; // clear

      info = {
        Code: request_info.id,
        U_action_id: request_info.action_id,
        U_role_id: request_info.role_id,
        U_status: request_info.status,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.updated_date,
        U_updated_time: request_info.updated_time,
        cookie: request_info.cookie
      };

      // update query
      const res = await admin.accessRightUpdate({ info });
      return res;
    }
  };
};
