module.exports = function listAccessRights({ admin }) {
  return async function mkaeListAccessRights() {
    const access_right = await admin.listAllAccessRights();
    const role = await admin.listAllRoles();
    const action = await admin.listAllActions();

    let access_rights = [];
    let roles = [];
    let actions = [];

    for (let i = 0; i < access_right.length; i++) {
      const e = access_right[i];
      const info = {
        id: e.Code,
        action_id: e.U_action_id,
        role_id: e.U_role_id,
        status: e.U_status,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        role_name: e.roleName,
        actions_id: e.actionId,
        actions_name: e.actionDesc,
      };
      access_rights.push(info);
    }

    for (let i = 0; i < role.length; i++) {
      const e = role[i];

      const info = {
        id: e.Code,
        name: e.U_name,
        status: e.U_status,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };
      roles.push(info);
    }

    for (let i = 0; i < action.length; i++) {
      const e = action[i];
      const info = {
        id: e.Code,
        module_id: e.U_module_id,
        description: e.U_description,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        status: e.U_status,
        createdAt: e.U_created_date,
        updatedAt: e.U_updated_date,
        moduleId: e.moduleId,
        "module.id": e.moduleId,
        "module.description": e.moduleDesc,
        "module.created_by": e.moduleCreateBy,
        "module.updated_by": e.moduleUpdateBy,
        "module.status": e.moduleStatus,
        "module.createdAt": e.moduleCreatedDate,
        "module.updatedAt": e.moduleUpdateDate,
      };

      actions.push(info);
    }
    
    return { access_rights, roles, actions };
  };
};
