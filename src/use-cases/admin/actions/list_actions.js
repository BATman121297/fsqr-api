module.exports = function listActions({ admin }) {
  return async function makeListActions() {
    const module = await admin.listAllModules();
    const action = await admin.listAllActions();

    // remodel
    let actions = [];
    let modules = [];

    for (let i = 0; i < action.length; i++) {
      const e = action[i];
      const info = {
        id: e.Code,
        module_id: e.U_module_id,
        description: e.U_description,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        status: e.U_status,
        createdAt: e.U_created_date,
        updatedAt: e.U_updated_date,
        moduleId: e.moduleId,
        "module.id": e.moduleId,
        "module.description": e.moduleDesc,
        "module.created_by": e.moduleCreateBy,
        "module.updated_by": e.moduleUpdateBy,
        "module.status": e.moduleStatus,
        "module.createdAt": e.moduleCreatedDate,
        "module.updatedAt": e.moduleUpdateDate,
      };

      actions.push(info);
    }

    for (let i = 0; i < module.length; i++) {
      const e = module[i];

      const info = {
        id: e.Code,
        description: e.U_description,
        status: e.U_status,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };
      modules.push(info);
    }
    return { actions, modules };
  };
};
