module.exports = function listActionRole({ admin }) {
  return async function makeListActionRole(request_info) {
    const { role_id } = request_info;
    const action = await admin.findActionsByRole(role_id);

    let actions = [];
    for (let i = 0; i < action.length; i++) {
      const e = action[i];
      const info = {
        action_id: e.U_action_id,
        role_id: e.U_role_id,
        description: e.U_description,
      };
      actions.push(info);
    }

    return { actions };
  };
};
