const { makeUpdateAction } = require("../../../entities/actions");

module.exports = function putAction({ admin }) {
  return async function makeEditAction(request_info) {
    await makeUpdateAction(request_info);

    const { cookie } = request_info; // SL cookie
    delete request_info.cookie;

    const { id, description } = request_info;

    // check if exist;
    const check = await admin.actionsUpdateCheck(description, id);

    if (check.length > 0) {
      // exist
      throw new Error("Action already exists");
    } else {
      // update
      const info = {
        Code: request_info.id,
        Name: request_info.description,
        U_module_id: request_info.module_id,
        U_description: request_info.description,
        U_status: request_info.status,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.updated_date,
        U_updated_time: request_info.updated_time,
        cookie
      };

      const res = await admin.actionsUpdate({ info });
      return res;
    }
  };
};
