const { makeAction } = require("../../../entities/actions");

module.exports = function postAction({ admin }) {
  return async function makePostAction(request_info) {
    await makeAction(request_info);

    // get max code
    const code = await admin.actionsMaxCode();
    const max = code[0].maxCode;

    const check = await admin.actionsDescCheck(request_info.description);

    if (check.length > 0) {
      // exist
      throw new Error("Action already exists");
    } else {
      // insert
      const info = {
        Code: `${max}`,
        Name: request_info.description,
        U_module_id: request_info.module_id,
        U_description: request_info.description,
        U_status: "active",
        U_created_by: request_info.created_by,
        U_created_date: request_info.created_date,
        U_created_time: request_info.created_time,
        U_updated_by: request_info.created_by,
        U_updated_date: request_info.created_date,
        U_updated_time: request_info.created_time,
        cookie: request_info.cookie
      };

      const res = await admin.actionsAdd({ info });
      return res;
    }
  };
};
