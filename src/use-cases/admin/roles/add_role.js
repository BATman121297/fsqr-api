const { makeRole } = require("../../../entities/roles");

module.exports = function postRole({ admin }) {
  return async function makePostRole(request_info) {
    await makeRole(request_info);

    // get max code
    const code = await admin.rolesMaxCode();
    const max = code[0].maxCode;

    // check if role name exist

    const check = await admin.roleNameCheck(request_info.name);
    if (check.length > 0) {
      // exist
      throw new Error("Role name already exists");
    } else {
      // insert
      const info = {
        Code: max,
        Name: request_info.name,
        U_name: request_info.name,
        U_status: "active",
        U_created_by: request_info.created_by,
        U_created_date: request_info.created_date,
        U_created_time: request_info.created_time,
        U_updated_by: request_info.created_by,
        U_updated_date: request_info.created_date,
        U_updated_time: request_info.created_time,
        cookie: request_info.cookie
      };

      const res = await admin.roleAdd({ info });
      return res;
    }
  };
};
