const { makeUpdateRole } = require("../../../entities/roles");

module.exports = function putRole({ admin }) {
  return async function makeEditRole(request_info) {
    await makeUpdateRole(request_info);
    const { id, name } = request_info;

    // check if id exist
    const checkId = await admin.roleIdCheck(id);

    if (checkId.length == 0) throw new Error("Role does not exist");

    // check if role exist
    const check = await admin.roleNameUpdateCheck(name, id);

    if (check.length > 0)
      if (check[0].U_status.toLowerCase() == "active")
        throw new Error("Role name already exists");

    // append all batch string; no tab
    let batchStr = `
--a
Content-Type:multipart/mixed;boundary=b
`;

    // update role
    batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
PATCH /b1s/v1/U_FSQR_ROLES('${id}')

{
  "Name": "${name}",
  "U_name": "${name}",
  "U_status": "${request_info.status}",
  "U_updated_by": "${request_info.updated_by}",
  "U_updated_date": "${request_info.updated_date}",
  "U_updated_time": "${request_info.updated_time}"
}
`;

    // get all actions of the role
    const actions = await admin.rolesActions(id);

    // delete existing
    for (let i = 0; i < actions.length; i++) {
      const e = actions[i].Code;
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
DELETE /b1s/v1/U_FSQR_ACC_RIGHTS('${e}')
`;
    }

    // get max code
    const code = await admin.accessRightsMaxCode();
    let max = code[0].maxCode;

    // insert new actions
    const newActions = request_info.actions;
    for (let i = 0; i < newActions.length; i++) {
      const e = newActions[i];
      batchStr += `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_FSQR_ACC_RIGHTS

{
  "Code": "${max}",
  "Name": "${max}",
  "U_action_id": "${e}",
  "U_role_id": "${id}",
  "U_status": "active",
  "U_updated_by": "${request_info.updated_by}",
  "U_updated_date": "${request_info.updated_date}",
  "U_updated_time": "${request_info.updated_time}"
}
`;

      // increment max code
      max++;
    }

    batchStr += `
--b--
--a--
            `;

    // batch request
    const info = {
      str: batchStr,
      cookie: request_info.cookie
    };

    const res = await admin.rolesUpdate({ info });
    return res;
  };
};
