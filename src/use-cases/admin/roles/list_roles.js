module.exports = function listRoles({ admin }) {
  return async function makeListRoles() {
    const result = await admin.listAllRoles();
    const results = [];

    for (let i = 0; i < result.length; i++) {
      const e = result[i];
      const info = {
        id: e.Code,
        name: e.U_name,
        status: e.U_status,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };
      results.push(info);
    }
    return results;
  };
};
