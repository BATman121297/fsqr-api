const { makeUpdateModule } = require("../../../entities/modules");

module.exports = function putModule({ admin }) {
  return async function makeEditModule(request_info) {
    await makeUpdateModule(request_info);

    const { cookie } = request_info; // SL cookie
    delete request_info.cookie;

    const { id, description } = request_info;

    // check if exist
    const check = await admin.modulesUpdateCheck(description, id);

    if (check.length > 0) {
      // exist
      throw new Error("Module already exists");
    } else {
      // update here
      const info = {
        Code: request_info.id,
        Name: request_info.description,
        U_description: request_info.description,
        U_status: request_info.status,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie
      };

      const res = await admin.modulesUpdate({ info });
      return res;
    }
  };
};
