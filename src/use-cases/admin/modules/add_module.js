const { makeModule } = require("../../../entities/modules");

module.exports = function postModule({ admin }) {
  return async function makePostModule(request_info) {
    await makeModule(request_info);

    // get max code
    const code = await admin.modulesMaxCode();
    const max = code[0].maxCode;

    // check if modules exist
    const check = await admin.modulesDescCheck(request_info.description);

    if (check.length > 0) {
      // exist
      throw new Error("Module already exists");
    } else {
      // not exist; insert
      const { cookie } = request_info; // cookie from SL

      const info = {
        Code: max,
        Name: request_info.description,
        U_description: request_info.description,
        U_status: "active",
        U_created_by: request_info.created_by,
        U_created_date: request_info.created_date,
        U_created_time: request_info.created_time,
        U_updated_by: request_info.created_by,
        U_updated_date: request_info.created_date,
        U_updated_time: request_info.created_time,
        cookie
      };

      // insert query
      const res = await admin.modulesAdd({ info });
      return res;
    }
  };
};
