module.exports = function listModules({ admin }) {
  return async function makeListModules() {
    const result = await admin.listAllModules();
    let modules = [];
    for (let i = 0; i < result.length; i++) {
      const e = result[i];
      const info = {
        id: e.Code,
        description: e.U_description,
        status: e.U_status,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
      };
      modules.push(info);
    }
    return { modules };
  };
};
