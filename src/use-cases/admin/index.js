//Data access
const admin_db = require("../../data-access/admin/");
const sap_db = require("../../data-access/sap");
const encryption = require("../../encryption/encrypting");

// sap db
const { admin } = require("../../data-access/sap-integ/admin/index");
const { emp } = require("../../data-access/sap-integ/employees/index");

//Modules
const makeModuleList = require("./modules/list_modules");
const makeModule = require("./modules/add_module");
const makeUpdateModule = require("./modules/update_module");

const listModules = makeModuleList({ admin });
const addModule = makeModule({ admin });
const updateModule = makeUpdateModule({ admin });

//Action
const makeActionList = require("./actions/list_actions");
const makeAction = require("./actions/add_action");
const makeUpdateAction = require("./actions/update_action");
const makeActionRoleList = require("./actions/list_actions_role");

const listActions = makeActionList({ admin });
const addAction = makeAction({ admin });
const updateAction = makeUpdateAction({ admin });
const listActionRole = makeActionRoleList({ admin });

//Roles
const makeRoleList = require("./roles/list_roles");
const makeRole = require("./roles/add_role");
const makeUpdateRole = require("./roles/update_role");

const listRoles = makeRoleList({ admin });
const addRole = makeRole({ admin });
const updateRole = makeUpdateRole({ admin });

//Access rights
const makeAccessRightList = require("./access_rights/list_access_rights");
const makeAccessRight = require("./access_rights/add_access_right");
const makeUpdateAccessRight = require("./access_rights/update_access_right");

const listAccessRights = makeAccessRightList({ admin });
const addAccessRight = makeAccessRight({ admin });
const updateAccessRight = makeUpdateAccessRight({ admin });

// Employees
const makeEmployeeList = require("./employees/list_employees");
const makeEmployee = require("./employees/add_employees");
const makeUpdateEmployee = require("./employees/update_employee");
const makeUpdatePincode = require("./employees/update_pin");
const makeResetPassword = require("./employees/reset_password");
const listSAPEmployees = require("./employees/list-sap-employees");

const listEmployees = makeEmployeeList({ emp });
const addEmployee = makeEmployee({ emp });
const updateEmployee = makeUpdateEmployee({ emp });
const updatePin = makeUpdatePincode({ emp });
const resetPassword = makeResetPassword({ emp });
const listSAPEmployeess = listSAPEmployees({ emp });

// Activity logs
const makeListActivityLogs = require("./activity_logs/list_activitylogs");

const listActivityLogs = makeListActivityLogs({ admin_db }, { sap_db });

// Departments
const makeDeparmentList = require("./departments/list_departments");
const makeDepartment = require("./departments/add_deparment");
const makeUpdateDepartment = require("./departments/update_department");

const listDepartments = makeDeparmentList({ admin });
const addDepartment = makeDepartment({ admin });
const updateDepartment = makeUpdateDepartment({ admin });

const adminService = Object.freeze({
  //Modules
  listModules,
  addModule,
  updateModule,

  //Actions
  listActions,
  addAction,
  updateAction,
  listActionRole,

  //Roles
  listRoles,
  addRole,
  updateRole,

  //Access Rights
  listAccessRights,
  addAccessRight,
  updateAccessRight,

  // Employee
  listEmployees,
  addEmployee,
  updateEmployee,
  updatePin,
  resetPassword,
  listSAPEmployeess,

  // Activity Logs
  listActivityLogs,

  // Department
  listDepartments,
  addDepartment,
  updateDepartment,
});

module.exports = adminService;

module.exports = {
  //Modules
  listModules,
  addModule,
  updateModule,

  //Actions
  listActions,
  addAction,
  updateAction,
  listActionRole,

  //Roles
  listRoles,
  addRole,
  updateRole,

  //Access Rights
  listAccessRights,
  addAccessRight,
  updateAccessRight,

  // Employee
  listEmployees,
  addEmployee,
  updateEmployee,
  updatePin,
  resetPassword,
  listSAPEmployeess,

  // Activity logs
  listActivityLogs,

  // Department
  listDepartments,
  addDepartment,
  updateDepartment,
};
