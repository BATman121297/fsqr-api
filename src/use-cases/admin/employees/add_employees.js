const { makeEmployee } = require("../../../entities/employees");

module.exports = function postEmployee({ emp }) {
  return async function makePostEmployee(employee_info) {
    await makeEmployee(employee_info);

    const { employee_id, app_emp_id } = employee_info;

    // check if employee id exist;
    const check = await emp.empIdCheck(employee_id);

    if (check.length > 0)
      throw new Error(`Employee with EmpID ${employee_id} already exist.`);

    // check if employee is already registered
    const checks = await emp.empCheck(app_emp_id);
    if (checks.length > 0) throw new Error(`Employee was already registed.`);

    // get max code
    const code = await emp.empMaxCode();
    const max = code[0].maxCode;

    // insert
    const info = {
      Code: max,
      Name: max,
      U_role_id: employee_info.role_id,
      U_employee_id: employee_info.employee_id,
      U_password: employee_info.password,
      U_status: "active",
      U_APP_EMP_ID: employee_info.app_emp_id,
      U_pin_code: 1234,
      U_signature: null,
      U_department: employee_info.department,
      U_created_by: employee_info.created_by,
      U_created_date: employee_info.created_date,
      U_created_time: employee_info.created_time,
      U_updated_by: employee_info.created_by,
      U_updated_date: employee_info.created_date,
      U_updated_time: employee_info.created_time,
      cookie: employee_info.cookie
    };

    const res = await emp.empAdd({ info });
    return res;
    // employee_info.password = encryption.encrypt(
    //   employee_info.employee_id.toString()
    // );

    // employee_info.pin_code = encryption.encrypt("1234");

    // const result = await admin_db.addEmployee(employee_info);

    // if (!result || result.rowCount == 0) {
    //   throw new Error("Insert failed.");
    // }

    // const old_values = "{}";
    // const new_values = JSON.stringify(result.rows[0]);

    // const log_info = {
    //   created_by: employee_info.created_by,
    //   action: "create",
    //   table: "employees",
    //   old_values,
    //   new_values
    // };

    // await admin_db.addActivityLog(log_info);

    // return result.rows;
  };
};
