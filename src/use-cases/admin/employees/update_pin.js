const { makeChangePin } = require("../../../entities/employees");

module.exports = function putEmployee({ emp }) {
  return async function makeEditEmployee(request_info) {
    if (!request_info.reset_pincode) {
      await makeChangePin(request_info);

      // check if employee exist
      const check = await emp.empCheckExist(request_info.employee_id);
      if (check.length == 0) throw new Error("Employee doesn't exist.");

      // update pincode base on user input
      const info = {
        Code: request_info.employee_id,
        U_pin_code: request_info.pincode,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };

      // update
      const res = await emp.empUpdate({ info });
      return res;
    } else {
      request_info.pincode = "1234";

      // reset pincode to default
      const info = {
        Code: request_info.employee_id,
        U_pin_code: request_info.pincode,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };
      console.log(info);
      // update
      const res = await emp.empUpdate({ info });
      return res;
    }
    // // Get old value
    // const oldResult = await admin_db.getEmployeeByEmpId(
    //   request_info.employee_id
    // );
    // const old_values = JSON.stringify(oldResult.rows[0]);
    // const result = await admin_db.changePincode(request_info);
    // if (result.rowCount > 0) {
    //   delete request_info.role;
    //   delete request_info.modules;
    // } else {
    //   throw new Error("Updated failed");
    // }
    // // Store new values
    // const new_values = JSON.stringify(result.rows[0]);
    // // Define log_info to insert to activity logs
    // const log_info = {
    //   created_by: request_info.updated_by,
    //   action: "update",
    //   table: "employees",
    //   old_values,
    //   new_values
    // };
    // await admin_db.addActivityLog(log_info);
    // return request_info;
  };
};
