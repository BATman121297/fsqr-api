const { makeUpdateEmployee } = require("../../../entities/employees");

module.exports = function putEmployee({ emp }) {
  return async function makeEditEmployee(employee_info) {
    if (employee_info.password || employee_info.pin_code) {
    } else {
      await makeUpdateEmployee(employee_info);
    }

    // update password only;
    if (employee_info.password) {
      // get code base on employee id;
      let code = await emp.returnCode(employee_info.id);
      if (code.length == 0) throw new Error(`Employee doesn't exist.`);
      code = code[0];
      const info = {
        Code: code.Code,
        U_password: employee_info.password,
        cookie: employee_info.cookie,
      };
      const res = await emp.empUpdate({ info });
    } else {
      // update all employee data;

      const { id, employee_id } = employee_info;

      // update employee
      // check if employee exist; id is PK;
      const exist = await emp.empCheckExist(id);

      if (exist.length == 0) throw new Error(`Employee does not exist.`);

      // check if employee_id is already used
      const check = await emp.empIdUpdateCheck(employee_id, id);
      if (check.length > 0) throw new Error(`Employee ID is already in use.`);

      // update employee
      const info = {
        Code: employee_info.id,
        U_role_id: employee_info.role_id,
        U_status: employee_info.status,
        U_department: employee_info.department,
        U_updated_by: employee_info.updated_by,
        U_updated_date: employee_info.update_date,
        U_updated_time: employee_info.update_time,
        cookie: employee_info.cookie,
      };

      const res = await emp.empUpdate({ info });
      return res;
    }

    // update pin only;
    if (employee_info.pin_code) {
      // get code base on employee id;
      let code = await emp.returnCode(employee_info.id);
      if (code.length == 0) throw new Error(`Employee doesn't exist.`);
      code = code[0];
      const info = {
        Code: code.Code,
        U_pin_code: employee_info.pin_code,
        cookie: employee_info.cookie,
      };

      const res = await emp.empUpdate({ info });
    }
  };
};
