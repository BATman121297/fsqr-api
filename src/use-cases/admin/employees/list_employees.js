module.exports = function listEmployees({ emp }) {
  return async function makeListEmployees() {
    const res = await emp.listAllEmployees();

    let employees = [];
    for (let i = 0; i < res.length; i++) {
      const e = res[i];
      const role_id = e.U_role_id;
      let role = await emp.findRoleById(role_id);
      role = role[0];
      const info = {
        id: e.Code,
        employee_id: e.U_employee_id,
        name: `${e.firstName} ${e.lastName}`,
        role_id: e.U_role_id,
        department: e.U_department,
        password: e.U_password,
        pin_code: e.U_pin_code,
        token: null,
        status: e.U_status,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        role: [
          {
            id: role.Code ? role.Code : "",
            name: role.U_name ? role.U_name : "",
            status: role.U_status ? role.U_status : "",
            created_by: role.U_created_by ? role.U_created_by : "",
            updated_by: role.U_updated_by ? role.U_updated_by : "",
            created_at: role.U_created_date ? role.U_created_date : "",
            updated_at: e.U_updated_date ? e.U_updated_date : "",
          },
        ],
      };

      employees.push(info);
    }

    return { employees };
  };
};
