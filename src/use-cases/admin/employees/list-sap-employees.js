module.exports = function listSAPEmployees({ emp }) {
  return async function makeListEmployees() {
    const res = await emp.sapEmployees();
    return res;
  };
};
