const { makeResetPassword } = require("../../../entities/employees");

module.exports = function putEmployeePassword({ emp }) {
  return async function makeResetPasswordD(employee_info) {
    console.log(employee_info);
    await makeResetPassword(employee_info);

    // reset password to default; employee id;

    // check if exist
    const check = await emp.empCheckExist(employee_info.employee_id);
    if (check.length == 0) throw new Error("Employee doesn't exist.");

    const id = check[0].U_employee_id;

    // update
    const info = {
      Code: employee_info.employee_id,
      U_password: id,
      U_updated_by: employee_info.updated_by,
      U_updated_date: employee_info.update_date,
      U_updated_time: employee_info.update_time,
      cookie: employee_info.cookie
    };

    const res = await emp.empUpdate({ info });
    return res;
  };
};
