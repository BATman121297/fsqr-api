module.exports = function listActivityLogs({ admin_db }) {
    return async function makeListActivityLogs(request_info) {
      const { date_from, date_to } = request_info;
  
      let listActivityLogs = await admin_db.listActivityLogs(date_from, date_to);
  
      let activityLogs = listActivityLogs.rows;
      if (!activityLogs.length) {
        activityLogs = [];
      }

      for (let i = 0; i < activityLogs.length; i++) {
        
        activityLogs[i].new_values = JSON.parse(activityLogs[i].new_values);
        activityLogs[i].old_values = JSON.parse(activityLogs[i].old_values);
      }

  
      return { activityLogs };
    };
  };
  