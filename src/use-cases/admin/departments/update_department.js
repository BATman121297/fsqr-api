const { makeUpdateDepartment } = require("../../../entities/departments");

module.exports = function putDepartment({ admin }) {
  return async function makeEditDepartment(request_info) {
    await makeUpdateDepartment(request_info);

    const { id, name } = request_info;

    // check if exist
    const check = await admin.deptUpdateCheck(name, id);
    if (check.length > 0) {
      // exist
      throw new Error("Department already exists.");
    } else {
      // update
      const info = {
        Code: request_info.id,
        Name: request_info.name,
        U_name: request_info.name,
        U_status: request_info.status,
        U_updated_by: request_info.updated_by,
        U_updated_date: request_info.update_date,
        U_updated_time: request_info.update_time,
        cookie: request_info.cookie
      };

      const res = await admin.deptUpdate({ info });
      return res;
    }
  };
};
