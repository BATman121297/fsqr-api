module.exports = function listDepartments({ admin }) {
  return async function makeListDepartments() {
    const res = await admin.listAllDepartments();
    let departments = [];
    for (let i = 0; i < res.length; i++) {
      const e = res[i];
      const info = {
        id: e.Code,
        name: e.U_name,
        created_by: e.U_created_by,
        updated_by: e.U_updated_by,
        created_at: e.U_created_date,
        updated_at: e.U_updated_date,
        status: e.U_status,
      };
      departments.push(info);
    }
    return { departments };
  };
};
