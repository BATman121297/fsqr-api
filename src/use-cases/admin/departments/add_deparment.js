const { makeDepartment } = require("../../../entities/departments");

module.exports = function postDepartment({ admin }) {
  return async function makePostDepartment(employee_info) {
    await makeDepartment(employee_info);

    // check if dept name exist;
    const { name } = employee_info;

    const check = await admin.deptNameCheck(name);

    if (check.length > 0) {
      // exist
      throw new Error("Department already exists.");
    } else {
      // insert

      //   get max code
      const code = await admin.deptMaxCode();
      const max = code[0].maxCode;

      const info = {
        Code: max,
        Name: employee_info.name,
        U_name: employee_info.name,
        U_status: "active",
        U_created_by: employee_info.created_by,
        U_created_date: employee_info.created_date,
        U_created_time: employee_info.created_time,
        U_updated_by: employee_info.created_by,
        U_updated_date: employee_info.created_date,
        U_updated_time: employee_info.created_time,
        cookie: employee_info.cookie
      };

      const res = await admin.deptAdd({ info });
      return res;
    }

    // const result = await admin_db.addDepartment(employee_info);

    // if (!result || result.rowCount == 0) {
    //   throw new Error("Insert failed.");
    // }

    // const old_values = "{}";
    // const new_values = JSON.stringify(result.rows[0]);

    // const log_info = {
    //   created_by: employee_info.created_by,
    //   action: "create",
    //   table: "department",
    //   old_values,
    //   new_values
    // };

    // await admin_db.addActivityLog(log_info);

    // return result.rows;
  };
};
