module.exports = function buildRejectionReceipt() {
  return async function makeRejectionReceipt(request_info) {
    const {
      transaction_id,
      no_of_bags_rejected,
      reason_of_rejection,
      date
    } = request_info;

    if (!transaction_id) {
      throw new Error("Transaction ID must have a value");
    }
    if (isNaN(transaction_id)) {
      throw new Error("Transaction ID must be a number");
    }
    // if (!no_of_bags_rejected) {
    //   throw new Error("Number of bags rejected required.");
    // }
    if (isNaN(no_of_bags_rejected)) {
      throw new Error("Number of bags rejected must be a number");
    }
    // if (!reason_of_rejection) {
    //   throw new Error("Reason of rejection required");
    // }
    // if (!date) {
    //   throw new Error("Date must have a value");
    // }
    // if (!created_by) {
    //   throw new Error("Created by must have a value");
    // }
    // if (isNaN(created_by)) {
    //   throw new Error("Created by must be a number");
    // }
  };
};
