const buildRejectionReceipt = require('./add_rejection_receipt');
const buildUpdateRejectionReceipt = require('./edit_rejection_receipt');

const makeRejectionReceipt = buildRejectionReceipt();
const makeUpdateRejectionReceipt = buildUpdateRejectionReceipt();

module.exports = {
  makeRejectionReceipt,
  makeUpdateRejectionReceipt
};
