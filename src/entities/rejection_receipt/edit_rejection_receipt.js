module.exports = function buildUpdateRejectionReceipt() {
  return async function makeUpdateRejectionReceipt(request_info) {
    const {
      id,
      transaction_id,
      no_of_bags_rejected,
      reason_of_rejection,
      date,
      updated_by
    } = request_info;

    if (!id) {
      throw new Error("ID must have a value.");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }
    if (!transaction_id) {
      throw new Error("Transaction ID must have a value");
    }
    if (isNaN(transaction_id)) {
      throw new Error("Transaction ID must be a number");
    }
    if (!no_of_bags_rejected) {
      throw new Error("Number of bags rejected required.");
    }
    if (isNaN(no_of_bags_rejected)) {
      throw new Error("Number of bags rejected must be a number");
    }
    if (!reason_of_rejection) {
      throw new Error("Reason of rejection required");
    }
    // if (!date) {
    //   throw new Error("Date must have a value");
    // }
    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // const rejectionReceiptExists = await transaction_db.findRejectionReceiptById(
    //   id
    // );
    // if (rejectionReceiptExists.rowCount == 0) {
    //   throw new Error("Rejection receipt does not exist.");
    // }

    // const transactionExist = await transaction_db (Find if transaction exist.)
  };
};
