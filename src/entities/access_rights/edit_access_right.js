module.exports = function buildUpdateAccessRight() {
  return async function makeUpdateAccessRight(request_info) {
    const { id, action_id, role_id, status } = request_info;

    if (!id) {
      throw new Error("Access right id must have a value");
    }
    if (isNaN(id)) {
      throw new Error("Access right id must be a number");
    }

    // const accessRightIdExists = await admin_db.findAccessRightById(id);
    // if (!accessRightIdExists) {
    //   throw new Error("Access right does not exist");
    // }

    if (!action_id) {
      throw new Error("Action id must have a value");
    }
    if (isNaN(action_id)) {
      throw new Error("Action id must be a number");
    }
    if (!role_id) {
      throw new Error("Role id must have a value");
    }
    if (isNaN(role_id)) {
      throw new Error("role id must be a number");
    }
    if (!status) {
      throw new Error("Status must have a value");
    }
    if (!isNaN(status)) {
      throw new Error("Status must be a string");
    }
    if (
      status.toLowerCase() !== "active" &&
      status.toLowerCase() !== "inactive"
    ) {
      throw new Error("Invalid status");
    }
    //Check access right in role if exists
    // const accessRightExistsInRole = await admin_db.findAccessRightsByActionInRole(
    //   role_id,
    //   action_id
    // );
    // if (accessRightExistsInRole > 0 && status === "Active") {
    //   if (accessRightExistsInRole.rows.id !== id) {
    //     throw new Error("Access right already exists in selected role");
    //   }
    // }
  };
};
