module.exports = function buildAccessRight() {
  return async function makeAccessRight(request_info) {
    const { id, action_id, role_id } = request_info;

    if (id) {
      throw new Error("Access Right id is auto increment");
    }
    if (!action_id) {
      throw new Error("Action id must have a value");
    }
    if (isNaN(action_id)) {
      throw new Error("Action id must be a number");
    }
    if (!role_id) {
      throw new Error("Role id must have a value");
    }
    if (isNaN(role_id)) {
      throw new Error("role id must be a number");
    }

    //Check access rights in role
    // const accessRightExistsInRole = await admin_db.findAccessRightsByActionInRole(
    //   role_id,
    //   action_id
    // );
    // if (accessRightExistsInRole.rowCount > 0) {
    //   throw new Error('Access right already exists in selected role');
    // }
  };
};
