module.exports = function buildUpdateBusinessEntity() {
  return async function makeUpdateBusinessEntity(request_info) {
    const { id, description, updated_by, department_id } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }

    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findBusinessEntityById(id);
    // if (!statusExist) {
    //   throw new Error("Business entity does not exist");
    // }

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    if (!department_id) {
      throw new Error("Department ID must have a value");
    }

    // //check status description if already exists
    // const descriptionExists = await transaction_db.findBusinessEntityByName(description);
    // if (descriptionExists.rowCount != 0) {
    //   throw new Error("Business entity description already exists");
    // }
  };
};
