const buildBusinessEntity = require('./add_business_entity');
const buildUpdateBusinessEntity = require('./edit_business_entity');
const buildDeleteWarehouse = require('./remove_warehouse');

const makeBusinessEntity = buildBusinessEntity();
const makeUpdateBusinessEntity = buildUpdateBusinessEntity();
const makeDeleteWarehouse = buildDeleteWarehouse();

module.exports = {
  makeBusinessEntity,
  makeUpdateBusinessEntity,
  makeDeleteWarehouse
};
