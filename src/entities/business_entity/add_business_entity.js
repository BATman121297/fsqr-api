module.exports = function buildBusinessEntity() {
  return async function makeBusinessEntity(request_info) {
    const { description, created_by, department_id } = request_info;

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    if (!created_by) {
      throw new Error("Description must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Description must be a number");
    }

    if (!department_id) {
      throw new Error("Department ID must have a value");
    }

    // //check role name if exists
    // const nameExists = await transaction_db.findBusinessEntityByName(description);

    // if (nameExists.rowCount > 0) {
    //   throw new Error('Business entity already exists');
    // }
  };
};
