module.exports = function buildDeleteWarehouse() {
  return async function makeDeleteWarehouse(request_info) {
    const { id, description } = request_info;

    

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }


    // //check if status id exists
    // const statusExist = await transaction_db.findBusinessEntityById(id);
    // if (!statusExist) {
    //   throw new Error("Business entity does not exist");
    // }

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    // //check status description if already exists
    // const descriptionExists = await transaction_db.findBusinessEntityByName(description);
    // if (descriptionExists.rowCount != 0) {
    //   throw new Error("Business entity description already exists");
    // }
  };
};