module.exports = function buildTransaction() {
  return async function makeTransaction(request_info) {
    const {
      bir_id,
      business_entity_id,
      status_id,
      plate_number,
      supplier_name,
      address,
      transaction_type_id,
      driver_name,
      supplier_code,
      no_of_bags,
      guard_on_duty,
      qc_control_number,
      created_by
    } = request_info;

    // if (!delivery_receipt_id || isNaN(delivery_receipt_id)) {
    //   throw new Error("Invalid delivery receipt ID");
    // }
    // if (!rmaf_id || isNaN(rmaf_id)) {
    //   throw new Error("Invalid RMAF ID");
    // }

    if (!bir_id) {
      throw new Error("Item is required.");
    }
    if (!supplier_name) {
      throw new Error("Supplier name is required.");
    }
    if (!address) {
      throw new Error("Address is required.");
    }

    if (!driver_name) {
      throw new Error("Driver name is required.");
    }

    if (!plate_number) {
      throw new Error("Plate number is required.");
    }
    if (!no_of_bags) {
      throw new Error("No of bags required.");
    }
    if (!transaction_type_id || isNaN(transaction_type_id)) {
      throw new Error("Transaction type required.");
    }
    if (!supplier_code) {
      throw new Error("Supplier code required.");
    }

    if (!guard_on_duty) {
      throw new Error("Guard on duty required.");
    }

    // if (!qc_control_number) {
    //   throw new Error("QC control number required.");
    // }
    if (!created_by) {
      throw new Error("Created by have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
