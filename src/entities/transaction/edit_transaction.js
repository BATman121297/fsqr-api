module.exports = function buildUpdateTransaction() {
  return async function makeUpdateTransaction(request_info) {
    const {
      id,
      delivery_receipt_id,
      rmaf_id,
      bir_id,
      status_id,
      plate_number,
      transaction_type_id,
      driver_name,
      supplier_code,
      no_of_bags,
      description,
      guard_on_duty,
      time_end,
      qc_control_number,
      updated_by,
    } = request_info;



    if (!id || isNaN(id)) {
      throw new Error("Invalid ID");
    }
    console.log("id here")
    if (delivery_receipt_id && isNaN(delivery_receipt_id)) {
      throw new Error("Invalid delivery receipt ID");
    }
    if (!bir_id) {
      if (!business_entity_id || !item_id) {
        throw new Error("Business Entity ID and Item ID must be provided.");
      }
    }

    // if (status_id && isNaN(status_id)) {
    //   const ListStatus = await transaction_db.findStatusByName("Sampling");
    //   request_info.status_id = ListStatus.rows[0].id;
    // }
    if (!plate_number) {
      throw new Error("Plate number is required.");
    }

    if (!transaction_type_id && isNaN(transaction_type_id)) {
      throw new Error("Invalid Transaction type ID");
    }
    // if (!driver_name) {
    //   throw new Error("Driver name is required.");
    // }
    if (!no_of_bags && isNaN(no_of_bags)) {
      throw new Error("No of bags required.");
    }

    if (!guard_on_duty && isNaN(guard_on_duty)) {
      throw new Error("Guard on duty required.");
    }

    // if (!qc_control_number) {
    //     throw new Error("QC control number required.")
    // }
    if (!updated_by && isNaN(updated_by)) {
      throw new Error("Updated by must have a value");
    }
 
    
  };
};
