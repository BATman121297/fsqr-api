module.exports = function buildCorrectTransaction() {
  return async function makeCorrectTransaction(request_info) {
    const { id, driver_id, truck_info_id, supplier_address } = request_info;

    if (!id || isNaN(id)) {
      throw new Error("Invalid ID");
    }
    if (driver_id && isNaN(driver_id)) {
      throw new Error("Driver ID is required.");
    }
    if (truck_info_id && isNaN(truck_info_id)) {
      throw new Error("Truck info ID is required.");
    }
    if (supplier_address) {
      throw new Error("Supplier address is required.");
    }
    // if (!qc_control_number) {
    //     throw new Error("QC control number required.")
    // }
    if (updated_by && isNaN(updated_by)) {
      throw new Error("Updated by must have a value");
    }
  };
};
