const buildTransaction = require('./add_transaction');
const buildUpdateTransaction = require('./edit_transaction');
const buildCorrectTransaction = require('./correct_transaction')
const buildUpdateEntry = require('./edit_entry');
const buildUpdateExit = require('./edit_exit');

const makeTransaction = buildTransaction();
const makeUpdateTransaction = buildUpdateTransaction();
const makeCorrectTransaction = buildCorrectTransaction();
const makeUpdateEntry = buildUpdateEntry();
const makeUpdateExit = buildUpdateExit();


module.exports = {
  makeTransaction,
  makeUpdateTransaction,
  makeCorrectTransaction,
  makeUpdateEntry,
  makeUpdateExit
};
