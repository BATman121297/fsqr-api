const buildDeductionCriteria = require('./add_deduction_criteria');
const buildUpdateDeductionCriteria = require('./edit_deduction_criteria');

const makeDeductionCriteria = buildDeductionCriteria();
const makeUpdateDeductionCriteria = buildUpdateDeductionCriteria();

module.exports = {
  makeDeductionCriteria,
  makeUpdateDeductionCriteria
};
