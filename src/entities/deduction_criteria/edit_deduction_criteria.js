module.exports = function buildUpdateDeductionCriteria() {
  return async function makeUpdateDeductionCriteria(request_info) {
    const {
      id,
      criteria_id,
      quantity,
      deduction,
      total,
      deduction_receipt_id
    } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (!criteria_id) {
      throw new Error("DeductionCriteria ID must have a value");
    }
    if (!quantity) {
      throw new Error("Quantity must have a value");
    }
    if (!deduction) {
      throw new Error("Deduction must have a value");
    }
    if (!total) {
      throw new Error("Total must have a value");
    }

    if (!deduction_receipt_id) {
      throw new Error("Enter deduction receipt.");
    }
    // if (!updated_by) {
    //   throw new Error("Updated by must have a value");
    // }
    // if (isNaN(updated_by)) {
    //   throw new Error("Updated by must be a number");
    // }

    // //check if status id exists
    // const statusExist = await transaction_db.findDeductionCriteriaById(id);
    // if (!statusExist) {
    //   throw new Error("Deduction Criteria does not exist");
    // }
  };
};
