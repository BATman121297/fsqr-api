module.exports = function buildDeductionCriteria() {
  return async function makeDeductionCriteria(request_info) {
    const {
      criteria_id,
      quantity,
      deduction,
      total,
      deduction_receipt_id
    } = request_info;

    if (!criteria_id) {
      throw new Error("Criteria ID must have a value");
    }
    if (!deduction_receipt_id) {
      throw new Error("Enter deduction receipt.");
    }
    if (!deduction) {
      throw new Error("Deduction must have a value");
    }
    // if (!total) {
    //   throw new Error('Total must have a value');
    // }
    // if (!created_by) {
    //   throw new Error('Created by have a value');
    // }
    // if (isNaN(created_by)) {
    //   throw new Error('Created by must be a number');
    // }
  };
};
