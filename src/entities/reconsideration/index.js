const buildReconsideration = require('./add_reconsideration');
const buildUpdateReconsideration = require('./edit_reconsideration');

const makeReconsideration = buildReconsideration();
const makeUpdateReconsideration = buildUpdateReconsideration();

module.exports = {
  makeReconsideration,
  makeUpdateReconsideration
};
