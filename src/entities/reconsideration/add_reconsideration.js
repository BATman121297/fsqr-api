module.exports = function buildReconsideration() {
  return async function makeReconsideration(request_info) {
    const {
      for_consideration,
      reason,
      reconsidered_by,
      created_by,
      reconsidered_approved_by,
      noted_by
    } = request_info;

    if (!for_consideration) {
      throw new Error("For consideration must have a value");
    }
    if (!reason) {
      throw new Error("Reason must have a value");
    }
    if (!reconsidered_by) {
      throw new Error("Reconsidered by must have a value");
    }

    if (!created_by) {
      throw new Error("Created by have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
