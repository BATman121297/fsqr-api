module.exports = function buildUpdateReconsideration() {
  return async function makeUpdateReconsideration(request_info) {
    const {
      id,
      for_consideration,
      reason,
      reconsidered_by,
      updated_by,
      reconsidered_approved_by,
      noted_by
    } = request_info;
    if (!id) {
      throw new Error("ID must have a value");
    }
    if (!for_consideration) {
      throw new Error("For consideration must have a value");
    }
    if (!reason) {
      throw new Error("Reason must have a value");
    }
    if (!reconsidered_by) {
      throw new Error("Reconsidered by must have a value");
    }
    if (!reconsidered_approved_by) {
      throw new Error("Reconsideration approved by must have a value");
    }
    if (!noted_by) {
      throw new Error("Noted by must have a value");
    }
    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findReconsiderationById(id);
    // if (!statusExist) {
    //   throw new Error("Data does not exist");
    // }
  };
};
