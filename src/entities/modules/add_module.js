module.exports = function buildModule() {
  return async function makeRole(request_info) {
    const { id, description } = request_info;

    if (id) {
      throw new Error("Module id is auto increment");
    }
    if (!description) {
      throw new Error("Module description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Module description must be a string");
    }

    //check module description if already exists
    // const descriptionExists = await admin_db.findModuleByDescription(
    //   description
    // );

    // if (descriptionExists.rows.length != 0) {
    //   throw new Error("Module already exists");
    // }
  };
};
