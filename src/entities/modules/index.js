const buildModule = require('./add_module');
const buildUpdateModule = require('./edit_module');

const makeModule = buildModule();
const makeUpdateModule = buildUpdateModule();

module.exports = {
  makeModule,
  makeUpdateModule
};