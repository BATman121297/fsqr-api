module.exports = function buildUpdateModule() {
  return async function makeUpdateModule(request_info) {
    const { id, description, status } = request_info;

    if (!id) {
      throw new Error("Module id must have a value");
    }
    if (isNaN(id)) {
      throw new Error("Module id must be a number");
    }

    //check module id if exists
    // const moduleExists = await admin_db.findModuleById(id);
    // if (!moduleExists) {
    //   throw new Error('Module does not exist');
    // }

    if (!description) {
      throw new Error("Module description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Module description must be a string");
    }
    if (!status) {
      throw new Error("Status must have a value");
    }
    if (!isNaN(status)) {
      throw new Error("Status must be a string");
    }
    if (
      status.toLowerCase() !== "active" &&
      status.toLowerCase() !== "inactive"
    ) {
      throw new Error("Invalid status");
    }

    //check module description if already exists
    //  const descriptionExists = await admin_db.findModuleByDescription(
    //   description
    // );

    // if (descriptionExists.rows.length == 0) {
    //   throw new Error('Module does not exists');
    // }
  };
};
