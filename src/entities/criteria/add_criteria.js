module.exports = function buildCriteria() {
  return async function makeCriteria(request_info) {
    const {
      description,
      created_by,
      deduction_per_criteria,
      business_entity,
      item_code
    } = request_info;

    if (!deduction_per_criteria) {
      throw new Error("Deduction per criteria must have a value");
    }
    if (!business_entity) {
      throw new Error("Business entity must have a value");
    }
    if (!item_code) {
      throw new Error("Item Code must have a value");
    }
    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
