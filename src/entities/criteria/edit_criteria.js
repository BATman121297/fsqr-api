module.exports = function buildUpdateCriteria() {
  return async function makeUpdateCriteria(request_info) {
    const {
      id,
      description,
      updated_by,
      deduction_per_criteria,
      business_entity,
      item_code
    } = request_info;
    if (!deduction_per_criteria) {
      throw new Error("Deduction per criteria must have a value");
    }
    if (!business_entity) {
      throw new Error("Business entity must have a value");
    }
    if (!item_code) {
      throw new Error("Item Code must have a value");
    }
    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }

    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findCriteriaById(id);
    // if (!statusExist) {
    //   throw new Error("Criteria does not exist");
    // }

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    // //check status description if already exists
    // const descriptionExists = await transaction_db.findCriteriaByName(
    //   description
    // );
    // if (descriptionExists.rowCount != 0) {
    //   throw new Error("Criteria description already exists");
    // }
  };
};
