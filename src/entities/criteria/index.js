const buildCriteria = require('./add_criteria');
const buildUpdateCriteria = require('./edit_criteria');

const makeCriteria = buildCriteria();
const makeUpdateCriteria = buildUpdateCriteria();

module.exports = {
  makeCriteria,
  makeUpdateCriteria
};
