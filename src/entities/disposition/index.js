const buildDisposition = require('./add_disposition');
const buildUpdateDisposition = require('./edit_disposition');

const makeDisposition = buildDisposition();
const makeUpdateDisposition = buildUpdateDisposition();

module.exports = {
  makeDisposition,
  makeUpdateDisposition
};
