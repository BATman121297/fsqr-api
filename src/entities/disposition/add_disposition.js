module.exports = function buildDisposition() {
  return async function makeDisposition(request_info) {
    const { description, created_by, tag } = request_info;

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }

    if (!tag) {
      throw new Error("Tag must have a value");
    }

    // //check role name if exists
    // const nameExists = await transaction_db.findDispositionByName(description);

    // if (nameExists.rowCount > 0) {
    //   throw new Error("Disposition already exists");
    // }
  };
};
