module.exports = function buildUpdateDisposition() {
  return async function makeUpdateDisposition(request_info) {
    const { id, description, updated_by, tag } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }

    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    if (!tag) {
      throw new Error("Tag must have a value");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findDispositionById(id);
    // if (!statusExist) {
    //   throw new Error("Disposition does not exist");
    // }

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    // //check status description if already exists
    // const descriptionExists = await transaction_db.findDispositionByName(
    //   description
    // );
    // if (descriptionExists.rowCount != 0) {
    //   throw new Error("Disposition description already exists");
    // }
  };
};
