module.exports = function buildUpdateAction() {
  return async function makeUpdateAction(request_info) {
    const { id, module_id, description, status } = request_info;

    if (!id) {
      throw new Error("Action id must have a value");
    }
    if (isNaN(id)) {
      throw new Error("Action id must be a number");
    }

    // const actionExists = await admin_db.findActionById(id);
    // if (!actionExists) {
    //   throw new Error('Action does not exist');
    // }

    if (!module_id) {
      throw new Error("Module id must have a value");
    }
    if (isNaN(module_id)) {
      throw new Error("Module id must be a number");
    }
    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }
    if (!status) {
      throw new Error("Status must have a value");
    }
    if (!isNaN(status)) {
      throw new Error("Status must be a string");
    }
    if (
      status.toLowerCase() !== "active" &&
      status.toLowerCase() !== "inactive"
    ) {
      throw new Error("Invalid status");
    }
  };
};
