module.exports = function buildAction() {
  return async function makeAction(request_info) {
    const { id, module_id, description } = request_info;

    if (id) {
      throw new Error("Action id is auto increment");
    }
    if (!module_id) {
      throw new Error("Module id must have a value");
    }
    if (isNaN(module_id)) {
      throw new Error("Module id must be a number");
    }
    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }
    //check action in module if exists
    // const actionExists = await admin_db.findActionByDescription(
    //   module_id,
    //   description
    // );
    // if (actionExists.rowCount > 0) {
    //   throw new Error("Action already exists");
    // }
  };
};
