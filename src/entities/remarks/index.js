const buildRemarks = require('./add_remarks');
const buildUpdateRemarks = require('./update_remarks');

const makeRemarks = buildRemarks();
const makeUpdateRemarks = buildUpdateRemarks();

module.exports = {
  makeRemarks,
  makeUpdateRemarks
};
