module.exports = function buildRemarks() {
  return async function makeRemarks(request_info) {
    const { name, created_by, tag } = request_info;

    if (!tag) {
      throw new Error("Tag is required.");
    }

    if (!name) {
      throw new Error("Name is required.");
    }
    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
