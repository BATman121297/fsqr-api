module.exports = function buildUpdateRemarks() {
  return async function makeUpdateRemarks(request_info) {
    const { id, name, status, updated_by, tag } = request_info;

    if (!status) {
      throw new Error("Status is required.");
    }
    if (!name) {
      throw new Error("Name is required.");
    }
    if (!tag) {
      throw new Error("Tag is required.");
    }
    if (!id || isNaN(id)) {
      throw new Error("ID is required or is Invalid.");
    }
    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }
  };
};
