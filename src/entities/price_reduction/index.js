const buildPriceReduction = require('./add_price_reduction');
const buildUpdatePriceReduction = require('./edit_price_reduction');

const makePriceReduction = buildPriceReduction();
const makeUpdatePriceReduction = buildUpdatePriceReduction();

module.exports = {
  makePriceReduction,
  makeUpdatePriceReduction
};
