module.exports = function buildPriceReduction() {
  return async function makePriceReduction(request_info) {
    const { remarks, certified_by, created_by } = request_info;

    if (!remarks) {
      throw new Error("Description must have a value");
    }

    if (!certified_by) {
      throw new Error("Certified by must have a value");
    }
    if (isNaN(certified_by)) {
      throw new Error("Certified by must be a number");
    }

    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
