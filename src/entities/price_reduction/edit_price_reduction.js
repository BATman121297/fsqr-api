module.exports = function buildUpdatePriceReduction() {
  return async function makeUpdatePriceReduction(request_info) {
    const { id, remarks, certified_by, updated_by } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }

    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findPriceReductionById(id);
    // if (!statusExist) {
    //   throw new Error("Price Reduction does not exist");
    // }

    if (!remarks) {
      throw new Error("Remarks must have a value");
    }

    if (!certified_by) {
      throw new Error("Certified by must have a value");
    }
    if (isNaN(certified_by)) {
      throw new Error("Certified by must be a number");
    }
  };
};
