module.exports = function buildUpdatedDepartment() {
  return async function makeUpdateDepartment(request_info) {
    const { id, status, name, updated_by } = request_info;

    if (!id) {
      throw new Error("ID is required.");
    }
    if (!name) {
      throw new Error("Name is required.");
    }
    if (!status) {
      throw new Error("Status is required.");
    }
    if (!updated_by) {
      throw new Error("Updated by is required.");
    }

    // const exist = await admin_db.findDepartmentByName(name);

    // if (exist.rowCount != 0) {
    //     throw new Error("Department already exists.")
    // }
  };
};
