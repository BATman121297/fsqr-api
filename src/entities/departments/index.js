const buildDepartment = require('./add_deparment');
const buildUpdateDepartment = require('./edit_department');

const makeDepartment = buildDepartment();
const makeUpdateDepartment = buildUpdateDepartment();

module.exports = {
    makeDepartment,
    makeUpdateDepartment
}