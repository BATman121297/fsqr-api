module.exports = function buildUpdateStatus() {
  return async function makeUpdateStatus(request_info) {
    const { id, name, status } = request_info;

    if (!id) {
      throw new Error("Status id must have a value");
    }
    if (isNaN(id)) {
      throw new Error("Status id must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findStatusById(id);
    // if (!statusExist) {
    //   throw new Error("Status does not exist");
    // }
    if (!status) {
      throw new Error("Status must have a value");
    }
    if (!name) {
      throw new Error("Status name must have a value");
    }
    if (!isNaN(name)) {
      throw new Error("Status name must be a string");
    }

    // //check status name if already exists
    // const nameExists = await transaction_db.findStatusByName(name);
    // if (nameExists.rowCount != 0) {
    //   throw new Error("Status name already exists");
    // }
  };
};
