module.exports = function buildStatus() {
  return async function makeStatus(request_info) {
    const { name } = request_info;

    if (!name) {
      throw new Error("Status name must have a value");
    }
    if (!isNaN(name)) {
      throw new Error("Status name must be a string");
    }

    // //check role name if exists
    // const nameExists = await transaction_db.findStatusByName(name);

    // if (nameExists.rowCount > 0) {
    //   throw new Error('Status already exists');
    // }
  };
};
