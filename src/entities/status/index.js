const buildStatus = require('./add_status');
const buildUpdateStatus = require('./edit_status');

const makeStatus = buildStatus();
const makeUpdateStatus = buildUpdateStatus();

module.exports = {
  makeStatus,
  makeUpdateStatus
};
