const buildItem = require('./add_item');
const buildUpdateItem = require('./edit_item');

const makeItem = buildItem();
const makeUpdateItem = buildUpdateItem();

module.exports = {
  makeItem,
  makeUpdateItem
};
