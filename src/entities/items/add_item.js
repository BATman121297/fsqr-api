module.exports = function buildItem() {
  return async function makeItem(request_info) {
    const { description, created_by } = request_info;

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }

    // //check role name if exists
    // const nameExists = await transaction_db.findItemByName(description);

    // if (nameExists.rowCount > 0) {
    //   throw new Error("Item already exists");
    // }
  };
};
