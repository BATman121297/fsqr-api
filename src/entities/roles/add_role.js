module.exports = function buildRole() {
  return async function makeRole(request_info) {
    const { id, name } = request_info;

    if (id) {
      throw new Error("Role id is auto increment");
    }
    if (!name) {
      throw new Error("Role name must have a value");
    }
    if (!isNaN(name)) {
      throw new Error("Role name must be a string");
    }

    // //check role name if exists
    // const nameExists = await admin_db.findRoleByName(name);

    // if (nameExists.rowCount > 0) {
    //   throw new Error('Role name already exists');
    // }
  };
};
