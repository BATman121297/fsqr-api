module.exports = function buildUpdateRole() {
  return async function makeUpdateRole(request_info) {
    const { id, name, status } = request_info;

    if (!id) {
      throw new Error("Role id must have a value");
    }
    if (isNaN(id)) {
      throw new Error("Role id must be a number");
    }

    // //check if role id exists
    // const roleExist = await admin_db.findRoleById(id);
    // if (!roleExist) {
    //   throw new Error('Role does not exist');
    // }

    if (!name) {
      throw new Error("Role name must have a value");
    }
    if (!isNaN(name)) {
      throw new Error("Role name must be a string");
    }

    if (!status) {
      throw new Error("Status must have a value");
    }
    if (!isNaN(status)) {
      throw new Error("Status must be a string");
    }
    if (
      status.toLowerCase() !== "active" &&
      status.toLowerCase() !== "inactive"
    ) {
      throw new Error("Invalid status");
    }

    //check role name if already exists
    // const nameExists = await admin_db.findRoleByName(name);
    // if (nameExists && nameExists.status === "Active" && status == "Active") {
    //   if (nameExists.id !== id) {
    //     throw new Error("Role name already exists");
    //   }
    // }
  };
};
