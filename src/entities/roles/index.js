const buildRole = require('./add_role');
const buildUpdateRole = require('./edit_role');

const makeRole = buildRole();
const makeUpdateRole = buildUpdateRole();

module.exports = {
  makeRole,
  makeUpdateRole
};
