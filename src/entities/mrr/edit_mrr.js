module.exports = function buildUpdateMRR() {
  return async function makeUpdateMRR(request_info) {
    const {
      id,
      findings,
      specification,
      issued_by,
      noted_by,
      approved_by,
      updated_by
    } = request_info;

    if (!id || isNaN(id)) {
      throw new Error("Invalid ID");
    }
    if (!findings) {
      throw new Error("Description must have a value");
    }
    if (!specification) {
      throw new Error("Specification must have a value");
    }
    if (!issued_by || isNaN(issued_by)) {
      throw new Error("Invalid issued by");
    }
    if (!noted_by || isNaN(noted_by)) {
      throw new Error("Invalid noted by");
    }
    if (!approved_by || isNaN(approved_by)) {
      throw new Error("Invalid approved by");
    }
    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }
  };
};
