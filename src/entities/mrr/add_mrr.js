module.exports = function buildMRR() {
  return async function makeMRR(request_info) {
    const {
      findings,
      specification,
      issued_by,
      noted_by,
      approved_by,
      created_by
    } = request_info;

    // if (!findings) {
    //   throw new Error("Description must have a value");
    // }
    // if (!specification) {
    //   throw new Error("Specification must have a value");
    // }
    if (!issued_by || isNaN(issued_by)) {
      throw new Error("Invalid issued by");
    }
    // if (!noted_by || isNaN(noted_by)) {
    //   throw new Error("Invalid noted by");
    // }
    // if (!approved_by || isNaN(approved_by)) {
    //   throw new Error("Invalid approved by");
    // }
    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
