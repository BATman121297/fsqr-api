const buildMRR = require('./add_mrr');
const buildUpdateMRR = require('./edit_mrr');

const makeMRR = buildMRR();
const makeUpdateMRR = buildUpdateMRR();

module.exports = {
  makeMRR,
  makeUpdateMRR
};
