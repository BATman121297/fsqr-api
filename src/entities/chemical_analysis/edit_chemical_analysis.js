module.exports = function buildUpdatePhysicalAnalysis() {
  return async function makeUpdatePhysicalAnalysis(request_info) {
    const {
      id,
      disposition_id,
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      evaluated_by,
      certified_by,
      updated_by
    } = request_info;

    if (!id && isNaN(id)) {
      throw new Error("Invalid ID.");
    }
    if (reconsideration_id && isNaN(reconsideration_id)) {
      throw new Error("Invalid reconsideration id.");
    }
    if (!disposition_id) {
      throw new Error("Invalid disposition id.");
    }
    if (!crude_protein) {
      throw new Error("Crude protein is required.");
    }
    if (!calcium) {
      throw new Error("Calcium is required.");
    }
    if (!ffa_lauric_acid) {
      throw new Error("FFA Lauric Acid is required.");
    }
    if (!brix) {
      throw new Error("BRIX is required.");
    }
    if (!carbonate_test) {
      throw new Error("Carbonate test is required.");
    }

    if (evaluated_by && isNaN(evaluated_by)) {
      throw new Error("Invalid evaluated_by.");
    }
    if (certified_by && isNaN(certified_by)) {
      throw new Error("Invalid certified by.");
    }
    if (!updated_by || isNaN(updated_by)) {
      throw new Error("Updated by is required and should be an integer.");
    }
  };
};
