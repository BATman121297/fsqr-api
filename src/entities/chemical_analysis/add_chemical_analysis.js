module.exports = function buildPhysicalAnalysis() {
  return async function makePhysicalAnalysis(request_info) {
    const {
      disposition_id,
      reconsideration_id,
      crude_protein,
      calcium,
      ffa_lauric_acid,
      brix,
      carbonate_test,
      evaluated_by,
      certified_by,
      created_by
    } = request_info;

    if (reconsideration_id && isNaN(reconsideration_id)) {
      throw new Error("Invalid reconsideration id.");
    }
    if (!disposition_id) {
      throw new Error("Invalid disposition id.");
    }
    if (!crude_protein) {
      throw new Error("Crude protein is required.");
    }
    if (!calcium) {
      throw new Error("Calcium is required.");
    }
    if (!ffa_lauric_acid) {
      throw new Error("FFA Lauric Acid is required.");
    }
    if (!brix) {
      throw new Error("BRIX is required.");
    }
    if (!carbonate_test) {
      throw new Error("Carbonate test is required.");
    }
    if (!created_by || isNaN(created_by)) {
      throw new Error("Created by is required and should be an integer.");
    }
  };
};
