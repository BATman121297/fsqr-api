const buildChemicalAnalysis = require('./add_chemical_analysis');
const buildUpdateChemicalAnalysis = require('./edit_chemical_analysis');

const makeChemicalAnalysis = buildChemicalAnalysis();
const makeUpdateChemicalAnalysis = buildUpdateChemicalAnalysis();

module.exports = {
  makeChemicalAnalysis,
  makeUpdateChemicalAnalysis
};
