module.exports = function buildUpdatePhysicalAnalysis() {
  return async function makeUpdatePhysicalAnalysis(request_info) {
    const {
      CardCode,
      U_APP_PlateNo,
      U_APP_TruckModel,
      U_APP_Driver
    } = request_info;

    if (!CardCode) {
        throw new Error('CardCode is required.')
    }
    if (!U_APP_PlateNo) {
        throw new Error('U_APP_PlateNo is required.')
    }
    if (!U_APP_TruckModel) {
        throw new Error('U_APP_Driver is required.')
    }
    if (!U_APP_Driver) {
        throw new Error('U_APP_Driver is required.')
    }
  };
};
