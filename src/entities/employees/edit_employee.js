module.exports = function buildUpdateEmployee() {
  return async function updateEmployee(employee_info) {
    const { id, department, role_id, updated_by, status } = employee_info;

    if (!id) {
      throw new Error("Please enter employee to update.");
    }

    if (!role_id) {
      throw new Error("Role id is required.");
    }
    if (!department) {
      throw new Error("Department id is required.");
    }
    if (isNaN(role_id)) {
      throw new Error("Role id must be a number.");
    }

    if (!updated_by) {
      throw new Error("Updated by is required");
    }
    if (!status) {
      throw new Error("Status is required.");
    }
    if (!isNaN(status)) {
      throw new Error("Status must be a string");
    }

    // const employeeExist = await admin_db.getEmployeeById(id);

    // if (employeeExist.rowCount == 0) {
    //   throw new Error(`Employee with ID ${id} does not exist.`);
    // }
  };
};
