module.exports = function buildEmployee() {
  return async function makeEmployee(employee_info) {
    const {
      role_id,
      department,
      app_emp_id,
      employee_id,
      password,
      created_by
    } = employee_info;

    if (!role_id) {
      throw new Error("Role id is required.");
    }
    if (!department) {
      throw new Error("Department id is required.");
    }
    if (isNaN(role_id)) {
      throw new Error("Role id must be a number.");
    }
    if (!app_emp_id) {
      throw new Error("Please select an employee.");
    }
    if (!employee_id) {
      throw new Error("Employee ID is required.");
    }
    if (isNaN(employee_id)) {
      throw new Error("Employee ID must be a number.");
    }
    if (!password) {
      employee_info.password = employee_id.toString();
    }
    if (!created_by) {
      throw new Error("Created by is required");
    }

    // if (!status) {
    //     employee_info.status = 'Active'
    // }
    // if (!isNaN(status)) {
    //     throw new Error('Status must be a string');
    // }

    // const employeeExist = await admin_db.getEmployeeByEmpId(employee_id);

    // if (employeeExist.rowCount > 0) {
    //   throw new Error(`Employee with EmpID ${employee_id} already exist.`);
    // }
  };
};
