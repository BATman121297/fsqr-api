const buildEmployee = require('./add_employee');
const buildUpdateEmployee = require('./edit_employee');
const buildChangePin = require("./change_pin");
const buildCheckPin = require("./check_pincode");
const buildResetPassword = require("./reset_password");

const makeEmployee = buildEmployee();
const makeUpdateEmployee = buildUpdateEmployee();
const makeChangePin = buildChangePin();
const makeCheckPin = buildCheckPin();
const makeResetPassword = buildResetPassword();

module.exports = {
    makeEmployee,
    makeUpdateEmployee,
    makeChangePin,
    makeCheckPin,
    makeResetPassword
}