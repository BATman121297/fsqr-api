module.exports = function buildResetPassword() {
  return async function updatePassword(request_info) {
    const { employee_id, updated_by } = request_info;

    // if (oldpin == null || oldpin == undefined) {
    //   throw new Error("Old pin is required.");
    // }

    if (!employee_id) {
      throw new Error("Employee ID is required.");
    }
    if (!updated_by) {
      throw new Error("Updated by is required.");
    }
  };
};
