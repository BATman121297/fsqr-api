const buildPhysicalAnalysis = require('./add_physical_analysis');
const buildUpdatePhysicalAnalysis = require('./edit_physical_analysis');

const makePhysicalAnalysis = buildPhysicalAnalysis();
const makeUpdatePhysicalAnalysis = buildUpdatePhysicalAnalysis();

module.exports = {
  makePhysicalAnalysis,
  makeUpdatePhysicalAnalysis
};
