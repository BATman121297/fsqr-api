module.exports = function buildUpdatePhysicalAnalysis() {
  return async function makeUpdatePhysicalAnalysis(request_info) {
    const {
      id,
      price_reduction_id,
      reconsideration_id,
      moisture_tally_sheet_id,
      disposition_id,
      type,
      mc_result,
      impurities,
      foreign_material,
      evaluated_by,
      certified_by,
      noted_by,
      updated_by
    } = request_info;

    if (!id || isNaN(id)) {
      throw new Error("Id is required and should be and integer.");
    }

    if (price_reduction_id && isNaN(price_reduction_id)) {
      throw new Error("Invalid price reduction id.");
    }

    // const prid = await transaction_db.findReconsiderationById(reconsideration_id);
    // if (prid.rowCount == 0) {
    //     throw new Error("Price reduction doesnt exist.")
    // }

    // const reconid = await transaction_db.findReconsiderationById(reconsideration_id);
    // if (reconid.rowCount == 0) {
    //     throw new Error("Reconside doesnt exist.")
    // }

    if (reconsideration_id && isNaN(reconsideration_id)) {
      throw new Error("Invalid reconsideration id.");
    }
    if (moisture_tally_sheet_id && isNaN(moisture_tally_sheet_id)) {
      throw new Error("Invalid moisture tally sheet id.");
    }
    if (!disposition_id) {
      throw new Error("Invalid disposition id.");
    }
    if (!type) {
      throw new Error("Type is required.");
    }
    if (!mc_result) {
      throw new Error("MC Result is required.");
    }
    // if (!impurities) {
    //   throw new Error("Impurities is required. Use 'none' if unavailable");
    // }
    // if (!foreign_material) {
    //   throw new Error(
    //     "Foreign Material is required. Use 'none' if unavailable"
    //   );
    // }
    if (evaluated_by && isNaN(evaluated_by)) {
      throw new Error("Invalid evaluated_by.");
    }
    if (certified_by && isNaN(certified_by)) {
      throw new Error("Invalid certified by.");
    }
    if (!noted_by && isNaN(noted_by)) {
      throw new Error("Invalid noted by.");
    }
    if (!updated_by || isNaN(updated_by)) {
      throw new Error("Updated by is required and should be an integer.");
    }
  };
};
