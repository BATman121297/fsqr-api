const buildTransactionStatusLogs = require('./add_transaction_status_logs');
const buildUpdateTransactionStatusLogs = require('./edit_transaction_status_logs');

const makeTransactionStatusLogs = buildTransactionStatusLogs();
const updateTransactionStatusLogs = buildUpdateTransactionStatusLogs();

module.exports = {
    makeTransactionStatusLogs,
    updateTransactionStatusLogs
}