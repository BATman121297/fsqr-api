module.exports = function buildTransactionStatusLogs() {
  return async function makeTransactionStatusLogs(request_info) {
    const {
      transaction_id,
      status_id,
      status,
      start_time,
      created_by
    } = request_info;

    if (!transaction_id) {
      throw new Error("Name is required.");
    }

    if (!status_id && !status) {
      throw new Error("Status ID is required.");
    }

    if (!created_by) {
      throw new Error("Created by is required.");
    }
  };
};
