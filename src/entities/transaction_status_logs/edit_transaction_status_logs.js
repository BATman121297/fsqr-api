module.exports = function buildUpdateTransactionStatusLogs() {
  return async function makeUpdateTransactionStatusLogs(request_info) {
    const {
      id,
      transaction_id,
      status_id,
      end_time,
      updated_by
    } = request_info;

    if (!id) {
      throw new Error("ID is required.");
    }

    if (!transaction_id) {
      throw new Error("Name is required.");
    }

    if (!status_id) {
      throw new Error("Status ID is required.");
    }

    if (!end_time) {
      throw new Error("End time is required.");
    }

    if (!updated_by) {
      throw new Error("Updated by is required.");
    }
  };
};
