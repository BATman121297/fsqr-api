module.exports = function buildDeductionReceipt() {
  return async function makeDeductionReceipt(request_info) {
    const {
      id,
      transaction_id,
      date,
      prepared_by,
      checked_by,
      received_by,
      noted_by,
      total_deductions,
      updated_by
    } = request_info;

    if (!id || isNaN(id)) {
      throw new Error("Invalid ID or Missing ID.");
    }
    if (!transaction_id || isNaN(transaction_id)) {
      throw new Error("Invalid transaction ID.");
    }
    if (!date) {
      throw new Error("Date is required.");
    }
    if (prepared_by && isNaN(prepared_by)) {
      throw new Error("Invalid prepared by or Missing prepared by.");
    }
    if (checked_by && isNaN(checked_by)) {
      throw new Error("Invalid checked by or Missing checked by.");
    }
    if (received_by && isNaN(received_by)) {
      throw new Error("Invalid received by or Missing received by.");
    }
    if (noted_by && isNaN(noted_by)) {
      throw new Error("Invalid noted by or Missing noted by.");
    }
    if (!total_deductions) {
      throw new Error("Total deductions is required.");
    }
    if (!transaction_id || isNaN(transaction_id)) {
      throw new Error("Invalid transaction ID.");
    }
    if (!updated_by) {
      throw new Error("Updated by is required.");
    }
  };
};
