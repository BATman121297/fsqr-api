const buildDeductionReceipt = require('./add_deduction_receipt');
const buildUpdateDeductionReceipt = require('./edit_deduction_receipt');

const makeDeductionReceipt = buildDeductionReceipt();
const makeUpdateDeductionReceipt = buildUpdateDeductionReceipt();

module.exports = {
  makeDeductionReceipt,
  makeUpdateDeductionReceipt
};
