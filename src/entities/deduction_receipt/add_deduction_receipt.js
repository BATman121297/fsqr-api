module.exports = function buildDeductionReceipt() {
  return async function makeDeductionReceipt(request_info) {
    const { transaction_id, total_deductions, created_by } = request_info;

    if (!transaction_id || isNaN(transaction_id)) {
      throw new Error("Invalid transaction ID.");
    }

    // if (!total_deductions) {
    //   throw new Error("Total deductions is required.");
    // }
    if (!transaction_id || isNaN(transaction_id)) {
      throw new Error("Invalid transaction ID.");
    }

    if (!created_by) {
      throw new Error("Created by is required.");
    }
  };
};
