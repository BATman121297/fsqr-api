const buildRawMaterials = require('./add_raw_materials');
const buildUpdateRawMaterials = require('./edit_raw_materials');

const makeRawMaterials = buildRawMaterials();
const makeUpdateRawMaterials = buildUpdateRawMaterials();

module.exports = {
  makeRawMaterials,
  makeUpdateRawMaterials
};
