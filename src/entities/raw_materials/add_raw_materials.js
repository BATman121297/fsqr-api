module.exports = function buildRawMaterials() {
  return async function makeRawMaterials(request_info) {
    const { description, created_by } = request_info;

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    if (!created_by) {
      throw new Error("Description must have a value")``;
    }
    if (isNaN(created_by)) {
      throw new Error("Description must be a number");
    }

    // //check role name if exists
    // const nameExists = await transaction_db.findRawMaterialsByName(description);

    // if (nameExists.rowCount > 0) {
    //   throw new Error("Raw material already exists");
    // }
  };
};
