module.exports = function buildUpdateRawMaterials() {
  return async function makeUpdateRawMaterials(request_info) {
    const { id, description, updated_by } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }

    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findRawMaterialsById(id);
    // if (!statusExist) {
    //   throw new Error("RawMaterials does not exist");
    // }

    if (!description) {
      throw new Error("Description must have a value");
    }
    if (!isNaN(description)) {
      throw new Error("Description must be a string");
    }

    // //check status description if already exists
    // const descriptionExists = await transaction_db.findRawMaterialsByName(
    //   description
    // );
    // if (descriptionExists.rowCount != 0) {
    //   throw new Error("Raw material already exists");
    // }
  };
};
