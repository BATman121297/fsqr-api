const addMicroTransaction = ({}) => {
  return function entity({
    transaction_id,
    created_by,
    created_date,
    created_time,
    data,
  } = {}) {
    if (!transaction_id) {
      throw new Error("Please enter transaction.");
    }
    if (data.length == 0) {
      throw new Error(
        "Please enter package number, expiry date, quantity or remarks."
      );
    }
    if (!created_by) {
      throw new Error("Please enter created by.");
    }
    if (!created_date) {
      throw new Error("Please enter created date.");
    }
    if (!created_time) {
      throw new Error("Please enter created time.");
    }
  };
};

module.exports = addMicroTransaction;
