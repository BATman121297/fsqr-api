const buildDriver = require('./add_driver');
const buildUpdateDriver = require('./edit_driver');

const makeDriver = buildDriver();
const makeUpdateDriver = buildUpdateDriver();

module.exports = {
  makeDriver,
  makeUpdateDriver
};
