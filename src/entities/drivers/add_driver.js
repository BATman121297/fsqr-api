module.exports = function buildDriver() {
  return async function makeDriver(request_info) {
    const { firstname, middlename, lastname, name_extension } = request_info;

    if (!firstname) {
      throw new Error("First name must have a value");
    }
    if (!isNaN(firstname)) {
      throw new Error("Last name must be a string");
    }

    if (!lastname) {
      throw new Error("Last name must have a value");
    }
    if (!isNaN(lastname)) {
      throw new Error("Last name must be a string");
    }

    if (!middlename) {
      request_info.middlename = "";
    }
    if (!isNaN(middlename)) {
      throw new Error("Middle name must be a string");
    }

    if (!name_extension) {
      request_info.name_extension = "";
    }
    if (!isNaN(name_extension)) {
      throw new Error("Name extension must be a string");
    }
  };
};
