module.exports = function buildUpdateDriver() {
    return async function makeUpdateDriver(request_info, { transaction_db }) {
      const { id, firstname, middlename, lastname, name_extension, updated_by } = request_info;
  
      if (!id) {
        throw new Error("Driver id must have a value");
      }
      if (isNaN(id)) {
        throw new Error("Driver id must be a number");
      }
  
      //check if status id exists
      const driverExist = await transaction_db.findDriverById(id);
      if (!driverExist) {
        throw new Error("Driver does not exist");
      }
      
      if (!firstname) {
        throw new Error("First name must have a value");
      }
      if (!isNaN(firstname)) {
        throw new Error("First name must be a string");
      }

      if (!middlename) {
        throw new Error("Middle name must have a value");
      }
      if (!isNaN(middlename)) {
        throw new Error("Middle name must be a string");
      }

      if (!lastname) {
        throw new Error("Last name must have a value");
      }
      if (!isNaN(lastname)) {
        throw new Error("Last name must be a string");
      }
      
      if (name_extension == undefined) {
        throw new Error("Name extension must have a value");
      }

      if (!updated_by) {
        throw new Error("Updated by must have a value");
      }
     
    };
  };
  