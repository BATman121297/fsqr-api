module.exports = function buildUpdateItem() {
  return async function makeUpdateItem(request_info) {
    const {
      id,
      item_id,
      business_entity_id,
      raw_material_id,
      updated_by
    } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (isNaN(id)) {
      throw new Error("ID must be a number");
    }
    if (!item_id) {
      throw new Error("Item ID must have a value");
    }
    if (isNaN(item_id)) {
      throw new Error("Item ID be a number");
    }

    if (!business_entity_id) {
      throw new Error("Business Entity ID must have a value");
    }
    if (isNaN(business_entity_id)) {
      throw new Error("Business Entity ID must be a number");
    }

    if (!raw_material_id) {
      throw new Error("Raw material ID must have a value");
    }
    if (isNaN(raw_material_id)) {
      throw new Error("Raw material ID must be a number");
    }

    if (!updated_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
