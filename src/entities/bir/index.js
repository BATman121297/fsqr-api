const buildBIR = require('./add_bir');
const buildUpdateBIR = require('./edit_bir');

const makeBIR = buildBIR();
const makeUpdateBIR = buildUpdateBIR();

module.exports = {
  makeBIR,
  makeUpdateBIR
};
