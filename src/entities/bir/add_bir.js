module.exports = function buildBIR() {
  return async function makeBIR(request_info) {
    const {
      item_id,
      business_entity_id,
      raw_material_id,
      created_by
    } = request_info;

    if (!item_id) {
      throw new Error("Item ID must have a value");
    }
    if (isNaN(item_id)) {
      throw new Error("Item ID must be a number");
    }

    if (!business_entity_id) {
      throw new Error("Business Entity ID must have a value");
    }
    if (isNaN(business_entity_id)) {
      throw new Error("Business Entity ID must be a number");
    }

    if (!raw_material_id) {
      throw new Error("Raw material ID must have a value");
    }
    if (isNaN(raw_material_id)) {
      throw new Error("Raw material ID must be a number");
    }

    if (!created_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
