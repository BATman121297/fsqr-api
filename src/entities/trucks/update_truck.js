module.exports = function buildUpdateTruck() {
    return async function updateTruck(request_info) {
      const { id, truck_type_id, plate_number, updated_by } = request_info;
  
      if (!id) {
          throw new Error("ID is required.")
      }
      if (!truck_type_id) {
          throw new Error("Truck type ID is required.")
      }
      if (!plate_number) {
          throw new Error("Plate number is required.")
      }
      if (!updated_by) {
          throw new Error("Created by is required.")
      }
    
    };
  };
  