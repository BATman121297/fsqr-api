module.exports = function buildUpdateTruckType() {
    return async function updateTruckType(request_info) {
      const { id, truck_type, truck_model, truck_size, updated_by } = request_info;
  
      if (!id) {
          throw new Error("ID is required.")
      }
      if (!truck_type) {
          throw new Error("Truck type ID is required.")
      }
      if (!truck_model) {
          throw new Error("Plate number is required.")
      }
      if (!truck_size) {
          throw new Error("Truck size by is required.")
      }
      if (!updated_by) {
          throw new Error("Updated by is required.")
      }
    
    };
  };
  