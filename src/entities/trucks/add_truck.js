module.exports = function buildTruck() {
  return async function makeTruck(request_info) {
    const { truck_type_id, plate_number, created_by } = request_info;

    if (!truck_type_id) {
        throw new Error("Truck type ID is required.")
    }
    if (!plate_number) {
        throw new Error("Plate number is required.")
    }
    if (!created_by) {
        throw new Error("Created by is required.")
    }
  
  };
};
