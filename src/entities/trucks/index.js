const buildTruck = require('./add_truck');
const buildUpdateTruck = require('./update_truck');
const buildTruckType = require('./add_trucktype');
const buildUpdateTruckType = require('./update_trucktype');

const makeTruck = buildTruck();
const updateTruck = buildUpdateTruck();
const makeTrucktype = buildTruckType();
const updateTrucktype = buildUpdateTruckType();

module.exports = {
    makeTruck,
    updateTruck,
    makeTrucktype,
    updateTrucktype
}