module.exports = function buildTruckType() {
    return async function makeTruckType(request_info) {
      const { truck_type, truck_model, truck_size, created_by } = request_info;
  
      if (!truck_type) {
          throw new Error("Truck type is required.")
      }
      if (!truck_model) {
          throw new Error("Truck model is required.")
      }
      if (!truck_size) {
          throw new Error("Truck size is required.")
      }
      if (!created_by) {
          throw new Error("Created by is required.")
      }
    
    };
  };
  