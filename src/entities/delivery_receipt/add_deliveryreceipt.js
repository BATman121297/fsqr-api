module.exports = function buildDeliveryReceipt() {
  return async function makeDeliveryReceipt(request_info) {
    const {
      quantity,
      unit,
      description_of_articles,
      created_by
    } = request_info;

    

    if (!quantity) {
      throw new Error("Quantity is required.");
    }
    if (!unit) {
      throw new Error("Unit is required.");
    }
    if (!description_of_articles) {
      throw new Error("Description of Articles is required.");
    }
    if (!created_by) {
        throw new Error("Created by is required.")
    }
  };
};
