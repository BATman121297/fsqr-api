module.exports = function buildUpdateDeliveryReceipt() {
  return async function updatedDeliveryReceipt(request_info) {
    const {
      id,
      quantity,
      unit,
      description_of_articles,
      updated_by
    } = request_info;

    if (!id) {
      throw new Error("ID is required.");
    }
    if (!quantity) {
      throw new Error("Quantity is required.");
    }
    if (!unit) {
      throw new Error("Unit is required.");
    }
    if (!description_of_articles) {
      throw new Error("Description of Articles is required.");
    }
    if (!updated_by) {
      throw new Error("Created by is required.");
    }
  };
};
