const buildDeliveryReceipt = require('./add_deliveryreceipt');
const buildUpdateDeliveryReceipt = require('./update_deliveryreceipt');

const makeDeliveryReceipt = buildDeliveryReceipt();
const updateDeliveryReceipt = buildUpdateDeliveryReceipt();

module.exports = {
    makeDeliveryReceipt,
    updateDeliveryReceipt
}