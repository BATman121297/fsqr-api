module.exports = function buildTransactionType() {
  return async function makeTransactionType(request_info) {
    const { name, created_by } = request_info;

    if (!name) {
      throw new Error("Name is required.");
    }
    if (!created_by) {
      throw new Error("Created by is required.");
    }
  };
};
