const buildTransactionType = require('./add_transaction_type');
const buildUpdateTransactionType = require('./update_transaction_type');

const makeTransactionType = buildTransactionType();
const updateTransactionType = buildUpdateTransactionType();

module.exports = {
    makeTransactionType,
    updateTransactionType
}