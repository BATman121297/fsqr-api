module.exports = function buildUpdateTransactionType() {
  return async function updateTransactionType(request_info) {
    const { id, name, updated_by } = request_info;

    if (!id) {
      throw new Error("ID is required.");
    }
    if (!name) {
      throw new Error("Name is required.");
    }
    if (!updated_by) {
      throw new Error("Created by is required.");
    }
  };
};
