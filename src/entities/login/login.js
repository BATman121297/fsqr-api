module.exports = function buildLogin() {
  return function makeLogin({ id, password } = {}) {

    if (id == "" || password == "") {
      throw new Error("Empty Fields!")
    }
    if (!id) {
      throw new Error("Employee id must have a value");
    }
    if (isNaN(id)) {
      throw new Error("Employee id must be a number");
    }
    if (!password) {
      throw new Error("Password must have a value");
    }

    return Object.freeze({
      getEmployeeId: () => id,
      getPassword: () => password
    });
  };
};
