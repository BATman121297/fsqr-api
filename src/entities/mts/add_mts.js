module.exports = function buildMTS() {
  return async function makeMTS(request_info) {
    const {
      moisture_trial_1,
      moisture_trial_2,
      moisture_trial_3,
      average,
      analysed_by,
      created_by
    } = request_info;

    if (!moisture_trial_1) {
      throw new Error("Moisture Trial 1 must have a value");
    }
    if (!moisture_trial_2) {
      throw new Error("Moisture Trial 2 must have a value");
    }
    if (!moisture_trial_3) {
      throw new Error("Moisture Trial 3 must have a value");
    }
    if (!average) {
      throw new Error("Average must have a value");
    }

    if (!analysed_by) {
      throw new Error("Analysed by must have a value");
    }
    if (!created_by) {
      throw new Error("Created by have a value");
    }
    if (isNaN(created_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
