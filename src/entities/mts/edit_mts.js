module.exports = function buildUpdateMTS() {
  return async function makeUpdateMTS(request_info) {
    const {
      id,
      moisture_trial_1,
      moisture_trial_2,
      moisture_trial_3,
      average,
      analysed_by,
      updated_by
    } = request_info;

    if (!id) {
      throw new Error("ID must have a value");
    }
    if (!moisture_trial_1) {
      throw new Error("Moisture Trial 1 must have a value");
    }
    if (!moisture_trial_2) {
      throw new Error("Moisture Trial 2 must have a value");
    }
    if (!moisture_trial_3) {
      throw new Error("Moisture Trial 3 must have a value");
    }
    if (!average) {
      throw new Error("Average must have a value");
    }

    if (!analysed_by) {
      throw new Error("Analysed by must have a value");
    }

    if (!updated_by) {
      throw new Error("Updated by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Updated by must be a number");
    }

    // //check if status id exists
    // const statusExist = await transaction_db.findMTSById(id);
    // if (!statusExist) {
    //   throw new Error("Data does not exist");
    // }
  };
};
