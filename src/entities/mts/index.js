const buildMTS = require('./add_mts');
const buildUpdateMTS = require('./edit_mts');

const makeMTS = buildMTS();
const makeUpdateMTS = buildUpdateMTS();

module.exports = {
  makeMTS,
  makeUpdateMTS
};
