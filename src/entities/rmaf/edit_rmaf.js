module.exports = function buildUpdateRMAF() {
  return async function makeUpdateRMAF(request_info) {
    const {
      id,
      physical_analysis_id,
      chemical_analysis_id,
      material_rejection_report_id,
      updated_by
    } = request_info;

    if (chemical_analysis_id && isNaN(chemical_analysis_id)) {
      throw new Error("Invalid chemical analysis ID.");
    }
    if (material_rejection_report_id && isNaN(material_rejection_report_id)) {
      throw new Error("Material rejection report ID is invalid.");
    }

    if (!physical_analysis_id) {
      throw new Error("Physical Analysis ID is required.");
    }

    if (!id) {
      throw new Error("ID is required.");
    }

    if (!updated_by) {
      throw new Error("Created by must have a value");
    }
    if (isNaN(updated_by)) {
      throw new Error("Created by must be a number");
    }
  };
};
