module.exports = function buildRMAF() {
  return async function makeRMAF(request_info) {
    const {
      physical_analysis_id,
      chemical_analysis_id,
      date,
      material_rejection_report_id
    } = request_info;

    if (chemical_analysis_id && isNaN(chemical_analysis_id)) {
      throw new Error("Invalid chemical analysis ID.");
    }
    if (material_rejection_report_id && isNaN(material_rejection_report_id)) {
      throw new Error("Material rejection report ID is invalid.");
    }

    if (physical_analysis_id && isNaN(physical_analysis_id)) {
      throw new Error("Physical Analysis ID is invalid.");
    }
    if (!date) {
      throw new Error("Date is required.");
    }
    // if (!created_by) {
    //   throw new Error("Created by must have a value");
    // }
    // if (isNaN(created_by)) {
    //   throw new Error("Created by must be a number");
    // }
  };
};
