const buildRMAF = require('./add_rmaf');
const buildUpdateRMAF = require('./edit_rmaf');

const makeRMAF = buildRMAF();
const makeUpdateRMAF = buildUpdateRMAF();

module.exports = {
  makeRMAF,
  makeUpdateRMAF
};
