const axios = require("axios");
const https = require("https");
const axiosRequest = async ({ method, link, headers, data }) => {
  return await axios({
    method: method,
    url: `https://18.136.35.41:50000${link}`,
    headers,
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
    data
  });
};
const recursiveRequest = async (SessionId, link, nextLink) => {
  const arr = [];
  const result = await axiosRequest({
    method: "GET",
    link: nextLink ? nextLink : link,
    headers: {
      "Content-Type": "application/json",
      Cookie: `B1SESSION=${SessionId};`
    }
  }).catch(err => {
    if (err.response.data.error)
      throw new Error(err.response.data.error.message.value);
    else if (err.response.status === 502) {
      throw new Error(err.response.statusText);
    }
    else throw new Error(err.response.data);
  });
  arr.push(...result.data.value);
  if (result.data["odata.nextLink"]) {
    const validLink = result.data["odata.nextLink"].search("/b1s/v1");
    const res = await recursiveRequest(
      SessionId,
      validLink >= 0
        ? result.data["odata.nextLink"]
        : `/b1s/v1/${result.data["odata.nextLink"]}`
    );
    arr.push(...res);
  }
  return arr;
};
module.exports = { axiosRequest, recursiveRequest };