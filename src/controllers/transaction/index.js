const verifyToken = require("../../token/").verifyToken;
const verifyTokenUpload = require("../../token/").verifyTokenUpload;

// Use cases
const {
  getSuppliers,
  getPOSuppliers,
  getPOs,
  postGRPO,

  getnetweight,

  addDeliveryReceipt,
  getDeliverReceipts,
  updateDeliveryReceipt,

  addTruck,
  addTrucktype,
  getTrucks,
  getTrucktypes,
  updateTruck,
  updateTrucktype,

  addTransactionType,
  updateTransactionType,
  getTransactionTypes,

  addStatus,
  updateStatus,
  getStatus,

  addDriver,
  updateDriver,
  getDriver,

  addBusinessEntity,
  updateBusinessEntity,
  getBusinessEntity,

  addItem,
  updateItem,
  getItem,

  addRawMaterial,
  updateRawMaterial,
  getRawMaterial,

  addBIR,
  updateBIR,
  getBIR,
  getItemsBIR,

  addCriteria,
  updateCriteria,
  getCriteria,

  addDeductionCriteria,
  updateDeductionCriteria,
  getDeductionCriteria,

  addPriceReduction,
  updatePriceReduction,
  getPriceReduction,

  addMTS,
  updateMTS,
  getMTS,

  addReconsideration,
  updateReconsideration,
  getReconsideration,

  addDisposition,
  updateDisposition,
  getDisposition,

  addPhysicalAnalysis,
  updatePhysicalAnalysis,
  getPhysicalAnalysis,

  addChemicalAnalysis,
  updateChemicalAnalysis,
  getChemicalAnalysis,

  addMRR,
  updateMRR,
  getMRR,

  getQCCN,
  addRMAF,
  updateRMAF,
  getRMAF,

  addTransaction,
  updateTransaction,
  getTransaction,
  getCueNumber,
  findTransaction,

  addRejectionReceipt,
  updateRejectionReceipt,
  getRejectionReceipt,

  addDeductionReceipt,
  updateDeductionReceipt,
  getDeductionReceipt,

  addTransactionStatusLogs,
  updateTransactionStatusLogs,
  getTransactionStatusLogs,
  findTransactionStatusLogs,

  addRemarks,
  updateRemarks,
  getRemarks,
  findRemarks,

  getCheckPin,

  getEntry,
  getExit,
  updateEntry,
  updateExit,
  findQR,

  getWarehouse,
  removeWarehouse
} = require("../../use-cases/transaction");

// Truckscale
const makeNetWeight = require("./truckscale/get_net_weight");

const _getNetWeight = makeNetWeight({ getnetweight }, { verifyToken });

// SAP - suppliers
const makeListSuppliers = require("./suppliers/get_suppliers");

const _getSuppliers = makeListSuppliers({ getSuppliers }, { verifyToken });

// SAP = po suppliers
const makeListPOSuppliers = require("./po/get_posupplier");
const makeListPOs = require("./po/get_po");

const _getPOSuppliers = makeListPOSuppliers(
  { getPOSuppliers },
  { verifyToken }
);

const _getPOs = makeListPOs(
  { getPOs },
  { verifyToken }
);

const makeEndorseGRPO = require("./po/endorse_grpo");

const _postGRPO = makeEndorseGRPO({ postGRPO }, { verifyToken });

// Employee
const makeCheckPin = require("./employee/check_pin");

const checkPincode = makeCheckPin({ getCheckPin }, { verifyToken });

// Delivery Receipts
const makeListDeliveryReceipts = require("./delivery_receipt/get_deliveryreceipt");
const makeDeliveryReceipt = require("./delivery_receipt/post_deliveryreceipt");
const makeUpdateDeliveryReceipt = require("./delivery_receipt/put_deliveryreceipt");

const getDReceipts = makeListDeliveryReceipts(
  { getDeliverReceipts },
  { verifyToken }
);
const postDeliveryReceipt = makeDeliveryReceipt(
  { addDeliveryReceipt },
  { verifyToken }
);
const putDeliveryReceipt = makeUpdateDeliveryReceipt(
  { updateDeliveryReceipt },
  { verifyToken }
);

// Trucks
const makeListTrucks = require("./trucks/get_trucks");
const makeListTrucktypes = require("./trucks/get_trucktype");
const makeTruck = require("./trucks/post_truck");
const makeTrucktype = require("./trucks/post_trucktype");
const makeUpdateTruck = require("./trucks/put_truck");
const makeUpdateTrucktype = require("./trucks/put_trucktype");

const getTruckInfos = makeListTrucks({ getTrucks }, { verifyToken });
const getTruckTypes = makeListTrucktypes({ getTrucktypes }, { verifyToken });
const postTruckInfos = makeTruck({ addTruck }, { verifyToken });
const postTruckTypes = makeTrucktype({ addTrucktype }, { verifyToken });
const putTruck = makeUpdateTruck({ updateTruck }, { verifyToken });
const putTrucktype = makeUpdateTrucktype({ updateTrucktype }, { verifyToken });

// Transaction type
const makeTransactionType = require("./transaction_type/post_transaction_type");
const makeListTransactionTypes = require("./transaction_type/get_transaction_type");
const makeUpdateTransactionType = require("./transaction_type/put_transaction_type");

const postTransactionType = makeTransactionType(
  { addTransactionType },
  { verifyToken }
);
const putTransactionType = makeUpdateTransactionType(
  { updateTransactionType },
  { verifyToken }
);
const getTransactionType = makeListTransactionTypes(
  { getTransactionTypes },
  { verifyToken }
);

// Status
const makeStatus = require("./status/post_status");
const makeListStatus = require("./status/get_status");
const makeUpdateStatus = require("./status/put_status");

const postStatus = makeStatus({ addStatus }, { verifyToken });
const putStatus = makeUpdateStatus({ updateStatus }, { verifyToken });
const getListStatus = makeListStatus({ getStatus }, { verifyToken });

// Driver
const makeDriver = require("./driver/post_driver");
const makeListDriver = require("./driver/get_driver");
const makeUpdateDriver = require("./driver/put_driver");

const postDriver = makeDriver({ addDriver }, { verifyToken });
const putDriver = makeUpdateDriver({ updateDriver }, { verifyToken });
const getListDriver = makeListDriver({ getDriver }, { verifyToken });

// BusinessEntity
const makeBusinessEntity = require("./business_entity/post_business_entity");
const makeListBusinessEntity = require("./business_entity/get_business_entity");
const makeUpdateBusinessEntity = require("./business_entity/put_business_entity");

const makeListWarehouse = require("./business_entity/get_warehouse");
const makeDeleteWarehouse = require("./business_entity/delete_warehouse");

const postBusinessEntity = makeBusinessEntity(
  { addBusinessEntity },
  { verifyToken }
);
const putBusinessEntity = makeUpdateBusinessEntity(
  { updateBusinessEntity },
  { verifyToken }
);
const getListBusinessEntity = makeListBusinessEntity(
  { getBusinessEntity },
  { verifyToken }
);

const getListWarehouse = makeListWarehouse({ getWarehouse }, { verifyToken });
const deleteWarehouse = makeDeleteWarehouse({ removeWarehouse }, { verifyToken });

// Item
const makeItem = require("./items/post_item");
const makeListItem = require("./items/get_items");
const makeUpdateItem = require("./items/put_item");

const postItem = makeItem({ addItem }, { verifyToken });
const putItem = makeUpdateItem({ updateItem }, { verifyToken });
const getListItem = makeListItem({ getItem }, { verifyToken });

// Raw Material
const makeRawMaterial = require("./raw_materials/post_raw_material");
const makeListRawMaterial = require("./raw_materials/get_raw_materials");
const makeUpdateRawMaterial = require("./raw_materials/put_raw_material");

const postRawMaterial = makeRawMaterial({ addRawMaterial }, { verifyToken });
const putRawMaterial = makeUpdateRawMaterial(
  { updateRawMaterial },
  { verifyToken }
);
const getListRawMaterial = makeListRawMaterial(
  { getRawMaterial },
  { verifyToken }
);

// BIR
const makeBIR = require("./bir/post_bir");
const makeListBIR = require("./bir/get_bir");
const makeUpdateBIR = require("./bir/put_bir");
const makeListItemsBIR = require("./bir/get_items_bir");

const postBIR = makeBIR({ addBIR }, { verifyToken });
const putBIR = makeUpdateBIR({ updateBIR }, { verifyToken });
const getListBIR = makeListBIR({ getBIR }, { verifyToken });
const getListItemsBIR = makeListItemsBIR({ getItemsBIR }, { verifyToken });

// Criteria
const makeCriteria = require("./criteria/post_criteria");
const makeListCriteria = require("./criteria/get_criteria");
const makeUpdateCriteria = require("./criteria/put_criteria");

const postCriteria = makeCriteria({ addCriteria }, { verifyToken });
const putCriteria = makeUpdateCriteria({ updateCriteria }, { verifyToken });
const getListCriteria = makeListCriteria({ getCriteria }, { verifyToken });

// DeductionCriteria
const makeDeductionCriteria = require("./deduction_criteria/post_deduction_criteria");
const makeListDeductionCriteria = require("./deduction_criteria/get_deduction_criteria");
const makeUpdateDeductionCriteria = require("./deduction_criteria/put_deduction_criteria");

const postDeductionCriteria = makeDeductionCriteria(
  { addDeductionCriteria },
  { verifyToken }
);
const putDeductionCriteria = makeUpdateDeductionCriteria(
  { updateDeductionCriteria },
  { verifyToken }
);
const getListDeductionCriteria = makeListDeductionCriteria(
  { getDeductionCriteria },
  { verifyToken }
);

// PriceReduction
const makePriceReduction = require("./price_reduction/post_price_reduction");
const makeListPriceReduction = require("./price_reduction/get_price_reduction");
const makeUpdatePriceReduction = require("./price_reduction/put_price_reduction");

const postPriceReduction = makePriceReduction(
  { addPriceReduction },
  { verifyToken }
);
const putPriceReduction = makeUpdatePriceReduction(
  { updatePriceReduction },
  { verifyToken }
);
const getListPriceReduction = makeListPriceReduction(
  { getPriceReduction },
  { verifyToken }
);

// MTS
const makeMTS = require("./mts/post_mts");
const makeListMTS = require("./mts/get_mts");
const makeUpdateMTS = require("./mts/put_mts");

const postMTS = makeMTS({ addMTS }, { verifyToken });
const putMTS = makeUpdateMTS({ updateMTS }, { verifyToken });
const getListMTS = makeListMTS({ getMTS }, { verifyToken });

// Reconsideration
const makeReconsideration = require("./reconsideration/post_recon");
const makeListReconsideration = require("./reconsideration/get_recon");
const makeUpdateReconsideration = require("./reconsideration/put_recon");

const postReconsideration = makeReconsideration(
  { addReconsideration },
  { verifyToken }
);
const putReconsideration = makeUpdateReconsideration(
  { updateReconsideration },
  { verifyToken }
);
const getListReconsideration = makeListReconsideration(
  { getReconsideration },
  { verifyToken }
);

// Disposition
const makeDisposition = require("./disposition/post_disposition");
const makeListDisposition = require("./disposition/get_disposition");
const makeUpdateDisposition = require("./disposition/put_disposition");

const postDisposition = makeDisposition({ addDisposition }, { verifyToken });
const putDisposition = makeUpdateDisposition(
  { updateDisposition },
  { verifyToken }
);
const getListDisposition = makeListDisposition(
  { getDisposition },
  { verifyToken }
);

// PhysicalAnalysis
const makePhysicalAnalysis = require("./physical_analysis/post_physical_analysis");
const makeListPhysicalAnalysis = require("./physical_analysis/get_physical_analysis");
const makeUpdatePhysicalAnalysis = require("./physical_analysis/put_physical_analysis");

const postPhysicalAnalysis = makePhysicalAnalysis(
  { addPhysicalAnalysis },
  { verifyToken }
);
const putPhysicalAnalysis = makeUpdatePhysicalAnalysis(
  { updatePhysicalAnalysis },
  { verifyToken }
);
const getListPhysicalAnalysis = makeListPhysicalAnalysis(
  { getPhysicalAnalysis },
  { verifyToken }
);

// Chemical Analysis
const makeChemicalAnalysis = require("./chemical_analysis/post_chemical_analysis");
const makeListChemicalAnalysis = require("./chemical_analysis/get_chemical_analysis");
const makeUpdateChemicalAnalysis = require("./chemical_analysis/put_chemical_analysis");

const postChemicalAnalysis = makeChemicalAnalysis(
  { addChemicalAnalysis },
  { verifyToken }
);
const putChemicalAnalysis = makeUpdateChemicalAnalysis(
  { updateChemicalAnalysis },
  { verifyToken }
);
const getListChemicalAnalysis = makeListChemicalAnalysis(
  { getChemicalAnalysis },
  { verifyToken }
);

// MRR
const makeMRR = require("./mrr/post_mrr");
const makeListMRR = require("./mrr/get_mrr");
const makeUpdateMRR = require("./mrr/put_mrr");

const postMRR = makeMRR({ addMRR }, { verifyToken });
const putMRR = makeUpdateMRR({ updateMRR }, { verifyToken });
const getListMRR = makeListMRR({ getMRR }, { verifyToken });

// QCCN
const makeQCCN = require("./rmaf/getqccn");
const makeRMAF = require("./rmaf/post_rmaf");
const makeListRMAF = require("./rmaf/get_rmaf");
const makeUpdateRMAF = require("./rmaf/put_rmaf");

const postQCCN = makeQCCN({ getQCCN });
const postRMAF = makeRMAF({ addRMAF }, { verifyToken });
const putRMAF = makeUpdateRMAF({ updateRMAF }, { verifyToken });
const getListRMAF = makeListRMAF({ getRMAF }, { verifyToken });

// Transaction
const makeTransaction = require("./transaction/post_transaction");
const makeListTransaction = require("./transaction/get_transaction");
const makeUpdateTransaction = require("./transaction/put_transaction");
const makeCueNumber = require("./transaction/get_cue_number");
const makeFindTransaction = require("./transaction/find_transaction");
const makeEntry = require("./transaction/get_entry");
const makeExit = require("./transaction/get_exit");
const makeUpdateEntry = require("./transaction/put_entry");
const makeUpdateExit = require("./transaction/put_exit");
const makeFindQR = require("./transaction/find_qr");

const postTransaction = makeTransaction({ addTransaction }, { verifyToken });
const putTransaction = makeUpdateTransaction(
  { updateTransaction },
  { verifyToken }
);
const getListTransaction = makeListTransaction(
  { getTransaction },
  { verifyToken }
);
const getTransactionCueNumber = makeCueNumber(
  { getCueNumber },
  { verifyToken }
);
const getFindTransaction = makeFindTransaction(
  { findTransaction },
  { verifyToken }
);
const getTransactionEntry = makeEntry({ getEntry }, { verifyToken });
const getTransactionExit = makeExit({ getExit }, { verifyToken });

const putEntry = makeUpdateEntry({ updateEntry }, { verifyToken });
const putExit = makeUpdateExit({ updateExit }, { verifyToken });

const getFindQR = makeFindQR({ findQR }, { verifyToken });

// Signature
const makeUploadSignature = require("./transaction/upload_signature");

const uploadSignature = makeUploadSignature({ verifyTokenUpload });

// Rejection Receipt
const makeRejectionReceipt = require("./rejection_receipt/post_rejection_receipt");
const makeListRejectionReceipt = require("./rejection_receipt/get_rejection_receipt");
const makeUpdateRejectionReceipt = require("./rejection_receipt/put_rejection_receipt");

const postRejectionReceipt = makeRejectionReceipt(
  { addRejectionReceipt },
  { verifyToken }
);
const putRejectionReceipt = makeUpdateRejectionReceipt(
  { updateRejectionReceipt },
  { verifyToken }
);
const getListRejectionReceipt = makeListRejectionReceipt(
  { getRejectionReceipt },
  { verifyToken }
);

// Deduction Receipt
const makeDeductionReceipt = require("./deduction_receipt/post_deduction_receipt");
const makeListDeductionReceipt = require("./deduction_receipt/get_deduction_receipt");
const makeUpdateDeductionReceipt = require("./deduction_receipt/put_deduction_receipt");

const postDeductionReceipt = makeDeductionReceipt(
  { addDeductionReceipt },
  { verifyToken }
);
const putDeductionReceipt = makeUpdateDeductionReceipt(
  { updateDeductionReceipt },
  { verifyToken }
);
const getListDeductionReceipt = makeListDeductionReceipt(
  { getDeductionReceipt },
  { verifyToken }
);

// Transaction status logs
const makeTransactionStatusLogs = require("./transaction_status_logs/post_transaction_status_logs");
const makeListTransactionStatusLogs = require("./transaction_status_logs/get_transaction_status_logs");
const makeUpdateTransactionStatusLogs = require("./transaction_status_logs/put_transaction_status_logs");
const makeFindTransactionStatusLogs = require("./transaction_status_logs/find_transaction_status_logs");

const postTransactionStatusLogs = makeTransactionStatusLogs(
  { addTransactionStatusLogs },
  { verifyToken }
);
const putTransactionStatusLogs = makeUpdateTransactionStatusLogs(
  { updateTransactionStatusLogs },
  { verifyToken }
);
const getListTransactionStatusLogs = makeListTransactionStatusLogs(
  { getTransactionStatusLogs },
  { verifyToken }
);
const getFindTransactionStatusLogs = makeFindTransactionStatusLogs(
  { findTransactionStatusLogs },
  { verifyToken }
);

// Remarks
const makeRemarks = require("./remarks/post_remarks");
const makeListRemarks = require("./remarks/get_remarks");
const makeUpdateRemarks = require("./remarks/put_remarks");
const makeFindRemarks = require("./remarks/find_remarks");

const postRemarks = makeRemarks({ addRemarks }, { verifyToken });
const putRemarks = makeUpdateRemarks({ updateRemarks }, { verifyToken });
const getListRemarks = makeListRemarks({ getRemarks }, { verifyToken });
const getFindRemarks = makeFindRemarks({ findRemarks }, { verifyToken });

const transactionController = Object.freeze({
  // Truck scale
  _getNetWeight,

  // SAP - po supplier
  _getPOSuppliers,
  _getPOs,

  // SAP - suppliers
  _getSuppliers,
  _postGRPO,

  checkPincode,

  // Remarks
  postRemarks,
  putRemarks,
  getListRemarks,
  getFindRemarks,

  // Transacition status logs
  postTransactionStatusLogs,
  putTransactionStatusLogs,
  getListTransactionStatusLogs,
  getFindTransactionStatusLogs,

  // Delivery receipts
  getDReceipts,
  postDeliveryReceipt,
  putDeliveryReceipt,

  // Trucks
  getTruckInfos,
  getTruckTypes,
  postTruckInfos,
  postTruckTypes,
  putTruck,
  putTrucktype,

  // Transaction type
  postTransactionType,
  putTransactionType,
  getTransactionType,

  // Status
  postStatus,
  putStatus,
  getListStatus,

  // Driver
  postDriver,
  putDriver,
  getListDriver,

  // Business Entity
  postBusinessEntity,
  putBusinessEntity,
  getListBusinessEntity,

  getListWarehouse,
  deleteWarehouse,

  // Items
  postItem,
  putItem,
  getListItem,

  // Raw Material
  postRawMaterial,
  putRawMaterial,
  getListRawMaterial,

  // BIR
  postBIR,
  putBIR,
  getListBIR,
  getListItemsBIR,

  // Criteria
  postCriteria,
  putCriteria,
  getListCriteria,

  // Deduction Criteria
  postDeductionCriteria,
  putDeductionCriteria,
  getListDeductionCriteria,

  // Price reduction
  postPriceReduction,
  putPriceReduction,
  getListPriceReduction,

  // MTS
  postMTS,
  putMTS,
  getListMTS,

  // Reconsideration
  postReconsideration,
  putReconsideration,
  getListReconsideration,

  // Disposition
  postDisposition,
  putDisposition,
  getListDisposition,

  // Physical analysis
  postPhysicalAnalysis,
  putPhysicalAnalysis,
  getListPhysicalAnalysis,

  // Chemical Analysis
  postChemicalAnalysis,
  putChemicalAnalysis,
  getListChemicalAnalysis,

  // MRR
  postMRR,
  putMRR,
  getListMRR,

  // RMAF
  postQCCN,
  postRMAF,
  putRMAF,
  getListRMAF,

  // Transaction
  postTransaction,
  putTransaction,
  getListTransaction,
  getTransactionCueNumber,
  getTransactionEntry,
  putEntry,
  putExit,
  getFindQR,
  getTransactionExit,
  getFindTransaction,
  uploadSignature,

  // Rejection receipt
  postRejectionReceipt,
  putRejectionReceipt,
  getListRejectionReceipt,

  // Deduction receipt
  postDeductionReceipt,
  putDeductionReceipt,
  getDeductionReceipt,
});

module.exports = transactionController;
module.exports = {
  // Truck scale
  _getNetWeight,

  // SAP - po supplier
  _getPOSuppliers,
  _postGRPO,
  _getPOs,

  // SAP - suppliers
  _getSuppliers,

  checkPincode,

  // Remarks
  postRemarks,
  putRemarks,
  getListRemarks,
  getFindRemarks,

  // Transaction status logs
  postTransactionStatusLogs,
  putTransactionStatusLogs,
  getListTransactionStatusLogs,
  getFindTransactionStatusLogs,

  // Delivery receipts
  getDReceipts,
  postDeliveryReceipt,
  putDeliveryReceipt,

  // Trucks
  getTruckInfos,
  getTruckTypes,
  postTruckInfos,
  postTruckTypes,
  putTruck,
  putTrucktype,

  // Transaction type
  postTransactionType,
  putTransactionType,
  getTransactionType,

  // Status
  postStatus,
  putStatus,
  getListStatus,

  // Driver
  postDriver,
  putDriver,
  getListDriver,

  // Business Entity
  postBusinessEntity,
  putBusinessEntity,
  getListBusinessEntity,

  getListWarehouse,
  deleteWarehouse,

  // Items
  postItem,
  putItem,
  getListItem,

  // Raw Material
  postRawMaterial,
  putRawMaterial,
  getListRawMaterial,

  // BIR
  postBIR,
  putBIR,
  getListBIR,
  getListItemsBIR,

  // Criteria
  postCriteria,
  putCriteria,
  getListCriteria,

  // Deduction Criteria
  postDeductionCriteria,
  putDeductionCriteria,
  getListDeductionCriteria,

  // Price reduction
  postPriceReduction,
  putPriceReduction,
  getListPriceReduction,

  // MTS
  postMTS,
  putMTS,
  getListMTS,

  // Reconsideration
  postReconsideration,
  putReconsideration,
  getListReconsideration,

  // Disposition
  postDisposition,
  putDisposition,
  getListDisposition,

  // Physical analysis
  postPhysicalAnalysis,
  putPhysicalAnalysis,
  getListPhysicalAnalysis,

  // Chemical Analysis
  postChemicalAnalysis,
  putChemicalAnalysis,
  getListChemicalAnalysis,

  // MRR
  postMRR,
  putMRR,
  getListMRR,

  // RMAF
  postQCCN,
  postRMAF,
  putRMAF,
  getListRMAF,

  // Transaction
  postTransaction,
  putTransaction,
  getListTransaction,
  getTransactionCueNumber,
  getTransactionEntry,
  putEntry,
  putExit,
  getFindQR,
  getTransactionExit,
  getFindTransaction,
  uploadSignature,

  // Rejection receipt
  postRejectionReceipt,
  putRejectionReceipt,
  getListRejectionReceipt,

  // Deduction receipt
  postDeductionReceipt,
  putDeductionReceipt,
  getListDeductionReceipt,
};
