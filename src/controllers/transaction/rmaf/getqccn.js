module.exports = function fetchReconsideration({ getQCCN }) {
  return async function getTranTypes(httpRequest) {
    try {
      const result = await getQCCN();

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
