module.exports = function putRMAF({ updateRMAF }, { verifyToken }) {
  return async function editRMAF(httpRequest) {
    try {
      const request_info = httpRequest.body;

      if (!request_info.updated_by) {
        throw { status: 403, message: "Updated by must be provided" };
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      request_info.cookie = Authorization;

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   "secret",
      //   request_info.employee_id
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: "Forbidden" };
      // }

      const result = await updateRMAF(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
