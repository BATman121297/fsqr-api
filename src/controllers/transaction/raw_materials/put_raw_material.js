module.exports = function putRawMaterials(
  { updateRawMaterial },
  { verifyToken }
) {
  return async function editRawMaterials(httpRequest) {
    try {
      const request_info = httpRequest.body;

      if (!request_info.updated_by) {
        throw { status: 403, message: "Updated by must be provided" };
      }
      if (isNaN(request_info.updated_by)) {
        throw new Error("Updated by must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      request_info.cookie = Authorization;

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   request_info.updated_by
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      const result = await updateRawMaterial(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
