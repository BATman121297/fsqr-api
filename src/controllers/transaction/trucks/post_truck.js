module.exports = function postTruck({ addTruck }, { verifyToken }) {
    return async function insertTruck(httpRequest) {
      try {
        const token = httpRequest.token;
        const request_info = httpRequest.body;
        //const modules = httpRequest.body.modules;
  
        if (!token) {
          throw { status: 403, message: 'Token must be provided' };
        }
        if (!request_info.created_by) {
          throw { status: 403, message: 'Created by must be provided' };
        }
        if (isNaN(request_info.created_by)) {
          throw new Error('Created by must be a number');
        }
  
        //Verify Token
        const tokenExist = await verifyToken(
          token,
          'secret',
          request_info.created_by
        );
        if (!tokenExist) {
          throw { status: 403, message: 'Forbidden' };
        }
        const result = await addTruck(request_info);
  
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: 201,
          body: result
        };
      } catch (e) {
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: e.status ? e.status : 400,
          body: e.message
        };
      }
    };
  };
  