module.exports = function putTruckType({ updateTrucktype }, { verifyToken }) {
    return async function editTruckType(httpRequest) {
      try {
        const token = httpRequest.token;
        const request_info = httpRequest.body;
        //const modules = httpRequest.body.modules;
  
        if (!token) {
          throw { status: 403, message: 'Token must be provided' };
        }
        if (!request_info.updated_by) {
          throw { status: 403, message: 'Updated by must be provided' };
        }
        if (isNaN(request_info.updated_by)) {
          throw new Error('Updated by must be a number');
        }
  
        //Verify Token
        const tokenExist = await verifyToken(
          token,
          'secret',
          request_info.updated_by
        );
        if (!tokenExist) {
          throw { status: 403, message: 'Forbidden' };
        }

        const result = await updateTrucktype(request_info);
  
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: 200,
          body: result
        };
      } catch (e) {
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: e.status ? e.status : 400,
          body: e.message
        };
      }
    };
  };
  