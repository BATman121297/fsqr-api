module.exports = function fetchTrucks({ getTrucks }, { verifyToken }) {
    return async function getTruckinfos(httpRequest) {
      try {
        const token = httpRequest.token;
        const request_info = httpRequest.body;
        //const modules = httpRequest.body.modules;
  
        if (!token) {
          throw { status: 403, message: 'Token must be provided' };
        }
        if (!request_info.employee_id) {
          throw { status: 403, message: 'Employee id must be provided' };
        }
        if (isNaN(request_info.employee_id)) {
          throw new Error('Employee id must be a number');
        }
  
        //Verify Token
        const tokenExist = await verifyToken(
          token,
          'secret',
          request_info.employee_id
        );
        if (!tokenExist) {
          throw { status: 403, message: 'Forbidden' };
        }
    
        const result = await getTrucks();

        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: 200,
          body: result
        };
      } catch (e) {
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: e.status ? e.status : 400,
          body: e.message
        };
      }
    };
  };
  