module.exports = function postItem({ addItem }, { verifyToken }) {
  return async function insertItem(httpRequest) {
    try {
      const request_info = httpRequest.body;

      if (!request_info.created_by) {
        throw { status: 403, message: "Created by must be provided" };
      }
      if (isNaN(request_info.created_by)) {
        throw new Error("Created by must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      request_info.cookie = Authorization;

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   request_info.created_by
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      const result = await addItem(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
