module.exports = function fetchTransaction(
  { getTransaction },
  { verifyToken }
) {
  return async function getTranTypes(httpRequest) {
    try {
      const request_info = httpRequest.body;

      if (!request_info.employee_id) {
        throw { status: 403, message: "Employee id must be provided" };
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error("Employee id must be a number");
      }
      if (!request_info.date_from) {
        throw new Error("Date from required.");
      }
      if (!request_info.date_to) {
        throw new Error("Date to required.");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      request_info.cookie = Authorization;

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   "secret",
      //   request_info.employee_id
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: "Forbidden" };
      // }

      const result = await getTransaction(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      console.log(e);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
