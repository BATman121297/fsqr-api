module.exports = function fetchCueNumber({ getCueNumber }, { verifyToken }) {
  return async function getTranTypes(httpRequest) {
    try {
      const request_info = httpRequest.body;

      const result = await getCueNumber(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
