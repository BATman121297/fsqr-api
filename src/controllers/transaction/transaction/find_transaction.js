module.exports = function fetchTransaction(
  { findTransaction },
  { verifyToken }
) {
  return async function getTranTypes(httpRequest) {
    try {
      const token = httpRequest.token;
      const request_info = httpRequest.body;

      if (!request_info.employee_id) {
        throw { status: 403, message: "Employee id must be provided" };
      }
      if (request_info.employee_id != "truck_scale") {
        if (isNaN(request_info.employee_id)) {
          throw new Error("Employee id must be a number");
        }
        if (!request_info.transaction_id) {
          throw new Error("Transaction ID required.");
        }

        const { Authorization } = httpRequest.headers;

        if (!Authorization) {
          throw { status: 403, message: "Forbidden" };
        }

        // //Verify Token
        // const tokenExist = await verifyToken(
        //   token,
        //   "secret",
        //   request_info.employee_id
        // );
        // if (!tokenExist) {
        //   throw { status: 403, message: "Forbidden" };
        // }
      }

      const result = await findTransaction(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      console.log(e);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
