module.exports = function fetchEntry({ getEntry }, { verifyToken }) {
    return async function getTransactionEntry(httpRequest) {
      try {
        const request_info = httpRequest.body;

        // console.log(request_info);
  
        const result = await getEntry(request_info);
  
        return {
          headers: {
            "Content-Type": "application/json"
          },
          status: 200,
          body: result
        };
      } catch (e) {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          status: e.status ? e.status : 400,
          body: e.message
        };
      }
    };
  };
  