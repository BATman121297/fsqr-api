module.exports = function uploadEmployeeSignature({ verifyTokenUpload }) {
  return async function uploadSignature(httpRequest) {
    try {
      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }
      // const token = httpRequest.token;
      // if (!token) {
      //   throw { status: 403, message: 'Token must be provided' };
      // }

      // //Verify Token
      // const tokenExist = await verifyTokenUpload(token, 'secret');
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }
      if (httpRequest.files) {
        const filePath = `/employee_files/${httpRequest.files.filename}`;

        const files = {
          path: filePath,
        };
        return {
          headers: {
            "Content-Type": "application/json",
          },
          status: 200,
          body: files,
        };
      } else {
        return {
          headers: {
            "Content-Type": "application/json",
          },
          status: 200,
          body: true,
        };
      }
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json",
        },
        status: e.status ? e.status : 400,
        body: e.message,
      };
    }
  };
};
