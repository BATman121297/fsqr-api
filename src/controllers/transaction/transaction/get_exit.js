module.exports = function fetchExit({ getExit }, { verifyToken }) {
  return async function getTransactionExit(httpRequest) {
    try {
      const request_info = httpRequest.body;

      const result = await getExit(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
