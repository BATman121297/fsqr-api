module.exports = function fetchQR({ findQR }, { verifyToken }) {
  return async function getQR(httpRequest) {
    try {
      const request_info = httpRequest.body;

      // console.log("controllers");
      // console.log(request_info);

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      if (!request_info.id) {
        // console.log("id")
        // console.log(request_info.id)
        throw { status: 404, message: "Transaction ID is missing" };
      }

      request_info.cookie = Authorization;

      const result = await findQR(request_info);
      console.log(result);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
