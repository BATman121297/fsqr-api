module.exports = function makeLogin({ login }) {
  return async function postLogin(httpRequest) {
    try {
      const { ...loginInfo } = httpRequest.body;
      // const browser = httpRequest.client;

      const loginEmployee = await login({
        ...loginInfo
      });

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: loginEmployee
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
