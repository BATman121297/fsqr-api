//Use cases
const login = require('../../use-cases/login');

//Function
const makeLogin = require('./login');

const getLogin = makeLogin({ login });

const loginController = Object.freeze(getLogin);

module.exports = loginController;
module.exports = getLogin;
