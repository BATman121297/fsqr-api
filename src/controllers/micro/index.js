// use cases
const {
  addMicroTransactions,
  selectMicroTransactions,
  updateMicroTransactions,
  haulingFsqrs,
  transactionTagPOs,
  postAttachmentss,
  detailsPOs,
} = require("../../use-cases/micro/index");

// ###################
const transactionMicroAdd = require("./micro-add");
const transactionMicroSelect = require("./micro-select");
const transactionMicroUpdate = require("./micro-trns-update-status");
const fsqrHauling = require("./list-items-on-bu");
const tagPOTransaction = require("./auto-tag-po");
const attachmentsPost = require("./post-attachments");
const poDetails = require("./get-po-data");
// ###################
const transactionMicroAdds = transactionMicroAdd({ addMicroTransactions });
const transactionMicroSelects = transactionMicroSelect({
  selectMicroTransactions,
});
const transactionMicroUpdates = transactionMicroUpdate({
  updateMicroTransactions,
});
const fsqrHaulings = fsqrHauling({ haulingFsqrs });
const tagPOTransactions = tagPOTransaction({ transactionTagPOs });
const attachmentsPosts = attachmentsPost({ postAttachmentss });
const poDetailss = poDetails({ detailsPOs });
// ###################
const services = Object.freeze({
  transactionMicroAdds,
  transactionMicroSelects,
  transactionMicroUpdates,
  fsqrHaulings,
  tagPOTransactions,
  attachmentsPosts,
  poDetailss,
});

module.exports = services;
module.exports = {
  transactionMicroAdds,
  transactionMicroSelects,
  transactionMicroUpdates,
  fsqrHaulings,
  tagPOTransactions,
  attachmentsPosts,
  poDetailss,
};
