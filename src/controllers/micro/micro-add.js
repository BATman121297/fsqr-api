const transactionMicroAdd = ({ addMicroTransactions }) => {
  return async function post(httpRequest) {
    try {
      const info = httpRequest.body;

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      info.cookie = Authorization;

      const result = await addMicroTransactions(info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};

module.exports = transactionMicroAdd;
