module.exports = async function notFound() {
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      body: 'Not found.',
      status: 404
    };
  };
  