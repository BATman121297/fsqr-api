module.exports = function postEmployee({ addEmployee }, { verifyToken }) {
  return async function insertEmployee(httpRequest) {
    try {
      const employee_info = httpRequest.body;

      if (!employee_info.created_by) {
        throw { status: 403, message: "Created by must be provided" };
      }
      if (isNaN(employee_info.created_by)) {
        throw new Error("Created by must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      employee_info.cookie = Authorization;

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   employee_info.created_by
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      const result = await addEmployee(employee_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
