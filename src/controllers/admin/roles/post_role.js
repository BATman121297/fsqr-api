module.exports = function postRole({ addRole }, { verifyToken }) {
  return async function insertRole(httpRequest) {
    try {
      const request_info = httpRequest.body;
      //const modules = httpRequest.body.modules;

      // if (!token) {
      //   throw { status: 403, message: 'Token must be provided' };
      // }
      if (!request_info.created_by) {
        throw { status: 403, message: "Created by must be provided" };
      }
      if (isNaN(request_info.created_by)) {
        throw new Error("Created by must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      request_info.cookie = Authorization;

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   request_info.created_by
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      // if (!modules) {
      //   throw new Error('Modules must be provided');
      // }

      // //Check if allowed to use admin module
      // const adminModule = await modules.find(
      //   element => element.description.toLowerCase() === 'admin'
      // );

      // if (!adminModule) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to access Admin module'
      //   };
      // }
      // if (adminModule.status.toLowerCase() === 'inactive') {
      //   throw new Error('Admin module is inactive');
      // }

      // //Check if add role exists in actions
      // const addRoleExists = await adminModule.actions.find(
      //   action => action.description.toLowerCase() === 'add role'
      // );

      // if (!addRoleExists) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to add role'
      //   };
      // }
      // if (addRoleExists.status.toLowerCase() === 'inactive') {
      //   throw new Error('Add role is inactive');
      // }
      const result = await addRole(request_info);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
