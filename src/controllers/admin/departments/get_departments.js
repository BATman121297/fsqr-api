module.exports = function fetchDepartments(
  { listDepartments },
  { verifyToken }
) {
  return async function getDepartments(httpRequest) {
    try {
      const request_info = httpRequest.body;

      if (!request_info.employee_id) {
        throw { status: 403, message: "Employee id must be provided" };
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error("Employee id must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   request_info.employee_id
      // );

      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      const result = await listDepartments();

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
