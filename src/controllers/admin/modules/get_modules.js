module.exports = function fetchModules({ listModules }, { verifyToken }) {
  return async function getModules(httpRequest) {
    try {
      const request_info = httpRequest.body;

      if (!request_info.employee_id) {
        throw { status: 403, message: "Employee id must be provided" };
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error("Employee id must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      const result = await listModules();

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
