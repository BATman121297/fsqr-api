module.exports = function fetchActivityLogs({ listActivityLogs }, { verifyToken }) {
    return async function getActivityLogs(httpRequest) {
      try {
        const token = httpRequest.token;
        const request_info = httpRequest.body;
        //const modules = httpRequest.body.modules;

        const { date_from, date_to } = request_info;
  
        if (!token) {
          throw { status: 403, message: 'Token must be provided' };
        }
        if (!request_info.employee_id) {
          throw { status: 403, message: 'Employee id must be provided' };
        }
        if (isNaN(request_info.employee_id)) {
          throw new Error('Employee id must be a number');
        }
        if (!date_from) {
            throw new Error('Date from is required.')
        }
        if (!date_to) {
            throw new Error('Date to is required.')
        }
  
        //Verify Token
        const tokenExist = await verifyToken(
          token,
          'secret',
          request_info.employee_id
        );
        if (!tokenExist) {
          throw { status: 403, message: 'Forbidden' };
        }
  
        const result = await listActivityLogs(request_info);

        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: 200,
          body: result
        };
      } catch (e) {
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          status: e.status ? e.status : 400,
          body: e.message
        };
      }
    };
  };
  