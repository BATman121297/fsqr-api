module.exports = function fetchAccessRights(
  { listAccessRights },
  { verifyToken }
) {
  return async function getAccessRights(httpRequest) {
    try {
      const request_info = httpRequest.body;
      //const modules = httpRequest.body.modules;

      if (!request_info.employee_id) {
        throw { status: 403, message: "Employee id must be provided" };
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error("Employee id must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      // //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   "secret",
      //   request_info.employee_id
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: "Forbidden" };
      // }

      // if (!modules) {
      //   throw new Error('Modules must be provided');
      // }

      // //Check if allowed to use admin module
      // const adminModule = await modules.find(
      //   element => element.description.toLowerCase() === 'admin'
      // );

      // if (!adminModule) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to access Admin module'
      //   };
      // }
      // if (adminModule.status.toLowerCase() === 'inactive') {
      //   throw new Error('Admin module is inactive');
      // }

      // //Check if view access rights in actions
      // const viewAccessRights = await adminModule.actions.find(
      //   action => action.description.toLowerCase() === 'view access rights'
      // );

      // if (!viewAccessRights) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to view access rights'
      //   };
      // }
      // if (viewAccessRights.status.toLowerCase() === 'inactive') {
      //   throw new Error('View access rights is inactive');
      // }

      const result = await listAccessRights();
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
