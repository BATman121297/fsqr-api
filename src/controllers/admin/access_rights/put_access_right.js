module.exports = function putAccessRight(
  { updateAccessRight },
  { verifyToken }
) {
  return async function editAccessRight(httpRequest) {
    try {
      const request_info = httpRequest.body;
      //const modules = httpRequest.body.modules;

      if (!request_info.updated_by) {
        throw { status: 403, message: "Updated by must be provided" };
      }
      if (isNaN(request_info.updated_by)) {
        throw new Error("Updated by must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      request_info.cookie = Authorization;
      
      //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   request_info.updated_by
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      // if (!modules) {
      //   throw new Error('Modules must be provided');
      // }

      //Check if allowed to use admin module
      // const adminModule = await modules.find(
      //   element => element.description.toLowerCase() === 'admin'
      // );

      // if (!adminModule) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to access Admin module'
      //   };
      // }
      // if (adminModule.status.toLowerCase() === 'inactive') {
      //   throw new Error('Admin module is inactive');
      // }

      // //Check if edit access right exists in actions
      // const editAccess = await adminModule.actions.find(
      //   action => action.description.toLowerCase() === 'edit access right'
      // );

      // if (!editAccess) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to edit access right'
      //   };
      // }
      // if (editAccess.status.toLowerCase() === 'inactive') {
      //   throw new Error('Edit access right is inactive');
      // }

      const result = await updateAccessRight(request_info);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
