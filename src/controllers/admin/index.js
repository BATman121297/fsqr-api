const verifyToken = require("../../token/").verifyToken;

//Use cases
const {
  listModules,
  addModule,
  updateModule,

  listActions,
  addAction,
  updateAction,
  listActionRole,

  listRoles,
  addRole,
  updateRole,

  listAccessRights,
  addAccessRight,
  updateAccessRight,

  listEmployees,
  addEmployee,
  updateEmployee,
  updatePin,
  resetPassword,
  listSAPEmployeess,

  listActivityLogs,

  listDepartments,
  addDepartment,
  updateDepartment,
} = require("../../use-cases/admin");

//Modules
const makeListModules = require("./modules/get_modules");
const makeModule = require("./modules/post_module");
const makeUpdateModule = require("./modules/put_module");

const getModules = makeListModules({ listModules }, { verifyToken });
const postModule = makeModule({ addModule }, { verifyToken });
const putModule = makeUpdateModule({ updateModule }, { verifyToken });

//Actions
const makeListActions = require("./actions/get_actions");
const makeAction = require("./actions/post_action");
const makeUpdateAction = require("./actions/put_action");
const makeListActionRole = require("./actions/get_actionsrole");

const getActions = makeListActions({ listActions }, { verifyToken });
const postAction = makeAction({ addAction }, { verifyToken });
const putAction = makeUpdateAction({ updateAction }, { verifyToken });
const getActionRole = makeListActionRole({ listActionRole }, { verifyToken });

//Roles
const makeListRoles = require("./roles/get_roles");
const makeRole = require("./roles/post_role");
const makeUpdateRole = require("./roles/put_role");

const getRoles = makeListRoles({ listRoles }, { verifyToken });
const postRole = makeRole({ addRole }, { verifyToken });
const putRole = makeUpdateRole({ updateRole }, { verifyToken });

//Access right
const makeListAccessRights = require("./access_rights/get_access_rights");
const makeAccessRight = require("./access_rights/post_access_right");
const makeUpdateAccessRight = require("./access_rights/put_access_right");

const getAccessRights = makeListAccessRights(
  { listAccessRights },
  { verifyToken }
);
const postAccessRight = makeAccessRight({ addAccessRight }, { verifyToken });
const putAccessRight = makeUpdateAccessRight(
  { updateAccessRight },
  { verifyToken }
);

// Employees
const makeListEmployees = require("./employees/get_employees");
const makeEmployees = require("./employees/post_employees");
const makeUpdateEmployees = require("./employees/put_employees");
const makeUpdatePin = require("./employees/put_pin");
const makeResetPassword = require("./employees/put_password");
const fetchSAPEmployees = require("./employees/list-sap-employees");

const getEmployees = makeListEmployees({ listEmployees }, { verifyToken });
const postEmployees = makeEmployees({ addEmployee }, { verifyToken });
const putEmployees = makeUpdateEmployees({ updateEmployee }, { verifyToken });
const putPincode = makeUpdatePin({ updatePin }, { verifyToken });
const putPassword = makeResetPassword({ resetPassword }, { verifyToken });
const fetchSAPEmployeess = fetchSAPEmployees(
  { listSAPEmployeess },
  { verifyToken }
);

// Activity Logs
const makeListActivityLogs = require("./activity_logs/get_activitylogs");

// Department
const makeListDepartment = require("./departments/get_departments");
const makeDepartment = require("./departments/post_department");
const makeUpdateDepartment = require("./departments/put_department");

const getDeparment = makeListDepartment({ listDepartments }, { verifyToken });
const postDeparment = makeDepartment({ addDepartment }, { verifyToken });
const putDepartment = makeUpdateDepartment(
  { updateDepartment },
  { verifyToken }
);

const getActivityLogs = makeListActivityLogs(
  { listActivityLogs },
  { verifyToken }
);

const adminController = Object.freeze({
  getModules,
  postModule,
  putModule,

  getActions,
  postAction,
  putAction,
  getActionRole,

  getRoles,
  postRole,
  putRole,

  getAccessRights,
  postAccessRight,
  putAccessRight,

  getEmployees,
  postEmployees,
  putEmployees,
  putPincode,
  putPassword,
  fetchSAPEmployeess,

  getActivityLogs,

  getDeparment,
  postDeparment,
  putDepartment,
});

module.exports = adminController;
module.exports = {
  getModules,
  postModule,
  putModule,

  getActions,
  postAction,
  putAction,
  getActionRole,

  getRoles,
  postRole,
  putRole,

  getAccessRights,
  postAccessRight,
  putAccessRight,

  getEmployees,
  postEmployees,
  putEmployees,
  putPincode,
  putPassword,
  fetchSAPEmployeess,

  getActivityLogs,

  getDeparment,
  postDeparment,
  putDepartment,
};
