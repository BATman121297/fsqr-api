module.exports = function fetchActions({ listActions }, { verifyToken }) {
  return async function getActions(httpRequest) {
    try {
      const request_info = httpRequest.body;
      // const modules = httpRequest.body.modules;

      // for (let i = 0; i < modules.length; i++) {
      //   if (modules[i].description.toLowerCase() == 'admin' &&
      //   modules[i].status.toLowerCase() == 'inactive') {
      //     throw new Error('Admin module is currently inaactive. Please contact administrator.')
      //   }

      // }

      if (!request_info.employee_id) {
        throw { status: 403, message: "Employee id must be provided" };
      }
      if (isNaN(request_info.employee_id)) {
        throw new Error("Employee id must be a number");
      }

      const { Authorization } = httpRequest.headers;

      if (!Authorization) {
        throw { status: 403, message: "Forbidden" };
      }

      //Verify Token
      // const tokenExist = await verifyToken(
      //   token,
      //   'secret',
      //   request_info.employee_id
      // );
      // if (!tokenExist) {
      //   throw { status: 403, message: 'Forbidden' };
      // }

      // if (!modules) {
      //   throw new Error('Modules must be provided');
      // }

      // //Check if allowed to use admin module
      // const adminModule = await modules.find(
      //   element => element.description.toLowerCase() === 'admin'
      // );

      // if (!adminModule) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to access Admin module'
      //   };
      // }
      // if (adminModule.status.toLowerCase() === 'inactive') {
      //   throw new Error('Admin module is inactive');
      // }

      // //Check if view access exists in actions
      // const viewActions = await adminModule.actions.find(
      //   action => action.description.toLowerCase() === 'view actions'
      // );

      // if (!viewActions) {
      //   throw {
      //     status: 401,
      //     message: 'Access denied. Not authorized to view actions'
      //   };
      // }
      // if (viewActions.status.toLowerCase() === 'inactive') {
      //   throw new Error('View actions is inactive');
      // }

      const result = await listActions();

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: e.message
      };
    }
  };
};
